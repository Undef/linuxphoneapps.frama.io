+++
title = "Apps to be added"
description = "Apps that need to be evaluated and added."
date = 2023-01-08T07:00:00+00:00
draft = false

[extra]
lead = 'Apps that need to be evaluated.'
images = []
+++

## Purpose

This list contains apps that have not been added as a proper listing. This list can be useful if

- the app list does not contain what you need,
- you want to help adding apps (see [Contributing](@/docs/contributing/_index.md) for how to do this),
- or you want to see if someone has already started developing an app in order to join an existing project.


## Apps with tagged releases

### 2024
* xournalpp - Repo: https://github.com/xournalpp/xournalpp, Tag: nightly, Last Commit: Tue Feb 13 12:54:21 2024 +0000
* authpass, Flutter,  Repo: https://github.com/authpass/authpass, Tag: flathub-v1.9.11, Last Commit: Sat Feb 03 12:41:05 2024 +0100
* webapp-manager - Repo: https://github.com/linuxmint/webapp-manager, Tag: master.lmde6, Last Commit: Wed Jan 31 13:25:29 2024 -0500
* intiface-central, Flutter, Repo: https://github.com/intiface/intiface-central, Tag: v2.5.5, Last Commit: Sun Jan 28 11:50:57 2024 -0800
* spotube, Flutter, Repo: https://github.com/KRTirtho/spotube/, Tag: v3.4.1, Last Commit: Sat Jan 27 22:39:05 2024 +0600
### 2022
* keyring, GTK3,  Repo: https://git.sr.ht/~martijnbraam/keyring, Tag: 0.2.0, Last Commit: Fri Aug 26 20:30:53 2022 +0200
* brun, MauiKit,  Repo: https://invent.kde.org/maui/brun, Tag: v0.0.5, Last Commit: Sat Aug 20 10:58:59 2022 -0500
### 2021
* agenda - Repo: https://github.com/dahenson/agenda, Tag: 1.1.2, Last Commit: Sat Aug 14 00:14:09 2021 -0500
* om-camera - Repo: https://github.com/OpenMandrivaSoftware/om-camera, Tag: 0.0.4, Last Commit: Mon Apr 19 01:19:50 2021 +0100

## Apps without git tags - likely unreleased

### 2024
* simple-scan - Repo: https://gitlab.gnome.org/GNOME/simple-scan, Last Commit: Wed Feb 14 13:25:23 2024 +0000
* Kooha - Repo: https://github.com/SeaDve/Kooha, Last Commit: Wed Feb 14 20:37:59 2024 +0800
* booth - Repo: https://invent.kde.org/maui/booth, Last Commit: Wed Feb 14 02:15:56 2024 +0000
* icon-library - Repo: https://gitlab.gnome.org/World/design/icon-library, Last Commit: Mon Feb 12 20:59:03 2024 +0000
* Kaiteki - Repo: https://github.com/Kaiteki-Fedi/Kaiteki, Last Commit: Mon Feb 12 20:56:18 2024 +0100
* papers - Repo: https://gitlab.gnome.org/GNOME/Incubator/papers, Last Commit: Sat Feb 10 13:25:46 2024 +0000
* neosurf - Repo: https://github.com/CobaltBSD/neosurf, Last Commit: Thu Feb 08 02:09:00 2024 +0000
* nextcloud_password_client - Repo: https://gitlab.com/j0chn/nextcloud_password_client, Last Commit: Tue Feb 06 01:08:24 2024 +0100
* pods - Repo: https://github.com/marhkb/pods, Last Commit: Mon Feb 05 17:07:30 2024 +0100
* citations - Repo: https://gitlab.gnome.org/World/citations, Last Commit: Mon Feb 05 11:10:51 2024 +0100
* Noteworthy - Repo: https://github.com/SeaDve/Noteworthy, Last Commit: Sun Feb 04 10:19:24 2024 +0800
* argos - Repo: https://github.com/orontee/argos, Last Commit: Fri Feb 02 21:54:33 2024 +0100
* webfont-kit-generator - Repo: https://github.com/rafaelmardojai/webfont-kit-generator, Last Commit: Wed Jan 31 08:29:26 2024 -0500
* Personal-Voice-Assistent - Repo: https://github.com/Cyborgscode/Personal-Voice-Assistent, Last Commit: Tue Jan 30 12:16:37 2024 +0100
* kazv - Repo: https://lily-is.land/kazv/kazv, Last Commit: Sun Jan 28 16:13:03 2024 -0500
* sharit - Repo: https://gitlab.com/LyesSaadi/sharit, Last Commit: Sat Jan 27 21:23:45 2024 +0100
* Vremenar - Repo: https://github.com/ntadej/Vremenar, Last Commit: Fri Jan 26 22:28:15 2024 +0100
* Replay - Repo: https://github.com/ReplayDev/Replay, Last Commit: Thu Jan 25 22:31:16 2024 -0300
* argos-translate - Repo: https://github.com/argosopentech/argos-translate, Last Commit: Wed Jan 24 11:52:26 2024 -0600
* itd - Repo: https://gitea.elara.ws/Elara6331/itd, Last Commit: Sat Jan 20 04:56:54 2024 +0000
* abaddon - Repo: https://github.com/uowuo/abaddon, Last Commit: Thu Jan 18 20:30:54 2024 -0500
* share-preview - Repo: https://github.com/rafaelmardojai/share-preview, Last Commit: Wed Jan 17 03:06:21 2024 +0100
* settings - Repo: https://github.com/lirios/settings, Last Commit: Mon Jan 15 06:17:08 2024 +0100
* lorem - Repo: https://gitlab.gnome.org/World/design/lorem, Last Commit: Mon Jan 08 21:50:59 2024 +0000
* desktop-files-creator - Repo: https://github.com/alexkdeveloper/desktop-files-creator, Last Commit: Sat Jan 06 11:34:50 2024 +0800
### 2023
* PineCast - Repo: https://github.com/Jeremiah-Roise/PineCast, Last Commit: Tue Dec 19 21:04:43 2023 -0700
* lyrebird - Repo: https://github.com/constcharptr/lyrebird, Last Commit: Wed Dec 13 12:03:59 2023 -0500
* powerplant - Repo: https://invent.kde.org/utilities/powerplant, Last Commit: Sun Dec 03 17:20:34 2023 +0000
* liftoff - Repo: https://github.com/liftoff-app/liftoff, Last Commit: Mon Nov 27 13:56:50 2023 -0700
* qomp - Repo: https://github.com/qomp/qomp, Last Commit: Thu Nov 02 15:06:53 2023 +0200
* appimagepool - Repo: https://github.com/prateekmedia/appimagepool, Last Commit: Mon Oct 23 15:54:43 2023 +0530
* summarizer - Repo: https://github.com/Nalsai/summarizer, Last Commit: Wed Oct 11 14:01:16 2023 +0200
* astroid - Repo: https://github.com/astroidmail/astroid/, Last Commit: Sun Sep 17 20:57:48 2023 +0200
* Retiled - Repo: https://github.com/DrewNaylor/Retiled, Last Commit: Wed Sep 13 03:05:50 2023 -0400
* barcelona-trees - Repo: https://github.com/pedrolcl/barcelona-trees, Last Commit: Sat Aug 05 14:30:54 2023 +0200
* quickmail - Repo: https://invent.kde.org/carlschwan/quickmail, Last Commit: Fri Jun 09 09:41:14 2023 +0200
* toxer-desktop - Repo: https://gitlab.com/Toxer/toxer-desktop, Last Commit: Sat Jun 03 11:53:39 2023 +0200
* Katana - Repo: https://github.com/gavr123456789/Katana, Last Commit: Fri Apr 21 00:29:40 2023 +0300
* talk-desktop - Repo: https://github.com/CarlSchwan/talk-desktop, Last Commit: Thu Mar 16 15:44:37 2023 +0100
* bitsteward - Repo: https://github.com/Bitsteward/bitsteward, Last Commit: Sun Mar 12 19:51:32 2023 -0400
* sncf - Repo: https://git.sr.ht/~yukikoo/sncf/, Last Commit: Wed Mar 08 20:56:07 2023 +0100
* keyboard - Repo: https://github.com/grelltrier/keyboard, Last Commit: Thu Feb 09 19:21:52 2023 +0100
* ghostshot - Repo: https://gitlab.gnome.org/GabMus/ghostshot, Last Commit: Sun Jan 22 11:13:17 2023 +0100
* dot-matrix - Repo: https://github.com/lainsce/dot-matrix/, Last Commit: Sun Jan 15 22:55:47 2023 -0300

-------------------

CUT OFF LINE - things below are likely abandoned and not worth adding, unless they work and fill a niche

-------------------

### 2022
* camera - Repo: https://gitlab.gnome.org/jwestman/camera, Last Commit: Mon Dec 19 22:36:33 2022 -0600
* chert - Repo: https://gitlab.com/high-creek-software/chert, Last Commit: Thu Nov 24 07:36:44 2022 -0700
* wifiscanner - Repo: https://github.com/Cyborgscode/wifiscanner, Last Commit: Fri Nov 11 13:37:58 2022 +0100
* screenshot - Repo: https://github.com/lirios/screenshot, Last Commit: Sat Oct 22 00:41:26 2022 +0200
* networkmanager - Repo: https://github.com/lirios/networkmanager, Last Commit: Sat Oct 22 00:22:15 2022 +0200
* screencast - Repo: https://github.com/lirios/screencast, Last Commit: Sat Oct 22 00:05:17 2022 +0200
* keylight-control - Repo: https://github.com/mschneider82/keylight-control, Last Commit: Tue Oct 04 10:14:45 2022 +0200
* covidpass - Repo: https://github.com/pentamassiv/covidpass, Last Commit: Tue Sep 27 21:58:18 2022 +0200
* tunes - Repo: https://git.sr.ht/~jakob/tunes, Last Commit: Sun Aug 07 20:47:54 2022 -0400
* Qt-Audiobook-Player - Repo: https://github.com/rushic24/Qt-Audiobook-Player/, Last Commit: Tue May 17 15:08:07 2022 -0400
* mdwriter - Repo: https://gitlab.com/gabmus/mdwriter, Last Commit: Tue Apr 12 23:52:52 2022 +0200
* visurf - Repo: https://git.sr.ht/~sircmpwn/visurf/, Last Commit: Mon Mar 14 09:35:38 2022 +0100
* sharmavid - Repo: https://gitlab.gnome.org/ranfdev/sharmavid, Last Commit: Fri Feb 11 19:15:50 2022 +0100
* AugSteam - Repo: https://github.com/Augmeneco/AugSteam, Last Commit: Thu Feb 10 21:23:29 2022 +0300
* music - Repo: https://github.com/lirios/music, Last Commit: Wed Feb 09 08:18:14 2022 +0100
* pala - Repo: https://gitlab.com/gabmus/pala, Last Commit: Wed Jan 26 13:23:42 2022 +0100

### 2021
* AugVK-Messenger - Repo: https://github.com/Augmeneco/AugVK-Messenger, Last Commit: Sat Nov 27 01:22:07 2021 +0300
* GrepToolQML - Repo: https://github.com/presire/GrepToolQML, Last Commit: Mon Nov 22 21:56:02 2021 +0900
* pKPR - Repo: https://github.com/juliendecharentenay/pKPR, Last Commit: Fri Nov 19 04:39:07 2021 +0000
* umtp-responder-gui - Repo: https://github.com/JollyDevelopment/umtp-responder-gui, Last Commit: Sun Nov 14 14:59:20 2021 -0500
* camcam - Repo: https://github.com/JNissi/camcam, Last Commit: Sat Oct 16 18:40:14 2021 +0300
* trackball - Repo: https://github.com/NthElse/trackball, Last Commit: Tue Oct 05 18:25:01 2021 +0800
* mirdorph - Repo: https://gitlab.gnome.org/ranchester/mirdorph, Last Commit: Sun Sep 05 13:41:23 2021 +0300
* hydro_bot - Repo: https://github.com/ripxorip/hydro_bot, Last Commit: Mon Aug 09 09:46:10 2021 +0200
* rokugtk - Repo: https://github.com/cliftonts/rokugtk, Last Commit: Wed Jul 28 15:44:41 2021 +0100
* PineConnect - Repo: https://github.com/timaios/PineConnect, Last Commit: Tue Jul 27 12:29:43 2021 +0200
* mactrack - Repo: https://gitlab.com/Aresesi/mactrack, Last Commit: Sat Jun 05 06:19:20 2021 +0000
* Tracks - Repo: https://codeberg.org/som/Tracks, Last Commit: Fri May 28 16:49:23 2021 +0200
* solio - Repo: https://invent.kde.org/devinlin/solio, Last Commit: Thu May 20 23:52:31 2021 -0400
* LibreInvoice - Repo: https://codeberg.org/matiaslavik/LibreInvoice, Last Commit: Wed Apr 21 22:10:50 2021 +0200
* Flashcards - Repo: https://github.com/SeaDve/Flashcards, Last Commit: Wed Apr 14 07:31:03 2021 +0800
* hn - Repo: https://github.com/DavidVentura/hn, Last Commit: Mon Apr 12 23:47:15 2021 +0200
* mailmodel - Repo: https://invent.kde.org/vandenoever/mailmodel, Last Commit: Sun Apr 04 23:42:13 2021 +0200
* Interpret - Repo: https://codeberg.org/xaviers/Interpret, Last Commit: Tue Mar 23 01:21:09 2021 +0100
* fastcal - Repo: https://github.com/JNissi/fastcal, Last Commit: Sun Mar 21 16:50:16 2021 +0200
* clip2deepl - Repo: https://codeberg.org/yasht/clip2deepl, Last Commit: Tue Mar 02 21:59:05 2021 +0530
* telephant - Repo: https://github.com/muesli/telephant, Last Commit: Sat Jan 30 21:21:24 2021 +0100
* pui - Repo: https://github.com/nbdy/pui, Last Commit: Sat Jan 09 16:21:07 2021 +0000

### 2020
* camera-rs - Repo: https://gitlab.gnome.org/bilelmoussaoui/camera-rs, Last Commit: Fri Dec 25 21:47:26 2020 +0100
* paintable-rs - Repo: https://gitlab.gnome.org/bilelmoussaoui/paintable-rs, Last Commit: Wed Dec 23 00:39:25 2020 +0100
* lurkymclurkface - Repo: https://gitlab.com/armen138/lurkymclurkface, Last Commit: Wed Dec 16 14:29:03 2020 -0500
* pinephone-modemfw - Repo: https://git.sr.ht/~martijnbraam/pinephone-modemfw, Last Commit: Thu Nov 12 18:54:21 2020 +0100
* orion - Repo: https://github.com/drac69/orion, Last Commit: Tue Nov 10 16:47:56 2020 +0200
* papper - Repo: https://gitlab.com/GunnarGrop/papper, Last Commit: Wed Jul 01 19:05:06 2020 +0200
* simple-rss-reader-using-kde-kirigami - Repo: https://gitlab.com/SiriBeta/simple-rss-reader-using-kde-kirigami, Last Commit: Sat Jun 13 08:07:12 2020 +0000
* bitguarden - Repo: https://github.com/DenisPalchuk/bitguarden, Last Commit: Wed Apr 29 23:24:13 2020 +0300
* hangouts-gtk - Repo: https://github.com/do-sch/hangouts-gtk, Last Commit: Sun Apr 19 09:27:36 2020 +0200
* moodle - Repo: https://gitlab.gnugen.ch/afontain/moodle, Last Commit: Sat Mar 21 10:13:32 2020 +0100
* l5_shoppinglist - Repo: https://source.puri.sm/fphemeral/l5_shoppinglist, Last Commit: Sat Mar 07 16:01:59 2020 +0000
* Gifup - Repo: https://github.com/BharatKalluri/Gifup, Last Commit: Tue Mar 03 11:14:05 2020 +0530
* librem5_utils - Repo: https://source.puri.sm/fphemeral/librem5_utils, Last Commit: Sun Jan 26 12:30:06 2020 +0000

### 2019
* kodimote - Repo: https://github.com/scandinave/kodimote, Last Commit: Thu Dec 26 16:09:29 2019 +0100
* coffee - Repo: https://github.com/nick92/coffee, Last Commit: Thu Nov 14 13:07:31 2019 +0000
* wallet - Repo: https://gitlab.gnome.org/bilelmoussaoui/wallet, Last Commit: Sat Sep 28 14:26:40 2019 +0000
* venom - Repo: https://github.com/naxuroqa/venom, Last Commit: Mon Feb 18 11:42:02 2019 +0100

### 2018
* client - Repo: https://github.com/TheToxProject/client, Last Commit: Mon Aug 13 09:25:16 2018 +0200

### 2017
* medOS-kirigami - Repo: https://github.com/milohr/medOS-kirigami, Last Commit: Sun Oct 22 14:54:00 2017 -0500

### 2016
* Cooking-Calc - Repo: https://codeberg.org/WammKD/Cooking-Calc, Last Commit: Wed May 04 22:52:14 2016 -0500

## OLD ORDER - augment lines above, then delete this

### Needs help testing
* keyring <https://git.sr.ht/~martijnbraam/keyring> (needs Himitsu user https://himitsustore.org/ / packaging of himitsu/hare) 
* rokugtk <https://github.com/cliftonts/rokugtk> (a Roku remote app, requires a Roku device to test)
* AugVK-Messenger <https://github.com/Augmeneco/AugVK-Messenger> (requires a VKontakte account)
* AugSteam <https://github.com/Augmeneco/AugSteam> (if you use Steam's mobile authenticator app, please help out!)
* Kazv <https://lily-is.land/kazv/kazv> (can't even successfully build libkazv)
* Libre Invoice <https://codeberg.org/matiaslavik/LibreInvoice> Looks like LimeReport might need to be rebuild, but since I know nothing about Qt Android apps, that might not be all.
* Open Mandriva Camera app <https://github.com/OpenMandrivaSoftware/om-camera> (can't get this to work on other distributions)
* Bitsteward <https://github.com/Bitsteward/bitsteward> (Bitwarden client, needs bitwarden user)


### Needs testing

#### Kirigami
* Brun <https://invent.kde.org/maui/brun> (MauiKit calculator)
* PowerPlant <https://invent.kde.org/mbruchert/powerplant>
* Nextcloud Talk <https://github.com/CarlSchwan/talk-desktop>  (Kirigami-fication of a SailfishOS app) <https://codeberg.org/blizzz/harbour-nextcloud-talk/pulls/37>, see also: <https://carlschwan.eu/2021/12/18/more-kde-apps/>
* Clip2DeepL <https://codeberg.org/yasht/clip2deepl> (works when started on terminal on Phosh, needs testing on Plasma Mobile, last commit 2021-03-02)
* Pelikan <https://invent.kde.org/carlschwan/quickmail> (now called "Pelikan", see https://carlschwan.eu/2021/12/18/more-kde-apps/)
* mailmodel <https://invent.kde.org/vandenoever/mailmodel> (last commit 2021-04-04)
* Solio <https://invent.kde.org/devinlin/solio> Soundcloud client (last commit 2021-05-21)
* Booth <https://invent.kde.org/maui/booth> (convergent camera app)

#### QtWidget
* opensnitch <https://github.com/evilsocket/opensnitch/discussions/415>
* Qt-Audiobook-Player <https://github.com/rushic24/Qt-Audiobook-Player/>

#### QtQuick
* Grep Tool QML <https://github.com/presire/GrepToolQML>
* Barcelona Trees <https://github.com/pedrolcl/barcelona-trees>
* Toxer Desktop <https://gitlab.com/Toxer/toxer-desktop>
* Orion <https://github.com/drac69/orion> twitch client, 4 (last commit 2022-11-10)
* Telephant <https://github.com/muesli/telephant> mastodon client
* Qomp <https://github.com/qomp/qomp> music player
* Vrenemar <https://github.com/ntadej/Vremenar> weather app
* Project Trackball <https://github.com/NthElse/trackball> (Todo.txt desktop apps, scales fine on 20210930, but adding tasks does not seem to work yet, last commit 20211005)


#### GTK4
* Desktop Files Creator <https://github.com/alexkdeveloper/desktop-files-creator>
* Lorem	<https://gitlab.gnome.org/World/design/lorem> (at best a 4, and.. do we need Lorem Ipsum on a Phone?)
* Dot Matrix (<https://github.com/lainsce/dot-matrix/>, needs scale-to-fit io.github.lainsce.DotMatrix on)
* Kooha <https://github.com/SeaDve/Kooha>
* Share Preview <https://github.com/rafaelmardojai/share-preview>
* Webfont Kit Generator <https://github.com/rafaelmardojai/webfont-kit-generator>
* Icon Library <https://gitlab.gnome.org/World/design/icon-library>
* Mirdorph (Discord client) <https://gitlab.gnome.org/ranchester/mirdorph> - last commit 2021-09-05
* Tracks <https://codeberg.org/som/Tracks>
* SharMaVid (Invidious client) <https://gitlab.gnome.org/ranfdev/sharmavid>
* Summarizer <https://github.com/Nalsai/summarizer>
* Ghostshot <https://gitlab.gnome.org/GabMus/ghostshot> (last commit 20230122)
* MD Writer <https://gitlab.com/gabmus/mdwriter>
* Noteworthy <https://github.com/SeaDve/Noteworthy> (builds now, not yet tested on mobile)
* Pods <https://github.com/marhkb/pods> (Podman client)
* Citations <https://gitlab.gnome.org/World/citations>
* SNCF <https://git.sr.ht/~yukikoo/sncf/>
* Replay <https://github.com/ReplayDev/Replay> (YouTube client)
* Sharit <https://gitlab.com/LyesSaadi/sharit> (YouTube client)
* Katana <https://github.com/gavr123456789/Katana> (GTK Tree Structure Browser)
* Chert <https://gitlab.com/high-creek-software/chert> (Note taking app)


#### GTK3 
* Abbadon (Discord client) <https://github.com/uowuo/abaddon>
* PineCast <https://github.com/Jeremiah-Roise/PineCast>
* xournalpp <https://github.com/xournalpp/xournalpp>
* keylight-control <https://github.com/mschneider82/keylight-control> builds, but quits on restart
* Coffee <https://github.com/nick92/coffee> (Settings don't work well (janky), rest is okay) - last commit 5 years ago
* umtp-responder-gui <https://github.com/JollyDevelopment/umtp-responder-gui>
* Astroid <https://github.com/astroidmail/astroid/>
* Tunes (MPD Client) <https://git.sr.ht/~jakob/tunes>
* Argos <https://github.com/orontee/argos> (likely ironically not mobile frienly, last commit 5 years ago)
* Camera <https://gitlab.gnome.org/jwestman/camera> 
* GNOME Terminal (fine except for settings, but .., add after GTK4 rewrite)
* Camcam <https://github.com/JNissi/camcam> WIP Camera app written in Rust (last commit 2021-10-16)
* Fastcal <https://github.com/JNissi/fastcal> WIP Calendar app written in Rust (last commit 2021-03-21)
* hn <https://github.com/DavidVentura/hn> Python hacker news client (last commit 2021-04-12)
* Hydro Bot <https://github.com/ripxorip/hydro_bot> ("Pinephone app that keeps you hydrated", too much WIP to add on 2022-04-12)
* wifiscanner <https://github.com/Cyborgscode/wifiscanner> (last commit 2020-12-31)
* GTK Hangouts Client <https://github.com/do-sch/hangouts-gtk>
* pKPR <https://github.com/juliendecharentenay/pKPR>

##### GTK3 + Granite
* Agenda <https://github.com/dahenson/agenda>
* Bitguarden <https://github.com/DenisPalchuk/bitguarden> (last commit 2020-04-29)
* Gifup <https://github.com/BharatKalluri/Gifup> (last commit 2020-03-03)

#### Flutter
* Liftoff <https://github.com/liftoff-app/liftoff> (lemmy client)
* AppImage Pool (<https://github.com/prateekmedia/appimagepool>)
* Nextcloud Password Client <https://gitlab.com/j0chn/nextcloud_password_client>
* Spotube <https://github.com/KRTirtho/spotube/>
* AuthPass (Keepass client) <https://github.com/authpass/authpass> (Did not work in linmob's attempt to build it (white window after startup), supposedly there's an ARM64 snap build, but it returns an architecture warning.)
* Kaiteki (mastodon client) <https://github.com/Kaiteki-Fedi/Kaiteki>
* Intiface® Central <https://github.com/intiface/intiface-central>
* More here: <https://github.com/leanflutter/awesome-flutter-desktop#open-source-apps>

#### Fyne
* itd-gui <https://gitea.arsenm.dev/Arsen6331/itd#itgui>

#### Ubuntu Components
* Cooking calc <https://codeberg.org/WammKD/Cooking-Calc>

#### Cairo etc
* Visurf <https://sr.ht/~sircmpwn/visurf/>
  * see also neosurf <https://github.com/CobaltBSD/neosurf>


### Downstream forks
#### GTK3
* Evince (Purism) - do not add, superseeded by <https://gitlab.gnome.org/GNOME/Incubator/papers> 


### Needs initial testing 
* Personal Voice Assistant <https://github.com/Cyborgscode/Personal-Voice-Assistent> (Currently German only, requires Java. See also <https://marius.bloggt-in-braunschweig.de/2021/07/15/hallo-computer-bist-du-da/>)
* Pine Connect <https://github.com/timaios/PineConnect> (early state of development, work on the app has not started yet (but the daemon is being worked on)) - last commit 2021-07-27 

## From other applists
* <https://cubocore.org/> apps, see also <https://wiki.postmarketos.org/wiki/CoreApps>


### old Linux Phone apps:
* Navit
* FoxtrotGPS
* Maep




### unoptimized desktop apps that have been run on the Librem 5 or PinePhone successfully (only add if they are at least somewhat usable and have an advantage compared to more mobile-ready apps):
* Gnome Photos
* Gthumb
* Shotwell // apps from here on where spotted on a screenshot
* Claws Mail
* D-Feet
* Gpredict
* KiCad
* Shadowsocks-Qt5
* Lyrebird <https://github.com/constcharptr/lyrebird>
* Webapp Manager <https://github.com/linuxmint/webapp-manager>
* Verbiste <https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/7>
* Simple scan <https://gitlab.gnome.org/GNOME/simple-scan> (newer versions are only fine after "scale-to-fit simple-scan on")
* Marble (<https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/27>)
* Orage (<https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/28>)
* Solar System (<https://flathub.org/apps/details/org.sugarlabs.SolarSystem; https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/37>)
* AusweisApp 2 (<https://flathub.org/apps/details/de.bund.ausweisapp.ausweisapp2>; <https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/51>)
* Nixwriter <https://flathub.org/apps/details/com.gitlab.adnan338.Nixwriter>; <https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/69>
* SongRec <https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/91>
* Bleachbit <https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/101>
* Argos Translate <https://github.com/argosopentech/argos-translate>

## Needs help testing (low priority, inactive - check periodically for activity)
### GTK3
* Moody (Moodle client, GTK3/libhandy) <https://gitlab.gnugen.ch/afontain/moodle> (last commit 2020-03-20)
* Venom <https://github.com/naxuroqa/venom> (A modern Tox client for the Linux desktop, not mobile friendly, last commit 2020-04-23)

### Projects that seem to have been abandoned or are archived
* Wallet <https://gitlab.gnome.org/bilelmoussaoui/wallet>
* Kodimote <https://github.com/scandinave/kodimote> (WIP, last commit 2019-12-23)
* l5_shoppinglist <https://source.puri.sm/fphemeral/l5_shoppinglist>,<https://puri.sm/posts/an-interview-with-fphemeral-librem-5-early-adopter>
* librem5_utils <https://source.puri.sm/fphemeral/librem5_utils>,<https://puri.sm/posts/an-interview-with-fphemeral-librem-5-early-adopter> <https://stackoverflow.com/questions/1260748/how-do-i-remove-a-submodule/36593218#36593218/>
* Flashcards <https://github.com/SeaDve/Flashcards> (only UI, non-functional, archived)
* PinePhone modem firmware updater UI: <https://git.sr.ht/~martijnbraam/pinephone-modemfw> (solved with GNOME Firmware, still listed for historic purposes)
* camera-rs <https://gitlab.gnome.org/bilelmoussaoui/camera-rs> (last commit 2020-12-25)
* qrcode-generator <https://gitlab.gnome.org/bilelmoussaoui/paintable-rs> (last commit 2020-12-23)
* Interpret <https://codeberg.org/xaviers/Interpret> (GTK3 deepl based translation app, archived)
* Tox Client (Electron) <https://github.com/TheToxProject/client>
* Simple RSS Reader using KDE Kirigami <https://gitlab.com/SiriBeta/simple-rss-reader-using-kde-kirigami> (last commit 2020-06-13)
* Papper <https://gitlab.com/GunnarGrop/papper> (Kirigami reddit client, last commit 2020-07-01)
* LurkyMcLurkFace <https://gitlab.com/armen138/lurkymclurkface> (Kirigami reddit client, last commit 2020-12-16)
* Pala <https://gitlab.com/gabmus/pala>
* covidpass <https://github.com/pentamassiv/covidpass>

### Projects that seem to be gone
* clan <https://web.archive.org/web/20201201114016/https://github.com/mauikit/clan> (Seems to have been a launcher)

### Not an app
* Retiled (Windows Phone style launcher) <https://github.com/DrewNaylor/Retiled> (are launchers in scope?)
* PUI <https://github.com/nbdy/pui> (launcher?)
* <https://github.com/grelltrier/keyboard> Fingerboard

### No GUI
* Mactrack <https://gitlab.com/Aresesi/mactrack>


## Remains of other-apps.csv
Check, then sort or remove the following:

### This is a loose and non-formal collection of apps which are not added to the main list yet, because they are not in a usable state, are still in planning stage or wait to be transfered to the other list
* MedOS-kirigami <https://github.com/milohr/medOS-kirigami>
* Liri Screencast <https://github.com/lirios/screencast>
* Liri Screenshot <https://github.com/lirios/screenshot>
* Liri music <https://github.com/lirios/music>
* Liri Settings <https://github.com/lirios/settings>
* Liri NetworkManager <https://github.com/lirios/networkmanager>
* OwnCloud Sync <https://open-store.io/app/owncloud-sync>
* Music <https://wiki.gnome.org/Apps/Music>, <https://tchncs.de/_matrix/media/v1/download/talk.puri.sm/wDbVJsNtmaLUljbuxVVzRRhf>
* GadgetBridge <https://github.com/Freeyourgadget/Gadgetbridge> (afaik Android only, so: why?)
* Linphone (not mobile compatible, Ubuntu Touch version does not exist for 64bit yet)
* <https://doc.qt.io/qt-5/qtquick-codesamples.html>

### In planning stage:
* Compass <https://phabricator.kde.org/T8905>
* Konversation 2.0

### Sources for apps to be included:
* <https://binary-factory.kde.org/view/Android/>,28 apps (02.03.2020)
* <https://www.youtube.com/channel/UCIVaIdjtr6aNPdTm-JNbFAg/videos>
* <https://www.youtube.com/watch?v=K4OfNFis--g>
* <https://puri.sm/posts/what-is-mobile-pureos/>
* <https://source.puri.sm/Librem5/community-wiki/-/wikis/List-of-Apps-in-Development>

+++
title = "GNOME Sudoku"
description = "Test your logic skills in this number grid puzzle"
aliases = []
date = 2023-02-07
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
categories = [ "game",]
mobile_compatibility = [ "5",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
metadata_licenses = [ "CC0-1.0",]

[extra]
reported_by = "linmob"
verified = "✅"
repository = "https://gitlab.gnome.org/GNOME/gnome-sudoku"
homepage = "https://wiki.gnome.org/Apps/Sudoku"
more_information = []
summary_source_url = "https://flathub.org/apps/org.gnome.Sudoku"
screenshots = [ "https://gitlab.gnome.org/GNOME/gnome-sudoku/-/raw/master/data/screenshot.png",]
screenshots_img = []
app_id = "org.gnome.Sudoku"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Sudoku"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gnome-sudoku",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-sudoku/-/raw/master/data/org.gnome.Sudoku.appdata.xml.in"
updated_by = "script"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-sudoku"
donations = "https://www.gnome.org/donate/"
translations = "https://wiki.gnome.org/TranslationProject"
intended_for_mobile = true
all_features_touch = true

+++

### Description

Play the popular Japanese logic game. GNOME Sudoku is a must-install for
 Sudoku lovers, with a simple, unobtrusive interface that makes playing
 Sudoku fun for players of any skill level.


Each game is assigned a difficulty similar to those given by newspapers
 and websites, so your game will be as easy or as difficult as you want it
 to be.


If you like to play on paper, you can print games out. You can choose how
 many games you want to print per page and what difficulty of games you
 want to print: as a result, GNOME Sudoku can act a renewable Sudoku book
 for you.

[Source](https://gitlab.gnome.org/GNOME/gnome-sudoku/-/raw/master/data/org.gnome.Sudoku.appdata.xml.in)

+++
title = "RuneLite"
description = "RuneLite OSRS Client"
aliases = []
date = 2021-01-26
updated = 2024-01-02

[taxonomies]
project_licenses = [ "BSD-2-Clause",]
categories = [ "game",]
mobile_compatibility = [ "1",]
frameworks = []
backends = []
services = []
packaged_in = [ "aur", "flathub", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
app_author = [ "RuneLite",]
metadata_licenses = [ "CC0-1.0",]

[extra]
reported_by = "immychan"
verified = "❎"
repository = "https://github.com/runelite/runelite"
homepage = "https://runelite.net"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/flathub/net.runelite.RuneLite/master/net.runelite.RuneLite.appdata.xml"
screenshots = []
screenshots_img = []
app_id = "net.runelite.RuneLite.desktop"
scale_to_fit = ""
flathub = "https://flathub.org/apps/net.runelite.RuneLite"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "runelite",]
appstream_xml_url = "https://raw.githubusercontent.com/flathub/net.runelite.RuneLite/master/net.runelite.RuneLite.appdata.xml"
updated_by = "script"

+++

### Description

RuneLite is a free, open-source and super fast client for Old School RuneScape

[Source](https://raw.githubusercontent.com/flathub/net.runelite.RuneLite/master/net.runelite.RuneLite.appdata.xml)

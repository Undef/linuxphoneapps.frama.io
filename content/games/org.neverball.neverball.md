+++
title = "Neverball"
description = "Deftly Guide a Rolling Ball through Many Slick 3D Levels"
aliases = []
date = 2019-02-01
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
categories = [ "game",]
mobile_compatibility = [ "needs testing",]
frameworks = [ "SDL2",]
backends = []
services = []
packaged_in = [ "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "pureos_landing",]

[extra]
reported_by = "cahfofpai"
verified = "❎"
repository = "https://github.com/pseuudonym404/neverball-touch"
homepage = "https://neverball.org/"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/flathub/org.neverball.Neverball/master/org.neverball.Neverball.appdata.xml"
screenshots = [ "https://neverball.org/images/shots/01-neverball-easy/easy-07-01.jpg", "https://neverball.org/images/shots/01-neverball-easy/easy-12-01.jpg", "https://neverball.org/images/shots/04-neverball-mym1/mym1-13-04.jpg",]
screenshots_img = []
app_id = "org.neverball.Neverball.desktop"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.neverball.Neverball"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "neverball",]
appstream_xml_url = "https://raw.githubusercontent.com/flathub/org.neverball.Neverball/master/org.neverball.Neverball.appdata.xml"
updated_by = "script"
bugtracker = "https://github.com/Neverball/neverball"

+++

### Description

Guide a rolling ball through dangerous territory that you control by
 tilting the floor. Balance on narrow bridges, navigate mazes, ride
 moving platforms, and dodge pushers and shovers to get to the goal.
 Race against the clock to collect coins to earn extra balls. With nice
 physics and very clean and appealing 3D graphics, this is definitely a
 must play.


Bundled with Neverball is also Neverputt, a hot-seat local multiplayer
 miniature golf game, built on the physics and graphics engine of Neverball.

[Source](https://raw.githubusercontent.com/flathub/org.neverball.Neverball/master/org.neverball.Neverball.appdata.xml)

+++
title = "Superjumper"
description = "Super Jumper is a very simple Doodle Jump clone."
aliases = []
date = 2019-02-01

[taxonomies]
project_licenses = [ "not specified",]
categories = [ "demo", "game",]
mobile_compatibility = [ "needs testing",]
frameworks = [ "libGDX",]
backends = []
services = []
packaged_in = []

[extra]
reported_by = "cahfofpai"
verified = "❎"
repository = "https://github.com/libgdx/libgdx-demo-superjumper"
homepage = ""
more_information = []
summary_source_url = "https://github.com/libgdx/libgdx-demo-superjumper"
screenshots = [ "https://github.com/libgdx/libgdx-demo-superjumper",]
screenshots_img = []
app_id = "com.badlogicgames.superjumper"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""

+++
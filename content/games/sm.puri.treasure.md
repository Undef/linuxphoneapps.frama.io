+++
title = "Treasure"
description = "An application example, implementing a simple guessing game."
aliases = []
date = 2019-02-01
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
categories = [ "game",]
mobile_compatibility = [ "needs testing",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = []
metadata_licenses = [ "CC0-1.0",]

[extra]
reported_by = "cahfofpai"
verified = "❎"
repository = "https://source.puri.sm/david.boddie/treasure"
homepage = ""
more_information = []
summary_source_url = "https://source.puri.sm/david.boddie/treasure"
screenshots = []
screenshots_img = []
app_id = "sm.puri.treasure.desktop"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://source.puri.sm/david.boddie/treasure/-/raw/master/data/sm.puri.treasure.appdata.xml.in"
updated_by = "script"

+++
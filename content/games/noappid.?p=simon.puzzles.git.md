+++
title = "Simon Tatham's Portable Puzzle Collection"
description = "A collection of small computer programs which implement one-player puzzle games."
aliases = []
date = 2021-03-24

[taxonomies]
project_licenses = [ "MIT",]
categories = [ "game",]
mobile_compatibility = [ "4",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "AUR",]

[extra]
reported_by = "Moxvallix"
verified = "✅"
repository = "https://git.tartarus.org/?p=simon/puzzles.git"
homepage = "https://www.chiark.greenend.org.uk/~sgtatham/puzzles/"
more_information = []
summary_source_url = "https://www.chiark.greenend.org.uk/~sgtatham/puzzles/"
screenshots = []
screenshots_img = []
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""

+++



### Notice

Games mostly scale well, and can all be played with the touchscreen. Might clog your app draw with 15 different game icons.
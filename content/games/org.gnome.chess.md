+++
title = "GNOME Chess"
description = "Play the classic two-player board game of chess"
aliases = []
date = 2021-03-14
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
categories = [ "game",]
mobile_compatibility = [ "5",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
metadata_licenses = [ "CC0-1.0",]

[extra]
reported_by = "Moxvallix"
verified = "✅"
repository = "https://gitlab.gnome.org/GNOME/gnome-chess"
homepage = "https://wiki.gnome.org/Apps/Chess"
more_information = []
summary_source_url = "https://flathub.org/apps/org.gnome.Chess"
screenshots = [ "https://gitlab.gnome.org/GNOME/gnome-chess/-/raw/master/data/screenshot.png",]
screenshots_img = []
app_id = "org.gnome.Chess"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Chess"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gnome-chess",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-chess/-/raw/master/data/org.gnome.Chess.appdata.xml.in"
updated_by = "script"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-chess/issues/"
donations = "https://www.gnome.org/donate/"
translations = "https://wiki.gnome.org/TranslationProject"
intended_for_mobile = true
all_features_touch = true

+++

### Description

GNOME Chess is a simple chess game. You can play against your computer at
 three different difficulty levels, or against a friend at your computer.


Computer chess enthusiasts will appreciate GNOME Chess’s compatibility with
 nearly all modern computer chess engines, and its ability to detect several
 popular engines automatically if installed.

[Source](https://gitlab.gnome.org/GNOME/gnome-chess/-/raw/master/data/org.gnome.Chess.appdata.xml.in)

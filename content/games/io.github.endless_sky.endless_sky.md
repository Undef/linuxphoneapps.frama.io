+++
title = "Endless Sky"
description = "Space exploration and combat game"
aliases = []
date = 2021-03-14
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
categories = [ "game",]
mobile_compatibility = [ "1",]
frameworks = [ "SDL2",]
backends = []
services = []
packaged_in = [ "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Game", "Simulation",]
app_author = [ "Michael Zahniser",]
metadata_licenses = [ "CC0-1.0",]

[extra]
reported_by = "-Euso-"
verified = "❎"
repository = "https://github.com/endless-sky/endless-sky"
homepage = "https://endless-sky.github.io/"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/endless-sky/endless-sky/master/io.github.endless_sky.endless_sky.appdata.xml"
screenshots = [ "https://cdn.cloudflare.steamstatic.com/steam/apps/404410/ss_1e369b0752dba42ad18503e4ae6326faef4337b3.jpg", "https://cdn.cloudflare.steamstatic.com/steam/apps/404410/ss_4467e08dadf09d4bbaecf713e56faf134e590980.jpg", "https://cdn.cloudflare.steamstatic.com/steam/apps/404410/ss_93d13da0bdfe2c4b987eb4f5e06fb67344df518d.jpg", "https://cdn.cloudflare.steamstatic.com/steam/apps/404410/ss_9b2871e176b9a96ed1013e3870ee9be28b9c7ee8.jpg", "https://cdn.cloudflare.steamstatic.com/steam/apps/404410/ss_af75baf860ad0dddcdc86f00dfb7b3ff3761133f.jpg", "https://cdn.cloudflare.steamstatic.com/steam/apps/404410/ss_c7f18a682e8df27368c2c6a421624a395719c6c5.jpg", "https://cdn.cloudflare.steamstatic.com/steam/apps/404410/ss_c85f211c73990b5edebd087205ee0b919c073621.jpg", "https://cdn.cloudflare.steamstatic.com/steam/apps/404410/ss_df5a65876c0a23d178f047d53ed5f9944a6c438b.jpg", "https://cdn.cloudflare.steamstatic.com/steam/apps/404410/ss_eab3f8ba47f7b85030a98c3423caaef4852f0063.jpg",]
screenshots_img = []
app_id = "io.github.endless_sky.endless_sky"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.endless_sky.endless_sky"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "endless-sky",]
appstream_xml_url = "https://raw.githubusercontent.com/endless-sky/endless-sky/master/io.github.endless_sky.endless_sky.appdata.xml"
updated_by = "script"
bugtracker = "https://github.com/endless-sky/endless-sky/issues"

+++

### Description

Explore other star systems. Earn money by trading, carrying passengers, or completing missions. Use your earnings to buy a better ship or to upgrade the weapons and engines on your current one. Blow up pirates. Take sides in a civil war. Or leave human space behind and hope to find some friendly aliens whose culture is more civilized than your own...


Endless Sky is a sandbox-style space exploration game similar to Elite, Escape Velocity, or Star Control. You start out as the captain of a tiny spaceship and can choose what to do from there. The game includes a major plot line and many minor missions, but you can choose whether you want to play through the plot or strike out on your own as a merchant or bounty hunter or explorer.

[Source](https://raw.githubusercontent.com/endless-sky/endless-sky/master/io.github.endless_sky.endless_sky.appdata.xml)

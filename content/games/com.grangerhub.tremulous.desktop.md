+++
title = "Tremulous"
description = "Aliens vs Humans, First Person Shooter game with elements of Real Time Strategy"
aliases = []
date = 2021-03-14
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-2.0-only",]
categories = [ "game",]
mobile_compatibility = [ "1",]
frameworks = [ "SDL2",]
backends = []
services = []
packaged_in = [ "aur", "fedora_38", "fedora_39", "fedora_rawhide", "flathub",]
metadata_licenses = [ "CC-BY-SA-3.0",]

[extra]
reported_by = "-Euso-"
verified = "❎"
repository = "https://github.com/darklegion/tremulous"
homepage = "https://grangerhub.com"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/flathub/com.grangerhub.Tremulous/master/com.grangerhub.Tremulous.appdata.xml"
screenshots = []
screenshots_img = []
app_id = "com.grangerhub.Tremulous.desktop"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.grangerhub.Tremulous"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "tremulous",]
appstream_xml_url = "https://raw.githubusercontent.com/flathub/com.grangerhub.Tremulous/master/com.grangerhub.Tremulous.appdata.xml"
updated_by = "script"

+++

### Description

Tremulous is a free, open source game that blends a team based FPS with
 elements of an RTS.


Players can choose from 2 unique races, aliens and humans.
 Players on both teams are able to build working structures in-game like an
 RTS.

[Source](https://raw.githubusercontent.com/flathub/com.grangerhub.Tremulous/master/com.grangerhub.Tremulous.appdata.xml)

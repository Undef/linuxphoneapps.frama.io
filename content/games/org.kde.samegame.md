+++
title = "Samegame"
description = "A fun game"
aliases = []
date = 2020-09-07
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
categories = [ "game",]
mobile_compatibility = [ "5",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = []
app_author = [ "Sebastian Kügler <sebas@kde.org>",]
metadata_licenses = [ "CC0-1.0",]

[extra]
reported_by = "linmob"
verified = "✅"
repository = "https://invent.kde.org/plasma-mobile/plasma-samegame"
homepage = "https://plasma-mobile.org"
more_information = []
summary_source_url = "https://invent.kde.org/plasma-mobile/plasma-samegame/-/raw/master/org.kde.samegame.appdata.xml"
screenshots = [ "https://linmob.net/pinephone-building-plasma-mobile-apps-from-the-aur/20200906_22h15m48s_grim.jpg",]
screenshots_img = []
app_id = "org.kde.plasma.samegame"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://invent.kde.org/plasma-mobile/plasma-samegame/-/raw/master/org.kde.samegame.appdata.xml"
updated_by = "script"
donations = "https://www.kde.org/donate.php?app=org.kde.plasma.samegame"

+++
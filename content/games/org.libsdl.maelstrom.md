+++
title = "Maelstrom"
description = "High Quality Asteroids Clone"
aliases = []
date = 2021-05-06
updated = 2024-01-02

[taxonomies]
project_licenses = [ "CC-BY-3.0", "GPL-2.0-or-later",]
categories = [ "game",]
mobile_compatibility = [ "1",]
frameworks = [ "SDL",]
backends = []
services = []
packaged_in = [ "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
app_author = [ "Ambrosia Software",]
metadata_licenses = [ "CC0-1.0",]

[extra]
reported_by = "cornelius1968"
verified = "❎"
repository = "https://www.libsdl.org/projects/Maelstrom/source.html"
homepage = "https://www.libsdl.org/projects/Maelstrom/"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/flathub/org.libsdl.Maelstrom/master/org.libsdl.Maelstrom.appdata.xml"
screenshots = [ "https://upload.wikimedia.org/wikipedia/commons/4/44/Maelstrom_screenshot.png",]
screenshots_img = []
app_id = "org.libsdl.Maelstrom.desktop"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.libsdl.Maelstrom"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "maelstrom",]
appstream_xml_url = "https://raw.githubusercontent.com/flathub/org.libsdl.Maelstrom/master/org.libsdl.Maelstrom.appdata.xml"
updated_by = "script"

+++

### Description

You pilot your ship through the dreaded "Maelstrom" asteroid belt -- suddenly your best friend thrusts towards you and fires, directly at your cockpit. You raise your shields just in time, and the battle is joined.


The deadliest stretch of space known to mankind has just gotten deadlier. Everywhere massive asteroids jostle for a chance to crush your ship, and deadly shinobi fighter patrols pursue you across the asteroid belt. But the deadliest of them all is your sister ship, assigned to you on patrol. The pilot, trained by your own Navy, battle hardened by months in the Maelstrom, is equipped with a twin of your own ship and intimate knowledge of your tactics.


The lovely Stratocaster R&R facility never sounded so good, but as you fire full thrusters to dodge the latest barrage you begin to think you'll never get home...

[Source](https://raw.githubusercontent.com/flathub/org.libsdl.Maelstrom/master/org.libsdl.Maelstrom.appdata.xml)

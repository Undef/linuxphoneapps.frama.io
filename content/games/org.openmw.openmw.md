+++
title = "OpenMW"
description = "Reimplementation of The Elder Scrolls III: Morrowind"
aliases = []
date = 2021-03-14
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
categories = [ "game",]
mobile_compatibility = [ "1",]
frameworks = [ "SDL2",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Game", "RolePlaying",]
metadata_licenses = [ "CC0-1.0",]

[extra]
reported_by = "preflex"
verified = "❎"
repository = "https://gitlab.com/OpenMW/openmw"
homepage = "https://openmw.org"
more_information = []
summary_source_url = "https://gitlab.com/OpenMW/openmw/-/raw/master/files/openmw.appdata.xml"
screenshots = [ "https://wiki.openmw.org/images/0.40_Screenshot-Balmora_3.png", "https://wiki.openmw.org/images/Openmw_0.11.1_launcher_1.png", "https://wiki.openmw.org/images/Screenshot_Vivec_seen_from_Ebonheart_0.35.png", "https://wiki.openmw.org/images/Screenshot_mournhold_plaza_0.35.png",]
screenshots_img = []
app_id = "org.openmw.launcher"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.openmw.OpenMW"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "openmw",]
appstream_xml_url = "https://gitlab.com/OpenMW/openmw/-/raw/master/files/openmw.appdata.xml"
updated_by = "script"
bugtracker = "https://gitlab.com/OpenMW/openmw/issues"

+++


### Description

OpenMW is a new engine for 2002's Game of the Year, The Elder Scrolls 3: Morrowind.


It aims to be a fully playable (and improved!), open source implementation of the game's engine and functionality (including mods).


You will still need the original game data to play OpenMW.

[Source](https://gitlab.com/OpenMW/openmw/-/raw/master/files/openmw.appdata.xml)

+++
title = "Swooper"
description = "Minesweeper"
aliases = []
date = 2020-09-28
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3+",]
categories = [ "game",]
mobile_compatibility = [ "5",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = []
app_author = [ "lui",]
metadata_licenses = [ "FSFAP",]

[extra]
reported_by = "linmob"
verified = "❎"
repository = "https://invent.kde.org/luie/Swooper"
homepage = ""
more_information = []
summary_source_url = "https://invent.kde.org/luie/Swooper/-/raw/master/org.kde.swooper.appdata.xml"
screenshots = []
screenshots_img = []
app_id = "org.kde.swooper"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://invent.kde.org/luie/Swooper/-/raw/master/org.kde.swooper.appdata.xml"
updated_by = "script"

+++


### Description

Minesweeper!

[Source](https://invent.kde.org/luie/Swooper/-/raw/master/org.kde.swooper.appdata.xml)
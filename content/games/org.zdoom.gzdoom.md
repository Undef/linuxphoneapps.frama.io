+++
title = "GZDoom"
description = "Classic first-person-shooter engine for all classical Id games"
aliases = []
date = 2021-03-14
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
categories = [ "game",]
mobile_compatibility = [ "1",]
frameworks = [ "SDL2",]
backends = []
services = []
packaged_in = [ "alpine_edge", "aur", "flathub", "gentoo", "gnuguix", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed",]
app_author = [ "ZDoom team",]

[extra]
reported_by = "-Euso-"
verified = "❎"
repository = "https://github.com/ZDoom/gzdoom"
homepage = "https://zdoom.org/index"
bugtracker = "https://github.com/coelckers/gzdoom"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/flathub/org.zdoom.GZDoom/master/org.zdoom.GZDoom.appdata.xml"
screenshots = [ "https://raw.githubusercontent.com/flathub/org.zdoom.GZDoom/master/images/image_01.png", "https://raw.githubusercontent.com/flathub/org.zdoom.GZDoom/master/images/image_02.png", "https://raw.githubusercontent.com/flathub/org.zdoom.GZDoom/master/images/image_03.png", "https://raw.githubusercontent.com/flathub/org.zdoom.GZDoom/master/images/image_04.png",]
screenshots_img = []
app_id = "org.zdoom.GZDoom"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.zdoom.GZDoom"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gzdoom",]
appstream_xml_url = "https://raw.githubusercontent.com/flathub/org.zdoom.GZDoom/master/org.zdoom.GZDoom.appdata.xml"
updated_by = "script"

+++

### Description

GZDoom is a source port for the modern era, supporting current hardware and operating systems and sporting a vast array of user options. Make Doom your own again!


In addition to Doom, GZDoom supports Heretic, Hexen, Strife, Chex Quest, and fan-created games like Harmony and Hacx. Meet the entire idTech 1 family!


Experience mind-bending user-created mods, made possible by ZDoom's advanced mapping features and the new ZScript language. Or make a mod of your own!


* Can play all Doom engine games, including Ultimate Doom, Doom II, Heretic, Hexen, Strife, and more
* Supports all the editing features of Hexen. (ACS, hubs, new map format, etc.)
* Supports most of the Boom editing features
* Features complete translations of Doom, Heretic, Hexen, Strife and other games into over ten different languages with Unicode support for Latin, Cyrillic, and Hangul so far
* All Doom limits are gone
* Several softsynths for MUS and MIDI playback, including an OPL softsynth for an authentic “oldschool” flavor
* High resolutions
* Quake-style console and key bindings
* Crosshairs
* Free look (look up/down)
* Jumping, crouching, swimming, and flying
* Up to 8 player network games using UDP/IP, including team-based gameplay
* Support for the Bloodbath announcer from the classic Monolith game Blood
* Walk over/under monsters and other things


Commercial data files are required to run the supported games. For more info about all supported games and their data files, see: Help -> List of supported games.


With Flatpak, all file-access is restricted to ~/.var/app/org.zdoom.GZDoom/.config/gzdoom for privacy reasons. You'll have to manually place your IWADs and PWADS there, or you should use an application like Flatseal to give GZDoom additional permissions.

[Source](https://raw.githubusercontent.com/flathub/org.zdoom.GZDoom/master/org.zdoom.GZDoom.appdata.xml)

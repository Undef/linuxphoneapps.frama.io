+++
title = "KTuberling"
description = "A simple constructor game suitable for children and adults alike"
aliases = []
date = 2019-02-01
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
categories = [ "game",]
mobile_compatibility = [ "needs testing",]
frameworks = [ "QtWidgets",]
backends = []
services = []
packaged_in = [ "arch", "archlinuxarm_aarch64", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "opensuse_tumbleweed", "pureos_landing",]
app_author = [ "The KTuberling Developers",]
metadata_licenses = [ "CC0-1.0",]

[extra]
reported_by = "cahfofpai"
verified = "❎"
repository = "https://invent.kde.org/games/ktuberling"
homepage = "https://apps.kde.org/ktuberling/"
more_information = []
summary_source_url = "https://invent.kde.org/games/ktuberling/-/raw/master/org.kde.ktuberling.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/ktuberling/ktuberling.png",]
screenshots_img = []
app_id = "org.kde.ktuberling"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.ktuberling"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "ktuberling",]
appstream_xml_url = "https://invent.kde.org/games/ktuberling/-/raw/master/org.kde.ktuberling.appdata.xml"
updated_by = "script"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?format=guided&product=ktuberling"
donations = "https://www.kde.org/community/donations/?app=ktuberling&source=appdata"

+++

### Description

KTuberling a simple constructor game suitable for children and adults alike. The idea of the game is based around a once popular doll making concept.

[Source](https://invent.kde.org/games/ktuberling/-/raw/master/org.kde.ktuberling.appdata.xml)

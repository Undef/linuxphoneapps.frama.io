+++
title = "GCompris"
description = "Multi-Activity Educational game for children 2 to 10"
aliases = []
date = 2021-03-15
updated = 2024-01-02

[taxonomies]
project_licenses = [ "AGPL-3.0-only",]
categories = [ "educational game",]
mobile_compatibility = [ "5",]
frameworks = [ "QtQuick",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
metadata_licenses = [ "CC0-1.0",]

[extra]
reported_by = "skyrrd"
verified = "❎"
repository = "https://github.com/gcompris/GCompris-qt"
homepage = "https://gcompris.net/"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/gcompris/GCompris-qt/master/org.kde.gcompris.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/gcompris/gcompris.png", "https://gcompris.net/screenshots_qt/large/chess_partyend.png", "https://gcompris.net/screenshots_qt/large/click_on_letter_up.png", "https://gcompris.net/screenshots_qt/large/clockgame.png", "https://gcompris.net/screenshots_qt/large/color_mix.png", "https://gcompris.net/screenshots_qt/large/colors.png", "https://gcompris.net/screenshots_qt/large/crane.png", "https://gcompris.net/screenshots_qt/large/enumerate.png", "https://gcompris.net/screenshots_qt/large/fifteen.png", "https://gcompris.net/screenshots_qt/large/hexagon.png", "https://gcompris.net/screenshots_qt/large/scalesboard.png", "https://gcompris.net/screenshots_qt/large/traffic.png",]
screenshots_img = []
app_id = "org.kde.gcompris.desktop"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.gcompris"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gcompris",]
appstream_xml_url = "https://raw.githubusercontent.com/gcompris/GCompris-qt/master/org.kde.gcompris.appdata.xml"
updated_by = "script"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?format=guided&product=gcompris"
donations = "https://gcompris.net/donate-en.html"
translations = "https://gcompris.net/wiki/Developer%27s_corner#Translation"

+++

### Description

GCompris is a high quality educational software suite, including a large number of activities for children aged 2 to 10.


Some of the activities are game orientated, but nonetheless
 still educational.


Below you can find a list of categories with some of the
 activities available in that category.


* computer discovery: keyboard, mouse, different mouse gestures, ...
* arithmetic: table memory, enumeration, mirror image, balance
 the scale, change giving, ...
* science: the canal lock, color mixing, gravity concept, ...
* games: memory, connect 4, tic tac toe, sudoku, hanoi tower, ...
* reading: reading practice, ...
* other: learn to tell time, the braille system, maze, music
 instruments, ...


Currently GCompris offers in excess of 100 activities and more
 are being developed. GCompris is free software, that means that
 you can adapt it to your own needs, improve it and, most
 importantly, share it with children everywhere.

[Source](https://raw.githubusercontent.com/gcompris/GCompris-qt/master/org.kde.gcompris.appdata.xml)

+++
title = "TwoFun"
description = "Touch based reaction game for two players."
aliases = []
date = 2019-02-01
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
categories = [ "game",]
mobile_compatibility = [ "5",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "Flathub",]
freedesktop_categories = [ "Games",]
metadata_licenses = [ "CC0-1.0",]

[extra]
reported_by = "cahfofpai"
verified = "❎"
repository = "https://gitlab.com/tabos/twofun"
homepage = "https://tabos.org/projects/twofun/"
more_information = []
summary_source_url = "https://gitlab.com/tabos/twofun/-/raw/master/data/org.tabos.twofun.appdata.xml.in"
screenshots = [ "https://gitlab.com/tabos/twofun/-/raw/master/data/screenshots/twofun1.png", "https://gitlab.com/tabos/twofun/-/raw/master/data/screenshots/twofun2.png", "https://gitlab.com/tabos/twofun/-/raw/master/data/screenshots/twofun3.png", "https://gitlab.com/tabos/twofun/-/raw/master/data/screenshots/twofun4.png", "https://gitlab.com/tabos/twofun/-/raw/master/data/screenshots/twofun5.png",]
screenshots_img = []
app_id = "org.tabos.twofun"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.tabos.twofun"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://gitlab.com/tabos/twofun/-/raw/master/data/org.tabos.twofun.appdata.xml.in"
updated_by = "script"
bugtracker = "https://gitlab.com/tabos/twofun/issues"
donations = "https://www.paypal.me/tabos/10"
intended_for_mobile = true
all_features_touch = true

+++


### Description

Multiplayer game collection for touch devices.

[Source](https://gitlab.com/tabos/twofun/-/raw/master/data/org.tabos.twofun.appdata.xml.in)
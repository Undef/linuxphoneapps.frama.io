+++
title = "FreeOrion"
description = "Turn-based space empire and galactic conquest (4X) computer game"
aliases = []
date = 2021-03-14
updated = 2024-01-02

[taxonomies]
project_licenses = [ "CC-BY-SA-3.0", "GPL-2.0-only",]
categories = [ "game",]
mobile_compatibility = [ "1",]
frameworks = [ "SDL2",]
backends = []
services = []
packaged_in = [ "arch", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "Game", "StrategyGame",]
app_author = [ "FreeOrion Project",]
metadata_licenses = [ "CC-BY-SA-3.0",]

[extra]
reported_by = "-Euso-"
verified = "❎"
repository = "https://github.com/freeorion/freeorion"
homepage = "http://www.freeorion.org"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/freeorion/freeorion/master/packaging/org.freeorion.FreeOrion.metainfo.xml"
screenshots = [ "https://www.freeorion.org/screenshots/FreeOrion_GalaxyMapCombatGraph.png", "https://www.freeorion.org/screenshots/FreeOrion_GalaxyMapSystemPlanetsFleets.png", "https://www.freeorion.org/screenshots/FreeOrion_ProductionScreen.png", "https://www.freeorion.org/screenshots/FreeOrion_ResearchScreen.png",]
screenshots_img = []
app_id = "org.freeorion.FreeOrion"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.freeorion.FreeOrion"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "freeorion",]
appstream_xml_url = "https://raw.githubusercontent.com/freeorion/freeorion/master/packaging/org.freeorion.FreeOrion.metainfo.xml"
updated_by = "script"
bugtracker = "https://github.com/freeorion/freeorion/issues"
donations = "http://www.freeorion.org/index.php/Donations"

+++

### Description

FreeOrion is a free, Open Source, turn-based space empire and galactic conquest computer game.


FreeOrion is inspired by the tradition of the Master of Orion games, but does not try to be a clone or remake of that series or any other game. It builds on the classic 4X (eXplore, eXpand, eXploit and eXterminate) model.

[Source](https://raw.githubusercontent.com/freeorion/freeorion/master/packaging/org.freeorion.FreeOrion.metainfo.xml)

+++
title = "Unciv"
description = "Turn-based strategy game"
aliases = []
date = 2021-03-17
updated = 2024-01-02

[taxonomies]
project_licenses = [ "MPL-2.0",]
categories = [ "game",]
mobile_compatibility = [ "4",]
frameworks = [ "libGDX",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
app_author = [ "Yair Morgenstern",]
metadata_licenses = [ "CC0-1.0",]

[extra]
reported_by = "Nob0dy73"
verified = "❎"
repository = "https://github.com/yairm210/UnCiv"
homepage = "https://github.com/yairm210/Unciv/"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/flathub/io.github.yairm210.unciv/master/io.github.yairm210.unciv.appdata.xml"
screenshots = [ "https://raw.githubusercontent.com/yairm210/Unciv/master/extraImages/Screenshots/PolicyScreen.png", "https://raw.githubusercontent.com/yairm210/Unciv/master/extraImages/Screenshots/TechTree.png", "https://raw.githubusercontent.com/yairm210/Unciv/master/extraImages/Screenshots/TradeScreen.png", "https://raw.githubusercontent.com/yairm210/Unciv/master/extraImages/Screenshots/WorldScreen.png",]
screenshots_img = []
app_id = "io.github.yairm210.unciv"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.yairm210.unciv"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "unciv",]
appstream_xml_url = "https://raw.githubusercontent.com/flathub/io.github.yairm210.unciv/master/io.github.yairm210.unciv.appdata.xml"
updated_by = "script"
bugtracker = "https://github.com/yairm210/Unciv/issues"
translations = "https://yairm210.github.io/Unciv/Other/Translating/"

+++

### Description

A reimplementation of the most famous civilization-building game ever—fast, small, no ads, free forever!


Build your civilization, research technologies, expand your cities and defeat your foes!


The world awaits! Will you build your civilization into an empire that will stand the test of time?

[Source](https://raw.githubusercontent.com/flathub/io.github.yairm210.unciv/master/io.github.yairm210.unciv.appdata.xml)

### Notice

The resolution is pretty messed up with the text bordering on unreadable, but otherwise running fine

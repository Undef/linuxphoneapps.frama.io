+++
title = "Shattered Pixel Dungeon"
description = "A Roguelike RPG, with randomly generated levels, items, enemies, and traps!"
aliases = []
date = 2021-03-14
updated = 2023-12-31

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
categories = [ "game",]
mobile_compatibility = [ "5",]
frameworks = [ "libGDX",]
backends = []
services = []
packaged_in = [ "arch", "archlinuxarm_aarch64", "aur", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]

[extra]
reported_by = "Moxvallix"
verified = "✅"
repository = "https://github.com/ebolalex/shattered-pixel-dungeon"
homepage = "https://shatteredpixel.com/"
more_information = []
summary_source_url = ""
screenshots = []
screenshots_img = []
app_id = "com.shatteredpixel.shatteredpixeldungeon"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.shatteredpixel.shatteredpixeldungeon"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "shattered-pixel-dungeon",]
appstream_xml_url = "https://raw.githubusercontent.com/flathub/com.shatteredpixel.shatteredpixeldungeon/master/com.shatteredpixel.shatteredpixeldungeon.appdata.xml"
updated_by = "script"

+++

### Notice

Forked by ebolalex to allow it to run. Best in fullscreen mode. Needs java.

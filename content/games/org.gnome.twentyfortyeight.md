+++
title = "GNOME 2048"
description = "Obtain the 2048 tile"
aliases = []
date = 2021-03-14
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
categories = [ "game",]
mobile_compatibility = [ "5",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "opensuse_tumbleweed", "pureos_landing",]
app_author = [ "The GNOME Project",]
metadata_licenses = [ "CC-BY-SA-3.0",]

[extra]
reported_by = "Moxvallix"
verified = "✅"
repository = "https://gitlab.gnome.org/GNOME/gnome-2048"
homepage = "https://wiki.gnome.org/Apps/2048"
more_information = []
summary_source_url = "https://gitlab.gnome.org/GNOME/gnome-2048/-/raw/master/data/org.gnome.TwentyFortyEight.appdata.xml.in"
screenshots = [ "https://gitlab.gnome.org/GNOME/gnome-2048/raw/master/data/screenshot.png",]
screenshots_img = []
app_id = "org.gnome.TwentyFortyEight"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.TwentyFortyEight"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gnome-2048",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-2048/-/raw/master/data/org.gnome.TwentyFortyEight.appdata.xml.in"
updated_by = "script"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-2048/issues"
donations = "https://www.gnome.org/friends/"
translations = "https://wiki.gnome.org/TranslationProject"
intended_for_mobile = true
all_features_touch = true

+++

### Description

Play the highly addictive 2048 game. GNOME 2048 is a clone of the popular
 single-player puzzle game. Gameplay consists of joining numbers in a grid
 and obtain the 2048 tile.


Use your keyboard's arrow keys to slide all tiles in the desired direction.
 Be careful: all tiles slide to their farthest possible positions, you cannot
 slide just one tile or one row or column. Tiles with the same value are
 joined when slided one over the other.


With every new tile obtained you increase your score. If you think you can easily
 get the 2048 tile, do not let it stop you, the game does not end there, you can
 continue joining tiles and improving your score.


Originally created by Gabriele Cirulli, 2048 has gained much popularity
 due to it being highly addictive. Cirulli's 2048 is in turn a clone of
 the 1024 game and includes ideas from other clones.

[Source](https://gitlab.gnome.org/GNOME/gnome-2048/-/raw/master/data/org.gnome.TwentyFortyEight.appdata.xml.in)

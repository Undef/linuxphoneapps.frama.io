+++
title = "Gajim"
description = "Fully-featured XMPP chat client"
aliases = []
date = 2020-10-21
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC-BY-SA-3.0",]
app_author = [ "Gajim Team",]
categories = [ "chat",]
mobile_compatibility = [ "2",]
status = [ "mature",]
frameworks = [ "GTK3",]
backends = []
services = [ "XMPP",]
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Chat", "InstantMessaging",]
programming_languages = [ "Python",]
build_systems = [ "setup.py",]
requires_internet = []
tags = []

[extra]
repository = "https://dev.gajim.org/gajim/gajim"
homepage = "https://gajim.org/"
bugtracker = "https://dev.gajim.org/gajim/gajim"
donations = "https://gajim.org/development/#donations"
translations = "https://dev.gajim.org/gajim/gajim/-/wikis/development/devtranslate"
more_information = []
summary_source_url = "https://dev.gajim.org/gajim/gajim/-/raw/master/data/org.gajim.Gajim.appdata.xml.in"
screenshots = [ "https://gajim.org/img/screenshots/groupchat-window.png", "https://gajim.org/img/screenshots/history-window.png", "https://gajim.org/img/screenshots/plugins.png", "https://gajim.org/img/screenshots/single-window-mode.png", "https://gajim.org/img/screenshots/tabbed-chat.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/org.gajim.gajim/1.png",]
all_features_touch = false
intended_for_mobile = false
app_id = "org.gajim.Gajim"
scale_to_fit = "org.gajim.Gajim"
flathub = "https://flathub.org/apps/org.gajim.Gajim"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gajim",]
appstream_xml_url = "https://dev.gajim.org/gajim/gajim/-/raw/master/data/org.gajim.Gajim.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Gajim aims to be an easy to use and fully-featured XMPP client.
 Just chat with your friends or family, easily share pictures and 
 thoughts or discuss the news with your groups.


Chat securely with End-to-End encryption via OMEMO or OpenPGP.


Gajim integrates well with your other devices: simply continue conversations on your mobile device.


Features:


* Never miss a message, keep all your chat clients synchronized
* Invite friends to group chats or join one
* Easily send pictures, videos or other files to friends and groups
* Chat securely with End-to-End encryption via OMEMO or OpenPGP
* Use your favorite emoticons, set your own profile picture
* Keep and manage all your chat history
* Organize your chats with workspaces
* Automatic spell-checking for your messages
* Connect to other Messengers via Transports (Facebook, IRC, ...)
* Lookup things on Wikipedia, dictionaries or other search engines directly from the chat window
* Set your activity or tune to show your friends how you are feeling
* Support for multiple accounts
* XML console to see what's happening on the protocol layer
* Support for service discovery including nodes and search for users
* Even more features via plugins

[Source](https://dev.gajim.org/gajim/gajim/-/raw/master/data/org.gajim.Gajim.appdata.xml.in)

### Notice

Used to be great after scale-to-fit pre Gajim 1.4.0 and single window mode, now it’s ... just check the screenshot (with scale-to-fit enabled) below.

+++
title = "Sublime Music"
description = "Native Subsonic client for Linux"
aliases = []
date = 2020-10-15
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "FSFAP",]
app_author = [ "sublime-music",]
categories = [ "audio streaming",]
mobile_compatibility = [ "3",]
status = [ "mature",]
frameworks = [ "GTK3",]
backends = []
services = [ "Subsonic", "ampache", "Revel", "Airsonic", "Gonic", "Navidrome",]
packaged_in = [ "alpine_edge", "aur", "debian_12", "debian_13", "debian_unstable", "devuan_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Audio", "Player",]
programming_languages = [ "Python",]
build_systems = [ "poetry",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/sublime-music/sublime-music"
homepage = "https://sublimemusic.app"
bugtracker = "https://github.com/sublime-music/sublime-music/issues"
donations = ""
translations = ""
more_information = [ "https://docs.sublimemusic.app/", "https://github.com/sublime-music/sublime-music/issues/97#issuecomment-1400643486",]
summary_source_url = "https://raw.githubusercontent.com/sublime-music/sublime-music/master/sublime-music.metainfo.xml"
screenshots = [ "https://docs.sublimemusic.app/_images/albums.png", "https://docs.sublimemusic.app/_images/artists.png", "https://docs.sublimemusic.app/_images/browse.png", "https://docs.sublimemusic.app/_images/chromecasts.png", "https://docs.sublimemusic.app/_images/downloads.png", "https://docs.sublimemusic.app/_images/offline-mode.png", "https://docs.sublimemusic.app/_images/play-queue.png", "https://docs.sublimemusic.app/_images/playlists.png", "https://docs.sublimemusic.app/_images/search.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "app.sublimemusic.SublimeMusic"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "sublime-music",]
appstream_xml_url = "https://raw.githubusercontent.com/sublime-music/sublime-music/master/sublime-music.metainfo.xml"
reported_by = "ula"
updated_by = "script"

+++


### Description

Sublime Music is a GTK3
 Subsonic/Airsonic/Revel/Gonic/Navidrome/\*sonic client for the Linux
 Desktop.


Features:


* Switch between multiple Subsonic-API-compliant servers.
* Play music through Chromecast devices on the same LAN.
* Offline Mode where Sublime Music will not make any network
 requests.
* DBus MPRIS interface integration for controlling Sublime Music
 via DBus MPRIS clients such as playerctl, i3status-rust, KDE
 Connect, and many commonly used desktop environments.
* Browse songs by the sever-reported filesystem structure, or
 view them organized by ID3 tags in the Albums, Artists, and
 Playlists views.
* Intuitive play queue.
* Create/delete/edit playlists.
* Cache songs for offline listening.

[Source](https://raw.githubusercontent.com/sublime-music/sublime-music/master/sublime-music.metainfo.xml)

### Notice

Scale-to-fit required and switch from portrait to landscape display to access certain menus. Works but interface not designed for a small screen. There is no .desktop, you have to run the command or create the .desktop. This [issue comment](https://github.com/sublime-music/sublime-music/issues/97#issuecomment-1400643486) explains how to use a WIP libhandy branch.

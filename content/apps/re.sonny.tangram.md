+++
title = "Tangram"
description = "Browser for your pinned tabs"
aliases = []
date = 2021-06-06
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Sonny Piers",]
categories = [ "web browser",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "webkit2gtk",]
services = []
packaged_in = [ "alpine_edge", "aur", "flathub", "gentoo", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "WebBrowser",]
programming_languages = [ "JavaScript",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/sonnyp/Tangram"
homepage = "https://tangram.sonny.re"
bugtracker = "https://github.com/sonnyp/Tangram/issues"
donations = "https://ko-fi.com/sonnyp"
translations = "https://hosted.weblate.org/engage/tangram/"
more_information = [ "https://apps.gnome.org/app/re.sonny.Tangram/",]
summary_source_url = "https://raw.githubusercontent.com/sonnyp/Tangram/main/data/appdata/re.sonny.Tangram.metainfo.xml"
screenshots = [ "https://raw.githubusercontent.com/sonnyp/Tangram/main/data/appdata/desktop.png", "https://raw.githubusercontent.com/sonnyp/Tangram/main/data/appdata/mobile.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/re.sonny.tangram/1.png", "https://img.linuxphoneapps.org/re.sonny.tangram/2.png", "https://img.linuxphoneapps.org/re.sonny.tangram/3.png", "https://img.linuxphoneapps.org/re.sonny.tangram/4.png", "https://img.linuxphoneapps.org/re.sonny.tangram/5.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "re.sonny.Tangram"
scale_to_fit = "re.sonny.Tangram"
flathub = "https://flathub.org/apps/re.sonny.Tangram"
flatpak_link = ""
flatpak_recipe = "https://raw.githubusercontent.com/sonnyp/Tangram/main/re.sonny.Tangram.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "tangram",]
appstream_xml_url = "https://raw.githubusercontent.com/sonnyp/Tangram/main/data/appdata/re.sonny.Tangram.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Tangram is a new kind of browser. It is designed to organize and run your Web applications.
 Each tab is persistent and independent. You can set multiple tabs with different accounts for the same application.


Common use cases:


* Stay up to date with your favorite communities; Mastodon, Twitter, ...
* Merge all these chat applications into one; WhatsApp, Messenger, Telegram, ...
* Group your organization tools under one application; EMail, Calendar, ...
* One-stop for multiple sources of documentation or information

[Source](https://raw.githubusercontent.com/sonnyp/Tangram/main/data/appdata/re.sonny.Tangram.metainfo.xml)

### Notice

Perfect on phones since release 3.0. Used to be GTK3 before release 2.0.

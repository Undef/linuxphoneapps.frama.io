+++
title = "Keepassk"
description = "Password Manager"
aliases = []
date = 2021-05-04
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community",]
categories = [ "password manager",]
mobile_compatibility = [ "5",]
status = [ "early", "inactive", "pre-release",]
frameworks = [ "QtQuick", "Kirigami",]
backends = [ "KeePass",]
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Cpp", "QML", "Rust",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/tfella/keepassk"
homepage = ""
bugtracker = "https://invent.kde.org/tfella/keepassk/-/issues/"
donations = ""
translations = ""
more_information = [ "https://plasma-mobile.org/2021/04/27/plasma-mobile-update-march-april/",]
summary_source_url = "https://invent.kde.org/tfella/keepassk/-/raw/master/org.kde.keepassk.appdata.xml"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.keepassk"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "keepassk",]
appstream_xml_url = "https://invent.kde.org/tfella/keepassk/-/raw/master/org.kde.keepassk.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Keepassk is a password manager for KDBX-based password databases

[Source](https://invent.kde.org/tfella/keepassk/-/raw/master/org.kde.keepassk.appdata.xml)

### Notice

WIP, read only, only for password-only encrypted databases currently.

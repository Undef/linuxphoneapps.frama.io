+++
title = "OptiImage"
description = "Image optimizer to reduce the size of images"
aliases = []
date = 2021-04-29
updated = 2024-01-02

[taxonomies]
project_licenses = [ "LGPL-2.1-only", "LGPL-3.0-only", "LicenseRef-KDE-Accepted-LGPL",]
metadata_licenses = [ "FSFAP",]
app_author = [ "Carl Schwan",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "early", "inactive",]
frameworks = [ "QtQuick", "Kirigami",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Graphics",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/carlschwan/optiimage"
homepage = "https://apps.kde.org/optiimage/"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=OptitImage"
donations = ""
translations = ""
more_information = [ "https://plasma-mobile.org/2021/04/27/plasma-mobile-update-march-april/",]
summary_source_url = "https://invent.kde.org/carlschwan/optiimage/-/raw/master/org.kde.optiimage.appdata.xml"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.optiimage"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://invent.kde.org/carlschwan/optiimage/-/raw/master/.flatpak-manifest.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://invent.kde.org/carlschwan/optiimage/-/raw/master/org.kde.optiimage.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

OptiImage is an image optimizer. It allows you to optimize the optimize the spaced used by an image by removing metadata and improve the compression.


It uses the following tools:


* optipng
* jpegoptim

[Source](https://invent.kde.org/carlschwan/optiimage/-/raw/master/org.kde.optiimage.appdata.xml)
+++
title = "Banji"
description = "Call and receive calls with SIP or RING protocols"
aliases = []
date = 2020-02-16
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community",]
categories = [ "chat", "telephony",]
mobile_compatibility = [ "needs testing",]
status = [ "archived",]
frameworks = [ "QtQuick", "Kirigami",]
backends = []
services = [ "Jami",]
packaged_in = [ "aur",]
freedesktop_categories = [ "Qt", "KDE", "Network", "Telephony", "Chat",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/network/ring-kde"
homepage = "https://www.kde.org"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?format=guided&product=ring-kde"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/network/ring-kde/-/raw/master/data/org.kde.ring-kde.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/ring-kde/ring-kde.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.ring-kde.desktop"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "ring-kde",]
appstream_xml_url = "https://invent.kde.org/network/ring-kde/-/raw/master/data/org.kde.ring-kde.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

Banji is a graphical client for the Ring (www.jami.net) communication
 platform. It can either be used as a general purpose communication
 application with other Ring users or as a VoiP softphone compatible with
 the SIP industry standard used by most office phones.


Features:


* Calling real phone with accounts from common SIP service providers
* Support SIP, SIPS and the RING distributed communication protocol
* Audio, Video and rich text messages
* Compliant with various industry standards such as:
* Support multiple popular audio and video codecs
* SIP call transfer (both direct and attended)
* Audio and video conferences
* Multiple concurrent calls with "on hold" feature
* Multiple accounts
* Multiple users profiles
* System contact integration
* Configurable interactive system notification
* Configurable local and global keyboard shortcuts
* Different ringtones per account
* Call menu support automation with macro
* Call audio recordings
* Call history and bookmarks
* Auto answer (per account)
* Pulseaudio, portaudio, core audio, ALSA and windows audio integration
* Different audio device for ringing and talk
* Smart phone number auto completion

[Source](https://invent.kde.org/network/ring-kde/-/raw/master/data/org.kde.ring-kde.appdata.xml)

+++
title = "Endeavour"
description = "Manage your tasks"
aliases = []
date = 2022-10-04
updated = 2023-12-31

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Jamie Murphy",]
categories = [ "productivity",]
mobile_compatibility = [ "4",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Office",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/World/Endeavour"
homepage = "https://wiki.gnome.org/Apps/Todo"
bugtracker = "https://gitlab.gnome.org/World/Endeavour/issues"
donations = ""
translations = ""
more_information = [ "https://feaneron.com/2022/06/21/giving-up-on-gnome-to-do/",]
summary_source_url = "https://gitlab.gnome.org/World/Endeavour/-/raw/main/data/appdata/org.gnome.Todo.appdata.xml.in.in"
screenshots = [ "https://gitlab.gnome.org/World/Endeavour/raw/main/data/appdata/no-tasks.png", "https://gitlab.gnome.org/World/Endeavour/raw/main/data/appdata/task-list.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/org.gnome.todo/1.png", "https://img.linuxphoneapps.org/org.gnome.todo/2.png", "https://img.linuxphoneapps.org/org.gnome.todo/3.png", "https://img.linuxphoneapps.org/org.gnome.todo/4.png",]
all_features_touch = false
intended_for_mobile = false
app_id = "org.gnome.Todo"
scale_to_fit = "org.gnome.todo"
flathub = "https://flathub.org/apps/org.gnome.Todo"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-todo", "endeavour",]
appstream_xml_url = "https://gitlab.gnome.org/World/Endeavour/-/raw/main/data/appdata/org.gnome.Todo.appdata.xml.in.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description
Endeavour is a task management application designed for simplicity. Save and order your todos.
 Manage multiple todo lists. And more

[Source](https://gitlab.gnome.org/World/Endeavour/-/raw/main/data/appdata/org.gnome.Todo.appdata.xml.in.in)

### Notice
Previously known as GNOME To Do. Endeavour upstream is not yet adaptive, but Mobian has been shipping an [adaptive build](https://salsa.debian.org/Mobian-team/packages/gnome-todo) for a long time, with a [patchset](https://salsa.debian.org/Mobian-team/packages/gnome-todo/-/tree/mobian/debian/patches) maintained up to 41 release.

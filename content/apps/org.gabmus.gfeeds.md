+++
title = "Feeds"
description = "News reader for GNOME"
aliases = []
date = 2020-08-24
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Gabriele Musco",]
categories = [ "feed reader",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "RSS",]
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Feed", "News",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/World/gfeeds"
homepage = "https://gfeeds.gabmus.org"
bugtracker = "https://gitlab.gnome.org/World/gfeeds/-/issues"
donations = "https://liberapay.com/gabmus/donate"
translations = "https://gitlab.gnome.org/World/gfeeds/-/tree/master/po"
more_information = []
summary_source_url = "https://gitlab.gnome.org/World/gfeeds/-/raw/master/data/org.gabmus.gfeeds.appdata.xml.in"
screenshots = [ "https://gitlab.gnome.org/World/gfeeds/raw/website/static/screenshots/add_feed.png", "https://gitlab.gnome.org/World/gfeeds/raw/website/static/screenshots/filter.png", "https://gitlab.gnome.org/World/gfeeds/raw/website/static/screenshots/import_export.png", "https://gitlab.gnome.org/World/gfeeds/raw/website/static/screenshots/light_dark.png", "https://gitlab.gnome.org/World/gfeeds/raw/website/static/screenshots/mainwindow.png", "https://gitlab.gnome.org/World/gfeeds/raw/website/static/screenshots/preferences.png", "https://gitlab.gnome.org/World/gfeeds/raw/website/static/screenshots/reader.png", "https://gitlab.gnome.org/World/gfeeds/raw/website/static/screenshots/rss_content.png", "https://gitlab.gnome.org/World/gfeeds/raw/website/static/screenshots/webview.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.gabmus.gfeeds"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gabmus.gfeeds"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-feeds",]
appstream_xml_url = "https://gitlab.gnome.org/World/gfeeds/-/raw/master/data/org.gabmus.gfeeds.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Feeds is a minimal RSS/Atom feed reader built with speed and simplicity in mind.


It offers a simple user interface that only shows the latest news from your subscriptions.


Articles are shown in a web view by default, with javascript disabled for a faster and less intrusive user experience. There's also a reader mode included, built from the one GNOME Web/Epiphany uses.


Feeds can be imported and exported via OPML.

[Source](https://gitlab.gnome.org/World/gfeeds/-/raw/master/data/org.gabmus.gfeeds.appdata.xml.in)

### Notice

GTK3/libhandy before 1.0

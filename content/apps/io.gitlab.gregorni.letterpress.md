+++
title = "Letterpress"
description = "Create beautiful ASCII art"
aliases = []
date = 2023-09-24
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Gregor Niehl",]
categories = [ "ASCII art generator",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "jp2a",]
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "GNOME", "GTK", "Graphics", "ImageProcessing", "TextTools", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "Meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/World/Letterpress"
homepage = "https://gitlab.gnome.org/World/Letterpress"
bugtracker = "https://gitlab.gnome.org/World/Letterpress/-/issues"
donations = ""
translations = "https://hosted.weblate.org/engage/letterpress/"
more_information = []
summary_source_url = "https://gitlab.gnome.org/World/Letterpress/-/raw/main/data/io.gitlab.gregorni.Letterpress.appdata.xml.in"
screenshots = [ "https://gitlab.gnome.org/World/Letterpress/-/raw/main/data/screenshots/copied.png", "https://gitlab.gnome.org/World/Letterpress/-/raw/main/data/screenshots/overview.png", "https://gitlab.gnome.org/World/Letterpress/-/raw/main/data/screenshots/saved.png", "https://gitlab.gnome.org/World/Letterpress/-/raw/main/data/screenshots/width.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/io.gitlab.gregorni.letterpress/1.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "io.gitlab.gregorni.Letterpress"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.gitlab.gregorni.Letterpress"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://gitlab.gnome.org/World/Letterpress/-/raw/main/data/io.gitlab.gregorni.Letterpress.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Letterpress converts your images into a picture made up of ASCII characters.
 You can save the output to a file, copy it, and even change its resolution!
 High-res output can still be viewed comfortably by lowering the zoom factor.

[Source](https://gitlab.gnome.org/World/Letterpress/-/raw/main/data/io.gitlab.gregorni.Letterpress.appdata.xml.in)
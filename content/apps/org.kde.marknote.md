+++
title = "MarkNote"
description = "Note taking application"
aliases = []
date = 2024-01-14

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community",]
categories = [ "note taking",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "Kirigami", "QtQuick",]
backends = []
services = []
packaged_in = [ "snapcraft",]
freedesktop_categories = [ "Qt", "KDE", "Office",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/office/marknote"
homepage = ""
bugtracker = ""
donations = ""
translations = ""
more_information = [ "https://apps.kde.org/marknote/",]
summary_source_url = "https://invent.kde.org/office/marknote/-/raw/master/org.kde.marknote.metainfo.xml"
screenshots = [ "https://i.imgur.com/9DsuKFP.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.marknote"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://invent.kde.org/office/marknote/-/raw/master/.flatpak-manifest.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://invent.kde.org/office/marknote/-/raw/master/org.kde.marknote.metainfo.xml"
reported_by = "linmob"
updated_by = ""

+++


### Description

A WYSIWYG note taking app that lets you organize your notes in notebooks.

[Source](https://invent.kde.org/office/marknote/-/raw/master/org.kde.marknote.metainfo.xml)

### Notice

Pre-release, not packaged for any mobile distribution yet.

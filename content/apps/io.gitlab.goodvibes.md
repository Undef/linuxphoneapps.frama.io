+++
title = "Goodvibes"
description = "Play web radios"
aliases = []
date = 2020-11-07
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Arnaud Rebillout",]
categories = [ "internet radio",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK3",]
backends = []
services = [ "internet radio",]
packaged_in = [ "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Audio", "Player",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = [ "requires always",]
tags = []

[extra]
repository = "https://gitlab.com/goodvibes/goodvibes"
homepage = "https://gitlab.com/goodvibes/goodvibes"
bugtracker = "https://gitlab.com/goodvibes/goodvibes/issues"
donations = "https://liberapay.com/arnaudr/"
translations = "https://hosted.weblate.org/projects/goodvibes"
more_information = []
summary_source_url = "https://gitlab.com/goodvibes/goodvibes/-/raw/master/data/io.gitlab.Goodvibes.appdata.xml.in"
screenshots = [ "https://goodvibes.gitlab.io/appdata/screenshot-dark.png", "https://goodvibes.gitlab.io/appdata/screenshot-light.png", "https://goodvibes.gitlab.io/appdata/screenshot-preferences.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "io.gitlab.Goodvibes"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.gitlab.Goodvibes"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "goodvibes",]
appstream_xml_url = "https://gitlab.com/goodvibes/goodvibes/-/raw/master/data/io.gitlab.Goodvibes.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Goodvibes is a simple internet radio player for GNU/Linux.


It comes with every basic features you can expect from an audio player,
 such as multimedia keys, notifications, system sleep inhibition, and
 MPRIS2 support.

[Source](https://gitlab.com/goodvibes/goodvibes/-/raw/master/data/io.gitlab.Goodvibes.appdata.xml.in)

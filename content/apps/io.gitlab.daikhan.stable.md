+++
title = "Daikhan (Early Access)"
description = "Play Videos/Music with style"
aliases = []
date = 2024-02-12

[taxonomies]
project_licenses = [ "AGPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Mazhar Hussain",]
categories = [ "video player",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "gstreamer", "xxhash",]
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "AudioVideo", "Player",]
programming_languages = [ "Vala",]
build_systems = [ "Meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/daikhan/daikhan"
homepage = "https://gitlab.com/daikhan/daikhan"
bugtracker = "https://gitlab.com/daikhan/daikhan/-/issues"
donations = "https://realmazharhussain.github.io/donate"
translations = "https://hosted.weblate.org/projects/daikhan"
more_information = []
summary_source_url = "https://flathub.org/apps/io.gitlab.daikhan.stable"
screenshots = [ "https://gitlab.com/daikhan/screenshots/-/raw/eaedbec/dark-fullscreen.png", "https://gitlab.com/daikhan/screenshots/-/raw/eaedbec/dark.png", "https://gitlab.com/daikhan/screenshots/-/raw/eaedbec/light-fullscreen.png", "https://gitlab.com/daikhan/screenshots/-/raw/eaedbec/light.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = false
app_id = "io.gitlab.daikhan.stable"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.gitlab.daikhan.stable"
flatpak_link = "https://flathub.org/apps/io.gitlab.daikhan.stable.flatpakref"
flatpak_recipe = "https://gitlab.com/daikhan/daikhan/-/raw/main/build-aux/flatpak/io.gitlab.daikhan.stable.yml"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://gitlab.com/daikhan/daikhan/-/raw/main/data/daikhan.metainfo.xml.in"
reported_by = "linmob"
updated_by = ""

+++


### Description

A simple media player with beautiful interface.


NOTE: The app is in early stages of development and bound to change significantly
 in the future.

[Source](https://gitlab.com/daikhan/daikhan/-/raw/main/data/daikhan.metainfo.xml.in)

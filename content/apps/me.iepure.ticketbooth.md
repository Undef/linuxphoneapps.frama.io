+++
title = "Ticket Booth"
description = "Keep track of your favorite shows"
aliases = []
date = 2024-02-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Alessandro Iepure",]
categories = [ "utilities",]
mobile_compatibility = [ "4",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "TMDB",]
packaged_in = [ "aur", "debian_13", "debian_unstable", "devuan_unstable", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "Meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/aleiepure/ticketbooth"
homepage = "https://github.com/aleiepure/ticketbooth"
bugtracker = "https://github.com/aleiepure/ticketbooth/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/me.iepure.Ticketbooth"
screenshots = [ "https://raw.githubusercontent.com/aleiepure/ticketbooth/main/data/appstream/1.png", "https://raw.githubusercontent.com/aleiepure/ticketbooth/main/data/appstream/2.png", "https://raw.githubusercontent.com/aleiepure/ticketbooth/main/data/appstream/3.png", "https://raw.githubusercontent.com/aleiepure/ticketbooth/main/data/appstream/4.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "me.iepure.Ticketbooth"
scale_to_fit = ""
flathub = "https://flathub.org/apps/me.iepure.Ticketbooth"
flatpak_link = "https://flathub.org/apps/me.iepure.Ticketbooth.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/aleiepure/ticketbooth/main/me.iepure.Ticketbooth.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "ticketbooth",]
appstream_xml_url = "https://raw.githubusercontent.com/aleiepure/ticketbooth/main/data/me.iepure.Ticketbooth.metainfo.xml.in"
reported_by = "linmob"
updated_by = ""

+++

### Description

Ticket Booth allows you to build your watchlist of movies and TV Shows, keep track of
 watched titles, and find information about the latest releases.


Ticket Booth does not allow you to watch or download content. This app uses the TMDB API
 but is not endorsed or certified by TMDB.

[Source](https://raw.githubusercontent.com/aleiepure/ticketbooth/main/data/me.iepure.Ticketbooth.metainfo.xml.in)

### Notice

Just a few pixels too wide.

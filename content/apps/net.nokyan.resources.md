+++
title = "Resources"
description = "Monitor your system resources and processes"
aliases = []
date = 2023-11-25
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "nokyan",]
categories = [ "system utilities",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "System", "Monitor",]
programming_languages = [ "Rust",]
build_systems = [ "Meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/nokyan/resources"
homepage = "https://github.com/nokyan/resources"
bugtracker = "https://github.com/nokyan/resources/issues"
donations = ""
translations = "https://github.com/nokyan/resources/tree/main/po"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/nokyan/resources/main/data/net.nokyan.Resources.metainfo.xml.in.in"
screenshots = [ "https://raw.githubusercontent.com/nokyan/resources/main/data/resources/screenshots/1.png", "https://raw.githubusercontent.com/nokyan/resources/main/data/resources/screenshots/2.png", "https://raw.githubusercontent.com/nokyan/resources/main/data/resources/screenshots/3.png", "https://raw.githubusercontent.com/nokyan/resources/main/data/resources/screenshots/4.png", "https://raw.githubusercontent.com/nokyan/resources/main/data/resources/screenshots/5.png", "https://raw.githubusercontent.com/nokyan/resources/main/data/resources/screenshots/6.png", "https://raw.githubusercontent.com/nokyan/resources/main/data/resources/screenshots/7.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "net.nokyan.Resources"
scale_to_fit = ""
flathub = "https://flathub.org/apps/net.nokyan.Resources"
flatpak_link = ""
flatpak_recipe = "https://raw.githubusercontent.com/nokyan/resources/main/build-aux/net.nokyan.Resources.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "resources",]
appstream_xml_url = "https://raw.githubusercontent.com/nokyan/resources/main/data/net.nokyan.Resources.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Resources allows you to check the utilization of your system resources and control your running processes and applications. It’s designed to be user-friendly and feel right at home on a modern desktop by using GNOME’s libadwaita.


Resources supports monitoring the following components:


* CPU
* Memory
* GPU
* Network Interfaces
* Storage Devices

[Source](https://raw.githubusercontent.com/nokyan/resources/main/data/net.nokyan.Resources.metainfo.xml.in.in)

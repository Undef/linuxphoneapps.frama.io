+++
title = "Notes"
description = "Notes for GNOME"
aliases = []
date = 2019-02-01
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later", "LGPL-2.0-or-later", "LGPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "GNOME Developers",]
categories = [ "note taking",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = [ "gnome-settings-daemon",]
services = []
packaged_in = [ "arch", "archlinuxarm_aarch64", "aur", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "TextTools",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnome-notes"
homepage = "https://wiki.gnome.org/Apps/Notes"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-notes/issues"
donations = "http://www.gnome.org/friends/"
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/GNOME/gnome-notes/-/raw/master/data/appdata/org.gnome.Notes.appdata.xml.in"
screenshots = [ "https://gitlab.gnome.org/GNOME/gnome-notes/raw/master/data/appdata/default.png", "https://gitlab.gnome.org/GNOME/gnome-notes/raw/master/data/appdata/edit.png", "https://gitlab.gnome.org/GNOME/gnome-notes/raw/master/data/appdata/list.png", "https://gitlab.gnome.org/GNOME/gnome-notes/raw/master/data/appdata/select.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.gnome.Notes.desktop"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Notes"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-notes",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-notes/-/raw/master/data/appdata/org.gnome.Notes.appdata.xml.in"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

A quick and easy way to make freeform notes or jot down simple lists. Store as many notes as you like and share them by email.


You can store your notes locally on your computer or sync with online services like ownCloud.

[Source](https://gitlab.gnome.org/GNOME/gnome-notes/-/raw/master/data/appdata/org.gnome.Notes.appdata.xml.in)

+++
title = "Merkuro Contact"
description = "Manage your contacts with speed and ease"
aliases = []
date = 2023-11-25
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Clau Cambra and Carl Schwan", "KDE Community",]
categories = [ "contacts",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "Kirigami",]
backends = [ "Akonadi",]
services = []
packaged_in = [ "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "fedora_38", "fedora_39", "fedora_rawhide", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "KDE", "Qt", "Office", "ContactManagement",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/pim/merkuro/-/tree/master/src/contacts"
homepage = "https://apps.kde.org/merkuro.contact/"
bugtracker = "https://invent.kde.org/pim/merkuro/-/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/pim/merkuro/-/raw/master/src/contacts/org.kde.merkuro.contact.metainfo.xml"
screenshots = [ "https://cdn.kde.org/screenshots/merkuro/contact.png", "https://cdn.kde.org/screenshots/merkuro/contact_editor.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.kde.merkuro.contact"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "merkuro",]
appstream_xml_url = "https://invent.kde.org/pim/merkuro/-/raw/master/src/contacts/org.kde.merkuro.contact.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Merkuro contact book application.

[Source](https://invent.kde.org/pim/merkuro/-/raw/master/src/contacts/org.kde.merkuro.contact.metainfo.xml)

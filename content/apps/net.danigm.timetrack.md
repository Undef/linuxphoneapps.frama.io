+++
title = "Timetrack"
description = "A timetrack for GNOME"
aliases = []
date = 2020-08-25
updated = 2023-12-31

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Daniel García Moreno",]
categories = [ "productivity",]
mobile_compatibility = [ "3",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/danigm/timetrack"
homepage = "https://gitlab.gnome.org/danigm/timetrack"
bugtracker = "https://gitlab.gnome.org/danigm/timetrack/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/danigm/timetrack/-/raw/master/data/net.danigm.timetrack.appdata.xml.in.in"
screenshots = [ "https://gitlab.gnome.org/danigm/timetrack/raw/master/screenshots/timetrack1.png", "https://gitlab.gnome.org/danigm/timetrack/raw/master/screenshots/timetrack2.png", "https://gitlab.gnome.org/danigm/timetrack/raw/master/screenshots/timetrack3.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "net.danigm.timetrack"
scale_to_fit = "net.danigm.timetrack"
flathub = "https://flathub.org/apps/net.danigm.timetrack"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "timetrack",]
appstream_xml_url = "https://gitlab.gnome.org/danigm/timetrack/-/raw/master/data/net.danigm.timetrack.appdata.xml.in.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description
Timetrack is a simple time track application for GNOME

[Source](https://gitlab.gnome.org/danigm/timetrack/-/raw/master/data/net.danigm.timetrack.appdata.xml.in.in)

### Notice
GTK3/libhandy before 2.0.0.

+++
title = "eOVPN"
description = "OpenVPN Configuration Manager"
aliases = []
date = 2021-02-20
updated = 2023-12-31

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "FSFAP",]
app_author = [ "Jagadeesh Kotra",]
categories = [ "system utilities",]
mobile_compatibility = [ "4",]
status = [ "mature",]
frameworks = [ "GTK3",]
backends = []
services = [ "OpenVPN",]
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Network",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/jkotra/eOVPN"
homepage = "https://github.com/jkotra/eOVPN"
bugtracker = "https://github.com/jkotra/eOVPN/issues"
donations = "https://ko-fi.com/jkotra"
translations = ""
more_information = []
summary_source_url = "https://github.com/jkotra/eOVPN"
screenshots = [ "https://raw.githubusercontent.com/jkotra/eOVPN/master/static/window.png", "https://raw.githubusercontent.com/jkotra/eOVPN/master/static/window_connected.png", "https://raw.githubusercontent.com/jkotra/eOVPN/master/static/window_connected_with_flag.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "com.github.jkotra.eovpn"
scale_to_fit = "eovpn"
flathub = "https://flathub.org/apps/com.github.jkotra.eovpn"
flatpak_link = ""
flatpak_recipe = "https://raw.githubusercontent.com/jkotra/eOVPN/master/dist/flatpak/com.github.jkotra.eovpn.yml"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "eovpn",]
appstream_xml_url = "https://raw.githubusercontent.com/jkotra/eOVPN/master/data/com.github.jkotra.eovpn.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description
eOVPN is a application to connect, manage and update(from a remote .zip) OpenVPN configurations.

[Source](https://raw.githubusercontent.com/jkotra/eOVPN/master/data/com.github.jkotra.eovpn.metainfo.xml)

### Notice
scale-to-fit helps with properly scaling the settings menu to make it usable.

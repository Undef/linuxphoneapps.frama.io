+++
title = "Telegraph"
description = "Write and decode morse"
aliases = []
date = 2023-04-16
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Felipe Kinoshita",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "TextTools",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/fkinoshita/Telegraph/"
homepage = "https://github.com/fkinoshita/Telegraph"
bugtracker = "https://github.com/fkinoshita/Telegraph/issues"
donations = "https://ko-fi.com/fkinoshita"
translations = "https://github.com/fkinoshita/Telegraph/tree/main/po"
more_information = []
summary_source_url = ""
screenshots = [ "https://raw.githubusercontent.com/fkinoshita/Telegraph/main/data/screenshots/dark.png", "https://raw.githubusercontent.com/fkinoshita/Telegraph/main/data/screenshots/telegraph.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/io.github.fkinoshita.telegraph/1.png", "https://img.linuxphoneapps.org/io.github.fkinoshita.telegraph/2.png", "https://img.linuxphoneapps.org/io.github.fkinoshita.telegraph/3.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "io.github.fkinoshita.Telegraph"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.fkinoshita.Telegraph"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "telegraph",]
appstream_xml_url = "https://raw.githubusercontent.com/fkinoshita/Telegraph/main/data/io.github.fkinoshita.Telegraph.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Telegraph is a simple Morse translator, start typing your message to see
 the resulting Morse code and vice versa.

[Source](https://raw.githubusercontent.com/fkinoshita/Telegraph/main/data/io.github.fkinoshita.Telegraph.metainfo.xml.in.in)

+++
title = "Confy"
description = "Conference schedules viewer"
aliases = []
date = 2020-09-28
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC-BY-4.0",]
app_author = [ "Fabio Comuni",]
categories = [ "conference companion",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_19", "alpine_edge", "aur", "debian_12", "debian_13", "debian_unstable", "devuan_unstable", "flathub", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://git.sr.ht/~fabrixxm/confy"
homepage = "https://confy.kirgroup.net"
bugtracker = "https://todo.sr.ht/~fabrixxm/confy"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://confy.kirgroup.net/"
screenshots = [ "https://confy.kirgroup.net/appdata/confy1.png", "https://confy.kirgroup.net/appdata/confy2.png", "https://confy.kirgroup.net/appdata/confy3.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "net.kirgroup.confy"
scale_to_fit = ""
flathub = "https://flathub.org/apps/net.kirgroup.confy"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "confy",]
appstream_xml_url = "https://git.sr.ht/~fabrixxm/confy/blob/master/data/net.kirgroup.confy.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Confy lets you browse conference schedules.


Browse talks by day, track or room.
 Select the talks you are interested in and receive notification when they are about to start.
 View when two or more talks you are interested in overlap.

[Source](https://git.sr.ht/~fabrixxm/confy/blob/master/data/net.kirgroup.confy.metainfo.xml.in)

### Notice

GTK3/libhandy before 0.7.0.

+++
title = "Drawing"
description = "Edit screenshots or memes"
aliases = []
date = 2019-02-16
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Romain F. T.",]
categories = [ "drawing",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Graphics",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/maoschanz/drawing/"
homepage = "https://maoschanz.github.io/drawing"
bugtracker = "https://github.com/maoschanz/drawing/issues"
donations = "https://paypal.me/maoschannz"
translations = "https://github.com/maoschanz/drawing/blob/master/CONTRIBUTING.md#translating"
more_information = [ "https://apps.gnome.org/app/com.github.maoschanz.drawing/",]
summary_source_url = "https://raw.githubusercontent.com/maoschanz/drawing/master/data/com.github.maoschanz.drawing.appdata.xml.in"
screenshots = [ "https://raw.githubusercontent.com/maoschanz/drawing/master/docs/screenshots/1.0/gnome_menu.png", "https://raw.githubusercontent.com/maoschanz/drawing/master/docs/screenshots/1.0/gnome_new.png", "https://raw.githubusercontent.com/maoschanz/drawing/master/docs/screenshots/1.0/gnome_selection.png", "https://raw.githubusercontent.com/maoschanz/drawing/master/docs/screenshots/1.0/gnome_tools_preview.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "com.github.maoschanz.drawing"
scale_to_fit = "com.github.maoschanz.drawing"
flathub = "https://flathub.org/apps/com.github.maoschanz.drawing"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "drawing",]
appstream_xml_url = "https://raw.githubusercontent.com/maoschanz/drawing/master/data/com.github.maoschanz.drawing.appdata.xml.in"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

This basic image editor can resize, crop, or rotate an image, apply simple
 filters, insert or censor text, and manipulate a selected portion of the
 picture (cut/copy/paste/drag/…)


And of course, you can draw! Using tools such as the pencil, the straight
 line, the curve tool, many shapes, several brushes, and their various
 colors and options.


Supported file types include PNG, JPEG and BMP.

[Source](https://raw.githubusercontent.com/maoschanz/drawing/master/data/com.github.maoschanz.drawing.appdata.xml.in)

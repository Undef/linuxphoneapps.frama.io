+++
title = "Door Knocker"
description = "Check the availability of portals"
aliases = []
date = 2024-02-11

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "tytan652",]
categories = [ "system utilities",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita"]
backends = []
services = []
packaged_in = [ "aur", "flathub", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "C",]
build_systems = [ "Meson",]
requires_internet = []
tags = []

[extra]
repository = "https://codeberg.org/tytan652/door-knocker"
homepage = "https://codeberg.org/tytan652/door-knocker"
bugtracker = "https://codeberg.org/tytan652/door-knocker/issues"
donations = "https://linksta.cc/@tytan652"
translations = "https://translate.codeberg.org/engage/door-knocker/"
more_information = []
summary_source_url = "https://flathub.org/apps/xyz.tytanium.DoorKnocker"
screenshots = [ "https://codeberg.org/tytan652/door-knocker/raw/commit/3050e33d31c17b831a323f638c751406aa922f90/data/screenshots/door-knocker_desktop_dark.png", "https://codeberg.org/tytan652/door-knocker/raw/commit/3050e33d31c17b831a323f638c751406aa922f90/data/screenshots/door-knocker_desktop_light.png", "https://codeberg.org/tytan652/door-knocker/raw/commit/3050e33d31c17b831a323f638c751406aa922f90/data/screenshots/door-knocker_details_openned_desktop_dark.png", "https://codeberg.org/tytan652/door-knocker/raw/commit/3050e33d31c17b831a323f638c751406aa922f90/data/screenshots/door-knocker_details_openned_desktop_light.png", "https://codeberg.org/tytan652/door-knocker/raw/commit/3050e33d31c17b831a323f638c751406aa922f90/data/screenshots/door-knocker_full_desktop_dark.png", "https://codeberg.org/tytan652/door-knocker/raw/commit/3050e33d31c17b831a323f638c751406aa922f90/data/screenshots/door-knocker_full_desktop_light.png", "https://codeberg.org/tytan652/door-knocker/raw/commit/3050e33d31c17b831a323f638c751406aa922f90/data/screenshots/door-knocker_pinephone_xdg-desktop-portal-wlr_only.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "xyz.tytanium.DoorKnocker"
scale_to_fit = ""
flathub = "https://flathub.org/apps/xyz.tytanium.DoorKnocker"
flatpak_link = "https://dl.flathub.org/repo/appstream/xyz.tytanium.DoorKnocker.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "door-knocker",]
appstream_xml_url = "https://codeberg.org/tytan652/door-knocker/raw/branch/main/data/xyz.tytanium.DoorKnocker.appdata.xml.in.in"
reported_by = "linmob"
updated_by = ""

+++

### Description

Door Knocker allows you to check availability of all portals provided by xdg-desktop-portal.

[Source](https://codeberg.org/tytan652/door-knocker/raw/branch/main/data/xyz.tytanium.DoorKnocker.appdata.xml.in.in)

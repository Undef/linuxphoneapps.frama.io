+++
title = "Tokodon"
description = "A Mastodon client"
aliases = []
date = 2021-05-29
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The KDE Community",]
categories = [ "social media",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "Kirigami",]
backends = []
services = [ "Mastodon",]
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_13", "debian_unstable", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "Network",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/network/tokodon"
homepage = "https://apps.kde.org/tokodon/"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=Tokodon"
donations = ""
translations = ""
more_information = [ "https://carlschwan.eu/2021/12/18/more-kde-apps/", "https://plasma-mobile.org/2022/04/26/plasma-mobile-gear-22-04/#tokodon", "https://plasma-mobile.org/2022/11/30/plasma-mobile-gear-22-11/#tokodon",]
summary_source_url = "https://invent.kde.org/network/tokodon/-/raw/master/org.kde.tokodon.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/tokodon/tokodon-desktop.png", "https://cdn.kde.org/screenshots/tokodon/tokodon-home.png", "https://cdn.kde.org/screenshots/tokodon/tokodon-login.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.tokodon"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.tokodon"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "tokodon",]
appstream_xml_url = "https://invent.kde.org/network/tokodon/-/raw/master/org.kde.tokodon.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Tokodon is a Mastodon client. It allows you to interact with the Fediverse community.

[Source](https://invent.kde.org/network/tokodon/-/raw/master/org.kde.tokodon.appdata.xml)

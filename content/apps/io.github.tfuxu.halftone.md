+++
title = "Halftone"
description = "Give your images that pixel art-like style"
aliases = []
date = 2023-11-18
updated = 2024-01-14

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC-BY-SA-4.0",]
app_author = [ "tfuxu",]
categories = [ "image processing",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "imagemagick",]
services = []
packaged_in = [ "aur", "flathub", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "GNOME", "GTK", "Graphics", "Image", "ImageProcessing", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = [ "requires offline-only",]
tags = []

[extra]
repository = "https://github.com/tfuxu/Halftone"
homepage = ""
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/tfuxu/Halftone/main/data/io.github.tfuxu.Halftone.appdata.xml.in.in"
screenshots = [ "https://raw.githubusercontent.com/tfuxu/Halftone/main/data/screenshots/halftone-1.png", "https://raw.githubusercontent.com/tfuxu/Halftone/main/data/screenshots/halftone-2.png", "https://raw.githubusercontent.com/tfuxu/Halftone/main/data/screenshots/halftone-3.png", "https://raw.githubusercontent.com/tfuxu/Halftone/main/data/screenshots/halftone-4.png", "https://raw.githubusercontent.com/tfuxu/Halftone/main/data/screenshots/halftone-5.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "io.github.tfuxu.Halftone"
scale_to_fit = "io.github.tfuxu.Halftone"
flathub = "https://flathub.org/apps/details/io.github.tfuxu.Halftone"
flatpak_link = ""
flatpak_recipe = "https://raw.githubusercontent.com/tfuxu/Halftone/main/build-aux/flatpak/io.github.tfuxu.Halftone.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "halftone",]
appstream_xml_url = "https://raw.githubusercontent.com/tfuxu/Halftone/main/data/io.github.tfuxu.Halftone.appdata.xml.in.in"
reported_by = "linmob"
updated_by = "check_via_repology"

+++

### Description

A simple Libadwaita app for lossy image compression using dithering technique.

 Give your images a pixel art-like style and reduce the file size in the process with Halftone.

[Source](https://raw.githubusercontent.com/tfuxu/Halftone/main/data/io.github.tfuxu.Halftone.appdata.xml.in.in)
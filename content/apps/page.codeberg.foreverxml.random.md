+++
title = "Random"
description = "Make randomization easy"
aliases = []
date = 2022-03-26
updated = 2024-01-02

[taxonomies]
project_licenses = [ "AGPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Forever",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "archived",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://codeberg.org/foreverxml/random"
homepage = "https://random.amongtech.cc"
bugtracker = "https://codeberg.org/foreverxml/random/issues"
donations = ""
translations = ""
more_information = [ "http://web.archive.org/web/20220914101708/https://stickynote.amongtech.cc/randomapp/",]
summary_source_url = "https://codeberg.org/foreverxml/random"
screenshots = [ "https://codeberg.org/foreverxml/random/raw/branch/main/screenshots/coin.png", "https://codeberg.org/foreverxml/random/raw/branch/main/screenshots/number.png", "https://codeberg.org/foreverxml/random/raw/branch/main/screenshots/roulette.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "page.codeberg.foreverxml.Random"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://codeberg.org/foreverxml/random/raw/branch/main/page.codeberg.foreverxml.Random.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "randomgtk",]
appstream_xml_url = "https://codeberg.org/foreverxml/random/raw/branch/main/data/page.codeberg.foreverxml.Random.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

A tool to pick a random number or list item. Pick what chore to do, a number between 1 and 100, whether or not to jump on your mom's bed, etc.

[Source](https://codeberg.org/foreverxml/random/raw/branch/main/data/page.codeberg.foreverxml.Random.metainfo.xml.in)

### Notice

Archived on 2022-09-11.

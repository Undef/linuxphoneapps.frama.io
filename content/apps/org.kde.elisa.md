+++
title = "Elisa"
description = "Beautiful no-nonsense music player with online radio support"
aliases = []
date = 2021-03-15
updated = 2024-01-02

[taxonomies]
project_licenses = [ "LGPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "multimedia",]
categories = [ "music player",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Qt", "KDE", "Audio", "Player",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/multimedia/elisa"
homepage = "https://apps.kde.org/elisa"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=elisa"
donations = "https://www.kde.org/community/donations/?app=elisa&source=appdata"
translations = ""
more_information = [ "https://plasma-mobile.org/2021/04/27/plasma-mobile-update-march-april/",]
summary_source_url = "https://invent.kde.org/multimedia/elisa/-/blob/master/org.kde.elisa.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/elisa/elisa-mobile.png", "https://cdn.kde.org/screenshots/elisa/elisa.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.elisa"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.elisa"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "elisa",]
appstream_xml_url = "https://invent.kde.org/multimedia/elisa/-/raw/master/org.kde.elisa.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

The Elisa music player is developed by the KDE community and strives to be simple and pleasant to use. It's available on Linux, Microsoft Windows, and Android.


With Elisa, you can browse your local music collection by genre, artist, album, or track, listen to online radio, create and manage playlists, display lyrics, and more.


Elisa supports your full KDE color scheme when used on the Plasma desktop, or else the standard light and dark modes.


Chill out with Party Mode, which puts your music's album art front and center.

[Source](https://invent.kde.org/multimedia/elisa/-/raw/master/org.kde.elisa.appdata.xml)

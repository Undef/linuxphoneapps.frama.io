+++
title = "Saldo"
description = "Check your bank accounts"
aliases = [ "apps/org.tabos.banking/",]
date = 2019-02-01
updated = 2023-12-31

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Jan-Michael Brummer",]
categories = [ "banking",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK4", "libadwaita",]
backends = [ "python-fints",]
services = [ "FinTS",]
packaged_in = [ "flathub", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Office", "Finance",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/tabos/saldo"
homepage = "https://tabos.gitlab.io/projects/saldo/"
bugtracker = "https://gitlab.com/tabos/saldo/-/issues/"
donations = "https://www.paypal.com/paypalme/tabos/10/"
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/tabos/saldo/-/raw/main/data/org.tabos.saldo.appdata.xml.in.in"
screenshots = [ "https://gitlab.com/tabos/saldo/-/raw/main/data/screenshots/saldo1.png", "https://gitlab.com/tabos/saldo/-/raw/main/data/screenshots/saldo2.png", "https://gitlab.com/tabos/saldo/-/raw/main/data/screenshots/saldo3.png", "https://gitlab.com/tabos/saldo/-/raw/main/data/screenshots/saldo4.png", "https://gitlab.com/tabos/saldo/-/raw/main/data/screenshots/saldo5.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.tabos.saldo"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.tabos.saldo"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "banking",]
appstream_xml_url = "https://gitlab.com/tabos/saldo/-/raw/main/data/org.tabos.saldo.appdata.xml.in.in"
reported_by = "cahfofpai"
updated_by = "script"
+++


### Description

An easy way to access your online banking information. Show your balance and transaction based on FinTS online banking information.


Note: This is a frontend for python-fints. Report non working bank access at python-fints github page.

[Source](https://gitlab.com/tabos/saldo/-/raw/main/data/org.tabos.saldo.appdata.xml.in.in)

### Notice

Previously called "Banking" and based on aqbanking. GTK3/libhandy before 0.6.0.
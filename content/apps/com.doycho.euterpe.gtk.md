+++
title = "Euterpe"
description = "Media player client for the Euterpe media server."
aliases = []
date = 2022-03-25
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Doychin Atanasov",]
categories = [ "audio streaming",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = [ "euterpe",]
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "Audio", "AudioVideo", "GNOME", "GTK", "Player",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/ironsmile/euterpe-gtk"
homepage = "https://listen-to-euterpe.eu"
bugtracker = "https://github.com/ironsmile/euterpe-gtk/issues"
donations = "https://github.com/sponsors/ironsmile"
translations = ""
more_information = [ "https://listen-to-euterpe.eu/docs",]
summary_source_url = "https://raw.githubusercontent.com/ironsmile/euterpe-gtk/master/data/com.doycho.euterpe.gtk.appdata.xml.in"
screenshots = [ "https://i.imgur.com/B5MqGpB.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "com.doycho.euterpe.gtk.desktop"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.doycho.euterpe.gtk"
flatpak_link = ""
flatpak_recipe = "https://raw.githubusercontent.com/ironsmile/euterpe-gtk/master/com.doycho.euterpe.gtk.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "euterpe",]
appstream_xml_url = "https://raw.githubusercontent.com/ironsmile/euterpe-gtk/master/data/com.doycho.euterpe.gtk.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Mobile and desktop player for the self-hosted Euterpe streaming
 server. You need an access to an Euterpe server running somewhere in
 order for this program to be useful. You could try it out with the
 demo Euterpe server, accessible at the project's website.


These are some of the things which this player supports:


* Extremely light resource usage and fast. Excellent for constrained
 mobile devices such as phones and laptops.
* Accessing Euterpe server with or without authentication.
* Playing albums or a single tracks.
* Searching the database for music.
* Browsing by albums and artists based on their tag metadata.
* Mobile first but convergent. It works on both mobile and desktop Linux.

[Source](https://raw.githubusercontent.com/ironsmile/euterpe-gtk/master/data/com.doycho.euterpe.gtk.appdata.xml.in)

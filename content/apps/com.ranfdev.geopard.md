+++
title = "Geopard"
description = "A gemini browser"
aliases = []
date = 2022-05-10
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Lorenzo Miglietta",]
categories = [ "gemini browser",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "aur", "flathub", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Network",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/ranfdev/Geopard"
homepage = "https://ranfdev.com/projects/Geopard"
bugtracker = "https://github.com/ranfdev/Geopard/issues/"
donations = "https://github.com/sponsors/ranfdev"
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/ranfdev/Geopard/master/data/com.ranfdev.Geopard.metainfo.xml.in.in"
screenshots = [ "https://raw.githubusercontent.com/ranfdev/Geopard/master/data/screenshots/1.png", "https://raw.githubusercontent.com/ranfdev/Geopard/master/data/screenshots/2.png", "https://raw.githubusercontent.com/ranfdev/Geopard/master/data/screenshots/3.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/com.ranfdev.geopard/1.png", "https://img.linuxphoneapps.org/com.ranfdev.geopard/2.png", "https://img.linuxphoneapps.org/com.ranfdev.geopard/3.png", "https://img.linuxphoneapps.org/com.ranfdev.geopard/4.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "com.ranfdev.Geopard"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.ranfdev.Geopard"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "geopard",]
appstream_xml_url = "https://raw.githubusercontent.com/ranfdev/Geopard/master/data/com.ranfdev.Geopard.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Geopard is a browser for the gemini protocol, that is, a lighter alternative to the web.
 Use Geopard to browse the space and reach hundreds of gemini capsules! Read stories, download files, play games...


Features


* Colors!
 The browser will have a different color for each domain you visit.
* Fast (async core + caching).
 Streams content by default. That means you can open pages even when you have
 connection speeds of Kb/s.
 It also caches pages in the history, so you can go back in an instant
* Can download binary files.
 The download will start as soon as you open the corresponding link.
 You can always cancel it by opening another page.
* Bookmarks
* Search

[Source](https://raw.githubusercontent.com/ranfdev/Geopard/master/data/com.ranfdev.Geopard.metainfo.xml.in.in)

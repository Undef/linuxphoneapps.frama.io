+++
title = "Gemini"
description = "Office Suite"
aliases = [ "apps/noappid.office.calligra/",]
date = 2019-02-01
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community",]
categories = [ "office",]
mobile_compatibility = [ "3",]
status = [ "unmaintained",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Qt", "KDE", "Office",]
programming_languages = [ "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/office/calligra"
homepage = "https://www.calligra.org/gemini/"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?format=guided&product=calligragemini"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/office/calligra/-/raw/master/gemini/org.kde.calligragemini.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/calligra/presentation-loaded-from-dropbox.png", "https://cdn.kde.org/screenshots/calligra/words-desktop-mode.png", "https://cdn.kde.org/screenshots/calligra/words-touch-mode-with-notes-panel.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.calligragemini.desktop"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "calligra",]
appstream_xml_url = "https://invent.kde.org/office/calligra/-/raw/master/gemini/org.kde.calligragemini.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

The KDE Office suite for 2-in-1 devices.


2-in-1 devices which can act as both a touch only tablet and a classic clamshell notebook are fast becoming very popular, and after the success of Krita Gemini, the team behind it realised there were plenty of reasons to apply that same concept to other applications, and especially the others in the Calligra suite. In particular the word processor component Words and presentation tool Stage were deemed ripe for this treatment.


With that realisation was born the desire to create Calligra Gemini, an application encasing word processor and presentation components which would function both as a traditional desktop application with a mouse and keyboard, and transform into a touch friendly application on the go, changing the experience to one suitable for all-touch devices without the inconvenience of having to switch to a different application.


The Gemini applications combine the power of Calligra's main applications (in Calligra Gemini's case the main office desktop applications, at the moment Words and Stage), with pleasant and explicitly touch optimised experiences based around the same document types.


Calligra itself is a large suite of applications, split into three main categories: Office applications, Graphics Applications and Project Management. Please visit the Calligra website to see full descriptions of the many tools.


* Common Features


The applications in the Calligra Suite shares some common UI concepts that gives it a modern look better suited for the wide screens of today. One of them is that most formatting is done using dockers which are placed at the side of the windows instead of on the top. This makes more space available for the actual document contents and avoids opening dialogs on top of it. If the user chooses, s/he can rearrange the placement of the dockers around the document area or even tear loose them and let them float freely. The arrangement is saved and reused the next time Calligra is opened.


* Embeddable Objects


There are many objects and shapes that you can insert into your document, such as geometric shapes, sheet music score, and many more amazing options. All of these shapes are available in all applications, which makes implementing these objects much easier.


* Standard file format


Calligra uses the Open Document Format (ODF) as its main file format which makes it compatible with most other office applications including OpenOffice.org, LibreOffice and Microsoft Office. It can also import the native file formats of Microsoft Office with great accuracy, in many cases the best you can find.

[Source](https://invent.kde.org/office/calligra/-/raw/master/gemini/org.kde.calligragemini.appdata.xml)

### Notice

Unmaintained according to htt:ps://apps.kde.org/calligragemini/

+++
title = "Megapixels"
description = "A gnome camera application for phones"
aliases = []
date = 2020-09-12
updated = 2023-09-30

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "postmarketOS Developers",]
categories = [ "camera",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Graphics", "Photography",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/postmarketOS/megapixels"
homepage = "https://sr.ht/~martijnbraam/megapixels"
bugtracker = "https://gitlab.com/postmarketOS/megapixels/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/postmarketOS/megapixels/-/raw/master/data/org.postmarketos.Megapixels.metainfo.xml"
screenshots = [ "http://brixitcdn.net/metainfo/megapixels.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.postmarketos.Megapixels"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "megapixels", "megapixels-gtk3",]
appstream_xml_url = "https://gitlab.com/postmarketOS/megapixels/-/raw/master/data/org.postmarketos.Megapixels.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description
Megapixels is a camera application designed for phones and tablets. It
 implements the v4l2 and media-request apis so set up camera pipelines on
 ARM hardware and uses the raw data modes of the sensors to get the best
 quality pictures.

[Source](https://gitlab.com/postmarketOS/megapixels/-/raw/master/data/org.postmarketos.Megapixels.metainfo.xml)

### Notice
GTK3 branch is still available. Megapixels also scans QR codes.

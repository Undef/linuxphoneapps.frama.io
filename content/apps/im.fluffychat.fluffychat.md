+++
title = "Fluffychat"
description = "Chat with your friends"
aliases = []
date = 2021-05-04
updated = 2024-01-02

[taxonomies]
project_licenses = [ "AGPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "fluffychat.im",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "Flutter",]
backends = [ "Matrix",]
services = [ "Matrix",]
packaged_in = [ "alpine_edge", "aur", "flathub", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "snapcraft",]
freedesktop_categories = [ "Network", "InstantMessaging", "Chat",]
programming_languages = [ "Dart",]
build_systems = [ "cmake", "ninja",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/krille-chan/fluffychat"
homepage = "https://fluffychat.im"
bugtracker = "https://github.com/krille-chan/fluffychat/issues/"
donations = "https://liberapay.com/KrilleChritzelius"
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/flathub/im.fluffychat.Fluffychat/master/im.fluffychat.Fluffychat.metainfo.xml"
screenshots = [ "https://raw.githubusercontent.com/krille-chan/fluffychat/main/docs/screenshots/desktop.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = false
app_id = "im.fluffychat.Fluffychat"
scale_to_fit = ""
flathub = "https://flathub.org/apps/im.fluffychat.Fluffychat"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = "https://snapcraft.io/fluffychat"
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "fluffychat",]
appstream_xml_url = "https://raw.githubusercontent.com/flathub/im.fluffychat.Fluffychat/master/im.fluffychat.Fluffychat.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

FluffyChat is an open source, nonprofit and cute matrix messenger app. The app is easy to use but secure and decentralized.


Features


* Send all kinds of messages, images and files
* Voice messages
* Location sharing
* Push notifications
* Unlimited private and public group chats
* Public channels with thousands of participants
* Feature rich group moderation including all matrix features
* Discover and join public groups
* Dark mode
* Hides complexity of Matrix IDs behind simple QR codes
* Custom emotes and stickers
* Video calls via sharing links to Jitsi
* Spaces
* Compatible with Element, Nheko, NeoChat and all other Matrix apps
* End to end encryption
* Emoji verification & cross signing
* And much more...


FluffyChat comes with a dream


Imagine a world where everyone can choose the messenger they like and is still able to chat with all of their friends.


A world where there are no companies spying on you when you send selfies to friends and lovers.


And a world where apps are made for fluffyness and not for profit. ♥


Join the community: https://matrix.to/#/#fluffychat:matrix.org
 Website: http://fluffychat.im
 Microblog: https://metalhead.club/@krille

[Source](https://raw.githubusercontent.com/flathub/im.fluffychat.Fluffychat/master/im.fluffychat.Fluffychat.metainfo.xml)

### Notice

As of August 2023, Fluffychat does not seem to work on devices that only support OpenGL ES 2.0 (e.g., PinePhone, Librem 5). You can get flatpak builds to work on these devices by disabling acces to "GPU acceleration" and "All devices" in Flatseal.

+++
title = "Iotas"
description = "Simple note taking"
aliases = [ "apps/org.gnome.gitlab.cheywood.iotas/",]
date = 2022-04-06
updated = 2024-01-28

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Chris Heywood",]
categories = [ "note taking",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Nextcloud Notes",]
packaged_in = [ "aur", "debian_13", "debian_unstable", "devuan_unstable", "flathub", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "TextEditor", "TextTools", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/World/iotas"
homepage = "https://gitlab.gnome.org/World/iotas"
bugtracker = "https://gitlab.gnome.org/World/iotas/-/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/World/iotas/-/raw/main/data/org.gnome.World.Iotas.metainfo.xml.in.in"
screenshots = [ "https://gitlab.gnome.org/World/iotas/-/raw/main/data/screenshots/desktop_editor.png", "https://gitlab.gnome.org/World/iotas/-/raw/main/data/screenshots/desktop_editor_markdown.png", "https://gitlab.gnome.org/World/iotas/-/raw/main/data/screenshots/desktop_index.png", "https://gitlab.gnome.org/World/iotas/-/raw/main/data/screenshots/desktop_index_dark.png", "https://gitlab.gnome.org/World/iotas/-/raw/main/data/screenshots/desktop_markdown_render.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.World.Iotas"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.World.Iotas"
flatpak_link = "https://flathub.org/apps/org.gnome.World.Iotas.flatpakref"
flatpak_recipe = "https://gitlab.gnome.org/World/iotas/-/raw/main/build-aux/flatpak/org.gnome.World.Iotas.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "iotas",]
appstream_xml_url = "https://gitlab.gnome.org/World/iotas/-/raw/main/data/org.gnome.World.Iotas.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = "linmob"

+++


### Description

Iotas is a simple note taking app with mobile-first design and a focus on sync with Nextcloud Notes.


Although simple by design there are a few features


* Translators: Part of metainfo description
* Sync. with Nextcloud Notes
* Offline note editing, syncing when back online
* Basic search
* Favorites
* Spell checking
* Category editing and filtering
* Note backup and restoration from CLI for using without sync.
* The ability to change font size and toggle monospace style


Writing in markdown is supported but optional, featuring


* Translators: Part of metainfo description
* Syntax highlighting with themes
* A formatted view
* The ability to check off task lists from the formatted view


Slightly more technical details, for those into that type of thing


* Translators: Part of metainfo description
* Nextcloud Notes sync. is via the REST API, not WebDAV, which makes it snappy
* There's basic sync. conflict detection
* Notes are constantly saved
* Large note collections are partially loaded to quicken startup


Why "Iotas"? An iota is a little bit and this app is designed for jotting down little things on little devices. Iota stems from the same Greek word as jot and is commonly used in negative statements eg. "not one iota of …", but we think the word has more to give. Maybe somebody will take note?

[Source](https://gitlab.gnome.org/World/iotas/-/raw/main/data/org.gnome.World.Iotas.metainfo.xml.in.in)

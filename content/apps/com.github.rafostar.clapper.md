+++
title = "Clapper"
description = "Simple and modern GNOME media player"
aliases = []
date = 2021-05-04
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Rafał Dzięgiel",]
categories = [ "video player",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_edge", "aur", "debian_12", "debian_13", "debian_unstable", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "AudioVideo", "Video",]
programming_languages = [ "JavaScript", "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/Rafostar/clapper"
homepage = "https://rafostar.github.io/clapper"
bugtracker = "https://github.com/Rafostar/clapper/issues"
donations = "https://liberapay.com/Clapper"
translations = ""
more_information = [ "https://briandaniels.me/2021/07/06/hardware-accelerated-video-playback-on-the-pinephone-with-clapper.html",]
summary_source_url = "https://raw.githubusercontent.com/Rafostar/clapper/master/data/com.github.rafostar.Clapper.metainfo.xml"
screenshots = [ "https://raw.githubusercontent.com/wiki/Rafostar/clapper/media/screenshot-floating.png", "https://raw.githubusercontent.com/wiki/Rafostar/clapper/media/screenshot-fullscreen.png", "https://raw.githubusercontent.com/wiki/Rafostar/clapper/media/screenshot-mobile.png", "https://raw.githubusercontent.com/wiki/Rafostar/clapper/media/screenshot-windowed.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "com.github.rafostar.Clapper"
scale_to_fit = "com.github.rafostar.Clapper"
flathub = "https://flathub.org/apps/com.github.rafostar.Clapper"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "clapper",]
appstream_xml_url = "https://raw.githubusercontent.com/Rafostar/clapper/master/data/com.github.rafostar.Clapper.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Clapper is a GNOME media player built using GJS with GTK4 toolkit.
 The media player is using GStreamer as a media backend and renders
 everything via OpenGL. Player works natively on both Xorg and Wayland.
 It also supports hardware acceleration through VA-API on AMD/Intel GPUs,
 NVDEC on Nvidia and V4L2 on mobile devices.


The media player has an adaptive GUI. When viewing videos in "Windowed Mode",
 Clapper will use mostly unmodified GTK widgets to match your OS look nicely.
 When player enters "Fullscreen Mode" all GUI elements will become darker, bigger
 and semi-transparent for your viewing comfort. It also has a "Floating Mode" which
 displays only video on top of all other windows for a PiP-like viewing experience.
 Mobile friendly transitions are also supported.

[Source](https://raw.githubusercontent.com/Rafostar/clapper/master/data/com.github.rafostar.Clapper.metainfo.xml)

### Notice

Supports hardware accelerated playback on PinePhone and Librem 5.

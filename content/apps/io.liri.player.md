+++
title = "Player"
description = "Play movies"
aliases = []
date = 2020-11-28
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The Liri developers",]
categories = [ "video player",]
mobile_compatibility = [ "4",]
status = [ "inactive",]
frameworks = [ "QtQuick", "fluid",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Qt", "AudioVideo", "Audio", "Video", "Player",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/lirios/player"
homepage = "https://liri.io/apps/player"
bugtracker = "https://github.com/lirios/player/issues/new"
donations = "https://liri.io/get-involved/"
translations = "https://www.transifex.com/lirios/liri-player/"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/lirios/player/develop/data/appdata/io.liri.Player.appdata.xml"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "io.liri.Player"
scale_to_fit = "io.liri.Player"
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/lirios/player/develop/data/appdata/io.liri.Player.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Player is the official movie player of the Liri desktop environment.


It can open local files and lets you manage a play list.

[Source](https://raw.githubusercontent.com/lirios/player/develop/data/appdata/io.liri.Player.appdata.xml)
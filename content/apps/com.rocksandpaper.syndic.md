+++
title = "Syndic"
description = "Touch-friendly RSS/Atom reader"
aliases = []
date = 2022-09-03
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC-BY-3.0",]
app_author = [ "cscarney",]
categories = [ "feed reader",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "QtQuick", "Kirigami",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Qt", "KDE", "Network", "Feed", "News",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/cscarney/syndic"
homepage = "http://syndic.rocksandpaper.com/"
bugtracker = "https://github.com/cscarney/syndic/issues"
donations = ""
translations = "https://hosted.weblate.org/engage/syndic/"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/cscarney/syndic/master/com.rocksandpaper.syndic.appdata.xml"
screenshots = [ "http://syndic.rocksandpaper.com/screenshots/syndic-mid.png", "http://syndic.rocksandpaper.com/screenshots/syndic-narrow.png", "http://syndic.rocksandpaper.com/screenshots/syndic-wide.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "com.rocksandpaper.syndic"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.rocksandpaper.syndic"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/cscarney/syndic/master/com.rocksandpaper.syndic.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Syndic is a simple, responsive feed reader designed for casual browsing.


It is designed to adapt to either mouse and touch input, and run well on both desktop and mobile devices.


The UI is designed with Plasma Desktop and Android in mind, but it should run in other environments as well.

[Source](https://raw.githubusercontent.com/cscarney/syndic/master/com.rocksandpaper.syndic.appdata.xml)
+++
title = "Forklift"
description = "Video and audio download application"
aliases = []
date = 2020-08-26
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "johnn3y",]
categories = [ "video downloader",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/Johnn3y/Forklift"
homepage = "http://github.com/Johnn3y/Forklift"
bugtracker = "https://github.com/Johnn3y/Forklift/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/com.github.Johnn3y.Forklift"
screenshots = [ "https://raw.githubusercontent.com/Johnn3y/Forklift/master/data/screenshots/1.png", "https://raw.githubusercontent.com/Johnn3y/Forklift/master/data/screenshots/2.png", "https://raw.githubusercontent.com/Johnn3y/Forklift/master/data/screenshots/3.png", "https://raw.githubusercontent.com/Johnn3y/Forklift/master/data/screenshots/4.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "com.github.Johnn3y.Forklift"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.github.Johnn3y.Forklift"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "forklift",]
appstream_xml_url = "https://raw.githubusercontent.com/Johnn3y/Forklift/master/data/com.github.Johnn3y.Forklift.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Forklift is a program used to download video and audio files from popular Video and Audio platforms. Using the popular CLI application youtube-dl as base, Forklift supports dozents of websites. Forklift is also optimized for smaller screens, so you can easily navigate on your mobile device.


Main Features:


* Search function for YouTube and Soundcloud
* Download original files or
* Convert video and audio files to other formats like mp3, ogg, etc

[Source](https://raw.githubusercontent.com/Johnn3y/Forklift/master/data/com.github.Johnn3y.Forklift.appdata.xml.in)

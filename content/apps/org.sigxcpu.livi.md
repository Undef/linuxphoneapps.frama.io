+++
title = "Light Video"
description = "A simple GTK4 based video player for mobile phones"
aliases = []
date = 2021-07-19
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Guido Günther",]
categories = [ "video player",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "gstreamer", "x264",]
services = []
packaged_in = [ "alpine_3_19", "alpine_edge", "aur", "debian_experimental", "flathub",]
freedesktop_categories = [ "Video",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/guidog/livi"
homepage = "https://gitlab.gnome.org/guidog/livi"
bugtracker = "https://gitlab.gnome.org/guidog/livi/issues"
donations = ""
translations = ""
more_information = [ "https://social.librem.one/tags/%C2%B5Player", "https://social.librem.one/@agx/110184844885409115",]
summary_source_url = "https://gitlab.gnome.org/guidog/livi/-/raw/main/data/org.sigxcpu.Livi.metainfo.xml.in"
screenshots = [ "https://gitlab.gnome.org/guidog/livi/-/raw/main/screenshots/landscape-fullscreen.png?inline=false",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.sigxcpu.Livi"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.sigxcpu.Livi"
flatpak_link = ""
flatpak_recipe = "https://gitlab.gnome.org/guidog/livi/-/raw/main/org.sigxcpu.Livi.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "livi",]
appstream_xml_url = "https://gitlab.gnome.org/guidog/livi/-/raw/main/data/org.sigxcpu.Livi.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

A minimalistic GTK4 and gstreamer based video player for mobile phones like the Librem 5 aiming for minimal battery usage.


It supports:


* Inhibiting suspend/idle when playing video
* Stopping video playback on (i.e. power button toggled) blank
* Registering as default video player in GNOME control center
* An indicator whether hardware accleration is in use

[Source](https://gitlab.gnome.org/guidog/livi/-/raw/main/data/org.sigxcpu.Livi.metainfo.xml.in)

### Notice

Provides accelerated video playback, also on PinePhone. Needs to be set as default app for videos, as it lacks a dialog to open files.

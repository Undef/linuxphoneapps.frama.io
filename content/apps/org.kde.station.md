+++
title = "Station"
description = "Terminal Emulator"
aliases = []
date = 2019-02-01
updated = 2024-01-02

[taxonomies]
project_licenses = [ "LGPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "maui",]
categories = [ "terminal emulator",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "MauiKit", "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "Qt", "System", "TerminalEmulator",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/maui/station"
homepage = "https://apps.kde.org/station"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=station"
donations = "https://kde.org/community/donations"
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/maui/station/-/raw/master/org.kde.station.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/station/station-stable.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = false
app_id = "org.kde.station.desktop"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "maui-station", "station-terminal-emulator",]
appstream_xml_url = "https://invent.kde.org/maui/station/-/raw/master/org.kde.station.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

Station is a convergent terminal emulator, ready for desktop and mobile devices using GNU/Linux.

[Source](https://invent.kde.org/maui/station/-/raw/master/org.kde.station.appdata.xml)

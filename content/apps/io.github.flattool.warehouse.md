+++
title = "Warehouse"
description = "Manage all things Flatpak"
aliases = []
date = 2024-02-10

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Heliguy",]
categories = [ "settings", "flatpak management",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "flatpak",]
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "System", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "Meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/flattool/warehouse"
homepage = "https://github.com/flattool/warehouse"
bugtracker = "https://github.com/flattool/warehouse/issues"
donations = "https://ko-fi.com/heliguy"
translations = "https://weblate.fyralabs.com/projects/flattool/warehouse/"
more_information = []
summary_source_url = "https://flathub.org/apps/io.github.flattool.Warehouse"
screenshots = [ "https://raw.githubusercontent.com/flattool/warehouse/main/app_page_screeshots/Screenshot%20from%202023-12-17%2023-24-22.png", "https://raw.githubusercontent.com/flattool/warehouse/main/app_page_screeshots/Screenshot%20from%202023-12-17%2023-25-16.png", "https://raw.githubusercontent.com/flattool/warehouse/main/app_page_screeshots/Screenshot%20from%202023-12-17%2023-25-35.png", "https://raw.githubusercontent.com/flattool/warehouse/main/app_page_screeshots/Screenshot%20from%202023-12-17%2023-26-23.png", "https://raw.githubusercontent.com/flattool/warehouse/main/app_page_screeshots/Screenshot%20from%202023-12-17%2023-27-55.png", "https://raw.githubusercontent.com/flattool/warehouse/main/app_page_screeshots/Screenshot%20from%202023-12-17%2023-28-58.png", "https://raw.githubusercontent.com/flattool/warehouse/main/app_page_screeshots/Screenshot%20from%202023-12-17%2023-30-07.png", "https://raw.githubusercontent.com/flattool/warehouse/main/app_page_screeshots/Screenshot%20from%202023-12-17%2023-30-34.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "io.github.flattool.Warehouse"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.flattool.Warehouse"
flatpak_link = "https://flathub.org/apps/io.github.flattool.Warehouse.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/flattool/warehouse/main/io.github.flattool.Warehouse.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "warehouse",]
appstream_xml_url = "https://raw.githubusercontent.com/flattool/warehouse/main/data/io.github.flattool.Warehouse.metainfo.xml.in"
reported_by = "linmob"
updated_by = ""

+++

### Description

Warehouse is an app that manages installed Flatpaks, their user data, and Flatpak remotes.


Features:


* Show and filter the list of installed Flatpaks
* Display properties of installed Flatpaks
* Manage large groups of Flatpaks at once
* Add and remove Flatpak remotes
* Find and trash leftover user data
* Reinstall apps that have leftover data

[Source](https://raw.githubusercontent.com/flattool/warehouse/main/data/io.github.flattool.Warehouse.metainfo.xml.in)

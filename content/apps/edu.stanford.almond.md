+++
title = "Almond"
description = "The Open Virtual Assistant"
aliases = [ "apps/edu.stanford.almond.desktop/",]
date = 2021-05-29
updated = 2024-01-02

[taxonomies]
project_licenses = [ "Apache-2.0",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Stanford Open Virtual Assistant Lab",]
categories = [ "virtual assistant",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "JavaScript",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/stanford-oval/almond-gnome"
homepage = "https://almond.stanford.edu"
bugtracker = "https://github.com/stanford-oval/almond-gnome/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/stanford-oval/almond-gnome/release/data/edu.stanford.Almond.appdata.xml.in"
screenshots = [ "https://flatpak.almond.stanford.edu/screenshot-main.png", "https://flatpak.almond.stanford.edu/screenshot-my-goods.png", "https://flatpak.almond.stanford.edu/screenshot-my-rules.png", "https://flatpak.almond.stanford.edu/screenshot-nytimes.png", "https://flatpak.almond.stanford.edu/screenshot-xkcd.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "edu.stanford.Almond.desktop"
scale_to_fit = ""
flathub = "https://flathub.org/apps/edu.stanford.Almond"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/stanford-oval/almond-gnome/release/data/edu.stanford.Almond.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Almond is a virtual assistant that lets you interact with your services and accounts in natural language, with flexibility and privacy.
 Almond draws its power from the crowdsourced Thingpedia, an open collection of Web and Internet of Things APIs.
 Anyone can contribute support for the favorite service, with few lines of code and a handful of natural language sentences.

[Source](https://raw.githubusercontent.com/stanford-oval/almond-gnome/release/data/edu.stanford.Almond.appdata.xml.in)
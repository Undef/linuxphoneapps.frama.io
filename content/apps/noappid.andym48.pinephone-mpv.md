+++
title = "Pinephone MPV"
description = "A simple wrapper for MPV for the Pinephone and other Linux mobile devices. Includes On Screen Controls for MPV adapted for the Pinephone."
aliases = [ "apps/noappid.andym48.pinephone_mpv/",]
date = 2023-08-28

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "AndyM48",]
categories = [ "image viewer", "media", "music player", "video player",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "Tk",]
backends = [ "mpv",]
services = []
packaged_in = []
freedesktop_categories = []
programming_languages = [ "Tcl", "Lua",]
build_systems = [ "custom",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/AndyM48/pinephone_mpv"
homepage = ""
bugtracker = "https://gitlab.com/AndyM48/pinephone_mpv/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/AndyM48/pinephone_mpv"
screenshots = [ "https://gitlab.com/AndyM48/pinephone_mpv/-/raw/main/screenshots/folders.jpg", "https://gitlab.com/AndyM48/pinephone_mpv/-/raw/main/screenshots/files_folders.jpg", "https://gitlab.com/AndyM48/pinephone_mpv/-/raw/main/screenshots/mpv.jpg",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "AndyM48"
updated_by = ""

+++

### Description

Pinephone MPV is a wrapper for MPV which combines an adapted set of On Screen Controls, with, optionally, 'autoload' to view the next/previous media, 'coverart' to load the cover art for audio files and 'thumbfast' to view video thumbnails. All of which can be configured to your requirements.

### Notice

Enhances MPV for Linux mobiles. Standard MPV is not affected by Pinephone MPV

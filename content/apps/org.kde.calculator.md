+++
title = "plasma mobile calculator app"
description = "A short summary describing what this software is about"
aliases = []
date = 2019-09-30
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "GPL (http://www.gnu.org/licenses/gpl-3.0.txt)",]
app_author = [ "Antoni Przybylik",]
categories = [ "calculator",]
mobile_compatibility = [ "5",]
status = [ "inactive",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Qt", "KDE", "Utility", "Calculator",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/antoniprzybylik/calculator"
homepage = ""
bugtracker = "https://github.com/antoniprzybylik/calculator/issues/"
donations = ""
translations = ""
more_information = [ "https://invent.kde.org/antonip/calculator", "https://phabricator.kde.org/T8900",]
summary_source_url = "https://raw.githubusercontent.com/antoniprzybylik/calculator/master/org.kde.calculator.appdata.xml"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.calculator"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/antoniprzybylik/calculator/master/org.kde.calculator.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

features:


* basic calculator
* scientific calculator
* programmer calculator

[Source](https://raw.githubusercontent.com/antoniprzybylik/calculator/master/org.kde.calculator.appdata.xml)

### Notice

Not in active development, may be replaced by Kalk. Inactive since November 2020 (GitHub)/July 2019 (invent).
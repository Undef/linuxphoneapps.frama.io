+++
title = "Keysmith"
description = "Two-factor code generator for Plasma Mobile and Desktop"
aliases = []
date = 2019-04-16
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community",]
categories = [ "multi-factor authentication",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "KDE", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/utilities/keysmith"
homepage = "https://apps.kde.org/keysmith/"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=Keysmith"
donations = ""
translations = ""
more_information = [ "https://blog.bshah.in/2019/12/18/keysmith-v0-1-release/",]
summary_source_url = "https://invent.kde.org/utilities/keysmith/-/raw/master/org.kde.keysmith.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/keysmith/keysmith.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = false
app_id = "org.kde.keysmith"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.keysmith"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "keysmith",]
appstream_xml_url = "https://invent.kde.org/utilities/keysmith/-/raw/master/org.kde.keysmith.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

Keysmith generates tokens for your two-factor logins (2FA)


* Time and Hash-based OTP

[Source](https://invent.kde.org/utilities/keysmith/-/raw/master/org.kde.keysmith.appdata.xml)

+++
title = "KleverNotes"
description = "KleverNotes is a note taking and management application"
aliases = []
date = 2024-01-14

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "FSFAP",]
app_author = [ "The KDE Community",]
categories = [ "note taking",]
mobile_compatibility = [ "needs testing",]
status = [ "early",]
frameworks = [ "Kirigami", "QtQuick",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "Qt", "KDE", "Office", "TextTools",]
programming_languages = [ "C", "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/office/klevernotes"
homepage = ""
bugtracker = ""
donations = ""
translations = ""
more_information = [ "https://apps.kde.org/klevernotes/",]
summary_source_url = "https://invent.kde.org/office/klevernotes/-/raw/master/org.kde.klevernotes.metainfo.xml"
screenshots = [ "https://cdn.kde.org/screenshots/klevernotes/main_note_mobile.png", "https://cdn.kde.org/screenshots/klevernotes/todo_mobile.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.klevernotes"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "klevernotes",]
appstream_xml_url = "https://invent.kde.org/office/klevernotes/-/raw/master/org.kde.klevernotes.metainfo.xml"
reported_by = "linmob"
updated_by = ""

+++

### Description

KleverNotes is a note taking and management application for your mobile and desktop devices. It uses markdown and allow you to preview your content.

[Source](https://invent.kde.org/office/klevernotes/-/raw/master/org.kde.klevernotes.metainfo.xml)

### Notice

Pre-release, currently in KDE incubator, not packaged for any mobile distribution yet. Needs to be evaluated.

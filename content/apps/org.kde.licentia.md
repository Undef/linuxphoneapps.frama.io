+++
title = "Licentia"
description = "Choose a license for your project"
aliases = []
date = 2024-01-14

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Felipe Kinoshita", "KDE Community",]
categories = [ "development",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "Kirigami", "QtQuick",]
backends = []
services = []
packaged_in = [ "snapcraft",]
freedesktop_categories = [ "Qt", "KDE", "Development",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/sdk/licentia"
homepage = "https://invent.kde.org/sdk/licentia"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=Licentia&component=general"
donations = "https://kde.org/community/donations/?app=org.kde.licentia"
translations = ""
more_information = [ "https://apps.kde.org/licentia/",]
summary_source_url = "https://invent.kde.org/sdk/licentia/-/raw/master/org.kde.licentia.metainfo.xml"
screenshots = [ "https://cdn.kde.org/screenshots/licentia/licentia.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = true
app_id = "org.kde.licentia"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://invent.kde.org/sdk/licentia/-/raw/master/org.kde.licentia.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "licentia",]
appstream_xml_url = "https://invent.kde.org/sdk/licentia/-/raw/master/org.kde.licentia.metainfo.xml"
reported_by = "linmob"
updated_by = ""

+++


### Description

Licentia helps you choose a license for your project, it allows you to check the license's permissions, conditions and limitations, how to add said license to your project and read the full license.

[Source](https://invent.kde.org/sdk/licentia/-/raw/master/org.kde.licentia.metainfo.xml)

### Notice

Pre-release, not packaged for any mobile distribution yet.

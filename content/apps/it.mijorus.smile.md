+++
title = "Smile"
description = "An emoji picker"
aliases = []
date = 2022-05-31
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Lorenzo Paderi",]
categories = [ "emoji picker",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GTK", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/mijorus/smile"
homepage = "https://smile.mijorus.it"
bugtracker = "https://github.com/mijorus/smile/issues"
donations = "https://ko-fi.com/mijorus"
translations = ""
more_information = []
summary_source_url = "https://github.com/mijorus/smile/blob/master/data/it.mijorus.smile.appdata.xml.in"
screenshots = [ "https://raw.githubusercontent.com/mijorus/smile/master/docs/preview1.png", "https://raw.githubusercontent.com/mijorus/smile/master/docs/preview2.png", "https://raw.githubusercontent.com/mijorus/smile/master/docs/preview3.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/it.mijorus.smile/1.png", "https://img.linuxphoneapps.org/it.mijorus.smile/2.png", "https://img.linuxphoneapps.org/it.mijorus.smile/3.png",]
all_features_touch = false
intended_for_mobile = false
app_id = "it.mijorus.smile.desktop"
scale_to_fit = ""
flathub = "https://flathub.org/apps/it.mijorus.smile"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "smile",]
appstream_xml_url = "https://raw.githubusercontent.com/mijorus/smile/master/data/it.mijorus.smile.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Smile is a simple emoji picker for linux with custom tags support

[Source](https://raw.githubusercontent.com/mijorus/smile/master/data/it.mijorus.smile.appdata.xml.in)

### Notice

Was GTK3 pre 2.0.

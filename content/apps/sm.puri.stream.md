+++
title = "Stream"
description = "A Web Video Player"
aliases = []
date = 2021-05-08
updated = 2023-09-30

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "todd",]
categories = [ "video player",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK3", "libhandy",]
backends = [ "Invidious API",]
services = [ "YouTube",]
packaged_in = [ "aur",]
freedesktop_categories = [ "GTK", "Network", "Video", "Player",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://source.puri.sm/todd/Stream"
homepage = "https://source.puri.sm/todd/Stream"
bugtracker = "https://source.puri.sm/todd/Stream/-/issues"
donations = "https://puri.sm/fund-your-app/"
translations = ""
more_information = []
summary_source_url = "https://source.puri.sm/todd/Stream/-/raw/master/data/sm.puri.Stream.metainfo.xml.in"
screenshots = [ "https://source.puri.sm/todd/Stream/-/raw/master/data/screenshots/sm.puri.Stream.1.png", "https://source.puri.sm/todd/Stream/-/raw/master/data/screenshots/sm.puri.Stream.2.png", "https://source.puri.sm/todd/Stream/-/raw/master/data/screenshots/sm.puri.Stream.3.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "sm.puri.Stream"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://source.puri.sm/todd/Stream/-/raw/master/sm.puri.Stream.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "purism-stream",]
appstream_xml_url = "https://source.puri.sm/todd/Stream/-/raw/master/data/sm.puri.Stream.metainfo.xml.in"
reported_by = "linmob"
updated_by = "linmob"

+++


### Description
A Web Video Player

[Source](https://source.puri.sm/todd/Stream/-/raw/master/data/sm.puri.Stream.metainfo.xml.in)

### Notice
Pretty nice, but lacks hardware accelerated video playback.

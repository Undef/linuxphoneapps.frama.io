+++
title = "KBibTeX"
description = "BibTeX editor by KDE to edit bibliographies used with LaTeX"
aliases = []
date = 2019-10-22
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "feature",]
categories = [ "bibliography editor",]
mobile_compatibility = [ "5",]
status = [ "inactive",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Qt", "KDE", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/office/kbibtex/-/tree/feature/kirigami"
homepage = "https://userbase.kde.org/KBibTeX"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?format=guided&product=kbibtex"
donations = ""
translations = ""
more_information = [ "https://t-fischer.dreamwidth.org/9049.html", "https://invent.kde.org/office/kbibtex/-/commit/fd6dc23fab199e769da5e12df5ef99a4d8c9268f",]
summary_source_url = "https://invent.kde.org/office/kbibtex/-/raw/master/src/program/org.kde.kbibtex.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/kbibtex/kbibtex-kf5.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.kbibtex.desktop"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "kbibtex",]
appstream_xml_url = "https://invent.kde.org/office/kbibtex/-/raw/master/src/program/org.kde.kbibtex.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

KBibTeX is a reference management application which can be used to collect TeX/LaTeX bibliographies and export them in various formats.


KBibTeX can do the following things:


* Preview bibliography entries in various formats (Source (BibTeX), Source (RIS), Wikipedia, standard (XML/XSLT), fancy (XML/XSLT), and abstract-only (XML/XSLT)). Additional preview styles become available when bibtex2html is installed.
* Import data in various bibliography file formats such as BibTeX, RIS and ISI (requires bibutils) and export data to PDF (requires pdflatex), PostScript (requires latex), RTF (requires latex2rtf), and HTML.
* Search for the bibliography entries data in online databases (e.g. Google Scholar, ACM, IEEE, arXiv, etc.)
* Preview local or remote (online) resources, e.g. PDF files, linked in the BibTEX entry.
* Find and merge duplicate entries in bibliography.
* Integrate your bibliographies with LaTeX editors such as Kile and LyX.
* Import your Zotero library.

[Source](https://invent.kde.org/office/kbibtex/-/raw/master/src/program/org.kde.kbibtex.appdata.xml)

### Notice

https://invent.kde.org/office/kbibtex/-/tree/master/mobile/sailfishos Sailfish GUI

+++
title = "Lemmur"
description = "A client for lemmy - a federated reddit alternative"
aliases = []
date = 2023-01-07
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-2.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Filip Krawczyk",]
categories = [ "social media",]
mobile_compatibility = [ "5",]
status = [ "archived",]
frameworks = [ "Flutter",]
backends = []
services = [ "Lemmy",]
packaged_in = [ "aur",]
freedesktop_categories = [ "GTK", "Network",]
programming_languages = [ "Dart",]
build_systems = [ "flutter",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/LemmurOrg/lemmur"
homepage = "https://github.com/LemmurOrg/lemmur"
bugtracker = "https://github.com/LemmurOrg/lemmur/issues"
donations = "https://www.buymeacoffee.com/lemmur"
translations = "https://weblate.join-lemmy.org/engage/lemmur"
more_information = [ "https://lemmy.ml/post/677871",]
summary_source_url = "https://github.com/LemmurOrg/lemmur"
screenshots = [ "https://f-droid.org/repo/com.krawieck.lemmur/en-US/phoneScreenshots/1.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/com.krawieck.lemmur/1.png", "https://img.linuxphoneapps.org/com.krawieck.lemmur/2.png", "https://img.linuxphoneapps.org/com.krawieck.lemmur/3.png", "https://img.linuxphoneapps.org/com.krawieck.lemmur/4.png",]
all_features_touch = true
intended_for_mobile = false
app_id = "com.krawieck.lemmur"
scale_to_fit = ""
flathub = ""
flatpak_link = "https://github.com/LouisMarotta/lemmur/releases/tag/0.8"
flatpak_recipe = "https://raw.githubusercontent.com/LouisMarotta/lemmur/flatpak-linux/flatpak/com.krawieck.lemmur.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "lemmur",]
appstream_xml_url = "https://raw.githubusercontent.com/LouisMarotta/lemmur/flatpak-linux/flatpak/com.krawieck.lemmur.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Lemmur aims to provide a seamless experience when browsing different Lemmy instances.


You can have multiple multiple instances added at the same time without having to awkwardly switch between them.

[Source](https://raw.githubusercontent.com/LouisMarotta/lemmur/flatpak-linux/flatpak/com.krawieck.lemmur.metainfo.xml)

### Notice

Flatpak support was pending, see pull request: [https://github.com/LemmurOrg/lemmur/pull/3](https://github.com/LemmurOrg/lemmur/pull/346). Sadly, this very unlikely to ever be merged, as the project was archived since.
The app has been forked and continued as [Liftoff](https://github.com/liftoff-app/liftoff).

+++
title = "Flatsweep"
description = "Flatpak leftover cleaner"
aliases = []
date = 2023-09-24
updated = 2024-01-02

[taxonomies]
project_licenses = [ "MPL-2.0",]
metadata_licenses = [ "FSFAP",]
app_author = [ "giantpinkrobots",]
categories = [ "Utility",]
mobile_compatibility = [ "4",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "flatpak",]
packaged_in = [ "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "System", "Monitor",]
programming_languages = [ "Python",]
build_systems = [ "Meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/giantpinkrobots/flatsweep"
homepage = "https://github.com/giantpinkrobots/flatsweep"
bugtracker = "https://github.com/giantpinkrobots/flatsweep/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/giantpinkrobots/flatsweep/main/data/io.github.giantpinkrobots.flatsweep.appdata.xml.in"
screenshots = [ "https://raw.githubusercontent.com/giantpinkrobots/flatsweep/main/screenshots/Screenshot-Flatsweep-1.png", "https://raw.githubusercontent.com/giantpinkrobots/flatsweep/main/screenshots/Screenshot-Flatsweep-2.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/io.github.giantpinkrobots.flatsweep/1.png",]
all_features_touch = false
intended_for_mobile = false
app_id = "io.github.giantpinkrobots.flatsweep"
scale_to_fit = "io.github.giantpinkrobots.flatsweep"
flathub = "https://flathub.org/apps/io.github.giantpinkrobots.flatsweep"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/giantpinkrobots/flatsweep/main/data/io.github.giantpinkrobots.flatsweep.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

When you uninstall a Flatpak, it can leave some files behind on your computer. Flatsweep helps you easily get rid of the residue left on your system by uninstalled Flatpaks.


Flatsweep uses GTK4 and Libadwaita to provide a coherent user interface that integrates nicely with GNOME, but you can use it on any desktop environment of course.


Caution: Flatsweep exclusively looks at the default Flatpak install directory. If you have set a custom install path, it might accidentally delete files that weren't supposed to be deleted. If you have no idea what a 'custom install path' is, you'll be fine.

[Source](https://raw.githubusercontent.com/giantpinkrobots/flatsweep/main/data/io.github.giantpinkrobots.flatsweep.appdata.xml.in)
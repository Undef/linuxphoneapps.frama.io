+++
title = "GHex"
description = "Inspect and edit binary files"
aliases = []
date = 2020-10-21
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Logan Rathbone",]
categories = [ "development",]
mobile_compatibility = [ "4",]
status = [ "mature",]
frameworks = [ "GTK4",]
backends = [ "libgtkhex",]
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Development",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/GNOME/ghex"
homepage = "https://gitlab.gnome.org/GNOME/ghex"
bugtracker = "https://gitlab.gnome.org/GNOME/ghex/issues"
donations = "http://www.gnome.org/friends/"
translations = ""
more_information = [ "https://help.gnome.org/users/ghex/stable/",]
summary_source_url = "https://gitlab.gnome.org/GNOME/ghex/-/raw/master/data/org.gnome.GHex.appdata.xml.in.in"
screenshots = [ "https://gitlab.gnome.org/GNOME/ghex/raw/4fb3db22937386545e6f6d9f7d34ce1d42bdc457/data/appdata/ghex-1.png", "https://gitlab.gnome.org/GNOME/ghex/raw/4fb3db22937386545e6f6d9f7d34ce1d42bdc457/data/appdata/ghex-2.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/org.gnome.ghex/1.png", "https://img.linuxphoneapps.org/org.gnome.ghex/2.png", "https://img.linuxphoneapps.org/org.gnome.ghex/3.png", "https://img.linuxphoneapps.org/org.gnome.ghex/4.png", "https://img.linuxphoneapps.org/org.gnome.ghex/5.png",]
all_features_touch = false
intended_for_mobile = false
app_id = "org.gnome.GHex"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.GHex"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "ghex",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/ghex/-/raw/master/data/org.gnome.GHex.appdata.xml.in.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

GHex is a hex editor for the GNOME desktop.


GHex can load raw data from binary files and display them for editing in the
 traditional hex editor view. The display is split in two columns, with
 hexadecimal values in one column and the ASCII representation in the other.
 GHex is a useful tool for working with raw data.

[Source](https://gitlab.gnome.org/GNOME/ghex/-/raw/master/data/org.gnome.GHex.appdata.xml.in.in)

### Notice

Mostly mobile friendly with GHex 42 for simple browsing, options menu (bottom right) does not fit the screen.

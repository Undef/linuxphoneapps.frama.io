+++
title = "Giara"
description = "An app for Reddit"
aliases = []
date = 2020-09-28
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Gabriele Musco",]
categories = [ "social media",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Reddit",]
packaged_in = [ "alpine_3_18", "alpine_3_19", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "devuan_unstable", "flathub", "gnuguix", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "News",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/World/giara"
homepage = "https://giara.gabmus.org"
bugtracker = "https://gitlab.gnome.org/World/giara/-/issues"
donations = "https://liberapay.com/gabmus/donate"
translations = "https://gitlab.gnome.org/World/giara/-/tree/master/po"
more_information = [ "https://gabmus.org/posts/giara_is_a_reddit_app_for_linux/", "https://linmob.net/2020/10/06/reddit-clients-for-mobile-linux.html",]
summary_source_url = "https://gitlab.gnome.org/World/giara/-/raw/master/data/org.gabmus.giara.appdata.xml.in"
screenshots = [ "https://gitlab.gnome.org/World/giara/raw/website/static/screenshots/mainwindow.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.gabmus.giara"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gabmus.giara"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "giara",]
appstream_xml_url = "https://gitlab.gnome.org/World/giara/-/raw/master/data/org.gabmus.giara.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

An app for Reddit.


Browse Reddit from your Linux desktop or smartphone.

[Source](https://gitlab.gnome.org/World/giara/-/raw/master/data/org.gabmus.giara.appdata.xml.in)

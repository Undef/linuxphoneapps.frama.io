+++
title = "ABV Calculator"
description = "ABV Calculator is a refractometer ABV calculator intended for beer or wine"
aliases = []
date = 2021-01-24
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "the3dman",]
categories = [ "calculator",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "Calculator",]
programming_languages = [ "Python",]
build_systems = [ "setup.py",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/The3DmaN/abv-calc"
homepage = ""
bugtracker = "https://gitlab.com/The3DmaN/abv-calc/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/The3DmaN/abv-calc"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "abv-calc",]
appstream_xml_url = ""
reported_by = "The3DmaN"
updated_by = "script"

+++


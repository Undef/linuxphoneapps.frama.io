+++
title = "Okular Mobile"
description = "Document Viewer"
aliases = [ "apps/org.kde.mobile.okular/",]
date = 2019-02-16
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GFDL-1.3", "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community",]
categories = [ "document viewer", "pdf viewer",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Qt", "KDE", "Office", "Viewer",]
programming_languages = [ "Cpp", "C",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/graphics/okular"
homepage = "https://okular.kde.org/"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?format=guided&product=okular"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/graphics/okular/-/raw/master/mobile/app/org.kde.okular.kirigami.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/okular/okular-mobile.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.okular.kirigami.desktop"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "okular",]
appstream_xml_url = "https://invent.kde.org/graphics/okular/-/raw/master/mobile/app/org.kde.okular.kirigami.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

Okular is a universal document viewer developed by KDE. Okular works on multiple platforms, including but not limited to Linux, Windows, Mac OS X, \*BSD, etc.


Features:


* Supported Formats: PDF, PS, Tiff, CHM, DjVu, Images, DVI, XPS, Fiction Book, Comic Book, Plucker, EPub, Fax
* Sidebar with contents, thumbnails, reviews and bookmarks
* Annotations support

[Source](https://invent.kde.org/graphics/okular/-/raw/master/mobile/app/org.kde.okular.kirigami.appdata.xml)

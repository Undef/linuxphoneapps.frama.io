+++
title = "Store Cards"
description = "Keep all your store cards on your linux phone"
aliases = [ "apps/noappid.fdservices.storecards/",]
date = 2022-01-23
updated = 2023-07-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "AndyM48",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "maturing", ]
frameworks = [ "Tk",]
backends = [ "zint",]
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Tcl",]
build_systems = [ "none",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://gitlab.com/AndyM48/storecards"
homepage = ""
bugtracker = "https://gitlab.com/AndyM48/storecards/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/AndyM48/storecards"
screenshots = [ "https://user-content.gitlab-static.net/e6fbfb3857d668302138d61f0c7ef9629f0b9674/68747470733a2f2f6769746875622e636f6d2f666473657276696365732f73746f726563617264732f6173736574732f333030363033392f66353536326639362d656338622d346439622d383462382d346337316336613164333337", "https://user-content.gitlab-static.net/93cd284a84d255a7f268de6a0d3a4e6769b33e38/68747470733a2f2f6769746875622e636f6d2f666473657276696365732f73746f726563617264732f6173736574732f333030363033392f66376632653034332d633266642d346439652d623661662d616363356236386162613139", "https://user-content.gitlab-static.net/dbdeaae15299648680d4f7cf993814fd706d12ae/68747470733a2f2f6769746875622e636f6d2f666473657276696365732f73746f726563617264732f6173736574732f333030363033392f61633665313134342d346239642d343639332d616236392d386661366166663334363838",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "storecards",]
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "linmob"

+++


### Description

Store Cards is built using Tcl/TK and Zint
The aim is to keep all your store cards on your linux phone.

### Notice

Simple app to store membership/fidelity cards - enter details to display barcode and images.

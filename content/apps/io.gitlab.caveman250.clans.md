+++
title = "Clans"
description = "A Gtk4 / libAdwaita Discord client. Early WIP.\" "
aliases = []
date = 2022-04-18
updated = 2023-03-04

[taxonomies]
project_licenses = [ "No license.",]
metadata_licenses = []
app_author = [ "caveman250",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = [ "archived",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Discord",]
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Network", "Chat", "InstantMessaging",]
programming_languages = [ "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://gitlab.com/caveman250/clans"
homepage = ""
bugtracker = "https://gitlab.com/caveman250/clans/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/caveman250/clans"
screenshots = []
screenshots_img = [ "https://img.linuxphoneapps.org/io.gitlab.caveman250.clans/1.png", "https://img.linuxphoneapps.org/io.gitlab.caveman250.clans/2.png", "https://img.linuxphoneapps.org/io.gitlab.caveman250.clans/3.png", "https://img.linuxphoneapps.org/io.gitlab.caveman250.clans/4.png",]
all_features_touch = false
intended_for_mobile = false
app_id = "io.gitlab.caveman250.clans"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "linmob"

+++

### Description

A Gtk4 / libAdwaita Discord client. Early WIP. Warning: Use at your own risk. Using a third party Discord client may result in your account being banned. [Source](https://gitlab.com/caveman250/clans)

### Notice

Read the warning! If you want to try it on Arch or Manjaro, check out the experimental [PKGBUILD](https://framagit.org/linmobapps/pkgbuilds/-/tree/main/clans-git).

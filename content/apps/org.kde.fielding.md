+++
title = "Fielding"
description = "Test your REST APIs"
aliases = []
date = 2024-01-14

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Felipe Kinoshita", "KDE Community",]
categories = [ "development",]
mobile_compatibility = [ "needs testing",]
status = [ "early",]
frameworks = [ "Kirigami", "QtQuick",]
backends = []
services = []
packaged_in = [ "gnuguix",]
freedesktop_categories = [ "Qt", "KDE", "Utility",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/utilities/fielding"
homepage = "https://invent.kde.org/utilities/fielding"
bugtracker = "https://invent.kde.org/utilities/fielding/-/issues"
donations = ""
translations = ""
more_information = [ "https://apps.kde.org/fielding/",]
summary_source_url = "https://invent.kde.org/utilities/fielding/-/raw/master/org.kde.fielding.metainfo.xml"
screenshots = [ "https://cdn.kde.org/screenshots/fielding/fielding.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.kde.fielding"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://invent.kde.org/utilities/fielding/-/raw/master/org.kde.fielding.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "fielding",]
appstream_xml_url = "https://invent.kde.org/utilities/fielding/-/raw/master/org.kde.fielding.metainfo.xml"
reported_by = "linmob"
updated_by = ""

+++

### Description

Fielding makes it easy to test your REST API, it allows you to make requests for the most used HTTP methods.

[Source](https://invent.kde.org/utilities/fielding/-/raw/master/org.kde.fielding.metainfo.xml)

### Notice

Pre-release, not packaged for any mobile distribution yet. Needs to be evaluated.

+++
title = "gtkcord4"
description = "GTK4 Discord client in Go"
aliases = [ "apps/noappid.diamondburned.gtkcord4/", "apps/xyz.diamondb.gtkcord4/",]
date = 2022-04-11
updated = 2024-01-02

[taxonomies]
project_licenses = [ "AGPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "diamondburned",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Discord",]
packaged_in = [ "aur", "flathub", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "Network", "Chat",]
programming_languages = [ "Go",]
build_systems = [ "go",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/diamondburned/gtkcord4"
homepage = "https://github.com/diamondburned/gtkcord4"
bugtracker = "https://github.com/diamondburned/gtkcord4/issues"
donations = "https://github.com/sponsors/diamondburned"
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/diamondburned/gtkcord4/main/xyz.diamondb.gtkcord4.metainfo.xml"
screenshots = [ "https://raw.githubusercontent.com/diamondburned/gtkcord4/v0.0.14/.github/screenshot4.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "so.libdb.gtkcord4"
scale_to_fit = ""
flathub = "https://flathub.org/apps/so.libdb.gtkcord4"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gtkcord4",]
appstream_xml_url = "https://raw.githubusercontent.com/diamondburned/gtkcord4/main/so.libdb.gtkcord4.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

GTK4 Discord client in Go

[Source](https://raw.githubusercontent.com/diamondburned/gtkcord4/main/xyz.diamondb.gtkcord4.metainfo.xml)

### Notice

Warning: Use at your own risk. Using a third party Discord client may result in your account being banned.

gtkcord4 uses libadwaita since v0.0.10. [Check the gtkcord3 page](https://github.com/diamondburned/gtkcord3#logging-in) for information on how to obtain a Token to log in.

Pre-built Binaries are available on gtkcord4's CI which automatically builds each release for Linux x86_64 and aarch64.
See the [Releases](https://github.com/diamondburned/gtkcord4/releases) page for the binaries.

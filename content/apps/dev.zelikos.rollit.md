+++
title = "Chance"
description = "Roll the dice"
aliases = []
date = 2024-02-12

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Patrick Csikos",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Amusement", "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "Meson",]
requires_internet = [ "requires offline-only",]
tags = []

[extra]
repository = "https://gitlab.com/zelikos/rollit"
homepage = "https://zelikos.dev/apps/rollit"
bugtracker = "https://gitlab.com/zelikos/rollit/issues"
donations = "https://ko-fi.com/akzel94"
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/dev.zelikos.rollit"
screenshots = [ "https://gitlab.com/zelikos/rollit/-/raw/main/data/screenshots/01_rollit_wide_1.png", "https://gitlab.com/zelikos/rollit/-/raw/main/data/screenshots/02_rollit_wide_2.png", "https://gitlab.com/zelikos/rollit/-/raw/main/data/screenshots/03_rollit_narrow_1.png", "https://gitlab.com/zelikos/rollit/-/raw/main/data/screenshots/04_rollit_narrow_2.png", "https://gitlab.com/zelikos/rollit/-/raw/main/data/screenshots/05_rollit_dice_chooser.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "dev.zelikos.rollit"
scale_to_fit = ""
flathub = "https://flathub.org/apps/dev.zelikos.rollit"
flatpak_link = "https://flathub.org/apps/dev.zelikos.rollit.flatpakref"
flatpak_recipe = "https://gitlab.com/zelikos/rollit/-/raw/main/build-aux/flatpak/dev.zelikos.rollit.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://gitlab.com/zelikos/rollit/-/raw/main/data/dev.zelikos.rollit.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = ""

+++


### Description

Roll six-sided dice by default, or roll custom dice of up to 999 sides.


Copy roll results to the clipboard from the history pane. Results are remembered until the app is closed or they are manually cleared.


Keyboard shortcuts:


* Roll the dice with Ctrl+R
* Select dice with Ctrl+,
* Copy the last result with Ctrl+C
* Toggle the roll history pane with F9
* Clear roll history with Ctrl+L
* Undo the clear with Ctrl+Z

[Source](https://gitlab.com/zelikos/rollit/-/raw/main/data/dev.zelikos.rollit.metainfo.xml.in.in)

+++
title = "Sums"
description = "Calculate with postfix notation"
aliases = []
date = 2021-03-31
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Joshua Lee",]
categories = [ "calculator",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "Calculator",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/leesonwai/sums"
homepage = "https://gitlab.com/leesonwai/sums"
bugtracker = "https://gitlab.com/leesonwai/sums/-/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/leesonwai/sums/-/raw/main/data/io.gitlab.leesonwai.Sums.metainfo.xml.in"
screenshots = [ "https://gitlab.com/leesonwai/sums/-/raw/main/data/screenshots/sums-busy.png", "https://gitlab.com/leesonwai/sums/-/raw/main/data/screenshots/sums-empty.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "io.gitlab.leesonwai.Sums"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.gitlab.leesonwai.Sums"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "sums",]
appstream_xml_url = "https://gitlab.com/leesonwai/sums/-/raw/main/data/io.gitlab.leesonwai.Sums.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Sums is a postfix calculator focused on simplicy and designed to fit
 seamlessly into your GNOME desktop. Features include:


* A beautiful user interface that adheres to GNOME's design guidelines
* A selection of core mathematical operations and constants
* Two-way history navigation
* Copying your results with one click

[Source](https://gitlab.com/leesonwai/sums/-/raw/main/data/io.gitlab.leesonwai.Sums.metainfo.xml.in)

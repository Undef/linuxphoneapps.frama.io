+++
title = "Flowtime"
description = "Spend your time wisely"
aliases = []
date = 2021-12-24
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Diego Iván",]
categories = [ "productivity",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_19", "alpine_edge", "aur", "flathub", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "Clock",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/Diego-Ivan/Flowtime"
homepage = "https://github.com/Diego-Ivan/Flowtime"
bugtracker = "https://github.com/Diego-Ivan/Flowtime/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/Diego-Ivan/Flowtime/main/data/io.github.diegoivanme.flowtime.appdata.xml.in"
screenshots = [ "https://raw.githubusercontent.com/Diego-Ivan/Flowtime/main/data/screenshots/01.png", "https://raw.githubusercontent.com/Diego-Ivan/Flowtime/main/data/screenshots/02.png", "https://raw.githubusercontent.com/Diego-Ivan/Flowtime/main/data/screenshots/03.png", "https://raw.githubusercontent.com/Diego-Ivan/Flowtime/main/data/screenshots/04.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "io.github.diegoivanme.flowtime.desktop"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.diegoivanme.flowtime"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "flowtime",]
appstream_xml_url = "https://raw.githubusercontent.com/Diego-Ivan/Flowtime/main/data/io.github.diegoivanme.flowtime.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

The Pomodoro technique is efficient for tasks you find boring, but having to take a break when you are 100% concentrated in something you like might be annoying. That's why the Flowtime technique exists: take appropiate breaks without loosing you \*\*flow\*\*.

[Source](https://raw.githubusercontent.com/Diego-Ivan/Flowtime/main/data/io.github.diegoivanme.flowtime.appdata.xml.in)

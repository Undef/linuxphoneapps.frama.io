+++
title = "Dosage"
description = "Keep track of your treatments"
aliases = []
date = 2023-11-17
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Diego Povliuk",]
categories = [ "health",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "snapcraft",]
freedesktop_categories = [ "Calendar", "GNOME", "GTK", "MedicalSoftware", "Utility",]
programming_languages = [ "JavaScript",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/diegopvlk/Dosage"
homepage = "https://github.com/diegopvlk/Dosage"
bugtracker = "https://github.com/diegopvlk/Dosage/issues"
donations = "https://github.com/diegopvlk/Dosage#donate"
translations = "https://hosted.weblate.org/projects/dosage/dosage/"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/diegopvlk/Dosage/main/data/io.github.diegopvlk.Dosage.appdata.xml.in"
screenshots = [ "https://raw.githubusercontent.com/diegopvlk/Dosage/507f6cfcc136380398fb8d9609911e4dbcf4dbaa/screenshots/med-window-light-alt.png", "https://raw.githubusercontent.com/diegopvlk/Dosage/f962e4321686948b53c2254287a4d797b3e21579/screenshots/today-dark.png", "https://raw.githubusercontent.com/diegopvlk/Dosage/f962e4321686948b53c2254287a4d797b3e21579/screenshots/today-light.png",]
screenshots_img = []
all_features_touch = false 
intended_for_mobile = false 
app_id = "io.github.diegopvlk.Dosage"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.diegopvlk.Dosage"
flatpak_link = ""
flatpak_recipe = "https://github.com/flathub/io.github.diegopvlk.Dosage/blob/master/io.github.diegopvlk.Dosage.json"
snapcraft = "https://snapcraft.io/dosage-tracker"
snap_link = ""
snap_recipe = "https://raw.githubusercontent.com/diegopvlk/Dosage/main/snap/snapcraft.yaml"
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "dosage-tracker",]
appstream_xml_url = "https://raw.githubusercontent.com/diegopvlk/Dosage/main/data/io.github.diegopvlk.Dosage.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Easily manage track treatments with Dosage: Notifications, History, Multiple Doses, Flexible Frequency, Customization, Stock Monitoring, and Duration Control


Features:


* Notifications — Get reminders at the right time
* History — See which medications you took, skipped or missed
* Dosage management — Multiple doses with different times
* Frequency modes — Every day, specific days, cycle or just when necessary
* Color and icon — Give a shape for your treatment
* Inventory tracking — Monitor your stock and get reminded when it's low
* Duration — Define the start and end dates

[Source](https://raw.githubusercontent.com/diegopvlk/Dosage/main/data/io.github.diegopvlk.Dosage.appdata.xml.in)

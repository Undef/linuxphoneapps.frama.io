+++
title = "dragonstone"
description = "A simple GTK Gopher/Gemini client written in Vala"
aliases = []
date = 2021-07-24
updated = 2022-12-19

[taxonomies]
project_licenses = [ "https://codeberg.org/slatian/dragonstone/src/branch/master/LICENSE",]
metadata_licenses = []
app_author = [ "slatian",]
categories = [ "gemini browser", "gopher browser",]
mobile_compatibility = [ "5",]
status = [ "inactive",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "GTK", "GNOME", "Network",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://codeberg.org/slatian/dragonstone"
homepage = ""
bugtracker = "https://codeberg.org/slatian/dragonstone/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://codeberg.org/slatian/dragonstone"
screenshots = [ "https://fosstodon.org/@gamey/106635659774442376",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "com.gitlab.baschdel.dragonstone"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "dragonstone",]
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"

+++


### Description

A simple GTK Gopher/Gemini client written in Vala. This project is currently on long term support, meaning I won't add new features and only fix bugs if they get reported (or I notice them). If you want to adopt this project and make it into your own feel free to do so, I won't be mad if you change the license to a GPL or MIT one. [Source](https://codeberg.org/slatian/dragonstone)

### Notice
[Fossil](https://linuxphoneapps.org/apps/com.github.koyuspace.fossil/) is a fork of this project.

+++
title = "Glosbit"
description = "=Dictionary application powered by glosbe.com"
aliases = []
date = 2019-02-01
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3+",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "dkardarakos",]
categories = [ "translation tool",]
mobile_compatibility = [ "5",]
status = [ "inactive",]
frameworks = [ "Kirigami",]
backends = []
services = [ "glosbe.com",]
packaged_in = []
freedesktop_categories = [ "Qt", "KDE", "Network",]
programming_languages = [ "JavaScript", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/dkardarakos/glosbit"
homepage = ""
bugtracker = "https://gitlab.com/dimkard/glosbit/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/dkardarakos/glosbit/-/raw/master/org.kde.glosbit.appdata.xml"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.glosbit.desktop"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://invent.kde.org/dkardarakos/glosbit/-/raw/master/org.kde.glosbit.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Notice

Last commit in April 2019.
+++
title = "Glossaico"
description = "Language learning application"
aliases = []
date = 2021-10-20
updated = 2023-12-31

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "dimkard",]
categories = [ "education",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = [ "LibreLingo",]
services = [ "LibreLingo",]
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "flathub",]
freedesktop_categories = [ "Qt", "KDE", "Education", "Languages",]
programming_languages = [ "Python",]
build_systems = [ "none",]
requires_internet = []
tags = []

[extra]
repository = "https://codeberg.org/dimkard/glossaico"
homepage = "https://codeberg.org/dimkard/glossaico"
bugtracker = "https://codeberg.org/dimkard/glossaico/issues"
donations = ""
translations = ""
more_information = [ "http://web.archive.org/web/20211130004629/https://mastodon.technology/@dimitrisk/107111359669622273", "https://dimitris.cc/general/2021/11/07/glossaico_beta_1.html", "https://dimitris.cc/general/2021/12/02/glossaico_1_0_release.html",]
summary_source_url = "https://codeberg.org/dimkard/glossaico/raw/branch/main/org.codeberg.dimkard.glossaico.metainfo.xml"
screenshots = [ "https://codeberg.org/dimkard/glossaico/raw/branch/main/screenshots/glossaico-pm.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.codeberg.dimkard.glossaico"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.codeberg.dimkard.glossaico"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "glossaico",]
appstream_xml_url = "https://codeberg.org/dimkard/glossaico/raw/branch/main/org.codeberg.dimkard.glossaico.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description
Glossaico is a language learning application based on LibreLingo. It provides a user-ready course of Spanish for English speakers, but the users can opt to practice several other language courses that are still under development by the LibreLingo contributors.

[Source](https://codeberg.org/dimkard/glossaico/raw/branch/main/org.codeberg.dimkard.glossaico.metainfo.xml)

+++
title = "Audiobook QML"
description = "Minimal audio book reader for mobile and desktop."
aliases = []
date = 2020-12-12
updated = 2023-09-30

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "bit-shift-io",]
categories = [ "audiobook player",]
mobile_compatibility = [ "5",]
status = [ "inactive",]
frameworks = [ "QtQuick",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "Qt", "Audio", "Player",]
programming_languages = [ "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/bit-shift-io/audiobook"
homepage = ""
bugtracker = "https://github.com/bit-shift-io/audiobook/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/bit-shift-io/audiobook"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "audiobook",]
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "linmob"

+++


### Notice

Minimal feature set, works fine.
Inactive since 2021-08-25.

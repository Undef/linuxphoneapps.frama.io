+++
title = "Audio Sharing"
description = "Share your computer audio"
aliases = []
date = 2021-12-05
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Felix Häcker",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "Audio", "AudioVideo", "GNOME", "GTK",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/World/AudioSharing"
homepage = "https://gitlab.gnome.org/World/AudioSharing"
bugtracker = "https://gitlab.gnome.org/World/AudioSharing/issues"
donations = "https://liberapay.com/haecker-felix"
translations = "https://l10n.gnome.org/module/AudioSharing/"
more_information = []
summary_source_url = "https://gitlab.gnome.org/World/AudioSharing/-/raw/main/data/de.haeckerfelix.AudioSharing.metainfo.xml.in.in"
screenshots = [ "https://gitlab.gnome.org/World/AudioSharing/-/raw/main/data/screenshots/1.png", "https://gitlab.gnome.org/World/AudioSharing/-/raw/main/data/screenshots/2.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "de.haeckerfelix.AudioSharing"
scale_to_fit = ""
flathub = "https://flathub.org/apps/de.haeckerfelix.AudioSharing"
flatpak_link = ""
flatpak_recipe = "https://gitlab.gnome.org/World/AudioSharing/-/raw/main/build-aux/de.haeckerfelix.AudioSharing.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "audio-sharing",]
appstream_xml_url = "https://gitlab.gnome.org/World/AudioSharing/-/raw/main/data/de.haeckerfelix.AudioSharing.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

With Audio Sharing you can share your current computer audio playback in the form of an RTSP stream. This stream can then be played back by other devices, for example using VLC.


By sharing the audio as a network stream, you can also use common devices that are not intended to be used as audio sinks (eg. smartphones) to receive it.
 For example, there are audio accessories that are not compatible with desktop computers (e.g. because the computer does not have a Bluetooth module installed). With the help of this app, the computer audio can be played back on a smartphone, which is then connected to the Bluetooth accessory.

[Source](https://gitlab.gnome.org/World/AudioSharing/-/raw/main/data/de.haeckerfelix.AudioSharing.metainfo.xml.in.in)

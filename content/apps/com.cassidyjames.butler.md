+++
title = "Butler"
description = "Companion for Home Assistant"
aliases = []
date = 2024-02-06
updated = 2024-02-06

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC-BY-SA-4.0",]
app_author = [ "Cassidy James Blaede",]
categories = [ "smart home",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Home Assistant",]
packaged_in = [ "aur", "flathub", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "Network", "Network",]
programming_languages = [ "Vala",]
build_systems = [ "Meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/cassidyjames/butler"
homepage = "https://cassidyjames.com"
bugtracker = "https://github.com/cassidyjames/butler/issues"
donations = "https://cassidyjames.com/pay"
translations = ""
more_information = [ "https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/389",]
summary_source_url = "https://flathub.org/apps/com.cassidyjames.butler"
screenshots = [ "https://raw.githubusercontent.com/cassidyjames/butler/1.0.0/data/screenshots/light.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "com.cassidyjames.butler"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.cassidyjames.butler"
flatpak_link = "https://flathub.org/apps/com.cassidyjames.butler.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/cassidyjames/butler/main/com.cassidyjames.butler.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "butler",]
appstream_xml_url = "https://raw.githubusercontent.com/cassidyjames/butler/main/data/metainfo.xml"
reported_by = "linmob"
updated_by = ""

+++

### Description

Access your Home Assistant dashboard from a native companion UI, integrating better with your OS. Native features include:


* Icon in your App Grid, Applications Menu, Dash, Dock, etc.
* Native header bar
* Save and restore current view and size when closed and re-opened
* Two-finger swipe and mouse button support to go back/forward between views
* Cross-desktop light/dark style support (if supported by your Lovelace theme)


Butler is designed to make getting at your Home Assistant dashboard easier for kiosks, your laptop/desktop, or your Linux phone. It does not support companion app features from Android and iOS like location services, notifications, or exposing device sensors.


Other features include:


* Pinch-to-zoom
* Set the scaling with Ctrl+Plus/Minus or Ctrl+0 to reset
* Fullscreen from the menu, a keyboard shortcut, or a GSetting to better support kiosk use cases


Note WebRTC camera streams (i.e. used by some newer Nest cameras) are not currently supported.

[Source](https://raw.githubusercontent.com/cassidyjames/butler/main/data/metainfo.xml)

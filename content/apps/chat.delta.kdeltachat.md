+++
title = "KDeltaChat"
description = "DeltaChat client build with Kirigami"
aliases = []
date = 2021-09-04
updated = 2023-12-31

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "link2xt",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = [ "early", "pre-release",]
frameworks = [ "Kirigami",]
backends = [ "libdeltachat",]
services = [ "deltachat",]
packaged_in = [ "aur", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "Qt", "KDE", "Network", "Chat",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://git.sr.ht/~link2xt/kdeltachat"
homepage = ""
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "https://git.sr.ht/~link2xt/kdeltachat"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "chat.delta.KDeltaChat"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "kdeltachat",]
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"

+++

### Notice

No release yet, but works for simple, encrypted text conversations. Sending attachments (images etc.) is now implemented, received images are being scaled properly. Adding an account works via importing a backup or the add account screen. Compiling the necessary libdeltachat library takes a long time and requires more than 3GBs of RAM (at 4 threads) - doing this on device (PinePhone, Librem 5) is not recommended.

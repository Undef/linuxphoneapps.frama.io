+++
title = "Index"
description = "Manage your files"
aliases = []
date = 2019-02-01
updated = 2024-01-02

[taxonomies]
project_licenses = [ "LGPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "maui",]
categories = [ "file management",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "MauiKit", "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "Qt", "System", "FileManager",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/maui/index-fm"
homepage = "https://mauikit.org/"
bugtracker = "https://invent.kde.org/maui/index-fm/-/issues"
donations = "https://www.kde.org/community/donations"
translations = ""
more_information = [ "https://medium.com/@temisclopeolimac/index-overview-b2ddcb16534f",]
summary_source_url = "https://invent.kde.org/maui/index-fm/-/raw/master/org.kde.index.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/index/index.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = false
app_id = "org.kde.index.desktop"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.index"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "index-fm",]
appstream_xml_url = "https://invent.kde.org/maui/index-fm/-/raw/master/org.kde.index.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

Index allows you to navigate your computer and preview multimedia files.

[Source](https://invent.kde.org/maui/index-fm/-/raw/master/org.kde.index.appdata.xml)

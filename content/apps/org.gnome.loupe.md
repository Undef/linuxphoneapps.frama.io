+++
title = "Image Viewer"
description = "View images"
aliases = []
date = 2023-04-16
updated = 2024-01-14

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The GNOME Project",]
categories = [ "image viewer",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_experimental", "fedora_39", "fedora_rawhide", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "GNOME", "GTK", "Graphics", "Viewer",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/GNOME/loupe"
homepage = "https://apps.gnome.org/Loupe/"
bugtracker = "https://gitlab.gnome.org/GNOME/loupe/issues"
donations = "https://www.gnome.org/donate/"
translations = "https://l10n.gnome.org/module/loupe/"
more_information = []
summary_source_url = ""
screenshots = [ "https://static.gnome.org/appdata/gnome-45/loupe/loupe-main.png", "https://static.gnome.org/appdata/gnome-45/loupe/loupe-print.png", "https://static.gnome.org/appdata/gnome-45/loupe/loupe-properties.png", "https://static.gnome.org/appdata/gnome-45/loupe/loupe-svg.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/org.gnome.loupe/1.png", "https://img.linuxphoneapps.org/org.gnome.loupe/2.png", "https://img.linuxphoneapps.org/org.gnome.loupe/3.png", "https://img.linuxphoneapps.org/org.gnome.loupe/4.png", "https://img.linuxphoneapps.org/org.gnome.loupe/5.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.Loupe"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Loupe"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "loupe",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/loupe/-/raw/main/data/org.gnome.Loupe.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = "check_via_repology"

+++

### Description

Browse through your images and inspect their metadata with:


* Fast GPU accelerated image rendering
* Tiled rendering for vector graphics
* Extendable and sandboxed image decoding
* Accessible presentation of the most important metadata.

[Source](https://gitlab.gnome.org/GNOME/loupe/-/raw/main/data/org.gnome.Loupe.metainfo.xml.in.in)
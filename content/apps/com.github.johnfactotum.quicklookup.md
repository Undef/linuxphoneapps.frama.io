+++
title = "Quick Lookup"
description = "Look up words quickly"
aliases = []
date = 2020-10-21
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "John Factotum",]
categories = [ "dictionary",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Wiktionary",]
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "TextTools", "Dictionary",]
programming_languages = [ "JavaScript",]
build_systems = [ "custom",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/johnfactotum/quick-lookup"
homepage = "https://github.com/johnfactotum/quick-lookup"
bugtracker = "https://github.com/johnfactotum/quick-lookup/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/johnfactotum/quick-lookup/master/com.github.johnfactotum.QuickLookup.appdata.xml"
screenshots = [ "https://raw.githubusercontent.com/johnfactotum/quick-lookup/master/screenshot.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "com.github.johnfactotum.QuickLookup"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.github.johnfactotum.QuickLookup"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "quick-lookup",]
appstream_xml_url = "https://raw.githubusercontent.com/johnfactotum/quick-lookup/master/com.github.johnfactotum.QuickLookup.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Quick Lookup is a simple GTK dictionary application powered by Wiktionary™.


Features include:


* Lookup definitions for words or phrases
* Choose language by entering language name or ISO-639-1 code
* Open internal links within the app
* Go back to previous page with the back button
* Look up selected text from any app by setting a keyboard shortcut to launch this app with the “--selection” command line option


Wiktionary is a trademark of the Wikimedia Foundation. This program is not endorsed by or affiliated with the Wikimedia Foundation.

[Source](https://raw.githubusercontent.com/johnfactotum/quick-lookup/master/com.github.johnfactotum.QuickLookup.appdata.xml)

### Notice

Was GTK3, GTK4/libadwaita since release 2.0.

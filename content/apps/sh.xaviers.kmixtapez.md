+++
title = "KMixtapez"
description = "A Kirigami app for listening to mixtapes, albums & songs hosted on MyMixtapez dot com"
aliases = []
date = 2021-07-25
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-only",]
metadata_licenses = []
app_author = [ "xaviers",]
categories = [ "music player",]
mobile_compatibility = [ "5",]
status = [ "archived",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Qt", "KDE", "Audio", "Player",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://codeberg.org/xaviers/KMixtapez"
homepage = ""
bugtracker = "https://codeberg.org/xaviers/KMixtapez/issues/"
donations = ""
translations = ""
more_information = [ "https://fosstodon.org/@lowkeylone/106635935044071456",]
summary_source_url = "https://codeberg.org/xaviers/KMixtapez"
screenshots = [ "https://fosstodon.org/@lowkeylone/106635935044071456",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "sh.xaviers.kmixtapez"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://codeberg.org/xaviers/KMixtapez/raw/branch/main/sh.xaviers.kmixtapez.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++

### Notice

Repo has been archived, replaced by [Opentapes](https://codeberg.org/xaviers/Opentapes).

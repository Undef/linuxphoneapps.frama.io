+++
title = "Chats"
description = "Messaging application for mobile and desktop"
aliases = []
date = 2019-02-01
updated = 2024-01-14

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Purism",]
categories = [ "SMS", "chat",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "libpurple", "ModemManager",]
services = [ "SMS", "MMS", "XMPP", "Matrix",]
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Chat",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/World/Chatty"
homepage = "https://gitlab.gnome.org/World/Chatty"
bugtracker = "https://gitlab.gnome.org/World/Chatty/-/issues/"
donations = ""
translations = ""
more_information = [ "http://web.archive.org/web/20230323113901/https://wiki.mobian.org/doku.php?id=chatty",]
summary_source_url = "https://gitlab.gnome.org/World/Chatty/-/raw/main/data/sm.puri.Chatty.metainfo.xml.in.in"
screenshots = [ "https://gitlab.gnome.org/World/Chatty/-/raw/main/data/screenshots/screenshot.png?inline=false",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "sm.puri.Chatty"
scale_to_fit = ""
flathub = "https://flathub.org/apps/sm.puri.Chatty"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "chatty-im", "chatty-unclassified",]
appstream_xml_url = "https://gitlab.gnome.org/World/Chatty/-/raw/main/data/sm.puri.Chatty.metainfo.xml.in.in"
reported_by = "cahfofpai"
updated_by = "check_via_repology"

+++

### Description

Chats is a simple to use to messaging app for 1:1 communication and small groups supporting SMS, MMS, XMPP and Matrix. 

[Source](https://gitlab.gnome.org/World/Chatty/-/raw/main/data/sm.puri.Chatty.metainfo.xml.in.in)

### Notice

Used GTK3/libhandy before release 0.8.0.
+++
title = "Frog"
description = "Extract text from images"
aliases = []
date = 2022-11-30
updated = 2023-12-31

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Tender Owl",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "tesseract",]
services = []
packaged_in = [ "flathub", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/tenderowl/frog"
homepage = "https://getfrog.app"
bugtracker = "https://github.com/tenderowl/frog/issues"
donations = "https://www.buymeacoffee.com/tenderowl/"
translations = "https://hosted.weblate.org/projects/frog/default/"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/TenderOwl/Frog/master/data/com.github.tenderowl.frog.appdata.xml.in"
screenshots = [ "https://raw.githubusercontent.com/tenderowl/frog/master/data/screenshots/frog-window-dark.png", "https://raw.githubusercontent.com/tenderowl/frog/master/data/screenshots/frog-window-decoded.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/com.github.tenderowl.frog/1.png", "https://img.linuxphoneapps.org/com.github.tenderowl.frog/2.png", "https://img.linuxphoneapps.org/com.github.tenderowl.frog/3.png",]
all_features_touch = false
intended_for_mobile = false
app_id = "com.github.tenderowl.frog"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.github.tenderowl.frog"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-frog",]
appstream_xml_url = "https://raw.githubusercontent.com/TenderOwl/Frog/master/data/com.github.tenderowl.frog.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description
Extract text from images, websites, videos, and QR codes by taking a picture of the source.

[Source](https://raw.githubusercontent.com/TenderOwl/Frog/master/data/com.github.tenderowl.frog.appdata.xml.in)

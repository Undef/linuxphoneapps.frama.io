+++
title = "Delfin"
description = "Stream movies and TV shows from Jellyfin"
aliases = []
date = 2023-12-04
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC-BY-SA-4.0",]
app_author = []
categories = [ "Media",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Jellyfin",]
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "AudioVideo", "GNOME", "GTK", "Player", "TV", "Video",]
programming_languages = [ "Rust", "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://codeberg.org/avery42/delfin"
homepage = "https://delfin.avery.cafe/"
bugtracker = "https://codeberg.org/avery42/delfin/issues"
donations = ""
translations = "https://translate.codeberg.org/projects/delfin/"
more_information = []
summary_source_url = "https://codeberg.org/avery42/delfin/raw/branch/main/data/cafe.avery.Delfin.metainfo.xml.in"
screenshots = [ "https://delfin.avery.cafe/screenshots/flathub-1.png", "https://delfin.avery.cafe/screenshots/flathub-2.png", "https://delfin.avery.cafe/screenshots/flathub-3.png", "https://delfin.avery.cafe/screenshots/flathub-4.png", "https://delfin.avery.cafe/screenshots/flathub-5.png",]
screenshots_img = []
all_features_touch = false 
intended_for_mobile = false 
app_id = "cafe.avery.Delfin"
scale_to_fit = ""
flathub = "https://flathub.org/apps/cafe.avery.Delfin"
flatpak_link = "https://flathub.org/apps/cafe.avery.Delfin.flatpakref"
flatpak_recipe = "https://codeberg.org/avery42/delfin/raw/branch/main/build-aux/cafe.avery.Delfin.Devel.yaml"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "delfin",]
appstream_xml_url = "https://codeberg.org/avery42/delfin/raw/branch/main/data/cafe.avery.Delfin.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Delfin is a native client for the Jellyfin media server. It features a fast and clean
 interface to stream your media in an embedded MPV-based video player.


Delfin currently supprts streaming movies and TV shows from your library. The video
 player
 supports the Intro Skipper plugin for skipping intros automatically, and the Jellyscrub
 plugin
 to show thumbnails while scrubbing through videos.


This is an early release, you may run into bugs or missing features. Delfin does not come
 with any media, you must connect to a Jellyfin server. Plugins must be installed and
 configured on your server to be available from Delfin.

[Source](https://codeberg.org/avery42/delfin/raw/branch/main/data/cafe.avery.Delfin.metainfo.xml.in)

### Notice

Please note that this is only the rating is only preliminary test result based on the login screen. 
 If you have a Jellyfin server, please test this app and report back how well it fits the screen 
 and works with touch.

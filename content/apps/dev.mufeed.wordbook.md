+++
title = "Wordbook"
description = "Lookup definitions for any English term"
aliases = [ "apps/com.github.fushinari.wordbook",]
date = 2021-03-12
updated = 2023-12-31

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Mufeed Ali",]
categories = [ "dictionary",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "TextTools", "Dictionary",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = [ "requires first-run",]
tags = []

[extra]
repository = "https://github.com/mufeedali/Wordbook"
homepage = "https://github.com/mufeedali/Wordbook"
bugtracker = "https://github.com/mufeedali/Wordbook/issues"
donations = "https://liberapay.com/mufeedali/donate"
translations = ""
more_information = []
summary_source_url = "https://github.com/mufeedali/Wordbook"
screenshots = [ "https://raw.githubusercontent.com/mufeedali/Wordbook/main/images/ss.png", "https://raw.githubusercontent.com/mufeedali/Wordbook/main/images/ss1.png", "https://raw.githubusercontent.com/mufeedali/Wordbook/main/images/ss2.png", "https://raw.githubusercontent.com/mufeedali/Wordbook/main/images/ss3.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "dev.mufeed.Wordbook"
scale_to_fit = ""
flathub = "https://flathub.org/apps/dev.mufeed.Wordbook"
flatpak_link = ""
flatpak_recipe = "https://raw.githubusercontent.com/mufeedali/Wordbook/main/build-aux/flatpak/dev.mufeed.Wordbook.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "wordbook",]
appstream_xml_url = "https://raw.githubusercontent.com/mufeedali/Wordbook/main/data/dev.mufeed.Wordbook.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description
Wordbook is an offline English-English dictionary application powered by WordNet and eSpeak.

Features:

* Fully offline after initial data download
* Pronunciations
* Random Word
* Live Search
* Double click to search
* Dark mode

[Source](https://raw.githubusercontent.com/mufeedali/Wordbook/main/data/dev.mufeed.Wordbook.metainfo.xml.in)

### Notice 
GTK3/libadwaita until 0.2.0, GTK4/libadwaita since 0.4.0.

+++
title = "Packuru Desktop"
description = "Archiving utility"
aliases = [ "apps/noappid.kyeastmood.packuru/",]
date = 2020-11-28
updated = 2024-01-02

[taxonomies]
project_licenses = [ "BSD-2-Clause",]
metadata_licenses = [ "MIT",]
app_author = [ "kyeastmood",]
categories = [ "file management",]
mobile_compatibility = [ "needs testing",]
status = []
frameworks = [ "Kirigami", "QtQuick",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Archiving", "Compression", "KDE", "Qt", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "qmake",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/kyeastmood/packuru"
homepage = "https://gitlab.com/kyeastmood/packuru"
bugtracker = "https://gitlab.com/kyeastmood/packuru/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/kyeastmood/packuru/-/raw/master/integration/freedesktop.org/org.packuru.PackuruMobile.metainfo.xml"
screenshots = [ "https://gitlab.com/kyeastmood/packuru-screenshots/-/raw/master/mobile/browser-encrypted.png", "https://gitlab.com/kyeastmood/packuru-screenshots/-/raw/master/mobile/browser-global-menu.png", "https://gitlab.com/kyeastmood/packuru-screenshots/-/raw/master/mobile/browser-in-progress.png", "https://gitlab.com/kyeastmood/packuru-screenshots/-/raw/master/mobile/browser-properties.png", "https://gitlab.com/kyeastmood/packuru-screenshots/-/raw/master/mobile/browser.png", "https://gitlab.com/kyeastmood/packuru-screenshots/-/raw/master/mobile/create-new-2-archive.png", "https://gitlab.com/kyeastmood/packuru-screenshots/-/raw/master/mobile/create-new-3-options.png", "https://gitlab.com/kyeastmood/packuru-screenshots/-/raw/master/mobile/extract-2-options.png", "https://gitlab.com/kyeastmood/packuru-screenshots/-/raw/master/mobile/queue.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.packuru.PackuruMobile"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://gitlab.com/kyeastmood/packuru/-/raw/master/integration/freedesktop.org/org.packuru.PackuruMobile.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Packuru is an archive manager supporting a wide range of archive formats, batch processing and strong encryption.


Features:


* 2 complementary programs: Browser and Queue
* Batch processing in Queue
* Tabbed browsing
* View archive properties
* Create, extract and test archives; add and delete files; (depends on archive type and backend capabilities)
* Create separate archive for each input item
* Multipart archive support: 7z, tar/lzip, zip
* Strong encryption support: 7z, zip, zpaq
* Set plugin-specific options when creating archives or adding files
* Restart aborted/failed tasks
* View error log of failed tasks
* Plugin selection for each archive type
* File manager integration: KDE/Dolphin


Supported archive types:


Write:


7z, tar (brotli, bzip2, compress, gzip, lrzip, lz4, lzip, lzma, lzop, xz, zstd), zip, zpaq


All:


7z, ace, appimage, ar, arc, arj, bin/cue, bzip2, cab, chm, compress, cpio, cramfs, deb, dmg, ext, fat, gzip, img/ccd, iso9660, lha, lzip, lzma, lzop, mbr, mdf/mds, mdx, msi, nrg, ntfs, pak, pdf, qcow, rar (cbr), rpm, sit/sitx, squashfs (snap), swf, tar (brotli, bzip2, compress, gzip, lrzip, lz4, lzip, lzma, lzop, xz, zstd), udf, vdi, vhd, vmdk, wim, xar, xz, zip (apk, cbz, ear, ipa, jar, war, xpi, zipx), zpaq

[Source](https://gitlab.com/kyeastmood/packuru/-/raw/master/integration/freedesktop.org/org.packuru.PackuruMobile.metainfo.xml)
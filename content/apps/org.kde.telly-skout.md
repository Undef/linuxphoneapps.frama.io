+++
title = "Telly Skout"
description = "A convergent Kirigami TV guide"
aliases = []
date = 2021-08-14
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "plata",]
categories = [ "tv guide",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "Kirigami",]
backends = []
services = [ "xmltv.se",]
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "AudioVideo", "KDE", "Qt", "TV", "Video",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/utilities/telly-skout"
homepage = "https://apps.kde.org/telly-skout"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=Telly%20Skout"
donations = "https://kde.org/community/donations"
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/utilities/telly-skout/-/raw/master/org.kde.telly-skout.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/telly-skout/favorites.png", "https://cdn.kde.org/screenshots/telly-skout/select-favorites.png", "https://cdn.kde.org/screenshots/telly-skout/sort-favorites.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/org.kde.telly-skout/1.png", "https://img.linuxphoneapps.org/org.kde.telly-skout/2.png", "https://img.linuxphoneapps.org/org.kde.telly-skout/3.png", "https://img.linuxphoneapps.org/org.kde.telly-skout/4.png",]
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.telly-skout"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "telly-skout",]
appstream_xml_url = "https://invent.kde.org/utilities/telly-skout/-/raw/master/org.kde.telly-skout.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Telly Skout is a convergent Kirigami TV guide. It shows the TV program for your favorite channels from TV Spielfilm or an XMLTV file.

[Source](https://invent.kde.org/utilities/telly-skout/-/raw/master/org.kde.telly-skout.appdata.xml)

+++
title = "Space Launch"
description = "When will the next rocket soar to the skies?"
aliases = []
date = 2021-11-21
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Emilien Lescoute",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "spacelaunchnow.me",]
packaged_in = [ "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Utility",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/elescoute/spacelaunch"
homepage = "https://gitlab.com/elescoute/spacelaunch"
bugtracker = "https://gitlab.com/elescoute/spacelaunch/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/org.emilien.SpaceLaunch"
screenshots = [ "https://gitlab.com/elescoute/spacelaunch/raw/main/data/appdata/screenshot01.png", "https://gitlab.com/elescoute/spacelaunch/raw/main/data/appdata/screenshot02.png", "https://gitlab.com/elescoute/spacelaunch/raw/main/data/appdata/screenshot03.png", "https://gitlab.com/elescoute/spacelaunch/raw/main/data/appdata/screenshot04.png", "https://gitlab.com/elescoute/spacelaunch/raw/main/data/appdata/screenshot05.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.emilien.SpaceLaunch"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.emilien.SpaceLaunch"
flatpak_link = ""
flatpak_recipe = "https://gitlab.com/elescoute/spacelaunch/-/raw/main/build-aux/flatpak/org.emilien.SpaceLaunch.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://gitlab.com/elescoute/spacelaunch/-/raw/main/data/org.emilien.SpaceLaunch.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

With Space Launch you can keep track of upcoming launches.


Data on the launches are provided by spacelaunchnow.me.


Space Launch is currently in alpha release. Some crashes due to problems of communication with spacelaunchnow.me server can remain. More features will be added later.


Compatible with GNOME Mobile (PinePhone, Librem 5).

[Source](https://gitlab.com/elescoute/spacelaunch/-/raw/main/data/org.emilien.SpaceLaunch.metainfo.xml.in)

### Notice

This app has a great custom animation while loading data.
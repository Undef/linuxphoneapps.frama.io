+++
title = "Francis"
description = "Track your time"
aliases = []
date = 2023-09-24
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Felipe Kinoshita", "KDE Community",]
categories = [ "productivity",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "Kirigami", "QtQuick",]
backends = []
services = []
packaged_in = [ "alpine_edge", "aur", "gnuguix", "opensuse_tumbleweed",]
freedesktop_categories = [ "KDE", "Qt", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/utilities/francis"
homepage = "http://apps.kde.org/francis"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=Francis"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/utilities/francis/-/raw/master/org.kde.francis.metainfo.xml"
screenshots = [ "https://cdn.kde.org/screenshots/francis/main.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.kde.francis"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://invent.kde.org/utilities/francis/-/raw/master/org.kde.francis.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "francis",]
appstream_xml_url = "https://invent.kde.org/utilities/francis/-/raw/master/org.kde.francis.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Francis uses the well-known pomodoro technique to help you get more productive.

[Source](https://invent.kde.org/utilities/francis/-/raw/master/org.kde.francis.metainfo.xml)

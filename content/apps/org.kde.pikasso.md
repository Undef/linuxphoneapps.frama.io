+++
title = "Pikasso"
description = "Drawing Program"
aliases = []
date = 2021-02-03
updated = 2024-01-02

[taxonomies]
project_licenses = [ "LGPl-2.1-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE",]
categories = [ "drawing",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami", "QtQuick",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "Qt", "KDE", "Graphics", "RasterGraphics",]
programming_languages = [ "Cpp", "Rust", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/graphics/pikasso"
homepage = ""
bugtracker = "https://invent.kde.org/graphics/pikasso/-/issues/"
donations = ""
translations = ""
more_information = [ "https://carlschwan.eu/2021/01/25/pikasso-a-simple-drawing-application-in-qtquick-with-rust/",]
summary_source_url = "https://invent.kde.org/graphics/pikasso/-/raw/master/org.kde.pikasso.appdata.xml"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.pikasso"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "pikasso",]
appstream_xml_url = "https://invent.kde.org/graphics/pikasso/-/raw/master/org.kde.pikasso.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Pikasso is a simple drawing program for Plasma and Plasma Mobile.


Pikasso provides a few interesting features:


* Free form drawing
* Draw rectangles and ellipses
* SVG export
* Undo

[Source](https://invent.kde.org/graphics/pikasso/-/raw/master/org.kde.pikasso.appdata.xml)

+++
title = "Lingot"
description = "Universal tuner"
aliases = []
date = 2020-10-24
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-2.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "ibancg",]
categories = [ "musical tool",]
mobile_compatibility = [ "4",]
status = [ "inactive", "mature",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "gentoo", "gnuguix", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Education", "Music",]
programming_languages = [ "C",]
build_systems = [ "make",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/ibancg/lingot"
homepage = "https://www.nongnu.org/lingot/"
bugtracker = "https://github.com/ibancg/lingot/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/ibancg/lingot/master/org.nongnu.lingot.appdata.xml"
screenshots = [ "https://www.nongnu.org/lingot/images/lingot-screenshot-896x504.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.nongnu.lingot"
scale_to_fit = "lingot"
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "lingot",]
appstream_xml_url = "https://raw.githubusercontent.com/ibancg/lingot/master/org.nongnu.lingot.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

LINGOT is a musical instrument tuner. 
 It's accurate, easy to use, and highly configurable. 
 Originally conceived to tune electric guitars, it can 
 now be used to tune other instruments.


It looks like an analogue tuner, with a gauge indicating 
 the relative shift to a certain note, determined 
 automatically as the closest note to the estimated frequency.


* It's free software. LINGOT is distributed under the GPL license.
* It's really quick and accurate, perfect for real-time microtonal tuning.
* Easy to use. Just plug in your instrument and run it.
* LINGOT is a universal tuner. It can tune many musical instruments, you only need to provide the temperaments.
* Highly configurable via GUI. It’s possible to change any parameter while the program is running, without editing any file.

[Source](https://raw.githubusercontent.com/ibancg/lingot/master/org.nongnu.lingot.appdata.xml)

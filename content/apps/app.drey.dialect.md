+++
title = "Dialect"
description = "Translate between languages"
aliases = [ "apps/com.github.gi_lom.dialect/",]
date = 2020-10-16
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The Dialect Authors",]
categories = [ "translation tool",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Google Translate", "Libre Translate",]
packaged_in = [ "aur", "debian_12", "debian_13", "debian_unstable", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gnuguix", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "GTK", "Network",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/dialect-app/dialect/"
homepage = "https://dialectapp.org/"
bugtracker = "https://github.com/dialect-app/dialect/issues/"
donations = "https://opencollective.com/dialect/"
translations = "https://hosted.weblate.org/engage/dialect/"
more_information = [ "https://apps.gnome.org/app/app.drey.Dialect/",]
summary_source_url = "https://raw.githubusercontent.com/dialect-app/dialect/main/data/app.drey.Dialect.metainfo.xml.in.in"
screenshots = [ "https://raw.githubusercontent.com/dialect-app/dialect/main/preview-2.png", "https://raw.githubusercontent.com/dialect-app/dialect/main/preview-mobile.png", "https://raw.githubusercontent.com/dialect-app/dialect/main/preview.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "app.drey.Dialect"
scale_to_fit = ""
flathub = "https://flathub.org/apps/app.drey.Dialect"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "dialect",]
appstream_xml_url = "https://raw.githubusercontent.com/dialect-app/dialect/main/data/app.drey.Dialect.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

A translation app for GNOME.


Features:


* Translation based on Google Translate
* Translation based on the LibreTranslate API, allowing you to use any public instance
* Translation based on Lingva Translate API
* Translation based on Bing
* Translation based on Yandex
* Text to speech
* Translation history
* Automatic language detection
* Clipboard buttons

[Source](https://raw.githubusercontent.com/dialect-app/dialect/main/data/app.drey.Dialect.metainfo.xml.in.in)

### Notice

Supports Libre Translate since release 1.2.0, GTK4/libadwaita since release 2.0.

+++
title = "What IP"
description = "Info on your IP"
aliases = []
date = 2020-08-26
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Gabriele Musco",]
categories = [ "network",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "GTK", "Network", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/GabMus/whatip"
homepage = "https://whatip.gabmus.org"
bugtracker = "https://gitlab.gnome.org/gabmus/whatip/-/issues"
donations = "https://liberapay.com/gabmus/donate"
translations = "https://gitlab.gnome.org/gabmus/whatip/-/tree/master/po"
more_information = []
summary_source_url = "https://gitlab.gnome.org/GabMus/whatip"
screenshots = [ "https://gitlab.gnome.org/GabMus/whatip/-/raw/website/static/screenshots/lan.png", "https://gitlab.gnome.org/GabMus/whatip/-/raw/website/static/screenshots/mainwindow.png", "https://gitlab.gnome.org/GabMus/whatip/-/raw/website/static/screenshots/ports.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.gabmus.whatip"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gabmus.whatip"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "whatip",]
appstream_xml_url = "https://gitlab.gnome.org/GabMus/whatip/-/raw/master/data/org.gabmus.whatip.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

🌐️ Get your IP easily: Be it local, public or a virtual interface's, it's easy to understand and one click away


🔒️ Make sure your VPN is working: What IP shows your location based on your IP address, so that you can make sure your VPN is working


🧪 Test your ports: List the ports listening on your system, and check if they're publicly reachable


🖧 Discover devices on your LAN: List all devices on your LAN and easily copy their addresses

[Source](https://gitlab.gnome.org/GabMus/whatip/-/raw/master/data/org.gabmus.whatip.appdata.xml.in)

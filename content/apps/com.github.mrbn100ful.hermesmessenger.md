+++
title = "Hermes Messenger"
description = "Unofficial Fb Messenger app for linux smartphone"
aliases = []
date = 2021-05-08
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "mrbn100ful",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = [ "archived",]
frameworks = [ "Electron",]
backends = []
services = [ "Facebook Messenger", "Messenger.com",]
packaged_in = []
freedesktop_categories = [ "GTK", "Network", "InstantMessaging", "Chat",]
programming_languages = [ "C", "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/linuxphoneapps/HermesMessenger"
homepage = ""
bugtracker = "https://github.com/linuxphoneapps/HermesMessenger/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/linuxphoneapps/HermesMessenger"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "com.github.mrbn100ful.hermesmessenger.desktop"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://raw.githubusercontent.com/linuxphoneapps/HermesMessenger/master/com.github.mrbn100ful.hermesmessenger.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/linuxphoneapps/HermesMessenger/master/data/com.github.mrbn100ful.hermesmessenger.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Hermes Messenger is a unofficial Gtk WebKit Facebook messenger app made for linux smartphone (eg: librem 5, pinephone). It's based on the Web version of Fb messenger with some hacks to run properly on a smartphone screen.

[Source](https://github.com/linuxphoneapps/HermesMessenger)

### Notice

Original repo was removed, URLs were updated to a fork of a still existing fork.
Rewrite to GTK WebKit is in progress, check releases to download prebuilt packages.
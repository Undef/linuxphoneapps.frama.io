+++
title = "Kairo"
description = "Sport Training Timer Application"
aliases = []
date = 2021-02-03
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-2.0-only", "GPL-3.0-only", "LicenseRef-KDE-Accepted-GPL",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "utilities",]
categories = [ "timer",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami", "QtQuick",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "Qt", "KDE", "Utility", "Clock",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/utilities/kairo"
homepage = "https://community.kde.org/Kairo"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=kairo"
donations = "https://www.kde.org/community/donations"
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/utilities/kairo/-/raw/master/org.kde.kairo.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/kairo/kairo_blue_timer.png", "https://cdn.kde.org/screenshots/kairo/kairo_free_timer.png", "https://cdn.kde.org/screenshots/kairo/kairo_timers_list.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.kairo.desktop"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "kairo",]
appstream_xml_url = "https://invent.kde.org/utilities/kairo/-/raw/master/org.kde.kairo.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

An application to help doing sport exercises to train (bodybuilding, stretching, ...) using timers to help you.

[Source](https://invent.kde.org/utilities/kairo/-/raw/master/org.kde.kairo.appdata.xml)

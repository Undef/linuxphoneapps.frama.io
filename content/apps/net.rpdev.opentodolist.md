+++
title = "OpenTodoList"
description = "Todo list and note taking application"
aliases = []
date = 2021-06-15
updated = 2023-12-31

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Martin Höher",]
categories = [ "productivity",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "QtQuick",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "Qt", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/rpdev/opentodolist"
homepage = "https://opentodolist.rpdev.net/"
bugtracker = "https://gitlab.com/rpdev/opentodolist/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/rpdev/opentodolist/-/raw/development/net.rpdev.OpenTodoList.appdata.xml"
screenshots = [ "https://opentodolist.rpdev.net/images/appstream/library_page.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "net.rpdev.OpenTodoList"
scale_to_fit = ""
flathub = "https://flathub.org/apps/net.rpdev.OpenTodoList"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "opentodolist",]
appstream_xml_url = "https://gitlab.com/rpdev/opentodolist/-/raw/development/net.rpdev.OpenTodoList.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description
OpenTodoList is a todo list and note taking application. It is
 available for Linux, macOS and Windows as well as on Android.
 Your information can be shared across devices to keep everything
 up date date, where ever you go.

* Organize todo lists, notes and other items in libraries
* Synchronize out-of-the-box with NextCloud, ownCloud and other WebDAV
 servers
* Use sync clients of your choice to synchronize with arbitrary
 services
* Schedule items by setting due dates on them
* Use Markdown in item titles and descriptions to format text
 as you like

[Source](https://gitlab.com/rpdev/opentodolist/-/raw/development/net.rpdev.OpenTodoList.appdata.xml)

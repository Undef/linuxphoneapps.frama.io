+++
title = "Metronome"
description = "Keep the tempo"
aliases = []
date = 2021-08-11
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Adrien Plazas",]
categories = [ "musical tool",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_edge", "aur", "debian_11", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "flathub",]
freedesktop_categories = [ "GNOME", "GTK", "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/World/metronome"
homepage = "https://gitlab.gnome.org/World/metronome"
bugtracker = "https://gitlab.gnome.org/World/metronome/issues"
donations = "https://liberapay.com/adrienplazas"
translations = "https://l10n.gnome.org/module/metronome/"
more_information = []
summary_source_url = "https://gitlab.gnome.org/World/metronome/-/raw/master/data/com.adrienplazas.Metronome.metainfo.xml.in.in"
screenshots = [ "https://gitlab.gnome.org/World/metronome/raw/1.2.1/data/resources/screenshots/screenshot1.png", "https://gitlab.gnome.org/World/metronome/raw/1.2.1/data/resources/screenshots/screenshot2.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/com.adrienplazas.metronome/1.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "com.adrienplazas.Metronome"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.adrienplazas.Metronome"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-metronome",]
appstream_xml_url = "https://gitlab.gnome.org/World/metronome/-/raw/master/data/com.adrienplazas.Metronome.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Metronome beats the rhythm for you, you simply need to tell it the required time signature and beats per minute.


You can also tap to let the application guess the required beats per minute.

[Source](https://gitlab.gnome.org/World/metronome/-/raw/master/data/com.adrienplazas.Metronome.metainfo.xml.in.in)

+++
title = "Cozy"
description = "Listen to audio books"
aliases = []
date = 2020-10-15
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Julian Geywitz",]
categories = [ "audiobook player",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "aur", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gnuguix", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "GTK", "GNOME", "Audio", "Player",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/geigi/cozy/"
homepage = "https://cozy.sh"
bugtracker = "https://github.com/geigi/cozy/issues"
donations = "https://www.patreon.com/geigi"
translations = "https://www.transifex.com/geigi/cozy/"
more_information = [ "https://apps.gnome.org/app/com.github.geigi.cozy/",]
summary_source_url = "https://raw.githubusercontent.com/geigi/cozy/master/data/com.github.geigi.cozy.appdata.xml"
screenshots = [ "https://raw.githubusercontent.com/geigi/cozy/img/img/screenshot1.png", "https://raw.githubusercontent.com/geigi/cozy/img/img/screenshot2.png", "https://raw.githubusercontent.com/geigi/cozy/img/img/screenshot3.png", "https://raw.githubusercontent.com/geigi/cozy/img/img/screenshot4.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/com.github.geigi.cozy/1.png", "https://img.linuxphoneapps.org/com.github.geigi.cozy/2.png", "https://img.linuxphoneapps.org/com.github.geigi.cozy/3.png", "https://img.linuxphoneapps.org/com.github.geigi.cozy/4.png", "https://img.linuxphoneapps.org/com.github.geigi.cozy/5.png", "https://img.linuxphoneapps.org/com.github.geigi.cozy/6.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "com.github.geigi.cozy"
scale_to_fit = "com.github.geigi.cozy"
flathub = "https://flathub.org/apps/com.github.geigi.cozy"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "cozy-audiobooks",]
appstream_xml_url = "https://raw.githubusercontent.com/geigi/cozy/master/data/com.github.geigi.cozy.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Do you like audio books? Then lets get cozy!


Cozy is a audio book player. Here are some of the features:


* Import all your audio books into Cozy to browse them comfortably
* Listen to your DRM free mp3, m4b, m4a (aac, ALAC, …), flac, ogg and wav audio books
* Remembers your playback position
* Sleep timer
* Playback speed control for each book individually
* Search your library
* Multiple storage location support
* Offline Mode! This allows you to keep an audio book on your internal storage if you store your audio books on an external or network drive. Perfect to listen to on the go!
* Drag and Drop to import new audio books
* Sort your audio books by author, reader and name

[Source](https://raw.githubusercontent.com/geigi/cozy/master/data/com.github.geigi.cozy.appdata.xml)

### Notice

The app works great out of the box with release 1.20.

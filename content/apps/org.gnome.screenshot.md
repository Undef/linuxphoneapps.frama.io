+++
title = "Screenshot"
description = "Save images of your screen or individual windows"
aliases = []
date = 2021-08-07
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "GNOME Developers",]
categories = [ "utilities",]
mobile_compatibility = [ "4",]
status = [ "mature", "inactive",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnome-screenshot"
homepage = "https://gitlab.gnome.org/GNOME/gnome-screenshot"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-screenshot/issues"
donations = "http://www.gnome.org/friends/"
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/GNOME/gnome-screenshot/-/raw/master/data/org.gnome.Screenshot.metainfo.xml.in"
screenshots = [ "https://gitlab.gnome.org/GNOME/gnome-screenshot/-/raw/master/data/screenshots/gnome-screenshot-3.38-1.png", "https://gitlab.gnome.org/GNOME/gnome-screenshot/-/raw/master/data/screenshots/gnome-screenshot-3.38-2.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.gnome.Screenshot"
scale_to_fit = "gnome-screenshots"
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://gitlab.gnome.org/GNOME/gnome-screenshot/-/blob/master/org.gnome.Screenshot.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-screenshot",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-screenshot/-/raw/master/data/org.gnome.Screenshot.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Screenshot is a simple utility that lets you take pictures of your computer screen.
 Screenshots can be of your whole screen, any specific application, or a selected
 rectangular area.
 You can also copy the captured screenshot directly into the GNOME clipboard and
 paste it into other applications.


Screenshot allows you to take screenshots even when it’s not open: just
 press the PrtSc button on your keyboard, and a snapshot of your whole screen will
 be saved to your Pictures folder.
 Hold Alt while pressing PrtSc and you will get a screenshot of only the currently
 selected window.

[Source](https://gitlab.gnome.org/GNOME/gnome-screenshot/-/raw/master/data/org.gnome.Screenshot.metainfo.xml.in)

### Notice

Works for "Screen" capture area on Phosh 0.12.1 with release 40. Needs scale-to-fit.

+++
title = "MiTubo"
description = "Playback or download media content from video streaming websites"
aliases = []
date = 2021-09-29
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "FSFAP",]
app_author = [ "mardy",]
categories = [ "video player",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "QtQuick",]
backends = [ "gstreamer", "youtube-dl",]
services = [ "YouTube",]
packaged_in = []
freedesktop_categories = [ "Qt", "Network", "Feed", "AudioVideo", "Player",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "qbs",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/mardy/mitubo"
homepage = "http://mardy.it/mitubo"
bugtracker = "https://gitlab.com/mardy/mitubo/-/issues/"
donations = "https://gitlab.com/mardy/mitubo#contribute"
translations = ""
more_information = [ "https://www.mardy.it/blog/2021/09/mitubo-03-brings-basic-rss-support.html", "https://open-store.io/app/it.mardy.mitubo", "https://www.mardy.it/blog/2022/04/mitubo-update-search-channels-watch-later-queue.html",]
summary_source_url = "https://gitlab.com/mardy/mitubo/-/raw/master/data/it.mardy.mitubo.metainfo.xml"
screenshots = [ "https://www.mardy.it/archivos/imagines/mitubo/mainpage-0.9.png", "https://www.mardy.it/archivos/imagines/mitubo/playback-0.9.jpg",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "it.mardy.mitubo"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://gitlab.com/mardy/mitubo/-/raw/master/data/it.mardy.mitubo.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

MiTubo is a desktop application which allows one to watch videos from
 streaming websites, free of distractions and advertisement. It also lets
 one search for videos using different backends, and has support for
 subscribing to RSS video feeds and YouTube channels.


Features:


* Playback video from exact URL
* Video search (Yandex, Invidious, PeerTube)
* Organize videos into playlists
* Subscribe to RSS video feeds (including YouTube channels)
* Organize video feed subscriptions into folders
* Download media (with selection of video/audio quality)
* Drag URLs during playback to enqueue them in the “Watch later” list


MiTubo internally it uses the yt-dlp program to extract the video stream
 information from web pages.

[Source](https://gitlab.com/mardy/mitubo/-/raw/master/data/it.mardy.mitubo.metainfo.xml)

### Notice

Originally for Ubuntu Touch, this app works on other distributions too.
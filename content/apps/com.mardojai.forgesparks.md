+++
title = "Forge Sparks"
description = "Get Git forges notifications"
aliases = []
date = 2024-02-11

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Rafael Mardojai CM",]
categories = [ "development",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Forgejo", "Gitea", "GitHub",]
packaged_in = [ "flathub",]
freedesktop_categories = [ "Network",]
programming_languages = [ "JavaScript",]
build_systems = [ "Meson",]
requires_internet = []
tags = [ "GNOME Circle",]

[extra]
repository = "https://github.com/rafaelmardojai/forge-sparks"
homepage = "https://github.com/rafaelmardojai/forge-sparks"
bugtracker = "https://github.com/rafaelmardojai/forge-sparks/issues"
donations = "https://mardojai.com/donate/"
translations = "https://hosted.weblate.org/engage/forge-sparks/"
more_information = [ "https://apps.gnome.org/ForgeSparks/",]
summary_source_url = "https://flathub.org/apps/com.mardojai.ForgeSparks"
screenshots = [ "https://raw.githubusercontent.com/rafaelmardojai/forge-sparks/main/data/screenshots/1.png", "https://raw.githubusercontent.com/rafaelmardojai/forge-sparks/main/data/screenshots/2.png", "https://raw.githubusercontent.com/rafaelmardojai/forge-sparks/main/data/screenshots/3.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "com.mardojai.ForgeSparks"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.mardojai.ForgeSparks"
flatpak_link = "https://flathub.org/apps/com.mardojai.ForgeSparks.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/rafaelmardojai/forge-sparks/main/com.mardojai.ForgeSparksDevel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/rafaelmardojai/forge-sparks/main/data/com.mardojai.ForgeSparks.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = ""

+++


### Description

Simple notifier app with support for Github, Gitea and Forgejo.

[Source](https://raw.githubusercontent.com/rafaelmardojai/forge-sparks/main/data/com.mardojai.ForgeSparks.metainfo.xml.in.in)

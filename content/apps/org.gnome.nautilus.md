+++
title = "Files"
description = "Access and organize files"
aliases = []
date = 2022-10-07
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The GNOME Project",]
categories = [ "file management",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "System",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/GNOME/nautilus"
homepage = "https://apps.gnome.org/Nautilus/"
bugtracker = "https://gitlab.gnome.org/GNOME/nautilus/issues"
donations = "https://www.gnome.org/donate/"
translations = "https://wiki.gnome.org/TranslationProject"
more_information = [ "https://wiki.gnome.org/action/show/Apps/Nautilus",]
summary_source_url = "https://gitlab.gnome.org/GNOME/nautilus/-/raw/main/data/org.gnome.Nautilus.appdata.xml.in.in"
screenshots = [ "https://static.gnome.org/appdata/gnome-43/files/files-grid.png", "https://static.gnome.org/appdata/gnome-43/files/files-list.png", "https://static.gnome.org/appdata/gnome-43/files/files-other-locations.png", "https://static.gnome.org/appdata/gnome-43/files/files-search.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/org.gnome.nautilus/1.png", "https://img.linuxphoneapps.org/org.gnome.nautilus/2.png", "https://img.linuxphoneapps.org/org.gnome.nautilus/3.png", "https://img.linuxphoneapps.org/org.gnome.nautilus/4.png", "https://img.linuxphoneapps.org/org.gnome.nautilus/5.png", "https://img.linuxphoneapps.org/org.gnome.nautilus/6.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.Nautilus"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "nautilus",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/nautilus/-/raw/main/data/org.gnome.Nautilus.appdata.xml.in.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Files, also known as Nautilus, is the default file manager of the GNOME desktop.
 It provides a simple and integrated way of managing your files and browsing your file system.


Nautilus supports all the basic functions of a file manager and more.
 It can search and manage your files and folders, both locally and on a network,
 read and write data to and from removable media, run scripts, and launch apps.
 It has three views: Icon Grid, Icon List, and Tree List.
 Its functions can be extended with plugins and scripts.

[Source](https://gitlab.gnome.org/GNOME/nautilus/-/raw/main/data/org.gnome.Nautilus.appdata.xml.in.in)

### Notice

Adaptive since release 43. Not every dialog is adaptive yet, see screenshots.

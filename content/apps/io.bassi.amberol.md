+++
title = "Amberol"
description = "Plays music, and nothing else"
aliases = []
date = 2022-04-24
updated = 2024-01-14

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Emmanuele Bassi",]
categories = [ "music player",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "aur", "debian_13", "debian_unstable", "devuan_unstable", "flathub", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Audio", "Player",]
programming_languages = [ "Rust",]
build_systems = [ "meson", "cargo",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/World/amberol"
homepage = "https://gitlab.gnome.org/World/amberol"
bugtracker = "https://gitlab.gnome.org/World/amberol/-/issues"
donations = "https://ko-fi.com/emmanueleb"
translations = "https://l10n.gnome.org/module/amberol/"
more_information = [ "https://apps.gnome.org/app/io.bassi.Amberol/", "https://www.omgubuntu.co.uk/2022/04/amberol-simple-gtk-music-player/amp", "https://www.youtube.com/watch?v=P5YMekBLOns", "https://www.youtube.com/watch?v=J_GyS6zhyZk",]
summary_source_url = "https://gitlab.gnome.org/World/amberol/-/raw/main/data/io.bassi.Amberol.appdata.xml.in.in"
screenshots = [ "https://www.bassi.io/~ebassi/amberol/screenshots/0.10/amberol-compact.png", "https://www.bassi.io/~ebassi/amberol/screenshots/0.10/amberol-full.png", "https://www.bassi.io/~ebassi/amberol/screenshots/0.10/amberol-playlist.png", "https://www.bassi.io/~ebassi/amberol/screenshots/0.10/amberol-recolor.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/io.bassi.amberol/1.png", "https://img.linuxphoneapps.org/io.bassi.amberol/2.png", "https://img.linuxphoneapps.org/io.bassi.amberol/3.png", "https://img.linuxphoneapps.org/io.bassi.amberol/4.png", "https://img.linuxphoneapps.org/io.bassi.amberol/5.png", "https://img.linuxphoneapps.org/io.bassi.amberol/6.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "io.bassi.Amberol"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.bassi.Amberol"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "amberol",]
appstream_xml_url = "https://gitlab.gnome.org/World/amberol/-/raw/main/data/io.bassi.Amberol.appdata.xml.in.in"
reported_by = "linmob"
updated_by = "check_via_repology"

+++

### Description

Amberol is a music player with no delusions of grandeur. If you just
 want to play music available on your local system then Amberol is the
 music player you are looking for.


Current features:


* adaptive UI
* UI recoloring using the album art
* drag and drop support to queue songs
* shuffle and repeat
* MPRIS integration

[Source](https://gitlab.gnome.org/World/amberol/-/raw/main/data/io.bassi.Amberol.appdata.xml.in.in)
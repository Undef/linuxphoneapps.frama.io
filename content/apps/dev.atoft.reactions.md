+++
title = "Reactions"
description = "Simple GIF search."
aliases = []
date = 2022-03-22
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Alastair Toft",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "inactive", "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Giphy",]
packaged_in = [ "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Network",]
programming_languages = [ "Zig",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://codeberg.org/atoft/Reactions"
homepage = "https://codeberg.org/atoft/Reactions/"
bugtracker = "https://codeberg.org/atoft/Reactions/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://codeberg.org/atoft/Reactions/raw/branch/main/data/dev.atoft.Reactions.metainfo.xml.in"
screenshots = [ "https://codeberg.org/atoft/Reactions/raw/branch/main/data/screenshots/main.png", "https://codeberg.org/atoft/Reactions/raw/branch/main/data/screenshots/results.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "dev.atoft.Reactions"
scale_to_fit = ""
flathub = "https://flathub.org/apps/dev.atoft.Reactions"
flatpak_link = ""
flatpak_recipe = "https://codeberg.org/atoft/Reactions/raw/branch/main/dev.atoft.Reactions.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://codeberg.org/atoft/Reactions/raw/branch/main/data/dev.atoft.Reactions.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Search for the perfect GIF with Reactions!


* Search for reaction images, provided by Giphy.
* Drag-and-drop images to other apps, in GIF or MP4 formats.

[Source](https://codeberg.org/atoft/Reactions/raw/branch/main/data/dev.atoft.Reactions.metainfo.xml.in)

### Notice

Inactive, last commit 2022-07-31.
Gifs/content do not display successfully (tested with Flathub build on Librem 5/PureOS Byzantium).
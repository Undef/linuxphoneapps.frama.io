+++
title = "AudioTube"
description = "YouTube Music App for phones and desktop computers"
aliases = []
date = 2021-03-14
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community",]
categories = [ "audio streaming",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "Kirigami",]
backends = [ "ytmusicapi", "youtube-dl",]
services = [ "YouTube",]
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "Qt", "KDE", "Network", "Audio", "Player",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/multimedia/audiotube"
homepage = "https://apps.kde.org/en/audiotube"
bugtracker = "https://invent.kde.org/multimedia/audiotube/-/issues/"
donations = ""
translations = ""
more_information = [ "https://jbbgameich.github.io/kde/2021/03/13/audiotube.html", "https://plasma-mobile.org/2021/04/27/plasma-mobile-update-march-april/", "https://plasma-mobile.org/2022/11/30/plasma-mobile-gear-22-11/#audiotube", "https://plasma-mobile.org/2023/01/30/january-blog-post/#audiotube-youtube-music-client",]
summary_source_url = "https://invent.kde.org/multimedia/audiotube/-/raw/master/org.kde.audiotube.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/audiotube/desktop.png", "https://cdn.kde.org/screenshots/audiotube/library.png", "https://cdn.kde.org/screenshots/audiotube/mobile_album.png", "https://cdn.kde.org/screenshots/audiotube/mobile_playlist.png", "https://cdn.kde.org/screenshots/audiotube/player.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = false
app_id = "org.kde.audiotube"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.audiotube"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "audiotube",]
appstream_xml_url = "https://invent.kde.org/multimedia/audiotube/-/raw/master/org.kde.audiotube.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

AudioTube can search YouTube Music, list albums and artists, play automatically generated playlists, albums and allows to put your own playlist together.

[Source](https://invent.kde.org/multimedia/audiotube/-/raw/master/org.kde.audiotube.appdata.xml)

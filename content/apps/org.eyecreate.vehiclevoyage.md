+++
title = "Vehicle Voyage"
description = "Track vehicle service history."
aliases = []
date = 2020-08-25
updated = 2023-09-30

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "FSFAP",]
app_author = [ "eyecreate",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "QtQuick", "Kirigami",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Qt", "KDE", "Utility",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://git.eyecreate.org/eyecreate/vehicle-voyage"
homepage = "https://git.eyecreate.org/eyecreate/vehicle-voyage"
bugtracker = "https://git.eyecreate.org/eyecreate/vehicle-voyage/-/issues/"
donations = "https://liberapay.com/eyecreate/donate"
translations = ""
more_information = []
summary_source_url = "https://git.eyecreate.org/eyecreate/vehicle-voyage/-/raw/master/packaging/org.eyecreate.vehiclevoyage.appdata.xml"
screenshots = [ "https://git.eyecreate.org/eyecreate/vehicle-voyage/raw/v1.0/packaging/main_window.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.eyecreate.vehiclevoyage"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.eyecreate.vehiclevoyage"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://git.eyecreate.org/eyecreate/vehicle-voyage/-/raw/master/packaging/org.eyecreate.vehiclevoyage.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++

### Description
Track vehicle service history.

[Source](https://git.eyecreate.org/eyecreate/vehicle-voyage/-/raw/master/packaging/org.eyecreate.vehiclevoyage.appdata.xml)

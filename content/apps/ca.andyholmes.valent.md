+++
title = "Valent"
description = "Connect, control and sync devices"
aliases = []
date = 2022-06-29
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Andy Holmes",]
categories = [ "connectivity",]
mobile_compatibility = [ "5",]
status = [ "early", "pre-release",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "KDE Connect",]
packaged_in = [ "aur", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Network",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/andyholmes/valent"
homepage = "https://valent.andyholmes.ca"
bugtracker = "https://github.com/andyholmes/valent/issues"
donations = "https://github.com/sponsors/andyholmes"
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/andyholmes/valent/main/data/metainfo/ca.andyholmes.Valent.metainfo.xml.in.in"
screenshots = [ "https://raw.githubusercontent.com/andyholmes/valent/main/data/metainfo/01-devices.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/ca.andyholmes.valent/1.png", "https://img.linuxphoneapps.org/ca.andyholmes.valent/2.png", "https://img.linuxphoneapps.org/ca.andyholmes.valent/3.png", "https://img.linuxphoneapps.org/ca.andyholmes.valent/4.png", "https://img.linuxphoneapps.org/ca.andyholmes.valent/5.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "ca.andyholmes.Valent"
scale_to_fit = ""
flathub = ""
flatpak_link = "https://nightly.link/andyholmes/valent/workflows/cd/main/ca.andyholmes.Valent-aarch64.zip"
flatpak_recipe = "https://raw.githubusercontent.com/andyholmes/valent/main/build-aux/flatpak/ca.andyholmes.Valent.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "valent",]
appstream_xml_url = "https://raw.githubusercontent.com/andyholmes/valent/main/data/metainfo/ca.andyholmes.Valent.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Securely connect your devices to open files and links where you need them,
 get notifications when you need them, stay in control of your media and more.


Features:


* Sync contacts, notifications and clipboard content
* Control media players and volume
* Share files, links and text
* Virtual touchpad and keyboard
* Call and text notification
* Execute custom commands
* More…


Valent is an implementation of the KDE Connect protocol, built on GNOME
 platform libraries.

[Source](https://raw.githubusercontent.com/andyholmes/valent/main/data/metainfo/ca.andyholmes.Valent.metainfo.xml.in.in)

### Notice

The app is already connecting with KDE Connect (on KDE), can ring and send and receive files. On the date of addition, other functionality could not be tested successfully.

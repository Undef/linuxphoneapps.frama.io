+++
title = "Shortcut"
description = "Make app shortcuts"
aliases = []
date = 2024-02-09

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Achim Andrei",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Rust", "Python",]
build_systems = [ "cargo",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/andreibachim/shortcut"
homepage = "https://github.com/andreibachim/shortcut"
bugtracker = "https://github.com/andreibachim/shortcut/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/io.github.andreibachim.shortcut"
screenshots = [ "https://raw.githubusercontent.com/andreibachim/shortcut/main/data/screenshots/screenshot1.png", "https://raw.githubusercontent.com/andreibachim/shortcut/main/data/screenshots/screenshot2.png", "https://raw.githubusercontent.com/andreibachim/shortcut/main/data/screenshots/screenshot3.png", "https://raw.githubusercontent.com/andreibachim/shortcut/main/data/screenshots/screenshot4.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "io.github.andreibachim.shortcut"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.andreibachim.shortcut"
flatpak_link = "https://flathub.org/apps/io.github.andreibachim.shortcut.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/andreibachim/shortcut/main/io.github.andreibachim.shortcut.yml"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/andreibachim/shortcut/main/data/io.github.andreibachim.shortcut.metainfo.xml"
reported_by = "linmob"
updated_by = ""

+++


### Description

Shortcut is a tool that allows users to quickly pin executable files to their app launcher. 
 It guides users throught the process by providing file picker dialogs with relevant filters, input validation, and name and icon previews.

[Source](https://raw.githubusercontent.com/andreibachim/shortcut/main/data/io.github.andreibachim.shortcut.metainfo.xml)

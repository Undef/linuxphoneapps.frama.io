+++
title = "Disks"
description = "Disk management utility for GNOME"
aliases = []
date = 2020-12-12
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The GNOME Project",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnome-disk-utility"
homepage = "https://wiki.gnome.org/Apps/Disks"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-disk-utility/issues"
donations = "https://www.gnome.org/donate/"
translations = "https://wiki.gnome.org/TranslationProject"
more_information = [ "https://apps.gnome.org/app/org.gnome.DiskUtility/",]
summary_source_url = "https://gitlab.gnome.org/GNOME/gnome-disk-utility/-/raw/master/data/org.gnome.DiskUtility.appdata.xml.in"
screenshots = [ "https://gitlab.gnome.org/GNOME/gnome-disk-utility/raw/HEAD/data/screenshots/disks-additional-partition-options.png", "https://gitlab.gnome.org/GNOME/gnome-disk-utility/raw/HEAD/data/screenshots/disks-application-menu.png", "https://gitlab.gnome.org/GNOME/gnome-disk-utility/raw/HEAD/data/screenshots/disks-delete-selected-partition.png", "https://gitlab.gnome.org/GNOME/gnome-disk-utility/raw/HEAD/data/screenshots/disks-drive-options.png", "https://gitlab.gnome.org/GNOME/gnome-disk-utility/raw/HEAD/data/screenshots/disks-main.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.DiskUtility"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-disk-utility",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-disk-utility/-/raw/master/data/org.gnome.DiskUtility.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Disks provides an easy way to inspect, format, partition, and configure disks
 and block devices.


Using Disks, you can view SMART data, manage devices, benchmark physical
 disks, and image USB sticks.

[Source](https://gitlab.gnome.org/GNOME/gnome-disk-utility/-/raw/master/data/org.gnome.DiskUtility.appdata.xml.in)

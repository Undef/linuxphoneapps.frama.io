+++
title = "Mirage"
description = "Customizable and keyboard-operable Matrix client"
aliases = [ "apps/io.github.mirukana.mirage/",]
date = 2020-08-25
updated = 2024-01-02

[taxonomies]
project_licenses = [ "LGPL-3.0-or-later",]
metadata_licenses = [ "FSFAP",]
app_author = [ "mirukana",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = [ "archived",]
frameworks = [ "QtQuick",]
backends = [ "matrix-nio", "libolm",]
services = [ "Matrix",]
packaged_in = [ "aur",]
freedesktop_categories = [ "Chat", "InstantMessaging", "Network", "Qt",]
programming_languages = [ "QML", "Python",]
build_systems = [ "qmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/mirukana/mirage"
homepage = "https://github.com/mirukana/mirage"
bugtracker = "https://github.com/mirukana/mirage/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/mirukana/mirage/master/packaging/mirage.appdata.xml"
screenshots = [ "https://raw.githubusercontent.com/mirukana/mirage/master/docs/screenshots/01-chat.png", "https://raw.githubusercontent.com/mirukana/mirage/master/docs/screenshots/02-sign-in.png", "https://raw.githubusercontent.com/mirukana/mirage/master/docs/screenshots/03-account-settings.png", "https://raw.githubusercontent.com/mirukana/mirage/master/docs/screenshots/04-create-room.png", "https://raw.githubusercontent.com/mirukana/mirage/master/docs/screenshots/05-main-pane-small.png", "https://raw.githubusercontent.com/mirukana/mirage/master/docs/screenshots/06-chat-small.png", "https://raw.githubusercontent.com/mirukana/mirage/master/docs/screenshots/07-room-pane-small.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "io.github.mirakuna.mirage"
scale_to_fit = ""
flathub = ""
flatpak_link = "https://github.com/mirukana/mirage/tree/master/packaging/flatpak"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "matrix-mirage",]
appstream_xml_url = "https://raw.githubusercontent.com/mirukana/mirage/master/packaging/mirage.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

A fancy, customizable, keyboard-operable Qt/QML and Python Matrix chat client for encrypted and decentralized communication.

[Source](https://raw.githubusercontent.com/mirukana/mirage/master/packaging/mirage.appdata.xml)

### Notice

Fits the screen, but sometimes navigating the UI requires non-obvious gestures. Check out [Moment](https://linuxphoneapps.org/apps/xyz.mx_moment.moment/) if you’re looking for a maintained fork of Mirage. Marked as archive as it likely would be - [apparently](https://github.com/mirukana/mirage/pull/290#issuecomment-1793742583), all access rights to the repo apparently have been lost.

+++
title = "Geary"
description = "Send and receive email"
aliases = []
date = 2020-08-25
updated = 2024-01-02

[taxonomies]
project_licenses = [ "LGPL-2.1-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Geary Development Team",]
categories = [ "email",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Office", "Email",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/GNOME/geary"
homepage = "https://wiki.gnome.org/Apps/Geary"
bugtracker = "https://wiki.gnome.org/Apps/Geary/ReportingABug"
donations = "https://wiki.gnome.org/Apps/Geary/Donate"
translations = "https://wiki.gnome.org/Apps/Geary/Translating"
more_information = []
summary_source_url = "https://gitlab.gnome.org/GNOME/geary/-/raw/mainline/desktop/org.gnome.Geary.appdata.xml.in.in"
screenshots = [ "https://wiki.gnome.org/Apps/Geary?action=AttachFile&do=get&target=geary-3-36-composer.png", "https://wiki.gnome.org/Apps/Geary?action=AttachFile&do=get&target=geary-40-conversation.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.Geary"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Geary"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "geary",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/geary/-/raw/mainline/desktop/org.gnome.Geary.appdata.xml.in.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Geary is an email application built around conversations, for
 the GNOME 3 desktop. It allows you to read, find and send email
 with a straightforward, modern interface.


Conversations allow you to read a complete discussion without
 having to find and click from message to message.


Geary’s features include:


* Quick email account setup
* Shows related messages together in conversations
* Fast, full text and keyword search
* Full-featured HTML and plain text message composer
* Desktop notification of new mail
* Compatible with GMail, Yahoo! Mail, Outlook.com and other IMAP servers

[Source](https://gitlab.gnome.org/GNOME/geary/-/raw/mainline/desktop/org.gnome.Geary.appdata.xml.in.in)

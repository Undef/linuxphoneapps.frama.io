+++
title = "Max Control"
description = "Control software for Max! devices"
aliases = []
date = 2019-02-01
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-2.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Jan-Michael Brummer",]
categories = [ "smart home",]
mobile_compatibility = [ "needs testing",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/tabos/maxcontrol"
homepage = "http://www.tabos.org"
bugtracker = "https://gitlab.com/tabos/maxcontrol/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/tabos/maxcontrol/-/raw/master/data/org.tabos.maxcontrol.appdata.xml.in"
screenshots = [ "http://tabos.org/project/maxcontrol/maxcontrol.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.tabos.maxcontrol.desktop"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.tabos.maxcontrol"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "max-control",]
appstream_xml_url = "https://gitlab.com/tabos/maxcontrol/-/raw/master/data/org.tabos.maxcontrol.appdata.xml.in"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

Max Control is a user interface to control and administrate Max! devices, e.g. Max!Cube, Heating device, ...


Add/Remove device, set temperature levels and set modus for rooms and complete house!

[Source](https://gitlab.com/tabos/maxcontrol/-/raw/master/data/org.tabos.maxcontrol.appdata.xml.in)

### Notice

Testing requires a Max!Cube device, help needed.

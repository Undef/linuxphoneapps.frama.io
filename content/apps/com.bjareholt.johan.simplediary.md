+++
title = "Simple Diary"
description = "Simple and lightweight diary app."
aliases = []
date = 2021-04-11
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "MIT",]
app_author = [ "Johan Bjäreholt",]
categories = [ "note taking", "diary",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "Office",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/johan-bjareholt/simple-diary-gtk"
homepage = "https://github.com/johan-bjareholt/simple-diary-gtk"
bugtracker = "https://github.com/johan-bjareholt/simple-diary-gtk/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/johan-bjareholt/simple-diary-gtk"
screenshots = [ "https://johan.bjareholt.com/img/projects/simple-diary/entry_edit.png", "https://johan.bjareholt.com/img/projects/simple-diary/entry_view.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = false
app_id = "com.bjareholt.johan.SimpleDiary"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.bjareholt.johan.SimpleDiary"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "simple-diary-gtk",]
appstream_xml_url = "https://raw.githubusercontent.com/johan-bjareholt/simple-diary-gtk/master/res/com.bjareholt.johan.SimpleDiary.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Designed to be easy to use and be out of the way, to let you focus on documenting and reflecting over your daily life.


A few notable features are:


* Saves entries in standard markdown
* Adding images to your entries
* Scales on desktops, laptops, tablets and phones
* Dark mode

[Source](https://raw.githubusercontent.com/johan-bjareholt/simple-diary-gtk/master/res/com.bjareholt.johan.SimpleDiary.metainfo.xml.in)

### Notice

GTK3/libhandy before 0.3.1.

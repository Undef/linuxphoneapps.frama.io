+++
title = "GNOME Network Displays"
description = "Screencasting for GNOME"
aliases = []
date = 2024-02-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = []
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "libportal",]
services = [ "Miracast", "Chromecast",]
packaged_in = [ "aur", "debian_12", "debian_13", "debian_unstable", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "AudioVideo",]
programming_languages = [ "C",]
build_systems = [ "Meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnome-network-displays"
homepage = "https://gitlab.gnome.org/GNOME/gnome-network-displays"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-network-displays/-/issues"
donations = ""
translations = "https://l10n.gnome.org/module/gnome-network-displays/"
more_information = []
summary_source_url = "https://flathub.org/apps/org.gnome.NetworkDisplays"
screenshots = [ "https://gitlab.gnome.org/GNOME/gnome-network-displays/-/raw/master/data/appdata/device-list.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.gnome.NetworkDisplays"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.NetworkDisplays"
flatpak_link = "https://flathub.org/apps/org.gnome.NetworkDisplays.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-network-displays",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-network-displays/-/raw/master/data/org.gnome.NetworkDisplays.appdata.xml.in"
reported_by = "linmob"
updated_by = ""

+++

### Description

GNOME Network Displays allows you to cast your desktop to a remote display.
 Supports the Miracast and Chromecast protocols.

[Source](https://gitlab.gnome.org/GNOME/gnome-network-displays/-/raw/master/data/org.gnome.NetworkDisplays.appdata.xml.in)

### Notice

No successful use prior to addition (only tested with HP Elite X3 Lap Dock). Use with GNOME Shell recommended for best results.

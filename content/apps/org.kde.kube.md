+++
title = "Kube"
description = "E-mail client"
aliases = []
date = 2019-02-01
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "pim",]
categories = [ "email",]
mobile_compatibility = [ "2",]
status = []
frameworks = [ "QtQuick",]
backends = [ "Sink",]
services = []
packaged_in = [ "archlinuxarm_aarch64", "aur",]
freedesktop_categories = [ "Qt", "KDE", "Office", "Email",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/pim/kube"
homepage = "https://kube-project.com/"
bugtracker = "https://invent.kde.org/pim/kube/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/pim/kube/-/raw/master/applications/kube/kube.appdata.xml.in"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.kube"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "kube", "kube-develop",]
appstream_xml_url = "https://invent.kde.org/pim/kube/-/raw/master/applications/kube/kube.appdata.xml.in"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

Kube is a modern communication and collaboration client built with QtQuick on top of a high performance, low resource usage core. It provides online and offline access to all your mail, contacts, calendars, notes, todo's and more. With a strong focus on usability, the team works with designers and UX experts from the ground up, to build a product that is not only visually appealing but also a joy to use.


Communication and collaboration within large groups and spread over several time zones is complex and something of the norm in modern projects and the existing solutions available are not up to that challenge. Kube is a way to simplify and make communication elegant again by focusing on the key needs of users instead of filling it with any feature that comes to mind. Our strict focus on what is actually needed instead of what is wanted brings a new way of collaboration between group members and a simpler way of handling communication.

[Source](https://invent.kde.org/pim/kube/-/raw/master/applications/kube/kube.appdata.xml.in)

+++
title = "Imaginer"
description = "Imagine with AI"
aliases = []
date = 2023-06-25
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "0xMRTT",]
categories = [ "utilities", "content generator",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "StableDiffusion",]
packaged_in = [ "aur", "flathub", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://codeberg.org/Imaginer/Imaginer"
homepage = "https://imaginer.codeberg.page"
bugtracker = "https://codeberg.org/Imaginer/Imaginer/issues"
donations = ""
translations = "https://translate.codeberg.org/projects/imaginer/imaginer"
more_information = []
summary_source_url = ""
screenshots = [ "https://codeberg.org/Imaginer/Imaginer/raw/branch/master/data/screenshots/avocado-armchair.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/page.codeberg.imaginer.imaginer/1.png", "https://img.linuxphoneapps.org/page.codeberg.imaginer.imaginer/2.png", "https://img.linuxphoneapps.org/page.codeberg.imaginer.imaginer/3.png", "https://img.linuxphoneapps.org/page.codeberg.imaginer.imaginer/4.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "page.codeberg.Imaginer.Imaginer"
scale_to_fit = ""
flathub = "https://flathub.org/apps/page.codeberg.Imaginer.Imaginer"
flatpak_link = "https://codeberg.org/Imaginer/-/packages/generic/imaginer/48"
flatpak_recipe = "https://codeberg.org/Imaginer/Imaginer/raw/branch/master/build-aux/flatpak/page.codeberg.Imaginer.Imaginer.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "imaginer",]
appstream_xml_url = "https://codeberg.org/Imaginer/Imaginer/raw/branch/master/data/page.codeberg.Imaginer.Imaginer.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Generate pictures with Artificial Intelligence and save them

[Source](https://codeberg.org/Imaginer/Imaginer/raw/branch/master/data/page.codeberg.Imaginer.Imaginer.appdata.xml.in)

+++
title = "Kongress"
description = "KDE Conference companion application"
aliases = []
date = 2020-02-06
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community",]
categories = [ "conference companion",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Qt", "KDE", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/utilities/kongress"
homepage = "https://invent.kde.org/utilities/kongress"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=Kongress"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/utilities/kongress/-/raw/master/org.kde.kongress.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/kongress/desktop.png", "https://cdn.kde.org/screenshots/kongress/mobile.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.kongress.desktop"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "kongress",]
appstream_xml_url = "https://invent.kde.org/utilities/kongress/-/raw/master/org.kde.kongress.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

Kongress is a companion application for conferences made by KDE

[Source](https://invent.kde.org/utilities/kongress/-/raw/master/org.kde.kongress.appdata.xml)

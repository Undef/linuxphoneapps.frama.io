+++
title = "Stellarium"
description = "Desktop Planetarium"
aliases = []
date = 2020-10-27
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Stellarium team",]
categories = [ "entertainment",]
mobile_compatibility = [ "3",]
status = []
frameworks = [ "QtWidgets",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Education", "Science",]
programming_languages = [ "Cpp", "C",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/Stellarium/stellarium"
homepage = "https://www.stellarium.org/"
bugtracker = "https://github.com/Stellarium/stellarium/issues"
donations = "https://opencollective.com/stellarium"
translations = "https://app.transifex.com/stellarium/stellarium/"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/Stellarium/stellarium/master/data/org.stellarium.Stellarium.appdata.xml"
screenshots = [ "https://stellarium.org/img/screenshots/0.10-constellations.jpg", "https://stellarium.org/img/screenshots/0.10-from-mars.jpg", "https://stellarium.org/img/screenshots/0.10-orion-nebula.jpg", "https://stellarium.org/img/screenshots/0.10-planets.jpg", "https://www.stellarium.org/img/screenshots/0.10-constellation-art-on.jpg",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.stellarium.Stellarium"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.stellarium.Stellarium"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "stellarium",]
appstream_xml_url = "https://raw.githubusercontent.com/Stellarium/stellarium/master/data/org.stellarium.Stellarium.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Stellarium renders 3D photo-realistic skies in real time with OpenGL. It displays stars, constellations, planets and nebulae, and has many other features including multiple panoramic landscapes, fog, light pollution simulation and a built-in scripting engine.


Stellarium comes with a star catalogue of about 600 thousand stars and it is possible to download extra catalogues with up to 210 million stars.


Stellarium has multiple sky cultures - see the constellations from the traditions of Polynesian, Inuit, Navajo, Korean, Lakota, Egyptian and Chinese astronomers, as well as the traditional Western constellations.


It is also possible to visit other planets in the solar system - see what the sky looked like to the Apollo astronauts, or what the rings of Saturn looks like from Titan.

[Source](https://raw.githubusercontent.com/Stellarium/stellarium/master/data/org.stellarium.Stellarium.appdata.xml)

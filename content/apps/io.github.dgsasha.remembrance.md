+++
title = "Reminders"
description = "Set reminders and manage tasks"
aliases = []
date = 2023-06-25
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Reminders Developers",]
categories = [ "productivity",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "CalDAV", "Microsoft To Do",]
packaged_in = [ "flathub",]
freedesktop_categories = [ "GNOME", "GTK", "Office", "ProjectManagement",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/remindersdevs/Reminders"
homepage = "https://github.com/dgsasha/remembrance"
bugtracker = "https://github.com/dgsasha/remembrance/issues"
donations = ""
translations = "https://hosted.weblate.org/engage/reminders"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/remindersdevs/Reminders/main/data/io.github.remindersdevs.Reminders.metainfo.xml.in.in"
screenshots = [ "https://raw.githubusercontent.com/dgsasha/remembrance/c582346fdf564104042e67f329ea2caa5793920d/screenshot-dark.png", "https://raw.githubusercontent.com/dgsasha/remembrance/c582346fdf564104042e67f329ea2caa5793920d/screenshot-light.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/io.github.dgsasha.remembrance/1.png", "https://img.linuxphoneapps.org/io.github.dgsasha.remembrance/2.png", "https://img.linuxphoneapps.org/io.github.dgsasha.remembrance/3.png", "https://img.linuxphoneapps.org/io.github.dgsasha.remembrance/4.png", "https://img.linuxphoneapps.org/io.github.dgsasha.remembrance/5.png", "https://img.linuxphoneapps.org/io.github.dgsasha.remembrance/6.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "io.github.dgsasha.Remembrance"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.dgsasha.Remembrance"
flatpak_link = ""
flatpak_recipe = "https://raw.githubusercontent.com/remindersdevs/Reminders/main/flatpak/io.github.remindersdevs.Reminders.yml"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/remindersdevs/Reminders/main/data/io.github.remindersdevs.Reminders.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

What you can do with Reminders:


* Set recurring reminders
* Schedule notifications
* Sort, filter, and search for reminders
* Mark reminders as important or complete
* Organize reminders using lists
* Optionally play a sound when notifications are sent
* Optionally sync with Microsoft To Do
* Optionally sync with CalDAV servers
* Import and export ical/ics files

[Source](https://raw.githubusercontent.com/remindersdevs/Reminders/main/data/io.github.remindersdevs.Reminders.metainfo.xml.in.in)
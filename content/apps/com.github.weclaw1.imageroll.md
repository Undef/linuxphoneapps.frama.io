+++
title = "Image Roll"
description = "Image viewer with basic image manipulation tools"
aliases = []
date = 2021-06-13
updated = 2023-12-31

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Robert Węcławski",]
categories = [ "image viewer",]
mobile_compatibility = [ "5",]
status = [ "inactive", "mature",]
frameworks = [ "GTK4",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "aur", "flathub", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Graphics", "Viewer",]
programming_languages = [ "Rust",]
build_systems = [ "cargo",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/weclaw1/image-roll"
homepage = "https://github.com/weclaw1/image-roll"
bugtracker = "https://github.com/weclaw1/image-roll/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/weclaw1/image-roll/main/src/resources/com.github.weclaw1.ImageRoll.metainfo.xml"
screenshots = [ "https://raw.githubusercontent.com/weclaw1/image-roll/main/src/resources/screenshot.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "com.github.weclaw1.ImageRoll"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.github.weclaw1.ImageRoll"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "image-roll",]
appstream_xml_url = "https://raw.githubusercontent.com/weclaw1/image-roll/main/src/resources/com.github.weclaw1.ImageRoll.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description
Image Roll is a simple and fast GTK image viewer with basic image manipulation tools. Written in rust.

[Source](https://raw.githubusercontent.com/weclaw1/image-roll/main/src/resources/com.github.weclaw1.ImageRoll.metainfo.xml)

### Notice
Nice image viewer with features like rotation and cropping. GTK3 based until the 2.0.0 release.
Inactive since 2022-07-07.

+++
title = "Rattlensnake"
description = "Rattlesnake is a metronome app for mobile and desktop."
aliases = [ "apps/noappid.mbruchert.rattlesnake/",]
date = 2020-10-21
updated = 2023-04-23

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "mbruchert",]
categories = [ "musical tool",]
mobile_compatibility = [ "needs testing",]
status = [ "early",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge",]
freedesktop_categories = [ "Qt", "KDE", "Education", "Music",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/multimedia/rattlesnake"
homepage = ""
bugtracker = "https://invent.kde.org/multimedia/rattlesnake/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/multimedia/rattlesnake"
screenshots = [ "https://rimgo.bus-hit.me/lon9MC0.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.rattlesnake"
scale_to_fit = "Rattlesnake"
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://invent.kde.org/multimedia/rattlesnake/-/raw/master/org.kde.rattlesnake.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "rattlesnake",]
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "linmob"

+++


### Description

A metronome app for musicians on mobile and desktop devices

__Feature__

* Tap to the song you wan't to play and Rattlesnake automaticly figures out the right BPM
* Build your beat from multiple sounds and adjust the volume of individual beats

[Source](https://invent.kde.org/multimedia/rattlesnake)

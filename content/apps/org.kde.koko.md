+++
title = "Photos"
description = "Image Gallery"
aliases = []
date = 2019-02-01
updated = 2024-01-02

[taxonomies]
project_licenses = [ "LGPL-2.1-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "graphics",]
categories = [ "image viewer",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "Qt", "KDE", "Graphics", "Viewer",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/graphics/koko"
homepage = "https://apps.kde.org/en/koko"
bugtracker = "https://invent.kde.org/graphics/koko/-/issues"
donations = "https://kde.org/community/donations/?app=org.kde.koko"
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/graphics/koko/-/raw/master/org.kde.koko.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/koko/koko.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = false
app_id = "org.kde.koko.desktop"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.koko"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "koko",]
appstream_xml_url = "https://invent.kde.org/graphics/koko/-/raw/master/org.kde.koko.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

An Image gallery application

[Source](https://invent.kde.org/graphics/koko/-/raw/master/org.kde.koko.appdata.xml)

+++
title = "gtkbart"
description = "Public transportation client for the Bay Area Rapid Transit system"
aliases = []
date = 2019-04-16
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "curioussavage",]
categories = [ "public transport",]
mobile_compatibility = [ "5",]
status = [ "inactive",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = [ "Bay Area Rapid Transit API",]
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/curioussavage/gtkbart"
homepage = ""
bugtracker = "https://github.com/curioussavage/gtkbart/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://matrix.to/#/!BSqRHgvCtIsGittkBG:talk.puri.sm/$1551464398853539kMJNP:matrix.org?via=talk.puri.sm&via=matrix.org&via=disroot.org"
screenshots = [ "https://matrix-client.matrix.org/_matrix/media/r0/download/matrix.org/cEgTgSTmbRoHyyaZQqiQphgG", "https://matrix-client.matrix.org/_matrix/media/r0/download/matrix.org/yndozffmkaKuzcEohFeEsfZx",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.gnome.Bartapp.desktop"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/curioussavage/gtkbart/master/data/org.gnome.Bartapp.appdata.xml.in"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Notice

No commits since 2019, but it still works.
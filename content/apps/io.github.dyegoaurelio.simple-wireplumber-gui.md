+++
title = "Simple Wireplumber GUI"
description = "A simple GTK4 GUI for PipeWire"
aliases = []
date = 2024-02-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Dyego Aurélio",]
categories = [ "settings",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "wireplumber",]
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Audio", "AudioVideo", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "Meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/dyegoaurelio/simple-wireplumber-gui"
homepage = "https://github.com/dyegoaurelio/simple-wireplumber-gui"
bugtracker = "https://github.com/dyegoaurelio/simple-wireplumber-gui/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/io.github.dyegoaurelio.simple-wireplumber-gui"
screenshots = [ "https://raw.githubusercontent.com/dyegoaurelio/simple-wireplumber-gui/master/data/screenshots/main-window.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "io.github.dyegoaurelio.simple-wireplumber-gui"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.dyegoaurelio.simple-wireplumber-gui"
flatpak_link = "https://flathub.org/apps/io.github.dyegoaurelio.simple-wireplumber-gui.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/dyegoaurelio/simple-wireplumber-gui/main/io.github.dyegoaurelio.simple-wireplumber-gui.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/dyegoaurelio/simple-wireplumber-gui/main/data/io.github.dyegoaurelio.simple-wireplumber-gui.appdata.xml.in"
reported_by = "linmob"
updated_by = ""

+++


### Description

Simple Wireplumber GUI

[Source](https://raw.githubusercontent.com/dyegoaurelio/simple-wireplumber-gui/main/data/io.github.dyegoaurelio.simple-wireplumber-gui.appdata.xml.in)

### Notice

Be careful with this on your daily driver phone! Better use it on a secondary device first!

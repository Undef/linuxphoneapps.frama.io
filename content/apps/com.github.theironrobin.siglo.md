+++
title = "Siglo"
description = "Sync PineTime with your PinePhone"
aliases = []
date = 2021-02-28
updated = 2024-01-02

[taxonomies]
project_licenses = [ "MPL-2.0",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Alex R",]
categories = [ "watch companion",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "aur", "flathub", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/theironrobin/siglo"
homepage = "https://github.com/theironrobin/siglo"
bugtracker = "https://github.com/theironrobin/siglo/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/theironrobin/siglo/main/data/com.github.theironrobin.siglo.appdata.xml.in"
screenshots = [ "https://ironrobin.net/images/siglo-screenshot-1.png", "https://ironrobin.net/images/siglo-screenshot-2.png", "https://ironrobin.net/images/siglo-screenshot-3.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "com.github.theironrobin.siglo.desktop"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.github.theironrobin.siglo"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "siglo",]
appstream_xml_url = "https://raw.githubusercontent.com/theironrobin/siglo/main/data/com.github.theironrobin.siglo.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Features:


Supports all Phosh-based PinePhone distros.

[Source](https://raw.githubusercontent.com/theironrobin/siglo/main/data/com.github.theironrobin.siglo.appdata.xml.in)

### Notice

Great companion app for updating and time syncing InifiniTime, e.g. on the PineTime smartwatch.

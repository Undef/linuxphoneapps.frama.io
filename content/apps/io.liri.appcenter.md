+++
title = "App Center"
description = "Application manager for Liri OS"
aliases = []
date = 2020-11-07
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "lirios",]
categories = [ "app store",]
mobile_compatibility = [ "3",]
status = [ "maturing",]
frameworks = [ "QtQuick", "fluid",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "Qt", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/lirios/appcenter"
homepage = "https://liri.io"
bugtracker = "https://github.com/lirios/appcenter/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/lirios/appcenter/develop/src/app/io.liri.AppCenter.appdata.xml"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "io.liri.AppCenter.desktop"
scale_to_fit = "io.liri.AppCenter"
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "liri-appcenter",]
appstream_xml_url = "https://raw.githubusercontent.com/lirios/appcenter/develop/src/app/io.liri.AppCenter.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

App Center allows you to find and install new applications.

[Source](https://raw.githubusercontent.com/lirios/appcenter/develop/src/app/io.liri.AppCenter.appdata.xml)

### Notice

Fine with scale-to-fit, best reason to use it is that it provides a proper alphabetical overview of flathubs software.

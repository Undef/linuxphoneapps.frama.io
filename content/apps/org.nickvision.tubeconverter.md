+++
title = "Parabolic"
description = "Download web video and audio"
aliases = []
date = 2024-02-07

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Nickvision",]
categories = [ "video downloader",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "yt-dlp",]
services = [ "YouTube",]
packaged_in = [ "aur", "flathub", "nix_unstable", "snapcraft",]
freedesktop_categories = [ "AudioVideo", "Network",]
programming_languages = [ "Csharp",]
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://github.com/NickvisionApps/Parabolic"
homepage = "https://github.com/NickvisionApps/Parabolic"
bugtracker = "https://github.com/NickvisionApps/Parabolic/issues"
donations = "https://github.com/sponsors/nlogozzo"
translations = "https://github.com/NickvisionApps/Parabolic/blob/master/CONTRIBUTING.md#providing-translations"
more_information = []
summary_source_url = "https://flathub.org/apps/org.nickvision.tubeconverter"
screenshots = [ "https://raw.githubusercontent.com/NickvisionApps/Parabolic/master/NickvisionTubeConverter.GNOME/Screenshots/AddDownload.png", "https://raw.githubusercontent.com/NickvisionApps/Parabolic/master/NickvisionTubeConverter.GNOME/Screenshots/Dark.png", "https://raw.githubusercontent.com/NickvisionApps/Parabolic/master/NickvisionTubeConverter.GNOME/Screenshots/Light.png", "https://raw.githubusercontent.com/NickvisionApps/Parabolic/master/NickvisionTubeConverter.GNOME/Screenshots/StartScreen.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.nickvision.tubeconverter"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.nickvision.tubeconverter"
flatpak_link = "https://flathub.org/apps/org.nickvision.tubeconverter.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/NickvisionApps/Parabolic/main/flatpak/org.nickvision.tubeconverter.json"
snapcraft = "https://snapcraft.io/tube-converter"
snap_link = ""
snap_recipe = "https://raw.githubusercontent.com/NickvisionApps/Parabolic/main/snap/snapcraft.yaml"
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "parabolic",]
appstream_xml_url = "https://raw.githubusercontent.com/NickvisionApps/Parabolic/dd6f8838bdc907f72ad6c9c562e127d2767795df/NickvisionTubeConverter.Shared/org.nickvision.tubeconverter.metainfo.xml.in"
reported_by = "linmob"
updated_by = ""

+++


### Description

— A basic yt-dlp frontend


— Supports downloading videos in multiple formats (mp4, webm, mp3, opus, flac, and wav)


— Run multiple downloads at a time


— Supports downloading metadata and video subtitles

[Source](https://raw.githubusercontent.com/NickvisionApps/Parabolic/dd6f8838bdc907f72ad6c9c562e127d2767795df/NickvisionTubeConverter.Shared/org.nickvision.tubeconverter.metainfo.xml.in)

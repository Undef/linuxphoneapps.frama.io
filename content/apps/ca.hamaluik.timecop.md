+++
title = "Time Cop"
description = "A time tracking app that respects your privacy and gets the job done without getting too fancy"
aliases = []
date = 2024-02-13

[taxonomies]
project_licenses = [ "Apache-2.0",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Kenton Hamaluik",]
categories = [ "productivity", "time tracking",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "Flutter",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Dart",]
build_systems = [ "Flutter",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/hamaluik/timecop"
homepage = "https://timecop.app/en/"
bugtracker = "https://github.com/hamaluik/timecop/issues"
donations = ""
translations = "https://hosted.weblate.org/engage/timecop/"
more_information = []
summary_source_url = "https://flathub.org/apps/ca.hamaluik.Timecop"
screenshots = [ "https://raw.githubusercontent.com/hamaluik/timecop/master/screenshots/linux/dashboard.png", "https://raw.githubusercontent.com/hamaluik/timecop/master/screenshots/linux/editor.png", "https://raw.githubusercontent.com/hamaluik/timecop/master/screenshots/linux/export.png", "https://raw.githubusercontent.com/hamaluik/timecop/master/screenshots/linux/reports-bar.png", "https://raw.githubusercontent.com/hamaluik/timecop/master/screenshots/linux/settings.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "ca.hamaluik.Timecop"
scale_to_fit = ""
flathub = "https://flathub.org/apps/ca.hamaluik.Timecop"
flatpak_link = "https://flathub.org/apps/ca.hamaluik.Timecop.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/hamaluik/timecop/master/flatpak/flatpak_meta.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "timecop",]
appstream_xml_url = "https://raw.githubusercontent.com/hamaluik/timecop/master/flatpak/ca.hamaluik.Timecop.appdata.xml"
reported_by = "linmob"
updated_by = ""

+++

### Description

A time tracking app that respects your privacy and gets the job done without getting too fancy.


Features:


* Offline-only, mobile-centric design
* Runs on Linux, iOS, and Android
* Fully private—there is no tracking / spying / advertising / etc
* Keep track of tasks with multiple parallel timers that can be started with the tap of a button
* Associate timers with projects to group your work (or don't)
* Start, stop, edit, and delete timers whenever with no fuss
* Export data as a .csv file, filtered by timespans and projects
* Export the app's database for full access to all of its data
* Automatic light mode / dark mode based on your device settings
* Localized in several languages (thanks to Google Translate): English, Arabic, German, Spanish, French, Hindi, Indonesian, Japanese, Korean, Portuguese, Russian, Chinese (Simplified), Chinese (Traditional)
* Open source (licensed under Apache-2.0)—fork away (https://github.com/hamaluik/timecop)

[Source](https://raw.githubusercontent.com/hamaluik/timecop/master/flatpak/ca.hamaluik.Timecop.appdata.xml)

+++
title = "Olifant"
description = "Lightning fast client for Mastodon"
aliases = []
date = 2020-08-25
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "alexcleac",]
categories = [ "social media",]
mobile_compatibility = [ "3",]
status = [ "gone",]
frameworks = [ "GTK3", "granite",]
backends = []
services = [ "Mastodon",]
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Network", "Feed",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "http://web.archive.org/web/20230622070130/https://github.com/Alexmitter/olifant"
homepage = "https://github.com/cleac/olifant"
bugtracker = "https://github.com/cleac/olifant/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/Alexmitter/olifant/master/data/com.github.cleac.olifant.appdata.xml.in"
screenshots = [ "https://raw.githubusercontent.com/cleac/olifant/master/data/screenshot.png", "https://raw.githubusercontent.com/cleac/olifant/master/data/screenshot2.png", "https://raw.githubusercontent.com/cleac/olifant/master/data/screenshot3.png", "https://raw.githubusercontent.com/cleac/olifant/master/data/screenshot4.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "com.github.cleac.olifant"
scale_to_fit = "com.github.cleac.olifant"
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"

+++

### Description
Olifant is a client for the world’s largest free, open-source, decentralized microblogging network with real-time notifications and support for multiple accounts.

Mastodon is lovingly crafted with power and speed in mind, resulting in a free, independent, and popular alternative to the centralized social networks.

Anyone can run a Mastodon server. Each server hosts individual user accounts, the content they produce, and the content to which they are subscribed. Every user can follow each other and share their posts regardless of their server.

[Source](https://raw.githubusercontent.com/Alexmitter/olifant/master/data/com.github.cleac.olifant.appdata.xml.in)

### Notice
Mobile friendly fork was deleted by its author -> status gone. Previously no commits in June 2020, therefore marked inactive.

+++
title = "QHumble"
description = "Qhumble allows retrieving any content purchased from Humble Bundle that is DRM-Free."
aliases = []
date = 2019-02-22
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "eyecreate",]
categories = [ "utilities",]
mobile_compatibility = [ "needs testing",]
status = [ "early", "inactive", "pre-release",]
frameworks = [ "QtQuick",]
backends = []
services = [ "humblebundle.com",]
packaged_in = []
freedesktop_categories = [ "Qt", "Network",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "qmake",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://github.com/eyecreate/QHumble"
homepage = ""
bugtracker = "https://github.com/CMon/QHumble/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "no quotation"
screenshots = [ "https://www.eyecreate.org/images/qhumble-demo-fs8.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "eyecreate"
updated_by = "script"

+++

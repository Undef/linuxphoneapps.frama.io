+++
title = "ONVIFViewer"
description = "View and control network cameras using the ONVIF protocol"
aliases = []
date = 2019-02-02
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Casper Meijn",]
categories = [ "camera",]
mobile_compatibility = [ "5",]
status = [ "unmaintained",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "Qt", "KDE", "Photography",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/caspermeijn/onvifviewer"
homepage = "https://gitlab.com/caspermeijn/onvifviewer"
bugtracker = "https://gitlab.com/caspermeijn/onvifviewer/issues"
donations = "https://gitlab.com/caspermeijn/onvifviewer/blob/master/README.md#donations"
translations = "https://hosted.weblate.org/engage/onvifviewer/"
more_information = []
summary_source_url = "https://flathub.org/apps/net.meijn.onvifviewer"
screenshots = [ "https://gitlab.com/caspermeijn/onvifviewer/raw/master/desktop/screenshot1.png", "https://gitlab.com/caspermeijn/onvifviewer/raw/master/desktop/screenshot2.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "net.meijn.onvifviewer"
scale_to_fit = ""
flathub = "https://flathub.org/apps/net.meijn.onvifviewer"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "onvifviewer",]
appstream_xml_url = "https://gitlab.com/caspermeijn/onvifviewer/-/raw/master/desktop/net.meijn.onvifviewer.appdata.xml.in"
reported_by = "caspermeijn"
updated_by = "script"

+++


### Description

Use this open-source app to view your network cameras using the ONVIF protocol.


The development of this app was started as part of the ONVIF Spotlight Challenge.


You can connect to your network camera and view the video of it. If the camera is controllable you can move it as well.

[Source](https://gitlab.com/caspermeijn/onvifviewer/-/raw/master/desktop/net.meijn.onvifviewer.appdata.xml.in)

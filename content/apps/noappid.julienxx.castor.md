+++
title = "Castor"
description = "A graphical client for plain-text protocols written in Rust with GTK. It currently supports the Gemini, Gopher and Finger protocols."
aliases = []
date = 2021-02-03
updated = 2023-09-30

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = []
app_author = [ "julienxx",]
categories = [ "gemini browser",]
mobile_compatibility = [ "4",]
status = [ "inactive",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "alpine_edge", "aur", "gnuguix", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Network",]
programming_languages = [ "Rust",]
build_systems = [ "make",]
requires_internet = []
tags = []

[extra]
repository = "https://git.sr.ht/~julienxx/castor"
homepage = ""
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "https://git.sr.ht/~julienxx/castor"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "castor-browser",]
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "linmob"

+++

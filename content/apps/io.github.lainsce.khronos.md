+++
title = "Khronos"
description = "Log the time it took to do tasks"
aliases = []
date = 2021-04-24
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Lains",]
categories = [ "productivity",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_edge", "aur", "flathub", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "GNOME", "GTK", "Office",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/lainsce/khronos"
homepage = "https://github.com/lainsce/khronos/"
bugtracker = "https://github.com/lainsce/khronos/issues"
donations = "https://www.patreon.com/lainsce"
translations = "https://github.com/lainsce/khronos/blob/main/po/README.md"
more_information = [ "https://apps.gnome.org/app/io.github.lainsce.Khronos/",]
summary_source_url = "https://raw.githubusercontent.com/lainsce/khronos/main/data/io.github.lainsce.Khronos.metainfo.xml.in"
screenshots = [ "https://raw.githubusercontent.com/lainsce/khronos/main/data/shot.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/io.github.lainsce.khronos/1.png", "https://img.linuxphoneapps.org/io.github.lainsce.khronos/2.png", "https://img.linuxphoneapps.org/io.github.lainsce.khronos/3.png", "https://img.linuxphoneapps.org/io.github.lainsce.khronos/4.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "io.github.lainsce.Khronos"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.lainsce.Khronos"
flatpak_link = ""
flatpak_recipe = "https://raw.githubusercontent.com/lainsce/khronos/main/io.github.lainsce.Khronos.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "khronos",]
appstream_xml_url = "https://raw.githubusercontent.com/lainsce/khronos/main/data/io.github.lainsce.Khronos.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Start logging any task's time to completion, with the ability to stop the timer at any moment, with notifications for longer task sessions.


* Quit anytime with the shortcut Ctrl + Q

[Source](https://raw.githubusercontent.com/lainsce/khronos/main/data/io.github.lainsce.Khronos.metainfo.xml.in)

### Notice

Sadly deep sleep interferes with time tracking on the PinePhone (and other devices that use suspend to improve battery life), so make sure that it does not suspend while you track a task.

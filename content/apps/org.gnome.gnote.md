+++
title = "Gnote"
description = "A simple note-taking application"
aliases = []
date = 2021-06-21
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "GNOME Developers",]
categories = [ "note taking",]
mobile_compatibility = [ "3",]
status = [ "mature",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "TextEditor",]
programming_languages = [ "Cpp",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnote"
homepage = "https://wiki.gnome.org/Gnote"
bugtracker = "https://gitlab.gnome.org/GNOME/gnote/issues"
donations = "http://www.gnome.org/friends/"
translations = "https://wiki.gnome.org/TranslationProject"
more_information = [ "https://web.archive.org/web/20230601124335/https://wiki.mobian.org/doku.php?id=gnote",]
summary_source_url = "https://gitlab.gnome.org/GNOME/gnote/-/raw/master/data/org.gnome.Gnote.appdata.xml.in"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.gnome.Gnote"
scale_to_fit = "gnote"
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnote",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnote/-/raw/master/data/org.gnome.Gnote.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Gnote is a simple note-taking application for GNOME desktop environment. It allows you to capture your ideas, link them together using WikiWiki-style links, group together in notebooks and some extra features for everyday use.


Notes can be printed or exported as HTML documents.


Gnote also supports synchronization, making it simple to use it on multiple devices.

[Source](https://gitlab.gnome.org/GNOME/gnote/-/raw/master/data/org.gnome.Gnote.appdata.xml.in)

### Notice

Scales well (reviewed release 40.1), only the settings menu does not fit the screen properly. But: Opening notes is tedious and requires the use of the enter key on the software keyboard.

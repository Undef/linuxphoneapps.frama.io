+++
title = "Notorious"
description = "Keyboard centric notes"
aliases = []
date = 2020-08-25
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Gabriele Musco",]
categories = [ "note taking",]
mobile_compatibility = [ "5",]
status = [ "maturing", "inactive",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "TextEditor",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/GabMus/notorious"
homepage = "https://notorious.gabmus.org"
bugtracker = "https://gitlab.gnome.org/gabmus/notorious/-/issues"
donations = "https://liberapay.com/gabmus/donate"
translations = "https://gitlab.gnome.org/gabmus/notorious/-/tree/master/po"
more_information = []
summary_source_url = "https://gitlab.gnome.org/GabMus/notorious"
screenshots = [ "https://gabmus.gitlab.io/notorious/screenshots/darkmode.png", "https://gabmus.gitlab.io/notorious/screenshots/keyboard_shortcuts-light.png", "https://gabmus.gitlab.io/notorious/screenshots/markdown-light.png", "https://gabmus.gitlab.io/notorious/screenshots/markdown.png", "https://gitlab.gnome.org/gabmus/notorious/raw/website/website/screenshots/mainwindow-light.png", "https://gitlab.gnome.org/gabmus/notorious/raw/website/website/screenshots/mainwindow.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.gabmus.notorious"
scale_to_fit = "notorious"
flathub = "https://flathub.org/apps/org.gabmus.notorious"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "notorious",]
appstream_xml_url = "https://gitlab.gnome.org/GabMus/notorious/-/raw/master/data/org.gabmus.notorious.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Notorious is a keyboard centric notes app


Notorious can be controlled entirely with your keyboard. No need to move your hand to the mouse, just open it up and dump your brain.


In Notorious you can write your notes however you want, but if you like Markdown, you can easily enable syntax highlighting.


No need to worry about saving. When you change note or exit the app whatever you wrote gets automatically saved.


No need for cumbersome online sync features. Just choose a folder on your disk and that's it. Of course nothing stops you froms syncing that folder with your service of choice.

[Source](https://gitlab.gnome.org/GabMus/notorious/-/raw/master/data/org.gabmus.notorious.appdata.xml.in)

### Notice

Everything is adjusted for mobile phones, only the 'shortcuts' menu is not (which is of limited use anyway).

+++
title = "Blanket"
description = "Listen to ambient sounds"
aliases = []
date = 2020-09-04
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Rafael Mardojai CM",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "flathub", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "Audio", "AudioVideo", "GTK",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/rafaelmardojai/blanket"
homepage = "https://github.com/rafaelmardojai/blanket"
bugtracker = "https://github.com/rafaelmardojai/blanket/issues"
donations = "https://rafaelmardojai.com/donate/"
translations = "https://hosted.weblate.org/engage/blanket/"
more_information = [ "https://apps.gnome.org/app/com.rafaelmardojai.Blanket/",]
summary_source_url = "https://raw.githubusercontent.com/rafaelmardojai/blanket/master/data/com.rafaelmardojai.Blanket.metainfo.xml.in"
screenshots = [ "https://raw.githubusercontent.com/rafaelmardojai/blanket/master/brand/screenshot-1-dark.png", "https://raw.githubusercontent.com/rafaelmardojai/blanket/master/brand/screenshot-1.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "com.rafaelmardojai.Blanket"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.rafaelmardojai.Blanket"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "blanket",]
appstream_xml_url = "https://raw.githubusercontent.com/rafaelmardojai/blanket/master/data/com.rafaelmardojai.Blanket.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Improve focus and increase your productivity by listening to different ambient sounds


Also can help you to fall asleep in a noisy environment.


Features:


* Save presets
* Add custom sounds
* Auto start in background
* MPRIS integration


Included Sounds in the App:


* Birds
* Boat
* City
* Coffee Shop
* Fireplace
* Pink Noise
* Rain
* Summer Night
* Storm
* Stream
* Train
* Waves
* White Noise
* Wind

[Source](https://raw.githubusercontent.com/rafaelmardojai/blanket/master/data/com.rafaelmardojai.Blanket.metainfo.xml.in)

### Notice

GTK3/libhandy before 0.6.0.

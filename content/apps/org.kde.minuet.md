+++
title = "Minuet"
description = "Music Education Software"
aliases = []
date = 2019-02-01
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community",]
categories = [ "education",]
mobile_compatibility = [ "2",]
status = [ "mature",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Qt", "KDE", "Education", "Music",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/education/minuet"
homepage = "https://minuet.kde.org"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=minuet"
donations = "https://www.kde.org/community/donations/?app=minuet&source=appdata"
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/education/minuet/-/raw/master/org.kde.minuet.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/minuet/minuet.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.minuet.desktop"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.minuet"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "minuet",]
appstream_xml_url = "https://invent.kde.org/education/minuet/-/raw/master/org.kde.minuet.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

Minuet is an application for music education. It features a set of ear training exercises regarding intervals, chords, scales and more.

[Source](https://invent.kde.org/education/minuet/-/raw/master/org.kde.minuet.appdata.xml)

### Notice

Does not really work on mobile.

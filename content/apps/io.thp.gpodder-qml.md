+++
title = "gPodder QML UI"
description = "gPodder 4 QML UI Reference Implementation"
aliases = []
date = 2019-02-01
updated = 2023-09-30

[taxonomies]
project_licenses = [ "not specified",]
metadata_licenses = []
app_author = [ "gpodder",]
categories = [ "podcast client",]
mobile_compatibility = [ "5",]
status = [ "inactive",]
frameworks = [ "QtQuick",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "Qt", "Network", "Audio", "Feeds",]
programming_languages = [ "QML", "Python",]
build_systems = [ "make",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/gpodder/gpodder-ui-qml"
homepage = "https://invent.kde.org/jbbgameich/gpodder-qml"
bugtracker = "https://github.com/gpodder/gpodder-ui-qml/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/gpodder/gpodder-ui-qml"
screenshots = [ "https://phabricator.kde.org/file/data/kv3lfwvgg7p4vofk6dit/PHID-FILE-j6zmvux6lt57ygu4ptmq/gpodder-mobile.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "io.thp.gpodder-qml"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gpodder", "gpodder-adaptive",]
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = "script"

+++


### Notice
Last commit 2020-04-10.

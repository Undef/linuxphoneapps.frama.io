+++
title = "Lemonade"
description = "Follow discussions on Lemmy"
aliases = []
date = 2023-08-21
updated = 2024-01-02

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "CC-BY-SA-4.0",]
app_author = [ "Max Walters",]
categories = [ "social media",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "lemmy",]
packaged_in = [ "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Network",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/mdwalters/lemonade"
homepage = "https://github.com/mdwalters/lemonade"
bugtracker = "https://github.com/mdwalters/lemonade/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/mdwalters/lemonade/main/data/ml.mdwalters.Lemonade.appdata.xml.in"
screenshots = [ "https://i.ibb.co/0jf8RR4/image.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/ml.mdwalters.lemonade/1.png",]
all_features_touch = false
intended_for_mobile = false
app_id = "ml.mdwalters.Lemonade"
scale_to_fit = ""
flathub = "https://flathub.org/apps/ml.mdwalters.Lemonade"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/mdwalters/lemonade/main/data/ml.mdwalters.Lemonade.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Lemonade is a sleek and modern Lemmy client that is designed to provide a seamless browsing experience for Lemmy users. Built with the latest technology, Lemonade utilizes the power of libadwaita and GTK 4 to deliver a beautiful and intuitive user interface that is both easy to use and visually appealing.


With Lemonade, you can easily browse your favorite communities, discover new content, and engage with other users. The client is optimized for speed and performance, ensuring that you can quickly and efficiently navigate through Lemmy without any lag or delays.

[Source](https://raw.githubusercontent.com/mdwalters/lemonade/main/data/ml.mdwalters.Lemonade.appdata.xml.in)

### Notice

It's in an early state, the screenshot basically shows everything there is.
+++
title = "Jogger"
description = "Fitness tracker"
aliases = []
date = 2023-09-24
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "barkeerlounger",]
categories = [ "fitness",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "Meson",]
requires_internet = []
tags = []

[extra]
repository = "https://codeberg.org/baarkerlounger/jogger"
homepage = "https://codeberg.org/baarkerlounger/jogger"
bugtracker = "https://codeberg.org/baarkerlounger/jogger/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/xyz.slothlife.Jogger"
screenshots = [ "https://codeberg.org/baarkerlounger/jogger/raw/branch/main/data/screenshots/track_workout.png", "https://codeberg.org/baarkerlounger/jogger/raw/branch/main/data/screenshots/welcome.png", "https://codeberg.org/baarkerlounger/jogger/raw/branch/main/data/screenshots/workout_detail.png", "https://codeberg.org/baarkerlounger/jogger/raw/branch/main/data/screenshots/workout_list.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "xyz.slothlife.Jogger"
scale_to_fit = ""
flathub = "https://flathub.org/apps/xyz.slothlife.Jogger"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://codeberg.org/baarkerlounger/jogger/raw/branch/main/data/xyz.slothlife.Jogger.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Track and view your runs, walks, cycles, swims, and other workouts on mobile Linux.

[Source](https://codeberg.org/baarkerlounger/jogger/raw/branch/main/data/xyz.slothlife.Jogger.appdata.xml.in)
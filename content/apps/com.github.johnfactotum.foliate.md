+++
title = "Foliate"
description = "Read e-books in style"
aliases = []
date = 2020-10-08
updated = 2023-12-31

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "John Factotum",]
categories = [ "document viewer",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "epub.js",]
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_12", "debian_13", "debian_unstable", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Office", "Viewer",]
programming_languages = [ "JavaScript",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/johnfactotum/foliate"
homepage = "https://johnfactotum.github.io/foliate/"
bugtracker = "https://github.com/johnfactotum/foliate/issues"
donations = "https://www.buymeacoffee.com/johnfactotum"
translations = "https://github.com/johnfactotum/foliate/tree/gtk4/po"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/johnfactotum/foliate/gtk4/data/com.github.johnfactotum.Foliate.metainfo.xml.in"
screenshots = [ "https://raw.githubusercontent.com/johnfactotum/foliate/gtk4/data/screenshots/annotations.png", "https://raw.githubusercontent.com/johnfactotum/foliate/gtk4/data/screenshots/dark.png", "https://raw.githubusercontent.com/johnfactotum/foliate/gtk4/data/screenshots/lookup.png", "https://raw.githubusercontent.com/johnfactotum/foliate/gtk4/data/screenshots/screenshot.png", "https://raw.githubusercontent.com/johnfactotum/foliate/gtk4/data/screenshots/vertical.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "com.github.johnfactotum.Foliate"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.github.johnfactotum.Foliate"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "foliate",]
appstream_xml_url = "https://raw.githubusercontent.com/johnfactotum/foliate/gtk4/data/com.github.johnfactotum.Foliate.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Discover a new chapter in reading with Foliate, the modern e-book reader tailored for GNOME. Immerse yourself in a distraction-free interface, with customization features designed to match your unique preferences.


Features include:


* Open EPUB, Mobipocket, Kindle, FB2, CBZ, and PDF files
* Paginated mode and scrolled mode
* Customize font and line-spacing
* Light, sepia, dark, and invert mode
* Reading progress slider with chapter marks
* Bookmarks and annotations
* Find in book
* Quick dictionary lookup

[Source](https://raw.githubusercontent.com/johnfactotum/foliate/gtk4/data/com.github.johnfactotum.Foliate.metainfo.xml.in)

### Notice

GTK3/libadwaita before release 3.0.

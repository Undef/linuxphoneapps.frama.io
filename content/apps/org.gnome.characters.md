+++
title = "Characters"
description = "Character map application"
aliases = []
date = 2021-03-31
updated = 2024-01-02

[taxonomies]
project_licenses = [ "BSD-3-Clause", "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "GNOME Developers",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "C", "JavaScript",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnome-characters"
homepage = "https://apps.gnome.org/app/org.gnome.Characters/"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-characters/issues"
donations = "https://www.gnome.org/donate"
translations = ""
more_information = [ "https://apps.gnome.org/app/org.gnome.Characters/",]
summary_source_url = "https://wiki.gnome.org/Apps/Characters"
screenshots = [ "https://gitlab.gnome.org/GNOME/gnome-characters/raw/main/data/screenshots/screenshot1.png", "https://gitlab.gnome.org/GNOME/gnome-characters/raw/main/data/screenshots/screenshot2.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.Characters.desktop"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Characters"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-characters",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-characters/-/raw/main/data/org.gnome.Characters.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Characters is a simple utility application to find and insert unusual characters. It allows you to quickly find the character you are looking for by searching for keywords.


You can also browse characters by categories, such as Punctuation, Pictures, etc.

[Source](https://gitlab.gnome.org/GNOME/gnome-characters/-/raw/main/data/org.gnome.Characters.appdata.xml.in)

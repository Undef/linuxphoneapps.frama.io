+++
title = "Pyrocast"
description = "Another podcast app for Linux phones (e.g., Pinephone)"
aliases = []
date = 2021-05-29
updated = 2023-09-30

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "jnetterf",]
categories = [ "podcast client",]
mobile_compatibility = [ "5",]
status = [ "early", "pre-release", "inactive",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Network", "Audio", "Feed", "Player",]
programming_languages = [ "Rust",]
build_systems = [ "cargo",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://github.com/jocelyn-stericker/pyrocast"
homepage = ""
bugtracker = "https://github.com/jocelyn-stericker/pyrocast/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/jocelyn-stericker/pyrocast"
screenshots = []
screenshots_img = [ "https://img.linuxphoneapps.org/twitter_screenshots/ca.nettek.pyrocast/1.jpg", "https://img.linuxphoneapps.org/twitter_screenshots/ca.nettek.pyrocast/2.jpg", "https://img.linuxphoneapps.org/twitter_screenshots/ca.nettek.pyrocast/3.jpg", "https://img.linuxphoneapps.org/twitter_screenshots/ca.nettek.pyrocast/4.jpg",]
all_features_touch = false
intended_for_mobile = false
app_id = "ca.nettek.pyrocast"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "linmob"

+++

### Description

Pyrocast is a podcast app for Linux phones that use gtk.

Features:

- Browse or search iTunes podcasts.
- Subscribe to podcasts.
- Stream podcasts.

[Source](https://github.com/jocelyn-stericker/pyrocast/blob/master/README.md)

### Notice

Inactive - last commit Sept. 18th, 2020.
Was buggy with a few select feeds, no settings screen, during evaluation in 2021.

+++
title = "Rhythmbox"
description = "Play and organize your music collection"
aliases = []
date = 2020-11-28
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GFDL-1.3", "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "GNOME Developers",]
categories = [ "music player",]
mobile_compatibility = [ "3",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Audio", "Network", "Feed", "Player",]
programming_languages = [ "C", "Vala",]
build_systems = [ "make",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/GNOME/rhythmbox"
homepage = "https://wiki.gnome.org/Apps/Rhythmbox"
bugtracker = "https://gitlab.gnome.org/GNOME/rhythmbox/issues"
donations = "http://www.gnome.org/friends/"
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/GNOME/rhythmbox/-/raw/master/data/org.gnome.Rhythmbox3.appdata.xml.in"
screenshots = [ "https://gitlab.gnome.org/GNOME/rhythmbox/-/raw/master/data/screenshots/rhythmbox-main-window.png?inline=false",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.gnome.Rhythmbox3.desktop"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "rhythmbox",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/rhythmbox/-/raw/master/data/org.gnome.Rhythmbox3.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Rhythmbox is a music management application, designed to work well under the GNOME
 desktop. In addition to music stored on your computer, it supports network shares,
 podcasts, radio streams, portable music devices (including phones), and internet music
 services such as Last.fm and Magnatune.


Rhythmbox is Free software, based on GTK+ and GStreamer, and is extensible via plugins
 written in Python or C.

[Source](https://gitlab.gnome.org/GNOME/rhythmbox/-/raw/master/data/org.gnome.Rhythmbox3.appdata.xml.in)

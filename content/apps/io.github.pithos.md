+++
title = "Pithos"
description = "Pandora radio client"
aliases = []
date = 2020-11-07
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Pithos",]
categories = [ "internet radio",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK3",]
backends = []
services = [ "Pandora",]
packaged_in = [ "alpine_edge", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "flathub", "gentoo", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Audio", "Player",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/pithos/pithos"
homepage = "https://pithos.github.io"
bugtracker = "https://github.com/pithos/pithos/issues"
donations = "https://goo.gl/StrKkg"
translations = "https://github.com/pithos/pithos/wiki/Contributing"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/pithos/pithos/master/data/io.github.Pithos.appdata.xml.in"
screenshots = [ "http://i.imgur.com/2U2ce8v.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "io.github.Pithos"
scale_to_fit = "pithos"
flathub = "https://flathub.org/apps/io.github.Pithos"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "pithos",]
appstream_xml_url = "https://raw.githubusercontent.com/pithos/pithos/master/data/io.github.Pithos.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Pithos is a easy to use native Pandora Radio client that is more lightweight than the pandora.com web client and integrates with the desktop.


It supports most functionality of pandora.com such as rating songs, creating/managing stations, quickmix, etc. On top of that it has features such as last.fm scrobbling, media keys, notifications, proxies, and mpris support.

[Source](https://raw.githubusercontent.com/pithos/pithos/master/data/io.github.Pithos.appdata.xml.in)

### Notice

Please help with real world experience, service is US-only.

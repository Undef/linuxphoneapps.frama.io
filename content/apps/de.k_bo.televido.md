+++
title = "Televido"
description = "Access German-language public TV"
aliases = []
date = 2023-12-28
updated = 2023-12-31

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = []
categories = [ "multimedia",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "ARD Mediathek", "ZDF Mediathek",]
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "AudioVideo", "TV", "Video",]
programming_languages = [ "Rust",]
build_systems = [ "Meson",]
requires_internet = [ "requires always",]
tags = []

[extra]
repository = "https://github.com/d-k-bo/televido"
homepage = "https://github.com/d-k-bo/televido"
bugtracker = "https://github.com/d-k-bo/televido/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/d-k-bo/televido/main/data/de.k_bo.Televido.metainfo.xml.in"
screenshots = [ "https://raw.githubusercontent.com/d-k-bo/televido/main/data/screenshots/1.png", "https://raw.githubusercontent.com/d-k-bo/televido/main/data/screenshots/2.png", "https://raw.githubusercontent.com/d-k-bo/televido/main/data/screenshots/3.png", "https://raw.githubusercontent.com/d-k-bo/televido/main/data/screenshots/4.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "de.k_bo.Televido"
scale_to_fit = ""
flathub = "https://flathub.org/apps/de.k_bo.Televido"
flatpak_link = "https://flathub.org/apps/de.k_bo.Televido.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "televido",]
appstream_xml_url = "https://raw.githubusercontent.com/d-k-bo/televido/main/data/de.k_bo.Televido.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Televido (“Television” in Esperanto) lets you livestream, search, play and
 download media from German-language public television services. It is powered by APIs
 provided by the MediathekView project.


The presented content is provided directly by the respective television services, this
 program only facilitates finding and accessing the shows.


For video playback and download, Televido uses external programs that are installed on
 the user's system. Currently supported players: GNOME Videos (Totem), Celluloid,
 Clapper, Daikhan. Currently supported downloaders: Parabolic.

[Source](https://raw.githubusercontent.com/d-k-bo/televido/main/data/de.k_bo.Televido.metainfo.xml.in)

+++
title = "Currency Converter"
description = "A simple currency converter using Google-based data"
aliases = []
date = 2024-02-12

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Ideve Core",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Google Finance Currency Conversion",]
packaged_in = [ "flathub",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Python",]
build_systems = [ "Meson",]
requires_internet = [ "requires always",]
tags = []

[extra]
repository = "https://github.com/ideveCore/currency-converter"
homepage = "https://github.com/ideveCore/currency-converter"
bugtracker = "https://github.com/ideveCore/currency-converter/issues"
donations = "https://ko-fi.com/idevecore"
translations = "https://github.com/ideveCore/currency-converter/tree/main/po"
more_information = []
summary_source_url = "https://flathub.org/apps/io.github.idevecore.CurrencyConverter"
screenshots = [ "https://raw.githubusercontent.com/ideveCore/currency-converter/developer/data/screenshots/01.png", "https://raw.githubusercontent.com/ideveCore/currency-converter/developer/data/screenshots/02.png", "https://raw.githubusercontent.com/ideveCore/currency-converter/developer/data/screenshots/03.png", "https://raw.githubusercontent.com/ideveCore/currency-converter/developer/data/screenshots/04.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = false
app_id = "io.github.idevecore.CurrencyConverter"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.idevecore.CurrencyConverter"
flatpak_link = "https://flathub.org/apps/io.github.idevecore.CurrencyConverter.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/ideveCore/currency-converter/main/io.github.idevecore.CurrencyConverter.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/ideveCore/currency-converter/main/data/io.github.idevecore.CurrencyConverter.appdata.xml.in.in"
reported_by = "linmob"
updated_by = ""

+++


### Description

The currency converter is simple and fast, ideal for those who need to convert currencies repeatedly.

[Source](https://raw.githubusercontent.com/ideveCore/currency-converter/main/data/io.github.idevecore.CurrencyConverter.appdata.xml.in.in)

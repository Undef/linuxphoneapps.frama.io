+++
title = "Polypass"
description = "A simple, secure, and easy to use password manager"
aliases = []
date = 2024-02-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "MIT",]
app_author = [ "polypixeldev",]
categories = [ "password manager",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "Flutter",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Dart", "Cpp", "Rust",]
build_systems = [ "Flutter", "CMake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/polypixeldev/polypass"
homepage = "https://github.com/polypixeldev/polypass"
bugtracker = "https://github.com/polypixeldev/polypass/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/io.github.polypixeldev.Polypass"
screenshots = [ "https://raw.githubusercontent.com/polypixeldev/polypass/main/images/generator.png", "https://raw.githubusercontent.com/polypixeldev/polypass/main/images/home.png", "https://raw.githubusercontent.com/polypixeldev/polypass/main/images/vault.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = false
app_id = "io.github.polypixeldev.Polypass"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.polypixeldev.Polypass"
flatpak_link = "https://flathub.org/apps/io.github.polypixeldev.Polypass.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/flathub/io.github.polypixeldev.Polypass/master/io.github.polypixeldev.Polypass.yml"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "timecop",]
appstream_xml_url = "https://raw.githubusercontent.com/polypixeldev/polypass/main/linux/io.github.polypixeldev.Polypass.appdata.xml"
reported_by = "linmob"
updated_by = ""

+++


### Description

Polypass is a simple, secure, and easy to use password manager

[Source](https://raw.githubusercontent.com/polypixeldev/polypass/main/linux/io.github.polypixeldev.Polypass.appdata.xml)

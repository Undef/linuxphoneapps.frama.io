+++
title = "Moment"
description = "Customizable and keyboard-operable Matrix client"
aliases = []
date = 2022-04-03
updated = 2024-01-02

[taxonomies]
project_licenses = [ "LGPL-3.0-or-later",]
metadata_licenses = [ "FSFAP",]
app_author = [ "Moment contributors",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "QtQuick",]
backends = [ "matrix-nio", "libolm",]
services = [ "Matrix",]
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "aur", "flathub",]
freedesktop_categories = [ "Chat", "InstantMessaging", "Network", "Qt",]
programming_languages = [ "QML", "Python",]
build_systems = [ "qmake",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/mx-moment/moment"
homepage = "https://mx-moment.xyz"
bugtracker = "https://gitlab.com/mx-moment/moment/-/issues"
donations = ""
translations = ""
more_information = [ "https://gitlab.com/mx-moment/moment/-/blob/main/docs/INSTALL.md", "https://gitlab.com/mx-moment/moment/-/blob/main/docs/MIRAGEDIFF.md", "https://gitlab.com/mx-moment/moment/-/blob/main/docs/CHANGELOG.md",]
summary_source_url = "https://gitlab.com/mx-moment/moment/-/raw/main/packaging/moment.metainfo.xml"
screenshots = [ "https://gitlab.com/mx-moment/moment/-/raw/main/docs/screenshots/m01-chat.png", "https://gitlab.com/mx-moment/moment/-/raw/main/docs/screenshots/m02-sign-in.png", "https://gitlab.com/mx-moment/moment/-/raw/main/docs/screenshots/m03-account-settings.png", "https://gitlab.com/mx-moment/moment/-/raw/main/docs/screenshots/m04-create-room.png", "https://gitlab.com/mx-moment/moment/-/raw/main/docs/screenshots/m05-main-pane-small.png", "https://gitlab.com/mx-moment/moment/-/raw/main/docs/screenshots/m06-chat-small.png", "https://gitlab.com/mx-moment/moment/-/raw/main/docs/screenshots/m07-room-pane-small.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "xyz.mx_moment.moment"
scale_to_fit = ""
flathub = "https://flathub.org/apps/xyz.mx_moment.moment"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "moment",]
appstream_xml_url = "https://gitlab.com/mx-moment/moment/-/raw/main/packaging/moment.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

A fancy, customizable, keyboard-operable Matrix chat client for encrypted and decentralized communication.

[Source](https://gitlab.com/mx-moment/moment/-/raw/main/packaging/moment.metainfo.xml)

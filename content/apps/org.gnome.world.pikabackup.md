+++
title = "Pika Backup"
description = "Keep your data safe"
aliases = []
date = 2020-11-28
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC-BY-SA-4.0",]
app_author = [ "Small Mammal Collective",]
categories = [ "backup", "system utilities",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = [ "borg",]
packaged_in = [ "aur", "flathub", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "Archiving", "GNOME", "GTK", "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/World/pika-backup/"
homepage = "https://apps.gnome.org/app/PikaBackup/"
bugtracker = "https://gitlab.gnome.org/World/pika-backup/-/issues"
donations = "https://opencollective.com/pika-backup"
translations = "https://l10n.gnome.org/module/pika-backup/"
more_information = [ "https://apps.gnome.org/app/org.gnome.World.PikaBackup/",]
summary_source_url = "https://gitlab.gnome.org/World/pika-backup/-/raw/main/data/app.metainfo.xml.in"
screenshots = [ "https://gitlab.gnome.org/World/pika-backup/uploads/53e83ba7e2226880cef73675cd5d09d5/screenshot-3.png", "https://gitlab.gnome.org/World/pika-backup/uploads/82645d61948de08905c542b5d55e6709/screenshot-2.png", "https://gitlab.gnome.org/World/pika-backup/uploads/9fa4c818e0f8fda6983ca671f363698a/screenshot-1.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.World.PikaBackup"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.World.PikaBackup"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "pika-backup",]
appstream_xml_url = "https://gitlab.gnome.org/World/pika-backup/-/raw/main/data/app.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Doing backups the easy way. Plugin your USB drive and let the Pika do the rest for you.


* Create backups locally and remotely
* Set a schedule for regular backups
* Save time and disk space because Pika Backup does not need to copy known data again
* Encrypt your backups
* List created archives and browse through their contents
* Recover files or folders via your file browser


Pika Backup is designed to save your personal data and does not support complete system recovery. Pika Backup is powered by the well-tested BorgBackup software.

[Source](https://gitlab.gnome.org/World/pika-backup/-/raw/main/data/app.metainfo.xml.in)

+++
title = "Gerbil"
description = "Gerbil is a minimal gemini-client for linux and linux-mobile"
aliases = [ "apps/com.armen138.gerbil.desktop/",]
date = 2021-07-18
updated = 2024-01-02

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "armen138",]
categories = [ "gemini browser",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Network",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/armen138/gerbil"
homepage = ""
bugtracker = "https://gitlab.com/armen138/gerbil/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/armen138/gerbil"
screenshots = [ "https://twitter.com/linuxphoneapps/status/1416871975444746245",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "com.armen138.gerbil.desktop"
scale_to_fit = ""
flathub = ""
flatpak_link = "https://gitlab.com/armen138/gerbil/-/releases"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://gitlab.com/armen138/gerbil/-/raw/master/data/com.armen138.gerbil.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++
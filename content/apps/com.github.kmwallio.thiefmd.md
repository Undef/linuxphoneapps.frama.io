+++
title = "ThiefMD"
description = "The markdown editor worth stealing."
aliases = []
date = 2020-11-03
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "MIT",]
app_author = [ "kmwallio",]
categories = [ "text editor",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "Office", "Utility",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/kmwallio/ThiefMD/"
homepage = "https://thiefmd.com"
bugtracker = "https://github.com/kmwallio/thiefmd/issues"
donations = "https://github.com/sponsors/kmwallio"
translations = "https://poeditor.com/join/project?hash=iQkE5oTIOV"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/kmwallio/ThiefMD/master/data/com.github.kmwallio.thiefmd.appdata.xml"
screenshots = [ "https://thiefmd.com/images/grammar-notes.png", "https://thiefmd.com/images/preview.png", "https://thiefmd.com/images/theme_preferences.png", "https://thiefmd.com/images/thief_window.png", "https://thiefmd.com/images/thiefmd-screenplay.png", "https://thiefmd.com/images/write-good.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "com.github.kmwallio.thiefmd"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.github.kmwallio.thiefmd"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "thiefmd",]
appstream_xml_url = "https://raw.githubusercontent.com/kmwallio/ThiefMD/master/data/com.github.kmwallio.thiefmd.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Keep your Markdown managed. Write epic tales, a novel, that screen play, keep a journal, or finally write that book report.


ThiefMD is a Markdown Editor providing an easy way to organize, format, and compile your markdown documents. When you're ready to Publish, go to ePub, PDF, Office, Ghost, WordPress, Write-Freely, or more.


Import from and Export to Office, ePUB, HTML, and more. ThiefMD is your one stop shop for making and managing markdown mischief.


Working on a screenplay? ThiefMD can handle and combine Fountain files as well. Write your novel and prepare for that movie deal all from the same app.

[Source](https://raw.githubusercontent.com/kmwallio/ThiefMD/master/data/com.github.kmwallio.thiefmd.appdata.xml)

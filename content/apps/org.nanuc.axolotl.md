+++
title = "Axolotl"
description = "Axolotl is a crossplattform Signal client"
aliases = []
date = 2020-10-24
updated = 2023-12-31

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "nanu-c",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "Electron",]
backends = []
services = [ "Signal",]
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "Network", "Chat", "InstantMessaging",]
programming_languages = [ "Go", "Vue",]
build_systems = [ "various", "custom",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/nanu-c/axolotl"
homepage = "https://axolotl.chat/"
bugtracker = "https://github.com/nanu-c/axolotl/issues"
donations = "https://www.patreon.com/nanuc"
translations = ""
more_information = [ "https://web.archive.org/web/20230921190819/https://wiki.mobian.org/doku.php?id=axolotl",]
summary_source_url = "https://github.com/nanu-c/axolotl"
screenshots = [ "https://raw.githubusercontent.com/nanu-c/axolotl/main/screenshot.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.nanuc.Axolotl"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.nanuc.Axolotl"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "axolotl",]
appstream_xml_url = "https://raw.githubusercontent.com/nanu-c/axolotl/main/flatpak/org.nanuc.Axolotl.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description
Axolotl is a complete Signal client, it allows you to create a Signal
 account and have discussions with your contacts. Unlike the desktop
 Signal client, Axolotl is completely autonomous and doesn't require
 you to have created an account with the official Signal application.

[Source](https://raw.githubusercontent.com/nanu-c/axolotl/main/flatpak/org.nanuc.Axolotl.appdata.xml)

### Notice
To install it on Mobian see https://github.com/nuehm-arno/axolotl-mobian-package

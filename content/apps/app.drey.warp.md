+++
title = "Warp"
description = "Fast and secure file transfer"
aliases = []
date = 2022-05-21
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Fina Wilke",]
categories = [ "file transfer",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "Magic Wormhole",]
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "aur", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Rust", "Python",]
build_systems = [ "meson", "cargo",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/World/warp"
homepage = ""
bugtracker = "https://apps.gnome.org/app/app.drey.Warp/"
donations = ""
translations = "https://l10n.gnome.org/module/warp/"
more_information = []
summary_source_url = "https://apps.gnome.org/app/app.drey.Warp/"
screenshots = [ "https://gitlab.gnome.org/World/warp/raw/main/data/screenshots/screenshot1.png", "https://gitlab.gnome.org/World/warp/raw/main/data/screenshots/screenshot2.png", "https://gitlab.gnome.org/World/warp/raw/main/data/screenshots/screenshot3.png", "https://gitlab.gnome.org/World/warp/raw/main/data/screenshots/screenshot4.png", "https://gitlab.gnome.org/World/warp/raw/main/data/screenshots/screenshot5.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "app.drey.Warp"
scale_to_fit = "app.drey.Warp"
flathub = "https://flathub.org/apps/app.drey.Warp"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "warp-share-files",]
appstream_xml_url = "https://gitlab.gnome.org/World/warp/-/raw/main/data/app.drey.Warp.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Warp allows you to securely send files to each other via the internet or local network by
 exchanging a word-based code.


The best transfer method will be determined using the “Magic Wormhole” protocol which includes
 local network transfer if possible.


Features


* Send files between multiple devices
* Every file transfer is encrypted
* Directly transfer files on the local network if possible
* An internet connection is required
* QR Code support
* Compatibility with the Magic Wormhole command line client and all other compatible apps

[Source](https://gitlab.gnome.org/World/warp/-/raw/main/data/app.drey.Warp.metainfo.xml.in.in)

### Notice

Fully adaptive since 0.2.0.

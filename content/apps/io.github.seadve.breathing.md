+++
title = "Breathing"
description = "Relax and meditate"
aliases = []
date = 2021-08-07
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Dave Patrick Caberto",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/SeaDve/Breathing"
homepage = "https://github.com/SeaDve/Breathing"
bugtracker = "https://github.com/SeaDve/Breathing/issues"
donations = "https://www.buymeacoffee.com/seadve"
translations = "https://hosted.weblate.org/projects/kooha/breathing/"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/SeaDve/Breathing/main/data/io.github.seadve.Breathing.appdata.xml.in"
screenshots = [ "https://raw.githubusercontent.com/SeaDve/Breathing/main/screenshots/screenshot1.png", "https://raw.githubusercontent.com/SeaDve/Breathing/main/screenshots/screenshot2.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "io.github.seadve.Breathing"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.seadve.Breathing"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "breathing",]
appstream_xml_url = "https://raw.githubusercontent.com/SeaDve/Breathing/main/data/io.github.seadve.Breathing.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Relax, focus, and become stress-free.


Breathing is a very simple application that guides your breathing pattern. This pattern is recommended by experts that will help ease your anxiety. It also provides a calming sound to make it much easier to relax.


The main features of Breathing includes the following:


* 🌬️ Guide your breathing.
* 🌑 Change to a dark-mode with ease.
* 📱 Easy-to-use user interface.
* ⌨️ User-friendly keyboard shortcuts.

[Source](https://raw.githubusercontent.com/SeaDve/Breathing/main/data/io.github.seadve.Breathing.appdata.xml.in)

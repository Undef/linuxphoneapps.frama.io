+++
title = "Emulsion"
description = "Stock up on colors"
aliases = []
date = 2021-05-19
updated = 2023-12-31

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Lains",]
categories = [ "development",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Graphics", "Publishing",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/lainsce/emulsion"
homepage = "https://github.com/lainsce/emulsion/"
bugtracker = "https://github.com/lainsce/emulsion/issues"
donations = "https://www.ko-fi.com/lainsce/"
translations = "https://github.com/lainsce/emulsion/blob/master/po/README.md"
more_information = []
summary_source_url = "https://github.com/lainsce/emulsion/"
screenshots = [ "https://raw.githubusercontent.com/lainsce/emulsion/master/data/shot.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "io.github.lainsce.Emulsion"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.lainsce.Emulsion"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "emulsion",]
appstream_xml_url = "https://raw.githubusercontent.com/lainsce/emulsion/main/data/io.github.lainsce.Emulsion.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description
Store your palettes in an easy way, and edit them if needed.

[Source](https://raw.githubusercontent.com/lainsce/emulsion/main/data/io.github.lainsce.Emulsion.metainfo.xml.in)

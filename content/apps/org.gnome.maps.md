+++
title = "Maps"
description = "Find places around the world"
aliases = []
date = 2021-05-16
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The GNOME Project",]
categories = [ "maps and navigation", "geography",]
mobile_compatibility = [ "4",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "libshumate",]
services = [ "openstreetmap",]
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "Maps",]
programming_languages = [ "JavaScript", "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnome-maps"
homepage = "https://apps.gnome.org/Maps/"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-maps/issues"
donations = "https://www.gnome.org/donate/"
translations = "https://l10n.gnome.org/module/gnome-maps/"
more_information = [ "https://linmob.net/gnome-3-38-and-linux-smartphones/#gnome-maps", "https://apps.gnome.org/app/org.gnome.Maps/", "https://ml4711.blogspot.com/2021/05/spring-maps.html",]
summary_source_url = "https://gitlab.gnome.org/GNOME/gnome-maps/-/raw/main/data/org.gnome.Maps.appdata.xml.in.in"
screenshots = [ "https://gitlab.gnome.org/GNOME/gnome-maps/raw/HEAD/data/screenshots/maps-main.png", "https://gitlab.gnome.org/GNOME/gnome-maps/raw/HEAD/data/screenshots/maps-pinpoint.png", "https://gitlab.gnome.org/GNOME/gnome-maps/raw/HEAD/data/screenshots/maps-route.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/org.gnome.maps/1.png", "https://img.linuxphoneapps.org/org.gnome.maps/2.png", "https://img.linuxphoneapps.org/org.gnome.maps/3.png", "https://img.linuxphoneapps.org/org.gnome.maps/4.png", "https://img.linuxphoneapps.org/org.gnome.maps/5.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.Maps"
scale_to_fit = "org.gnome.Maps"
flathub = "https://flathub.org/apps/org.gnome.Maps"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-maps",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-maps/-/raw/main/data/org.gnome.Maps.appdata.xml.in.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Maps gives you quick access to maps all across the world. It allows you
 to quickly find the place you’re looking for by searching for a city or
 street, or locate a place to meet a friend.


Maps uses the collaborative OpenStreetMap database, made by hundreds of
 thousands of people across the globe.

[Source](https://gitlab.gnome.org/GNOME/gnome-maps/-/raw/main/data/org.gnome.Maps.appdata.xml.in.in)

### Notice

Release 45 scales perfectly fine - the navigation view that was problematic before, is now fixed. Was GTK3/libhandy before 43.

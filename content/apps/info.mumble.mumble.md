+++
title = "Mumble"
description = "Low latency encrypted VoIP client"
aliases = []
date = 2020-10-21
updated = 2023-12-31

[taxonomies]
project_licenses = [ "BSD-3-Clause",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The Mumble Dev-Team",]
categories = [ "voice chat",]
mobile_compatibility = [ "2",]
status = []
frameworks = [ "QtWidgets",]
backends = []
services = [ "Mumble",]
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Qt", "Network", "Audio",]
programming_languages = [ "Cpp", "C",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/mumble-voip/mumble"
homepage = "https://www.mumble.info/"
bugtracker = "https://github.com/mumble-voip/mumble/issues"
donations = "https://liberapay.com/mumble"
translations = "https://wiki.mumble.info/wiki/Language_Translation"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/mumble-voip/mumble/master/auxiliary_files/config_files/info.mumble.Mumble.appdata.xml.in"
screenshots = [ "https://raw.githubusercontent.com/mumble-voip/mumble/master/screenshots/Mumble.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "info.mumble.Mumble"
scale_to_fit = "net.sourgeforge.mumble.mumble"
flathub = "https://flathub.org/apps/info.mumble.Mumble"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "mumble",]
appstream_xml_url = "https://raw.githubusercontent.com/mumble-voip/mumble/master/auxiliary_files/config_files/info.mumble.Mumble.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++

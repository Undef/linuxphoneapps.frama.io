+++
title = "YouPlay"
description = "Search, download and play music from YouTube"
aliases = [ "apps/noappid.ralfhersel.youplay/",]
date = 2020-12-12
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Ralf Hersel",]
categories = [ "music player",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "yt-dlp", "mpv",]
services = [ "YouTube",]
packaged_in = [ "aur",]
freedesktop_categories = [ "GTK", "Audio", "Network", "Player",]
programming_languages = [ "Python",]
build_systems = [ "none",]
requires_internet = []
tags = []

[extra]
repository = "https://codeberg.org/ralfhersel/youplay"
homepage = "https://codeberg.org/ralfhersel/youplay/"
bugtracker = "https://codeberg.org/ralfhersel/youplay/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://codeberg.org/ralfhersel/youplay/raw/branch/main/org.codeberg.ralfhersel.youplay.metainfo.xml"
screenshots = [ "https://codeberg.org/ralfhersel/youplay/media/branch/main/img/screenshot_cli.png", "https://codeberg.org/ralfhersel/youplay/media/branch/main/img/screenshot_gui.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.codeberg.ralfhersel.youplay"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "youplay",]
appstream_xml_url = "https://codeberg.org/ralfhersel/youplay/raw/branch/main/org.codeberg.ralfhersel.youplay.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Do you love music, but hate restrictions? With YouPlay you can search for whatever music in your mind. It will list a number of matching songs from YouTube. You can listen to it and YouPlay will download it as an mp3 file.


YouPlay features:


* Search music on YouTube by title or artist
* Get 10 search results
* Play your song
* If you play a song, it will be downloaded to your music folder/youplay
* Songs will be downloaded in the highest available quality from YouTube
* Already downloaded songs will not be downloaded again
* You can pause, jump back and forward while playing
* YouPlay has a GUI and CLI mode

[Source](https://codeberg.org/ralfhersel/youplay/raw/branch/main/org.codeberg.ralfhersel.youplay.metainfo.xml)

+++
title = "gedit"
description = "Text editor"
aliases = []
date = 2020-12-12
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "GNOME Developers",]
categories = [ "text editor",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "TextEditor",]
programming_languages = [ "C", "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/GNOME/gedit"
homepage = "https://gedit-technology.github.io/apps/gedit/"
bugtracker = "https://gedit-technology.github.io/apps/gedit/reporting-bugs.html"
donations = ""
translations = "https://wiki.gnome.org/TranslationProject"
more_information = []
summary_source_url = "https://gitlab.gnome.org/GNOME/gedit/-/raw/master/data/org.gnome.gedit.appdata.xml.in"
screenshots = [ "https://gitlab.gnome.org/swilmet/gedit-extra/-/raw/main/screenshots/screenshot.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.gnome.gedit.desktop"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.gedit"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gedit",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gedit/-/raw/master/data/org.gnome.gedit.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

gedit is an easy-to-use and general-purpose text editor. Its development
 started in 1998, at the beginnings of the GNOME project, with a good
 integration with that desktop environment.


You can use it to write simple notes and documents, or you can enable more
 advanced features that are useful for software development.

[Source](https://gitlab.gnome.org/GNOME/gedit/-/raw/master/data/org.gnome.gedit.appdata.xml.in)

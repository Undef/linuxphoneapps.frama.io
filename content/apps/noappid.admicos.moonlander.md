+++
title = "Moonlander"
description = "Just another 'fancy' Gemini client."
aliases = []
date = 2021-05-13
updated = 2023-09-30

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = []
app_author = [ "admicos",]
categories = [ "gemini browser",]
mobile_compatibility = [ "4",]
status = [ "early", "inactive", "pre-release",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "aur", "nix_stable_23_05", "nix_stable_23_11",]
freedesktop_categories = [ "GTK", "GNOME", "Network",]
programming_languages = [ "Rust",]
build_systems = [ "cargo",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://git.sr.ht/~admicos/moonlander/"
homepage = ""
bugtracker = "https://todo.sr.ht/~admicos/moonlander"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://sr.ht/~admicos/moonlander/"
screenshots = [ "https://sr.ht/~admicos/moonlander/",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = "moonlander"
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "moonlander",]
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "linmob"

+++


### Description

Just another "fancy" Gemini client.

Status: Alpha, Maintenance mode

Moonlander is currently in maintenance mode. It works and I still use it as a daily driver, but I probably won't add any new features, bug fixes, or anything else for some time. I will most likely visit it later on, but just not today.

[Source](https://git.sr.ht/~admicos/moonlander)

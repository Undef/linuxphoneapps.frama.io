+++
title = "Journald Browser"
description = "Browser for journald databases"
aliases = []
date = 2024-01-14

[taxonomies]
project_licenses = [ "LGPL-2.1-or-later", "MIT",]
metadata_licenses = [ "CC0-1.0",]
app_author = []
categories = [ "system utilities",]
mobile_compatibility = [ "needs testing",]
status = [ "maturing",]
frameworks = [ "Kirigami", "QtQuick",]
backends = []
services = []
packaged_in = [ "arch", "archlinuxarm_aarch64", "fedora_38", "fedora_39", "fedora_rawhide", "manjaro_stable", "manjaro_unstable", "opensuse_tumbleweed", "snapcraft",]
freedesktop_categories = [ "Qt", "KDE", "System",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/system/kjournald"
homepage = "https://invent.kde.org/libraries/kjournald"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?format=guided&product=kjournald"
donations = "https://www.kde.org/community/donations/?app=kjournald&source=appdata"
translations = ""
more_information = [ "https://apps.kde.org/kjournaldbrowser/",]
summary_source_url = "https://invent.kde.org/system/kjournald/-/raw/master/browser/org.kde.kjournaldbrowser.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/kjournald/kjournald.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.kjournaldbrowser.desktop"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://invent.kde.org/system/kjournald/-/raw/master/.flatpak-manifest.json"
snapcraft = "https://snapcraft.io/kjournald"
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "kjournald",]
appstream_xml_url = "https://invent.kde.org/system/kjournald/-/raw/master/browser/org.kde.kjournaldbrowser.appdata.xml"
reported_by = "linmob"
updated_by = ""

+++

### Description

Journald-browser is a reference implementation of the kjournald library and provides a flexible tool to browse in local, remote and offline journald databases.

[Source](https://invent.kde.org/system/kjournald/-/raw/master/browser/org.kde.kjournaldbrowser.appdata.xml)

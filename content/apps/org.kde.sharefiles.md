+++
title = "Kirigami Example Application"
description = "A short summary describing what this software is about"
aliases = []
date = 2020-03-02
updated = 2024-01-02

[taxonomies]
project_licenses = [ "The license of this software as SPDX string, e.g. \"GPL-3+\"",]
metadata_licenses = [ "A permissive license for this metadata, e.g. \"FSFAP\"",]
app_author = [ "The software vendor name", "e.g. \"ACME Corporation\"",]
categories = [ "cloud syncing",]
mobile_compatibility = [ "5",]
status = [ "inactive",]
frameworks = [ "Kirigami",]
backends = []
services = [ "Nextcloud",]
packaged_in = []
freedesktop_categories = [ "Qt", "KDE", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/rpatwal/nextcloud-plasma-mobile"
homepage = ""
bugtracker = "https://invent.kde.org/rpatwal/nextcloud-plasma-mobile/-/issues/"
donations = ""
translations = ""
more_information = [ "https://orepoala.home.blog/2019/06/11/nextcloud-login-plugin-for-plamo/", "https://community.kde.org/GSoC/2019/StatusReports/ORePoala", "https://summerofcode.withgoogle.com/archive/2019/projects/6343810290810880/",]
summary_source_url = "https://invent.kde.org/rpatwal/nextcloud-plasma-mobile/-/raw/master/org.kde.sharefiles.appdata.xml"
screenshots = [ "https://orepoala.home.blog/2019/06/11/nextcloud-login-plugin-for-plamo/",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.sharefiles"
scale_to_fit = "org.kde.sharefiles"
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://invent.kde.org/rpatwal/nextcloud-plasma-mobile/-/raw/master/org.kde.sharefiles.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

Multiple paragraphs of long description, describing this software component.


You can also use ordered and unordered lists:


* Feature 1
* Feature 2


Keep in mind to XML-escape characters, and that this is not HTML markup.

[Source](https://invent.kde.org/rpatwal/nextcloud-plasma-mobile/-/raw/master/org.kde.sharefiles.appdata.xml)

### Notice

Last commit in July 2019.
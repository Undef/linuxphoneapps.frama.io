+++
title = "Weather"
description = "View real-time weather forecasts and other information"
aliases = []
date = 2020-08-25
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community",]
categories = [ "weather",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "Kirigami",]
backends = [ "api.met.no", "geonames.org", "geoip.ubuntu.com", "openweathermap.org",]
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "KDE", "Utility",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/utilities/kweather"
homepage = "https://invent.kde.org/plasma-mobile/kweather"
bugtracker = "https://bugs.kde.org/describecomponents.cgi?product=kweather"
donations = ""
translations = ""
more_information = [ "https://plasma-mobile.org/2022/11/30/plasma-mobile-gear-22-11/#weather",]
summary_source_url = "https://invent.kde.org/utilities/kweather/-/raw/master/org.kde.kweather.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/kweather/kweather-desktop-dynamic.png", "https://cdn.kde.org/screenshots/kweather/kweather-mobile-dynamic.png", "https://cdn.kde.org/screenshots/kweather/kweather-mobile-dynamic2.png", "https://cdn.kde.org/screenshots/kweather/kweather-mobile-flat.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.kde.kweather"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.kweather"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "kweather",]
appstream_xml_url = "https://invent.kde.org/utilities/kweather/-/raw/master/org.kde.kweather.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

A convergent weather application for Plasma. Has flat and dynamic/animated views for showing forecasts and other information.

[Source](https://invent.kde.org/utilities/kweather/-/raw/master/org.kde.kweather.appdata.xml)

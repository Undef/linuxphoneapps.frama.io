+++
title = "Liri Calculator"
description = "Perform arithmetic or scientific calculations"
aliases = []
date = 2019-02-01
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The Liri developers",]
categories = [ "calculator",]
mobile_compatibility = [ "4",]
status = []
frameworks = [ "QtQuick", "fluid",]
backends = []
services = []
packaged_in = [ "arch", "archlinuxarm_aarch64", "aur", "flathub", "manjaro_stable", "manjaro_unstable",]
freedesktop_categories = [ "Qt", "Utility", "Calculator",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/lirios/calculator"
homepage = "https://liri.io"
bugtracker = "https://github.com/lirios/calculator/issues/new"
donations = "https://liri.io/get-involved/"
translations = "https://www.transifex.com/lirios/liri-calculator/"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/lirios/calculator/develop/data/io.liri.Calculator.appdata.xml"
screenshots = [ "https://raw.githubusercontent.com/lirios/calculator/develop/.project/screenshots/calculator1.png", "https://raw.githubusercontent.com/lirios/calculator/develop/.project/screenshots/calculator2.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "io.liri.Calculator"
scale_to_fit = "io.liri.Calculator"
flathub = "https://flathub.org/apps/io.liri.Calculator"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "liri-calculator",]
appstream_xml_url = "https://raw.githubusercontent.com/lirios/calculator/develop/data/io.liri.Calculator.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

Liri Calculator is a Material Design application that solves mathematical equations.


At first it appears to be a simple calculator with only basic arithmetic operations.
 However it features a flexible expression parser with support for symbolic
 computation, a large set of built-in functions and constants and works with different
 data types like numbers, big numbers, complex numbers, fractions, units and matrices.

[Source](https://raw.githubusercontent.com/lirios/calculator/develop/data/io.liri.Calculator.appdata.xml)

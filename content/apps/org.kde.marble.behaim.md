+++
title = "Behaim Globe"
description = "Martin Behaim's Erdapfel"
aliases = []
date = 2019-02-01
updated = 2024-01-02

[taxonomies]
project_licenses = [ "LGPL-2.1-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community",]
categories = [ "geography",]
mobile_compatibility = [ "5",]
status = [ "inactive",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Qt", "KDE", "Education", "Geography",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/education/marble"
homepage = "https://marble.kde.org/"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?format=guided&product=marble"
donations = "https://www.kde.org/community/donations/?app=marble&source=appdata"
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/education/marble/-/raw/master/src/apps/behaim/org.kde.marble.behaim.appdata.xml"
screenshots = [ "http://web.archive.org/web/20190417061449/https://play.google.com/store/apps/details?id=org.kde.marble.behaim",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.marble.behaim.desktop"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "marble",]
appstream_xml_url = "https://invent.kde.org/education/marble/-/raw/master/src/apps/behaim/org.kde.marble.behaim.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

The oldest existent globe of the Earth. Martin Behaim and collaborators created the globe around 1492 at the time of Columbus' first sea travel to the west.

[Source](https://invent.kde.org/education/marble/-/raw/master/src/apps/behaim/org.kde.marble.behaim.appdata.xml)

### Notice

Part of the debian/pureos package marble-maps, does not come with a launcher though. Launch it with marble-behaim

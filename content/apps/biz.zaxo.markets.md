+++
title = "Markets"
description = "Keep track of your investments"
aliases = [ "apps/com.bitstower.markets/",]
date = 2020-08-25
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Tomasz Oponowicz",]
categories = [ "stocks",]
mobile_compatibility = [ "5",]
status = [ "archived",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "aur", "fedora_38", "gnuguix", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "News",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/tomasz-oponowicz/markets"
homepage = "https://github.com/tomasz-oponowicz/markets"
bugtracker = "https://github.com/tomasz-oponowicz/markets/issues"
donations = ""
translations = ""
more_information = [ "http://web.archive.org/web/20230128222300/https://apps.gnome.org/app/com.bitstower.Markets/",]
summary_source_url = "https://raw.githubusercontent.com/tomasz-oponowicz/markets/master/data/biz.zaxo.Markets.appdata.xml.in"
screenshots = [ "https://raw.githubusercontent.com/tomasz-oponowicz/markets/master/preview.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "biz.zaxo.Markets"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://raw.githubusercontent.com/tomasz-oponowicz/markets/master/biz.zaxo.Markets.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "markets",]
appstream_xml_url = "https://raw.githubusercontent.com/tomasz-oponowicz/markets/master/data/biz.zaxo.Markets.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

The Markets app delivers financial data to your fingertips.
 Stay on top of the market and never miss an investment opportunity!


Features:


* Create your personal portfolio
* Track stocks, currencies, cryptocurrencies, commodities and indexes
* Compatible with Linux smartphones (Librem5, PinePhone)
* Open any symbol in Yahoo Finance for more details
* Adjust the refresh rate
* Dark Mode

[Source](https://raw.githubusercontent.com/tomasz-oponowicz/markets/master/data/biz.zaxo.Markets.appdata.xml.in)

### Notice

Archived in late 2022, app ID changed from `com.bitstower.Markets` to `biz.zaxo.Markets`, last release and EOL'd/archived on 2023-03-03.

+++
title = "Warpinator"
description = "Send and Receive Files across the Network"
aliases = []
date = 2022-03-12
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Linux Mint",]
categories = [ "file transfer",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK3", "xapps",]
backends = []
services = []
packaged_in = [ "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "Network", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/linuxmint/warpinator"
homepage = "https://github.com/linuxmint/warpinator"
bugtracker = "https://github.com/linuxmint/warpinator/issues"
donations = "https://www.linuxmint.com/donors.php"
translations = "https://translations.launchpad.net/linuxmint/latest/+pots/warpinator"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/linuxmint/warpinator/master/data/org.x.Warpinator.appdata.xml.in.in"
screenshots = [ "https://github.com/linuxmint/warpinator/raw/a0d19c656068e84860c1e9b31a0ed20d6aeb266e/data/appdata/screenshot-1.png", "https://github.com/linuxmint/warpinator/raw/a0d19c656068e84860c1e9b31a0ed20d6aeb266e/data/appdata/screenshot-2.png", "https://github.com/linuxmint/warpinator/raw/a0d19c656068e84860c1e9b31a0ed20d6aeb266e/data/appdata/screenshot-3.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.x.Warpinator"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.x.Warpinator"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "warpinator",]
appstream_xml_url = "https://raw.githubusercontent.com/linuxmint/warpinator/master/data/org.x.Warpinator.appdata.xml.in.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Warpinator allows you to easily connect multiple computers on a local area network and share files quickly and securely.

[Source](https://raw.githubusercontent.com/linuxmint/warpinator/master/data/org.x.Warpinator.appdata.xml.in.in)

### Notice

While some elements of the screen may be cut off, it generally scales extremely well. With flatpak, the file picker dialog is not fitting well, but hitting enter can solve this.

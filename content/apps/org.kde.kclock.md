+++
title = "Clock"
description = "Set alarms and timers, use a stopwatch, and manage world clocks"
aliases = []
date = 2020-08-25
updated = 2024-01-14

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community",]
categories = [ "clock",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_13", "debian_unstable", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "Clock", "KDE", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/utilities/kclock"
homepage = "https://invent.kde.org/plasma-mobile/kclock"
bugtracker = "https://bugs.kde.org/describecomponents.cgi?product=KClock"
donations = ""
translations = ""
more_information = [ "https://plasma-mobile.org/2021/04/27/plasma-mobile-update-march-april/", "https://apps.kde.org/kclock/",]
summary_source_url = "https://invent.kde.org/utilities/kclock/-/raw/master/org.kde.kclock.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/kclock/kclock-desktop-timer.png", "https://cdn.kde.org/screenshots/kclock/kclock-desktop-timezones.png", "https://cdn.kde.org/screenshots/kclock/kclock-mobile-alarms.png", "https://cdn.kde.org/screenshots/kclock/kclock-mobile-stopwatch.png", "https://cdn.kde.org/screenshots/kclock/kclock-mobile-timers.png", "https://cdn.kde.org/screenshots/kclock/kclock-mobile-timezones.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.kde.kclock"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.kclock"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "kclock", "kde5-kclock",]
appstream_xml_url = "https://invent.kde.org/utilities/kclock/-/raw/master/org.kde.kclock.appdata.xml"
reported_by = "linmob"
updated_by = "check_via_repology"

+++

### Description

A universal clock application for desktop and mobile.


It contains alarm, timer, stopwatch and timezone functionalities. Alarms and timers are able to wake the device from suspend on Plasma.

[Source](https://invent.kde.org/utilities/kclock/-/raw/master/org.kde.kclock.appdata.xml)
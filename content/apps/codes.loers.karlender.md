+++
title = "Karlender"
description = "GTK calendar application"
aliases = []
date = 2022-03-31
updated = 2023-12-31

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "loers",]
categories = [ "calendar",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "minicaldav",]
services = [ "CalDAV",]
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "aur", "flathub", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Office", "Calendar",]
programming_languages = [ "Rust",]
build_systems = [ "cargo",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/floers/karlender"
homepage = ""
bugtracker = "https://gitlab.com/floers/karlender/-/issues"
donations = ""
translations = ""
more_information = [ "https://gitlab.com/floers/karlender/-/blob/main/ROADMAP.md",]
summary_source_url = "https://gitlab.com/floers/karlender"
screenshots = [ "https://gitlab.com/floers/karlender/-/raw/main/screenshots/portrait.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "codes.loers.Karlender"
scale_to_fit = ""
flathub = "https://flathub.org/apps/codes.loers.Karlender"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "karlender",]
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"

+++


### Description

GTK calendar application written in Rust. Karlender is a mobile-friendly calendar app. It uses libadwaita and can access Calendars using CalDAV. [Source](https://gitlab.com/floers/karlender)

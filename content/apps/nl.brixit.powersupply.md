+++
title = "Powersupply"
description = "Linux power subsystem debugger"
aliases = [ "apps/noappid.martijnbraam.powersupply/",]
date = 2022-03-25
updated = 2024-01-14

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Martijn Braam",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "alpine_edge", "aur", "debian_12", "debian_13", "debian_unstable", "devuan_unstable", "flathub", "pureos_landing",]
freedesktop_categories = [ "GTK", "System",]
programming_languages = [ "Python",]
build_systems = [ "setup.py",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/MartijnBraam/powersupply"
homepage = "https://gitlab.com/MartijnBraam/powersupply"
bugtracker = "https://gitlab.com/MartijnBraam/powersupply/-/issues"
donations = ""
translations = ""
more_information = [ "https://wiki.postmarketos.org/wiki/Applications_by_category#System",]
summary_source_url = "https://gitlab.com/MartijnBraam/powersupply"
screenshots = [ "http://brixitcdn.net/metainfo/powersupply.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = true
app_id = "nl.brixit.powersupply"
scale_to_fit = ""
flathub = "https://flathub.org/apps/nl.brixit.powersupply"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "powersupply",]
appstream_xml_url = "https://gitlab.com/MartijnBraam/powersupply/-/raw/master/data/nl.brixit.powersupply.appdata.xml"
reported_by = "linmob"
updated_by = "check_via_repology"

+++

### Description
An application that displays the state of various batteries and powersupplies in mobile platforms.

[Source](https://gitlab.com/MartijnBraam/powersupply/-/raw/master/data/nl.brixit.powersupply.appdata.xml)
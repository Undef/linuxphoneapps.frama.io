+++
title = "WebArchives"
description = "A web archives viewer"
aliases = []
date = 2021-04-29
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "birros",]
categories = [ "education",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Education", "Humanities",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/birros/web-archives"
homepage = "https://github.com/birros/web-archives"
bugtracker = "https://github.com/birros/web-archives/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/birros/web-archives/master/data/appdata/com.github.birros.WebArchives.appdata.xml.in"
screenshots = [ "https://raw.githubusercontent.com/birros/web-archives/master/data/appdata/screenshots/home.png", "https://raw.githubusercontent.com/birros/web-archives/master/data/appdata/screenshots/search.png", "https://raw.githubusercontent.com/birros/web-archives/master/data/appdata/screenshots/web-earth.png", "https://raw.githubusercontent.com/birros/web-archives/master/data/appdata/screenshots/web-wikipedia.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "com.github.birros.WebArchives.desktop"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.github.birros.WebArchives"
flatpak_link = ""
flatpak_recipe = "https://raw.githubusercontent.com/birros/web-archives/master/build-aux/flatpak/com.github.birros.WebArchives.yml"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/birros/web-archives/master/data/appdata/com.github.birros.WebArchives.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

A web archives viewer offering the ability to browse offline
 millions of articles from large community projects such as Wikipedia
 or Wikisource.


Features:


* List recently opened web archives
* List available local web archives
* List of web archives available to download
* Print a page
* Night mode (Darkreader)
* Zoom controls
* Search in page
* History
* Bookmarks
* Search a page
* Keyboard shortcuts
* Multi-windows
* Multi-tabs
* Random page
* Sandboxed pages (Pages are isolated from the web)
* Ask for confirmation when opening an external link
* Handle the opening of zim files from external applications (Nautilus...)

[Source](https://raw.githubusercontent.com/birros/web-archives/master/data/appdata/com.github.birros.WebArchives.appdata.xml.in)

### Notice

Only supports ZIM files, but works pretty great!
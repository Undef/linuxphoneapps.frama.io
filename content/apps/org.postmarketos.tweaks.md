+++
title = "postmarketOS Tweaks"
description = "Mobile settings tweak application"
aliases = []
date = 2021-04-07
updated = 2023-09-30

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "martijnbraam",]
categories = [ "system utilities",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "postmarketos_master",]
freedesktop_categories = [ "GTK", "GNOME", "Settings", "DesktopSettings",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/postmarketOS/postmarketos-tweaks"
homepage = ""
bugtracker = "https://gitlab.com/postmarketOS/postmarketos-tweaks/-/issues/"
donations = ""
translations = ""
more_information = [ "https://www.youtube.com/watch?v=X_QuQKhEVRA",]
summary_source_url = "https://gitlab.com/postmarketOS/postmarketos-tweaks/-/raw/master/data/org.postmarketos.Tweaks.metainfo.xml"
screenshots = [ "https://brixitcdn.net/metainfo/tweaks.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.postmarketos.Tweaks"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "postmarketos-tweaks",]
appstream_xml_url = "https://gitlab.com/postmarketOS/postmarketos-tweaks/-/raw/master/data/org.postmarketos.Tweaks.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description
A Gnome-Tweaks like application for modifying phone settings

[Source](https://gitlab.com/postmarketOS/postmarketos-tweaks/-/raw/master/data/org.postmarketos.Tweaks.metainfo.xml)

+++
title = "Calindori"
description = "Calendar application"
aliases = []
date = 2019-02-01
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Plasma Mobile Developers",]
categories = [ "calendar",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "Kirigami",]
backends = [ "icalendar",]
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_13", "debian_unstable", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "Calendar", "KDE", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/plasma-mobile/calindori"
homepage = "https://invent.kde.org/plasma-mobile/calindori"
bugtracker = "https://bugs.kde.org/describecomponents.cgi?product=calindori"
donations = "https://kde.org/community/donations/"
translations = "https://l10n.kde.org/stats/gui/trunk-kf5/package/calindori/"
more_information = [ "https://phabricator.kde.org/T6942",]
summary_source_url = "https://invent.kde.org/plasma-mobile/calindori/-/raw/master/org.kde.calindori.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/calindori/desktop.png", "https://cdn.kde.org/screenshots/calindori/mobile.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/org.kde.calindori/1-monthview.png", "https://img.linuxphoneapps.org/org.kde.calindori/2-dayview.png", "https://img.linuxphoneapps.org/org.kde.calindori/3-eventview.png", "https://img.linuxphoneapps.org/org.kde.calindori/4-taskview.png", "https://img.linuxphoneapps.org/org.kde.calindori/5-sidebar.png", "https://img.linuxphoneapps.org/org.kde.calindori/6-createevent.png", "https://img.linuxphoneapps.org/org.kde.calindori/7-timepicker.png", "https://img.linuxphoneapps.org/org.kde.calindori/8-createtask.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "org.kde.calindori.desktop"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "calindori",]
appstream_xml_url = "https://invent.kde.org/plasma-mobile/calindori/-/raw/master/org.kde.calindori.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

Calindori is a touch friendly calendar application. It has been designed for mobile devices but it can also run on desktop environments. Users of Calindori are able to check previous and future dates and manage tasks and events.


When executing the application for the first time, a new calendar file is created that follows the ical standard. Alternatively, users may create additional calendars or import existing ones.


Features:


* Agenda
* Events
* Todos
* Multiple calendars
* Import calendar

[Source](https://invent.kde.org/plasma-mobile/calindori/-/raw/master/org.kde.calindori.appdata.xml)

### Notice

Part of the GUI will be cut off if the window is too small (e.g. when the OSK is opened), reference those upstream issues: [1](https://invent.kde.org/plasma-mobile/calindori/-/issues/19) [2](https://bugs.kde.org/show_bug.cgi?id=456406
)

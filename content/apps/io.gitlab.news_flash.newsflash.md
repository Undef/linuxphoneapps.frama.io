+++
title = "NewsFlash"
description = "Keep up with your feeds"
aliases = [ "apps/com.gitlab.newsflash/",]
date = 2020-08-24
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Jan Lukas Gernert",]
categories = [ "feed reader",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Miniflux", "feedly", "feedbin", "RSS",]
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Feed", "News",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "editors choice",]

[extra]
repository = "https://gitlab.com/news_flash/news_flash_gtk"
homepage = "https://gitlab.com/news_flash/news_flash_gtk"
bugtracker = "https://gitlab.com/news_flash/news_flash_gtk/-/issues"
donations = ""
translations = "https://hosted.weblate.org/projects/newsflash/news_flash_gtk/"
more_information = [ "https://apps.gnome.org/app/com.gitlab.newsflash/", "https://linmob.net/2020/07/31/pinephone-daily-driver-challenge-part3-reading-apps-and-email.html#newsflash",]
summary_source_url = "https://gitlab.com/news-flash/news_flash_gtk/"
screenshots = [ "https://gitlab.com/news_flash/news_flash_gtk/-/raw/master/data/screenshots/Adaptive.png", "https://gitlab.com/news_flash/news_flash_gtk/-/raw/master/data/screenshots/Dark.png", "https://gitlab.com/news_flash/news_flash_gtk/-/raw/master/data/screenshots/Main.png", "https://gitlab.com/news_flash/news_flash_gtk/-/raw/master/data/screenshots/Preferences.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/com.gitlab.newsflash/1.png", "https://img.linuxphoneapps.org/com.gitlab.newsflash/2.png", "https://img.linuxphoneapps.org/com.gitlab.newsflash/3.png", "https://img.linuxphoneapps.org/com.gitlab.newsflash/4.png", "https://img.linuxphoneapps.org/com.gitlab.newsflash/5.png", "https://img.linuxphoneapps.org/com.gitlab.newsflash/6.png", "https://img.linuxphoneapps.org/com.gitlab.newsflash/7.png", "https://img.linuxphoneapps.org/com.gitlab.newsflash/8.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "io.gitlab.news_flash.NewsFlash"
scale_to_fit = "NewsflashGTK"
flathub = "https://flathub.org/apps/io.gitlab.news_flash.NewsFlash"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "newsflash",]
appstream_xml_url = "https://gitlab.com/news-flash/news_flash_gtk/-/raw/master/data/io.gitlab.news_flash.NewsFlash.appdata.xml.in.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

NewsFlash is a program designed to complement an already existing web-based RSS reader account.


It combines all the advantages of web based services like syncing across all your devices with everything you expect
 from a modern desktop program: Desktop notifications, fast search and filtering, tagging, handy keyboard shortcuts
 and having access to all your articles for as long as you like.

[Source](https://gitlab.com/news-flash/news_flash_gtk/-/raw/master/data/io.gitlab.news_flash.NewsFlash.appdata.xml.in.in)

### Notice

Was GTK3/libhandy before release 2.0.

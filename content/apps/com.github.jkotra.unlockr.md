+++
title = "unlockR"
description = "PDF Password remover"
aliases = []
date = 2024-02-08

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "FSFAP",]
app_author = [ "Jagadeesh Kotra",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "QPDF",]
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "C",]
build_systems = [ "Meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/jkotra/unlockr"
homepage = "https://github.com/jkotra/unlockr"
bugtracker = "https://github.com/jkotra/unlockr/issues"
donations = "https://ko-fi.com/jkotra"
translations = "https://github.com/jkotra/unlockr/tree/main/po"
more_information = []
summary_source_url = "https://flathub.org/apps/com.github.jkotra.unlockr"
screenshots = [ "https://raw.githubusercontent.com/jkotra/unlockr/master/static/dark.png", "https://raw.githubusercontent.com/jkotra/unlockr/master/static/light.png", "https://raw.githubusercontent.com/jkotra/unlockr/master/static/ok.png", "https://raw.githubusercontent.com/jkotra/unlockr/master/static/process.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "com.github.jkotra.unlockr"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.github.jkotra.unlockr"
flatpak_link = "https://flathub.org/apps/com.github.jkotra.unlockr.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/jkotra/unlockr/main/data/com.github.jkotra.unlockr.metainfo.xml.in"
reported_by = "linmob"
updated_by = ""

+++


### Description

unlockR is a simple app to unlock / decrypt PDF files.

[Source](https://raw.githubusercontent.com/jkotra/unlockr/main/data/com.github.jkotra.unlockr.metainfo.xml.in)

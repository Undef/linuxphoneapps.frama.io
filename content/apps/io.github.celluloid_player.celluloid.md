+++
title = "Celluloid"
description = "GTK+ frontend for mpv"
aliases = []
date = 2020-11-07
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC-BY-SA-4.0",]
app_author = [ "The Celluloid Developers",]
categories = [ "video player",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "mpv",]
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "AudioVideo", "Video", "Player",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/celluloid-player/celluloid"
homepage = "http://celluloid-player.github.io"
bugtracker = "http://github.com/celluloid-player/celluloid/issues"
donations = "https://paypal.me/CelluloidProject"
translations = "https://hosted.weblate.org/projects/celluloid/"
more_information = [ "https://web.archive.org/web/20230921180854/https://wiki.mobian.org/doku.php?id=celluloid",]
summary_source_url = "https://raw.githubusercontent.com/celluloid-player/celluloid/master/data/io.github.celluloid_player.Celluloid.appdata.xml.in"
screenshots = [ "http://celluloid-player.github.io/images/screenshot-0.png", "http://celluloid-player.github.io/images/screenshot-1.png", "http://celluloid-player.github.io/images/screenshot-2.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "io.github.celluloid_player.Celluloid"
scale_to_fit = "io.github.celluloid_player.Celluloid"
flathub = "https://flathub.org/apps/io.github.celluloid_player.Celluloid"
flatpak_link = ""
flatpak_recipe = "https://raw.githubusercontent.com/celluloid-player/celluloid/master/flatpak/io.github.celluloid_player.Celluloid.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "celluloid",]
appstream_xml_url = "https://raw.githubusercontent.com/celluloid-player/celluloid/master/data/io.github.celluloid_player.Celluloid.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Celluloid is a simple media player that can play virtually all video and
 audio formats. It supports playlists and MPRIS2 media player controls. The
 design of Celluloid follows the GNOME Human Interface Guidelines, but can
 also be adapted for other systems that don't use client-side decorations
 (CSD). It is based on the mpv library and GTK.


Features:


* Drag and drop playlist
* Loading external mpv configuration files
* MPRIS2 D-Bus interface

[Source](https://raw.githubusercontent.com/celluloid-player/celluloid/master/data/io.github.celluloid_player.Celluloid.appdata.xml.in)

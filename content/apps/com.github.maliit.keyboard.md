+++
title = "Maliit Keyboard"
description = "A virtual keyboard for touch devices"
aliases = []
date = 2020-10-24
updated = 2024-01-02

[taxonomies]
project_licenses = [ "LGPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Maliit Team",]
categories = [ "virtual keyboard",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "QtQuick",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "debian_12", "debian_13", "debian_unstable", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/maliit/keyboard"
homepage = "https://maliit.github.io"
bugtracker = "https://github.com/maliit/keyboard/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/maliit/keyboard/master/com.github.maliit.keyboard.metainfo.xml"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "com.github.maliit.keyboard"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "maliit-keyboard",]
appstream_xml_url = "https://raw.githubusercontent.com/maliit/keyboard/master/com.github.maliit.keyboard.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

This is a Virtual Keyboard for touch devices. It's used by many projects like Plasma Mobile, Ubuntu Touch and originally the Nokia N9. You can use it to write into the applications in several different languages including many different scripts as well.


Beyond typing, it supports word correction and prediction, multitouch, different layouts, emoji and styling.

[Source](https://raw.githubusercontent.com/maliit/keyboard/master/com.github.maliit.keyboard.metainfo.xml)

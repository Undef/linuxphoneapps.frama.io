+++
title = "Wiremapper"
description = "Linux client for Pockethernet network tester"
aliases = []
date = 2020-10-21
updated = 2024-01-02

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "martijnbraam",]
categories = [ "network",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = [ "PocketEthernet",]
packaged_in = [ "alpine_edge", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/MartijnBraam/wiremapper"
homepage = "https://gitlab.com/MartijnBraam/wiremapper"
bugtracker = "https://gitlab.com/MartijnBraam/wiremapper/-/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/MartijnBraam/wiremapper/-/raw/master/data/nl.brixit.wiremapper.metainfo.xml"
screenshots = [ "https://gitlab.com/MartijnBraam/wiremapper/-/raw/master/data/wiremapper-appstore.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "nl.brixit.wiremapper"
scale_to_fit = ""
flathub = "https://flathub.org/apps/nl.brixit.wiremapper"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "wiremapper",]
appstream_xml_url = "https://gitlab.com/MartijnBraam/wiremapper/-/raw/master/data/nl.brixit.wiremapper.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Wiremapper is an application for running network tests using the Pockethernet hardware.


Features:


* Quicktest feature which is a single-button test-everything sensible mode.
 If you have a wiremap inserted it will run a wiremap and otherwise it
 will run link/poe tests
* Pair the pockethernet once with your regular bluetooth menu and
 Wiremapper will deal with connecting for you. There's a dropdown
 in the header to switch between paired pockethernets if you have
 multiple (showing bluetooth device aliases set in the regular bluetooth
 menu)
* Run a custom test with multiple result "slots", this is mainly for testing
 a complete office building and exporting a .csv file with the results for
 each ethernet outlet in the building.
* Responsive application that should work on any phone/tablet/laptop/desktop

[Source](https://gitlab.com/MartijnBraam/wiremapper/-/raw/master/data/nl.brixit.wiremapper.metainfo.xml)

+++
title = "OSM Scout Server"
description = "Maps server providing tiles, geocoder, and router"
aliases = []
date = 2019-02-01
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC-BY-SA-3.0",]
app_author = [ "rinigus",]
categories = [ "maps server",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "QtQuick", "Kirigami", "Silica",]
backends = []
services = [ "openstreetmap",]
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "flathub", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "Maps", "Qt", "Science",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "qmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/rinigus/osmscout-server"
homepage = "https://rinigus.github.io/osmscout-server"
bugtracker = "https://github.com/rinigus/osmscout-server/issues"
donations = "https://rinigus.github.io/donate"
translations = "https://github.com/rinigus/osmscout-server/tree/master/translations"
more_information = []
summary_source_url = "https://github.com/rinigus/osmscout-server"
screenshots = [ "https://raw.githubusercontent.com/rinigus/osmscout-server/master/examples/screenshots/languages.png", "https://raw.githubusercontent.com/rinigus/osmscout-server/master/examples/screenshots/main.png", "https://raw.githubusercontent.com/rinigus/osmscout-server/master/examples/screenshots/profiles.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "io.github.rinigus.OSMScoutServer"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.rinigus.OSMScoutServer"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "osmscout-server",]
appstream_xml_url = "https://raw.githubusercontent.com/rinigus/osmscout-server/master/packaging/osmscout-server.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

OSM Scout Server can be used as a drop-in replacement for online map services providing map tiles, search, and routing. As a result, an offline operation is possible if the device has a server and map client programs installed and running.


To use the server, you have to start it and configure the client to access it. An example pages with JavaScript-based clients are provided. The server is supported by such clients as Pure Maps and modRana.

[Source](https://raw.githubusercontent.com/rinigus/osmscout-server/master/packaging/osmscout-server.appdata.xml)

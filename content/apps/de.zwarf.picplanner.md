+++
title = "PicPlanner"
description = "Plan your next photo locations"
aliases = []
date = 2022-09-22
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Zwarf",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "debian_13", "debian_unstable", "devuan_unstable", "flathub", "opensuse_tumbleweed",]
freedesktop_categories = [ "GTK", "GNOME", "Graphics", "Photography",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/Zwarf/picplanner"
homepage = "https://gitlab.com/Zwarf/picplanner"
bugtracker = "https://gitlab.com/Zwarf/picplanner/-/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/Zwarf/picplanner/-/raw/main/data/de.zwarf.picplanner.metainfo.xml.in"
screenshots = [ "https://gitlab.com/Zwarf/picplanner/-/raw/main/screenshots/picplanner-1.png", "https://gitlab.com/Zwarf/picplanner/-/raw/main/screenshots/picplanner-2.png", "https://gitlab.com/Zwarf/picplanner/-/raw/main/screenshots/picplanner-3.png", "https://gitlab.com/Zwarf/picplanner/-/raw/main/screenshots/picplanner-4.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/de.zwarf.picplanner/1.png", "https://img.linuxphoneapps.org/de.zwarf.picplanner/2.png", "https://img.linuxphoneapps.org/de.zwarf.picplanner/3.png", "https://img.linuxphoneapps.org/de.zwarf.picplanner/4.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "de.zwarf.picplanner.desktop"
scale_to_fit = ""
flathub = "https://flathub.org/apps/de.zwarf.picplanner"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "picplanner",]
appstream_xml_url = "https://gitlab.com/Zwarf/picplanner/-/raw/main/data/de.zwarf.picplanner.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Calculate the position of the sun,
 moon, and milky way to plan the perfect time to take a picture.


People who love to photograph know, that the biggest problem is being at the right spot at the right time.
 Therefore, landscape pictures should be planned before visiting the place of choice. To plan the location it is important to know the position of the sun, moon, and sometimes also the milky way.
 Where to find the sun at which time is most of the time easy to guess but when is the sunset? And for the milky way normally nobody knows where to find it in the night sky at which time at a specific location.
 This small program should answer all these questions.

[Source](https://gitlab.com/Zwarf/picplanner/-/raw/main/data/de.zwarf.picplanner.metainfo.xml.in)

+++
title = "Capsule"
description = "Medication tracker"
aliases = []
date = 2023-02-19
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Jonas Dreßler",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "GTK", "Utility",]
programming_languages = [ "JavaScript",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/verdre/capsule"
homepage = "https://gitlab.gnome.org/verdre/capsule"
bugtracker = "https://gitlab.gnome.org/verdre/capsule/issues"
donations = ""
translations = "https://gitlab.gnome.org/verdre/Capsule/-/tree/main/po"
more_information = []
summary_source_url = "https://gitlab.gnome.org/verdre/Capsule/-/raw/main/data/nl.v0yd.Capsule.metainfo.xml.in"
screenshots = [ "https://gitlab.gnome.org/verdre/capsule/raw/main/data/screenshots/add-medication.png", "https://gitlab.gnome.org/verdre/capsule/raw/main/data/screenshots/medications.png", "https://gitlab.gnome.org/verdre/capsule/raw/main/data/screenshots/today.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/nl.v0yd.capsule/1.png", "https://img.linuxphoneapps.org/nl.v0yd.capsule/2.png", "https://img.linuxphoneapps.org/nl.v0yd.capsule/3.png", "https://img.linuxphoneapps.org/nl.v0yd.capsule/4.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "nl.v0yd.Capsule"
scale_to_fit = "Capsule"
flathub = "https://flathub.org/apps/nl.v0yd.Capsule"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://gitlab.gnome.org/verdre/Capsule/-/raw/main/data/nl.v0yd.Capsule.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Schedule and track your medication intake.


Note: This app does not run in the background or send reminder notifications.

[Source](https://gitlab.gnome.org/verdre/Capsule/-/raw/main/data/nl.v0yd.Capsule.metainfo.xml.in)
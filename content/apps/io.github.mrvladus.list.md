+++
title = "Errands"
description = "Manage your tasks"
aliases = []
date = 2023-04-17
updated = 2023-09-30

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Vlad Krupinski",]
categories = [ "productivity",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Utility", "Utility",]
programming_languages = [ "C",]
build_systems = [ "make",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/mrvladus/Errands"
homepage = "https://github.com/mrvladus/Errands"
bugtracker = "https://github.com/mrvladus/Errands/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = ""
screenshots = [ "https://raw.githubusercontent.com/mrvladus/Errands/main/screenshots/main.png", "https://raw.githubusercontent.com/mrvladus/Errands/main/screenshots/secondary.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/io.github.mrvladus.list/1.png", "https://img.linuxphoneapps.org/io.github.mrvladus.list/2.png", "https://img.linuxphoneapps.org/io.github.mrvladus.list/3.png", "https://img.linuxphoneapps.org/io.github.mrvladus.list/4.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "io.github.mrvladus.List"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.mrvladus.List"
flatpak_link = ""
flatpak_recipe = "https://raw.githubusercontent.com/mrvladus/Errands/main/io.github.mrvladus.List.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/mrvladus/Errands/main/data/io.github.mrvladus.List.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++

### Description
Todo application for those who prefer simplicity.

Features:

* Add, remove, edit tasks and sub-tasks
* Mark task and sub-tasks as completed
* Add accent color for each task
* Drag and Drop support

[Source](https://raw.githubusercontent.com/mrvladus/Errands/main/data/io.github.mrvladus.List.metainfo.xml)

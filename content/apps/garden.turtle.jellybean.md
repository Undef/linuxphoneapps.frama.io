+++
title = "Jellybean"
description = "Manage inventories of various things"
aliases = []
date = 2023-11-17
updated = 2023-12-31

[taxonomies]
project_licenses = [ "AGPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "skøldis",]
categories = [ "Utility",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = [ "requires offline-only",]
tags = []

[extra]
repository = "https://codeberg.org/turtle/jellybean"
homepage = "https://codeberg.org/turtle/jellybean"
bugtracker = "https://codeberg.org/turtle/jellybean/issues"
donations = ""
translations = "https://translate.codeberg.org/engage/jellybean/"
more_information = []
summary_source_url = "https://codeberg.org/turtle/jellybean/raw/branch/main/data/garden.turtle.Jellybean.metainfo.xml.in"
screenshots = [ "https://codeberg.org/turtle/jellybean/raw/branch/main/screenshots/edit-view.png", "https://codeberg.org/turtle/jellybean/raw/branch/main/screenshots/item-view.png", "https://codeberg.org/turtle/jellybean/raw/branch/main/screenshots/main-view.png",]
screenshots_img = []
all_features_touch = false 
intended_for_mobile = true
app_id = "garden.turtle.Jellybean"
scale_to_fit = ""
flathub = "https://flathub.org/apps/garden.turtle.Jellybean"
flatpak_link = ""
flatpak_recipe = "https://codeberg.org/turtle/jellybean/raw/branch/main/garden.turtle.Jellybean.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "jellybean",]
appstream_xml_url = "https://codeberg.org/turtle/jellybean/raw/branch/main/data/garden.turtle.Jellybean.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Jellybean is an app that allows you to manage your inventory of various things. It provides you with an easy way to quickly use items whose inventories are managed by Jellybean, as well as simple use and refill functions and a handy low-stock indicator to let you know when your stock of an item is running low.

[Source](https://codeberg.org/turtle/jellybean/raw/branch/main/data/garden.turtle.Jellybean.metainfo.xml.in)

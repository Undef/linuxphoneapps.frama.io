+++
title = "Decibels"
description = "Play audio files"
aliases = []
date = 2023-11-25
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Angelo Verlain",]
categories = [ "audio player", "music player",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "GStreamer",]
services = []
packaged_in = [ "aur", "flathub", "opensuse_tumbleweed",]
freedesktop_categories = [ "GTK", "GNOME", "Audio", "Player",]
programming_languages = [ "TypeScript",]
build_systems = [ "Meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/vixalien/decibels"
homepage = "https://github.com/vixalien/decibels"
bugtracker = "https://github.com/vixalien/decibels/issues"
donations = "https://www.buymeacoffee.com/vixalien"
translations = "https://github.com/vixalien/decibels/tree/main/po"
more_information = [ "https://apps.gnome.org/Decibels/",]
summary_source_url = "https://raw.githubusercontent.com/vixalien/decibels/main/data/com.vixalien.decibels.metainfo.xml.in.in"
screenshots = [ "https://github.com/vixalien/decibels/raw/HEAD/data/screenshots/screenshot-1.png", "https://github.com/vixalien/decibels/raw/HEAD/data/screenshots/screenshot-2.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "com.vixalien.decibels"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.vixalien.decibels"
flatpak_link = ""
flatpak_recipe = "https://raw.githubusercontent.com/vixalien/decibels/main/build-aux/flatpak/com.vixalien.decibels.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "decibels",]
appstream_xml_url = "https://raw.githubusercontent.com/vixalien/decibels/main/data/com.vixalien.decibels.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Current features:


* Shows the waveform of the track
* Adjust playback speed
* Easy seek controls

[Source](https://raw.githubusercontent.com/vixalien/decibels/main/data/com.vixalien.decibels.metainfo.xml.in.in)

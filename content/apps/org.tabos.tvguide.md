+++
title = "TV Guide"
description = "European televion guide for GNOME"
aliases = []
date = 2019-02-01
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The Tabos Team",]
categories = [ "tv guide",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/tabos/tvguide"
homepage = "https://www.tabos.org"
bugtracker = "https://gitlab.com/tabos/tvguide/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/tabos/tvguide/-/raw/master/data/org.tabos.tvguide.appdata.xml.in"
screenshots = [ "https://www.tabos.org/project/tvguide/tvguide.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.tabos.tvguide"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://gitlab.com/tabos/tvguide/-/raw/master/data/org.tabos.tvguide.appdata.xml.in"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

TV Guide for GNOME offers an overview and a detailed schedule page for European televion programms.


This software uses open data provided by xmltv.se.

[Source](https://gitlab.com/tabos/tvguide/-/raw/master/data/org.tabos.tvguide.appdata.xml.in)
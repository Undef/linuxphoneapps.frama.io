+++
title = "Ambience"
description = "Control LIFX lights"
aliases = []
date = 2021-01-04
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Luka Jankovic",]
categories = [ "smart home",]
mobile_compatibility = [ "5",]
status = [ "archived",]
frameworks = [ "GTK3", "libhandy",]
backends = [ "lifxlan",]
services = [ "LIFX",]
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/LukaJankovic/Ambience"
homepage = "https://github.com/LukaJankovic/Ambience"
bugtracker = "https://github.com/LukaJankovic/Ambience/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/LukaJankovic/Ambience/stable/data/io.github.lukajankovic.ambience.metainfo.xml"
screenshots = [ "https://github.com/LukaJankovic/Ambience/raw/stable/screenshots/store-controls.png", "https://github.com/LukaJankovic/Ambience/raw/stable/screenshots/store-tiles.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "io.github.lukajankovic.ambience"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://raw.githubusercontent.com/LukaJankovic/Ambience/stable/io.github.lukajankovic.ambience.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/LukaJankovic/Ambience/stable/data/io.github.lukajankovic.ambience.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Control LIFX lights on the local network. Use the discovery mode to add the lights you wish to control to your main list.

[Source](https://raw.githubusercontent.com/LukaJankovic/Ambience/stable/data/io.github.lukajankovic.ambience.metainfo.xml)

### Notice

Deprecated/archived since 2022-06-15, one seemingly abandoned attempt to continue it can be found [here](https://github.com/kerryhatcher/AmbienceNG/tree/ambienceng).
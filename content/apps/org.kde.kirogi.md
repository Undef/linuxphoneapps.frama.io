+++
title = "Kirogi"
description = "UAV (drone) ground control"
aliases = []
date = 2019-10-22
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-2.0-or-later", "LGPL-2.1-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Kirogi Developers",]
categories = [ "drone control",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "aur", "gnuguix",]
freedesktop_categories = [ "Geoscience", "Photography", "RemoteAccess", "Science", "Sports",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/utilities/kirogi"
homepage = "https://www.kirogi.org/"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?format=guided&product=kirogi"
donations = ""
translations = ""
more_information = [ "https://blogs.kde.org/2019/09/08/introducing-kirogi-ground-control-application-drones", "https://mirror.kumi.systems/kde/files/akademy/2019/102-Taking_KDE_to_the_skies_Making_the_drone_ground_control_Kirogi.mp4", "https://invidio.us/channel/shoblubb", "https://mastodon.technology/tags/kirogi",]
summary_source_url = "https://invent.kde.org/utilities/kirogi/-/raw/master/data/org.kde.kirogi.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/kirogi/kirogi.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.kirogi"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "kirogi",]
appstream_xml_url = "https://invent.kde.org/utilities/kirogi/-/raw/master/data/org.kde.kirogi.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

Kirogi is a ground control application for drones. Take to the skies, open source-style.


Features:


* Direct flight controls
* Fly by touch on a Navigation Map
* Trigger vehicle actions (e.g. flips, trim)
* Gamepad/joypad support
* Live video
* Take photo and record video
* Configure flight parameters (speed, altitude limits)
* Support for Parrot (Anafi, Bebop 2) and Ryze Tello drones (more planned)

[Source](https://invent.kde.org/utilities/kirogi/-/raw/master/data/org.kde.kirogi.appdata.xml)

+++
title = "Teleprompter"
description = "Stay on track during speeches"
aliases = []
date = 2023-06-25
updated = 2024-02-22

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Nokse",]
categories = [ "teleprompter",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/Nokse22/teleprompter"
homepage = "https://github.com/Nokse22/teleprompter"
bugtracker = "https://github.com/Nokse22/teleprompter/issues"
donations = ""
translations = "https://github.com/Nokse22/teleprompter/tree/main/po"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/Nokse22/teleprompter/main/data/io.github.nokse22.teleprompter.appdata.xml.in"
screenshots = [ "https://raw.githubusercontent.com/Nokse22/teleprompter/main/data/resources/Screenshot%201.png", "https://raw.githubusercontent.com/Nokse22/teleprompter/main/data/resources/Screenshot%202.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = false
app_id = "io.github.nokse22.teleprompter"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.nokse22.teleprompter"
flatpak_link = "https://flathub.org/apps/io.github.nokse22.teleprompter.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "teleprompter",]
appstream_xml_url = "https://raw.githubusercontent.com/Nokse22/teleprompter/main/data/io.github.nokse22.teleprompter.appdata.xml.in"
reported_by = "linmob"
updated_by = "linmob"

+++

### Description

A simple app to read scrolling text from your screen. It has an adaptive layout to work on smartphones too.

[Source](https://raw.githubusercontent.com/Nokse22/teleprompter/main/data/io.github.nokse22.teleprompter.appdata.xml.in)

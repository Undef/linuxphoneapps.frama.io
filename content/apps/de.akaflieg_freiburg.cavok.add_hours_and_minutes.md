+++
title = "Add Times"
description = "Simple calculator for adding hours and minutes"
aliases = []
date = 2021-12-01
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Akaflieg Freiburg",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "QtQuick",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Calculator", "Utility",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/Akaflieg-Freiburg/addhoursandminutes"
homepage = "https://akaflieg-freiburg.github.io/addhoursandminutes"
bugtracker = "https://github.com/Akaflieg-Freiburg/addhoursandminutes/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/Akaflieg-Freiburg/addhoursandminutes/master/metadata/de.akaflieg_freiburg.cavok.add_hours_and_minutes.appdata.xml"
screenshots = [ "https://akaflieg-freiburg.github.io/addhoursandminutes/assets/images/screenshot-linux.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "de.akaflieg_freiburg.cavok.add_hours_and_minutes"
scale_to_fit = ""
flathub = "https://flathub.org/apps/de.akaflieg_freiburg.cavok.add_hours_and_minutes"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/Akaflieg-Freiburg/addhoursandminutes/master/metadata/de.akaflieg_freiburg.cavok.add_hours_and_minutes.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

This calculator adds times given in hours and minutes. It helps with the
 recording of machine running times, with the addition of flight times for
 pilots' flight logs, or driving times of truck drivers.

[Source](https://raw.githubusercontent.com/Akaflieg-Freiburg/addhoursandminutes/master/metadata/de.akaflieg_freiburg.cavok.add_hours_and_minutes.appdata.xml)
+++
title = "Headlines"
description = "A GTK4/libAdwaita Reddit client designed for use with Linux phones."
aliases = []
date = 2021-08-24
updated = 2023-09-30

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "FSFAP",]
app_author = [ "caveman250",]
categories = [ "social media",]
mobile_compatibility = [ "5",]
status = [ "archived",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Reddit",]
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "aur", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Feed", "News",]
programming_languages = [ "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/caveman250/Headlines"
homepage = "https://gitlab.com/caveman250/headlines"
bugtracker = "https://gitlab.com/caveman250/Headlines/-/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/caveman250/Headlines/-/raw/master/io.gitlab.caveman250.headlines.metainfo.xml"
screenshots = [ "https://gitlab.com/caveman250/Headlines/raw/master/screenshots/headlines-1.png", "https://gitlab.com/caveman250/Headlines/raw/master/screenshots/headlines-2.png", "https://gitlab.com/caveman250/Headlines/raw/master/screenshots/headlines-3.png", "https://gitlab.com/caveman250/Headlines/raw/master/screenshots/headlines-4.png", "https://gitlab.com/caveman250/Headlines/raw/master/screenshots/headlines-5.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "io.gitlab.caveman250.headlines"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "headlines",]
appstream_xml_url = "https://gitlab.com/caveman250/Headlines/-/raw/master/io.gitlab.caveman250.headlines.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description
Headlines is a GTK4/libAdwaita Reddit client written in C++, designed for use with Linux phones such as the Pinephone.

[Source](https://gitlab.com/caveman250/Headlines/-/raw/master/io.gitlab.caveman250.headlines.metainfo.xml)

### Notice
Great reddit client, sadly archived.

+++
title = "Pomodoro"
description = "Pomodoro is a productivity-focused timer"
aliases = []
date = 2023-12-04

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Ideve Core",]
categories = [ "productivity",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Clock", "Utility",]
programming_languages = [ "JavaScript",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/idevecore/pomodoro"
homepage = "https://gitlab.com/idevecore/pomodoro"
bugtracker = "https://gitlab.com/idevecore/pomodoro/-/issues"
donations = "https://ko-fi.com/idevecore"
translations = "https://hosted.weblate.org/engage/pomodoro"
more_information = []
summary_source_url = "https://gitlab.com/idevecore/pomodoro/-/raw/main/data/io.gitlab.idevecore.Pomodoro.appdata.xml.in.in"
screenshots = [ "https://gitlab.com/idevecore/pomodoro/-/raw/main/data/screenshots/01.png", "https://gitlab.com/idevecore/pomodoro/-/raw/main/data/screenshots/02.png", "https://gitlab.com/idevecore/pomodoro/-/raw/main/data/screenshots/03.png", "https://gitlab.com/idevecore/pomodoro/-/raw/main/data/screenshots/04.png", "https://gitlab.com/idevecore/pomodoro/-/raw/main/data/screenshots/05.png", "https://gitlab.com/idevecore/pomodoro/-/raw/main/data/screenshots/06.png", "https://gitlab.com/idevecore/pomodoro/-/raw/main/data/screenshots/07.png", "https://gitlab.com/idevecore/pomodoro/-/raw/main/data/screenshots/08.png", "https://gitlab.com/idevecore/pomodoro/-/raw/main/data/screenshots/09.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = false 
app_id = "io.gitlab.idevecore.Pomodoro"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.gitlab.idevecore.Pomodoro"
flatpak_link = "https://flathub.org/apps/io.gitlab.idevecore.Pomodoro.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://gitlab.com/idevecore/pomodoro/-/raw/main/data/io.gitlab.idevecore.Pomodoro.appdata.xml.in.in"
reported_by = "linmob"
updated_by = ""

+++

### Description

Pomodoro is a timer utility with rules, ideal for better productivity.

[Source](https://gitlab.com/idevecore/pomodoro/-/raw/main/data/io.gitlab.idevecore.Pomodoro.appdata.xml.in.in)

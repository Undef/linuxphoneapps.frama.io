+++
title = "Jami"
description = "Privacy-oriented voice, video, chat, and conference platform"
aliases = []
date = 2022-04-26
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC-BY-SA-3.0",]
app_author = [ "savoirfairelinux",]
categories = [ "chat", "telephony",]
mobile_compatibility = [ "4",]
status = [ "early",]
frameworks = [ "QtQuick 6",]
backends = []
services = [ "Jami", "SIP",]
packaged_in = [ "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "flathub", "manjaro_stable", "manjaro_unstable",]
freedesktop_categories = [ "Chat", "Communication", "FileTransfer", "InstantMessaging", "Network", "P2P", "Productivity",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://git.jami.net/savoirfairelinux/jami-client-qt"
homepage = "https://jami.net/"
bugtracker = "https://git.jami.net/savoirfairelinux/jami-client-qt/issues"
donations = "https://www.paypal.com/donate/?hosted_button_id=MGUDJLQZ4TP5W"
translations = "https://www.transifex.com/savoirfairelinux/jami"
more_information = [ "https://jami.net/help/",]
summary_source_url = "https://git.jami.net/savoirfairelinux/jami-client-qt/-/raw/master/extras/data/jami.appdata.xml"
screenshots = [ "https://dl.jami.net/media-resources/screenshots/jami_linux_audiovideo.png", "https://dl.jami.net/media-resources/screenshots/jami_linux_screenshare.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/net.jami.jami/1.png", "https://img.linuxphoneapps.org/net.jami.jami/2.png", "https://img.linuxphoneapps.org/net.jami.jami/3.png", "https://img.linuxphoneapps.org/net.jami.jami/4.png", "https://img.linuxphoneapps.org/net.jami.jami/5.png", "https://img.linuxphoneapps.org/net.jami.jami/6.png",]
all_features_touch = false
intended_for_mobile = false
app_id = "net.jami.Jami"
scale_to_fit = "net.jami.jami-qt"
flathub = "https://flathub.org/apps/net.jami.Jami"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "jami-qt",]
appstream_xml_url = "https://git.jami.net/savoirfairelinux/jami-client-qt/-/raw/master/extras/data/jami.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

An end-to-end encrypted secure and distributed voice, video, and
 chat communication platform that requires no central server and
 leaves the power of privacy and freedom in the hands of users.


Jami supports the following key features:


* One-to-one conversations
* File sharing
* Audio calls and conferences
* Video calls and conferences
* Screen sharing in video calls and conferences
* Recording and sending audio messages
* Recording and sending video messages
* Functioning as a SIP phone software


Client applications for GNU/Linux, Windows, macOS, iOS, Android,
 and Android TV are available, making Jami an interoperable and
 cross-platform communication framework.

[Source](https://git.jami.net/savoirfairelinux/jami-client-qt/-/raw/master/extras/data/jami.appdata.xml)

### Notice

Arch ARM and flatpak builds were evaluated. It fits the screen mostly okay without scale-to-fit for the chat and phone UI after setting [recommended env vars](https://img.linuxphoneapps.org/net.jami.jami/flatseal.png) (you won't see what you type), scale-to-fit makes the app behave worse in many circumstances. Voice calls work, the button to hang up is invisible, just tap the center of the lower half, chat while video/audio makes the window far two wide (somewhat usable in landscape). Video calls don't work out of the box but may work after setting up camera/video pipelines properly.

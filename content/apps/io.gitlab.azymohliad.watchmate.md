+++
title = "WatchMate"
description = "Sync and integrate with PineTime smart watch"
aliases = []
date = 2022-09-17
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Andrii Zymohliad",]
categories = [ "watch companion",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_edge", "aur", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "cargo",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/azymohliad/watchmate"
homepage = "https://gitlab.com/azymohliad/watchmate"
bugtracker = "https://gitlab.com/azymohliad/watchmate/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/azymohliad/watchmate/-/raw/main/assets/io.gitlab.azymohliad.WatchMate.metainfo.xml"
screenshots = [ "https://gitlab.com/azymohliad/watchmate/uploads/a2b49a69432d3b757fef9d5858c8e787/collage_2022-08-14.png", "https://gitlab.com/azymohliad/watchmate/uploads/db3afec736de83de622ac88ab79ea0c0/dashboard_2022-08-14.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/io.gitlab.azymohliad.watchmate/1.png", "https://img.linuxphoneapps.org/io.gitlab.azymohliad.watchmate/2.png", "https://img.linuxphoneapps.org/io.gitlab.azymohliad.watchmate/3.png", "https://img.linuxphoneapps.org/io.gitlab.azymohliad.watchmate/4.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "io.gitlab.azymohliad.WatchMate"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.gitlab.azymohliad.WatchMate"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "watchmate",]
appstream_xml_url = "https://gitlab.com/azymohliad/watchmate/-/raw/main/assets/io.gitlab.azymohliad.WatchMate.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Companion app for PineTime smart watch running InfiniTime firmware.


Visually optimized for GNOME, adaptive for Linux phone and desktop.


Features:


* Serve current time to the watch
* Read various data form the watch
* Perform OTA firmware update
* Automatically check for available firmware updates
* Media player control

[Source](https://gitlab.com/azymohliad/watchmate/-/raw/main/assets/io.gitlab.azymohliad.WatchMate.metainfo.xml)

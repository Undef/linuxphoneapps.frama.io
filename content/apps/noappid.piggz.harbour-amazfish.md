+++
title = "Amazfish"
description = "Amazfish Bip support for SailfishOS"
aliases = []
date = 2021-05-05
updated = 2023-09-30

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "piggz",]
categories = [ "watch companion",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "QtQuick", "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "aur", "manjaro_stable", "manjaro_unstable",]
freedesktop_categories = [ "Qt", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://github.com/piggz/harbour-amazfish"
homepage = ""
bugtracker = "https://github.com/piggz/harbour-amazfish/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/piggz/harbour-amazfish"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "amazfish",]
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"

+++

### Notice

Companion app for smartwatches, including PineTime and Bangle.js. Originally for SailfishOS, has been ported to Kirigami.

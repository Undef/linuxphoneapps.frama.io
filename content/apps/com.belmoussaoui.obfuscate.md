+++
title = "Obfuscate"
description = "Censor private information"
aliases = []
date = 2021-07-15
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Bilal Elmoussaoui",]
categories = [ "privacy", "utilities",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "aur", "debian_13", "debian_unstable", "devuan_unstable", "flathub", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/World/obfuscate/"
homepage = "https://gitlab.gnome.org/World/obfuscate/"
bugtracker = "https://gitlab.gnome.org/World/obfuscate/issues"
donations = "https://liberapay.com/bielmoussaoui"
translations = "https://l10n.gnome.org/module/obfuscate/"
more_information = [ "https://apps.gnome.org/app/com.belmoussaoui.Obfuscate/",]
summary_source_url = "https://gitlab.gnome.org/World/obfuscate/-/raw/master/data/com.belmoussaoui.Obfuscate.metainfo.xml.in.in"
screenshots = [ "https://gitlab.gnome.org/World/obfuscate/raw/master/data/resources/screenshots/screenshot1.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "com.belmoussaoui.Obfuscate"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.belmoussaoui.Obfuscate"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-obfuscate",]
appstream_xml_url = "https://gitlab.gnome.org/World/obfuscate/-/raw/master/data/com.belmoussaoui.Obfuscate.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Obfuscate lets you redact your private information from any image.

[Source](https://gitlab.gnome.org/World/obfuscate/-/raw/master/data/com.belmoussaoui.Obfuscate.metainfo.xml.in.in)

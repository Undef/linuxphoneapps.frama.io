+++
title = "Kalm"
description = "Breathing techniques"
aliases = []
date = 2023-10-21
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "plata",]
categories = [ "education", "health",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "KDE", "Qt", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/utilities/kalm"
homepage = "https://apps.kde.org/kalm"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=Kalm"
donations = "https://kde.org/community/donations"
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/utilities/kalm/-/raw/master/org.kde.kalm.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/kalm/general.png", "https://cdn.kde.org/screenshots/kalm/info.png", "https://cdn.kde.org/screenshots/kalm/menu.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.kalm"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "kalm",]
appstream_xml_url = "https://invent.kde.org/utilities/kalm/-/raw/master/org.kde.kalm.appdata.xml"
reported_by = "plata"
updated_by = "script"

+++


### Description

Kalm can teach you different breathing techniques.

[Source](https://invent.kde.org/utilities/kalm/-/raw/master/org.kde.kalm.appdata.xml)

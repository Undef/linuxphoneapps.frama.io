+++
title = "Fretboard"
description = "Look up guitar chords"
aliases = []
date = 2023-10-29
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Brage Fuglseth",]
categories = [ "musical tool",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Education", "GNOME", "GTK",]
programming_languages = [ "Rust",]
build_systems = [ "Meson",]
requires_internet = [ "requires offline-only",]
tags = []

[extra]
repository = "https://github.com/bragefuglseth/fretboard"
homepage = "https://apps.gnome.org/Fretboard"
bugtracker = "https://github.com/bragefuglseth/fretboard/issues"
translations = "https://hosted.weblate.org/engage/fretboard/"
donations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/bragefuglseth/fretboard/main/data/dev.bragefuglseth.Fretboard.metainfo.xml.in.in"
screenshots = [ "https://raw.githubusercontent.com/bragefuglseth/fretboard/main/data/screenshots/screenshot-1.png", "https://raw.githubusercontent.com/bragefuglseth/fretboard/main/data/screenshots/screenshot-2.png", "https://raw.githubusercontent.com/bragefuglseth/fretboard/main/data/screenshots/screenshot-3.png", "https://raw.githubusercontent.com/bragefuglseth/fretboard/main/data/screenshots/screenshot-4.png",]
screenshots_img = []
intended_for_mobile = true
all_features_touch = true
app_id = "dev.bragefuglseth.Fretboard"
scale_to_fit = ""
flathub = "https://flathub.org/apps/dev.bragefuglseth.Fretboard"
flatpak_link = "https://flathub.org/apps/dev.bragefuglseth.Fretboard.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/bragefuglseth/fretboard/main/dev.bragefuglseth.Fretboard.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/bragefuglseth/fretboard/main/data/dev.bragefuglseth.Fretboard.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

How was that chord played, again? Fretboard lets you find guitar chords
 by typing their names or plotting them on an interactive guitar neck.
 When you have identified a chord, you can experiment with changing it,
 see more ways to play it, or bookmark it to save it for later. No matter
 if you are a beginner or an advanced guitarist, you can use Fretboard to
 practice, learn, and master your favorite songs!


Fretboard is proudly part of GNOME Circle, an initiative that champions the great software that is available for the GNOME platform. For more information, visit the GNOME Circle website.

[Source](https://raw.githubusercontent.com/bragefuglseth/fretboard/main/data/dev.bragefuglseth.Fretboard.metainfo.xml.in.in)
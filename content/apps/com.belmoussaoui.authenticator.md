+++
title = "Authenticator"
description = "Generate Two-Factor Codes"
aliases = [ "apps/com.github.bilelmoussaoui.authenticator/",]
date = 2019-02-16
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Bilal Elmoussaoui",]
categories = [ "multi-factor authentication",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "aur", "debian_11", "debian_unstable", "devuan_4_0", "devuan_unstable", "flathub", "gnuguix", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/World/Authenticator"
homepage = "https://gitlab.gnome.org/World/Authenticator"
bugtracker = "https://gitlab.gnome.org/World/Authenticator/issues"
donations = "https://liberapay.com/bielmoussaoui"
translations = "https://l10n.gnome.org/module/authenticator/"
more_information = [ "https://apps.gnome.org/app/com.belmoussaoui.Authenticator/",]
summary_source_url = "https://gitlab.gnome.org/World/Authenticator/-/raw/master/data/com.belmoussaoui.Authenticator.metainfo.xml.in.in"
screenshots = [ "https://gitlab.gnome.org/World/Authenticator/raw/master/data/screenshots/screenshot1.png", "https://gitlab.gnome.org/World/Authenticator/raw/master/data/screenshots/screenshot2.png", "https://gitlab.gnome.org/World/Authenticator/raw/master/data/screenshots/screenshot3.png", "https://gitlab.gnome.org/World/Authenticator/raw/master/data/screenshots/screenshot4.png", "https://gitlab.gnome.org/World/Authenticator/raw/master/data/screenshots/screenshot5.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "com.belmoussaoui.Authenticator"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.belmoussaoui.Authenticator"
flatpak_link = ""
flatpak_recipe = "https://gitlab.gnome.org/World/Authenticator/-/raw/master/build-aux/com.belmoussaoui.Authenticator.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-authenticator",]
appstream_xml_url = "https://gitlab.gnome.org/World/Authenticator/-/raw/master/data/com.belmoussaoui.Authenticator.metainfo.xml.in.in"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

Simple application for generating Two-Factor Authentication Codes.


Features:


* Time-based/Counter-based/Steam methods support
* SHA-1/SHA-256/SHA-512 algorithms support
* QR code scanner using a camera or from a screenshot
* Lock the application with a password
* Beautiful UI
* GNOME Shell search provider
* Backup/Restore from/into known applications like FreeOTP+, Aegis (encrypted / plain-text), andOTP, Google Authenticator

[Source](https://gitlab.gnome.org/World/Authenticator/-/raw/master/data/com.belmoussaoui.Authenticator.metainfo.xml.in.in)

### Notice

Previously, before release 4.*, this program was using GTK3/libhandy and written in Python, e.g. Debian still ship that release.

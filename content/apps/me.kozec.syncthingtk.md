+++
title = "Syncthing GTK"
description = "GUI and notification area icon for Syncthing"
aliases = [ "apps/org.syncthing-gtk.syncthing-gtk/",]
date = 2023-08-07
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-2.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "syncthing-gtk organisation",]
categories = [ "file transfer", "file sync",]
mobile_compatibility = [ "2",]
status = [ "mature",]
frameworks = [ "GTK3",]
backends = [ "Syncthing",]
services = [ "Syncthing",]
packaged_in = [ "alpine_edge", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "flathub", "gnuguix", "pureos_landing",]
freedesktop_categories = [ "FileTools", "FileTransfer", "GTK", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "setup.py",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/syncthing-gtk/syncthing-gtk"
homepage = "https://github.com/syncthing-gtk/syncthing-gtk"
bugtracker = "https://github.com/syncthing-gtk/syncthing-gtk/issues"
donations = "https://liberapay.com/kozec"
translations = "https://www.transifex.com/syncthing-gtk/syncthing-gtk/"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/syncthing-gtk/syncthing-gtk/main/org.syncthing-gtk.syncthing-gtk.appdata.xml"
screenshots = [ "https://raw.githubusercontent.com/syncthing-gtk/syncthing-gtk/main/doc/syncthing-gtk.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.syncthing-gtk.syncthing-gtk"
scale_to_fit = ""
flathub = "https://flathub.org/apps/me.kozec.syncthingtk"
flatpak_link = ""
flatpak_recipe = "https://raw.githubusercontent.com/flathub/me.kozec.syncthingtk/master/me.kozec.syncthingtk.yaml"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "syncthing-gtk",]
appstream_xml_url = "https://raw.githubusercontent.com/syncthing-gtk/syncthing-gtk/main/org.syncthing-gtk.syncthing-gtk.appdata.xml"
reported_by = "Guido Günther"
updated_by = "script"

+++


### Description

Supported Syncthing features:


* Everything what WebUI can display
* Adding/editing/deleting nodes
* Adding/editing/deleting repositories
* Restart/shutdown server
* Editing daemon settings


Additional features:


* First run wizard for initial configuration
* Running Syncthing daemon in background
* Half-automatic setup for new nodes and repositories
* Nautilus (a.k.a. Files), Nemo and Caja integration
* Desktop notifications

[Source](https://raw.githubusercontent.com/syncthing-gtk/syncthing-gtk/main/org.syncthing-gtk.syncthing-gtk.appdata.xml)

### Notice

Previously, the program was developed at [https://github.com/kozec/syncthing-gtk](https://github.com/kozec/syncthing-gtk).

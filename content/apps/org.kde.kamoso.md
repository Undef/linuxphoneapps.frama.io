+++
title = "Kamoso"
description = "Use your webcam to take pictures and make videos"
aliases = []
date = 2020-02-06
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community",]
categories = [ "camera",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = [ "gstreamer",]
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Qt", "KDE", "Graphics", "Photography",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/multimedia/kamoso"
homepage = "https://userbase.kde.org/Kamoso"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?format=guided&product=kamoso"
donations = ""
translations = ""
more_information = [ "https://userbase.kde.org/Kamoso",]
summary_source_url = "https://invent.kde.org/multimedia/kamoso/-/raw/master/org.kde.kamoso.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/kamoso/kamoso.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.kamoso.desktop"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "kamoso",]
appstream_xml_url = "https://invent.kde.org/multimedia/kamoso/-/raw/master/org.kde.kamoso.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

Kamoso is a simple and friendly program to use your camera. Use it to take pictures and make videos to share.

[Source](https://invent.kde.org/multimedia/kamoso/-/raw/master/org.kde.kamoso.appdata.xml)

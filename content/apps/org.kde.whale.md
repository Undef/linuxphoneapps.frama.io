+++
title = "Whale"
description = "File manager"
aliases = []
date = 2021-12-18
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Carl Schwan",]
categories = [ "file management",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Qt", "KDE", "System", "FileTools", "FileManager",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/carlschwan/whale"
homepage = ""
bugtracker = "https://invent.kde.org/carlschwan/whale/-/issues/"
donations = ""
translations = ""
more_information = [ "https://carlschwan.eu/2021/12/18/more-kde-apps/",]
summary_source_url = "https://invent.kde.org/carlschwan/whale/-/raw/master/org.kde.whale.appdata.xml"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.whale.desktop"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://invent.kde.org/carlschwan/whale/-/raw/master/org.kde.whale.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

A file managr

[Source](https://invent.kde.org/carlschwan/whale/-/raw/master/org.kde.whale.appdata.xml)

### Notice

WIP fun project, worked well when tested on 2021-12-18.
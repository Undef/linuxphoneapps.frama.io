+++
title = "Speech Note"
description = "Note taking, reading and translating with offline Speech to Text, Text to Speech and Machine Translation"
aliases = []
date = 2023-06-25
updated = 2024-01-02

[taxonomies]
project_licenses = [ "MPL-2.0",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Michal Kosciesza",]
categories = [ "note taking", "text to speech", "speech to text",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "QtQuick", "Silica",]
backends = [ "Coqui STT", "Vosk", "whisper.cpp", "espeak-ng", "MBROLA", "Piper", "RHVoice", "Coqui TTS",]
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "AudioVideo",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/mkiol/dsnote"
homepage = "https://github.com/mkiol/dsnote"
bugtracker = "https://github.com/mkiol/dsnote/issues"
donations = ""
translations = "https://app.transifex.com/mkiol/dsnote"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/mkiol/dsnote/main/desktop/dsnote.metainfo.xml"
screenshots = [ "https://raw.githubusercontent.com/mkiol/dsnote/main/desktop/screenshots/speechnote-screenshot-fullscreen-translator.png", "https://raw.githubusercontent.com/mkiol/dsnote/main/desktop/screenshots/speechnote-screenshot-fullscreen.png", "https://raw.githubusercontent.com/mkiol/dsnote/main/desktop/screenshots/speechnote-screenshot-mobile-translator.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/net.mkiol.speechnote/1.png", "https://img.linuxphoneapps.org/net.mkiol.speechnote/2.png", "https://img.linuxphoneapps.org/net.mkiol.speechnote/3.png", "https://img.linuxphoneapps.org/net.mkiol.speechnote/4.png",]
all_features_touch = false
intended_for_mobile = false
app_id = "net.mkiol.SpeechNote"
scale_to_fit = ""
flathub = "https://flathub.org/apps/net.mkiol.SpeechNote"
flatpak_link = "https://raw.githubusercontent.com/mkiol/dsnote/main/flatpak/net.mkiol.SpeechNote.yaml"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/mkiol/dsnote/main/desktop/dsnote.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Speech Note let you take, read and translate notes in multiple languages.
 It uses Speech to Text, Text to Speech and Machine Translation to do so.
 Text and voice processing take place entirely offline, locally on your
 computer, without using a network connection. Your privacy is always
 respected. No data is sent to the Internet.

[Source](https://raw.githubusercontent.com/mkiol/dsnote/main/desktop/dsnote.metainfo.xml)
+++
title = "Furtherance"
description = "Track your time without being tracked"
aliases = []
date = 2022-03-28
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Ricky Kresslein",]
categories = [ "productivity",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "Clock",]
programming_languages = [ "Rust",]
build_systems = [ "meson", "cargo",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/lakoliu/Furtherance"
homepage = "https://furtherance.app"
bugtracker = "https://github.com/lakoliu/Furtherance/issues"
donations = "https://www.paypal.com/donate/?hosted_button_id=TLYY8YZ424VRL"
translations = ""
more_information = []
summary_source_url = "https://github.com/lakoliu/Furtherance"
screenshots = [ "https://github.com/lakoliu/Furtherance/raw/main/data/screenshots/furtherance-screenshot-edit-task.png", "https://github.com/lakoliu/Furtherance/raw/main/data/screenshots/furtherance-screenshot-main.png", "https://github.com/lakoliu/Furtherance/raw/main/data/screenshots/furtherance-screenshot-settings.png", "https://github.com/lakoliu/Furtherance/raw/main/data/screenshots/furtherance-screenshot-task-details.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "com.lakoliu.Furtherance"
scale_to_fit = "com.lakoliu.Furtherance"
flathub = "https://flathub.org/apps/com.lakoliu.Furtherance"
flatpak_link = ""
flatpak_recipe = "https://raw.githubusercontent.com/lakoliu/Furtherance/main/com.lakoliu.Furtherance.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "furtherance",]
appstream_xml_url = "https://raw.githubusercontent.com/lakoliu/Furtherance/main/data/com.lakoliu.Furtherance.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Furtherance is a time tracking app built for GNOME with Rust
 and GTK4. In addition to tracking time spent on tasks, you can edit that
 time, delete entries, and change settings. It even has idle detection.

[Source](https://raw.githubusercontent.com/lakoliu/Furtherance/main/data/com.lakoliu.Furtherance.appdata.xml.in)

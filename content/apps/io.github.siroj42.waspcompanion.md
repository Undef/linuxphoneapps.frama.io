+++
title = "Wasp Companion"
description = "A companion app for wasp-os"
aliases = [ "apps/com.arteeh.companion/", "apps/io.github.siroj42.WaspCompanion/",]
date = 2021-07-18
updated = 2024-01-02

[taxonomies]
project_licenses = [ "The Unlicense",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Maarten de Jong",]
categories = [ "watch companion",]
mobile_compatibility = [ "5",]
status = [ "early", "pre-release", "inactive",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "custom",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/Siroj42/wasp-companion"
homepage = "https://gitlab.com/arteeh/wasp-companion"
bugtracker = "https://github.com/Siroj42/wasp-companion/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/arteeh/wasp-companion/master/flatpak/com.arteeh.Companion.metainfo.xml"
screenshots = [ "http://comical.sourceforge.net/images/comical-0.5-linux.jpg", "http://comical.sourceforge.net/images/comical-0.6-win32.jpg",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "io.github.siroj42.WaspCompanion"
scale_to_fit = ""
flathub = ""
flatpak_link = "https://github.com/Siroj42/wasp-companion/actions/workflows/flatpak.yml"
flatpak_recipe = "https://raw.githubusercontent.com/Siroj42/wasp-companion/master/flatpak/io.github.siroj42.WaspCompanion.yml"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/Siroj42/wasp-companion/master/flatpak/io.github.siroj42.WaspCompanion.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Wasp Companion is a companion app for the wasp-os smartwatch operating system.

[Source](https://raw.githubusercontent.com/Siroj42/wasp-companion/master/flatpak/io.github.siroj42.WaspCompanion.metainfo.xml)
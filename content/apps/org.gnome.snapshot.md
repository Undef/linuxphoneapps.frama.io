+++
title = "Camera"
description = "Take pictures and videos"
aliases = []
date = 2023-11-16
updated = 2023-12-31

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The GNOME Project",]
categories = [ "camera",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "libcamera", "pipewire",]
services = []
packaged_in = [ "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "fedora_39", "fedora_rawhide", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "GNOME", "GTK", "Graphics", "Photography",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/GNOME/snapshot"
homepage = "https://apps.gnome.org/Snapshot/"
bugtracker = "https://gitlab.gnome.org/GNOME/snapshot/issues"
donations = "https://www.gnome.org/donate"
translations = "https://l10n.gnome.org/module/snapshot/"
more_information = [ "https://teams.pages.gitlab.gnome.org/Websites/welcome.gnome.org/en/app/Snapshot/",]
summary_source_url = "https://gitlab.gnome.org/GNOME/snapshot/-/raw/main/data/org.gnome.Snapshot.metainfo.xml.in.in"
screenshots = [ "https://gitlab.gnome.org/GNOME/snapshot/raw/main/data/screenshots/gallery.png", "https://gitlab.gnome.org/GNOME/snapshot/raw/main/data/screenshots/screenshot1.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.Snapshot"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Snapshot"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "snapshot",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/snapshot/-/raw/main/data/org.gnome.Snapshot.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Take pictures and videos on your computer, tablet, or phone.

[Source](https://gitlab.gnome.org/GNOME/snapshot/-/raw/main/data/org.gnome.Snapshot.metainfo.xml.in.in)

### Notice

Among other devices, this Camera app works on the PINE64 PinePhone Pro.

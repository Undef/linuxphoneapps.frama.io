+++
title = "Discover"
description = "Discover"
aliases = [ "apps/org.kde.plasma.discover/",]
date = 2019-02-01
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community",]
categories = [ "software center",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Qt", "KDE", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/plasma/discover"
homepage = "https://apps.kde.org/discover"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?format=guided&product=Discover"
donations = "https://www.kde.org/community/donations/?app=discover&source=appdata"
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/plasma/discover/-/raw/master/discover/org.kde.discover.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/plasma-discover/plasma-discover.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.discover"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "plasma-discover",]
appstream_xml_url = "https://invent.kde.org/plasma/discover/-/raw/master/discover/org.kde.discover.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

Discover helps you find and install applications, games, and tools. You can search or browse by category, and look at screenshots and read reviews to help you pick the perfect app.


With Discover, you can manage software from multiple sources, including your operating system's software repository, Flatpak repos, the Snap store, or even AppImages from store.kde.org.


Finally, Discover also allows you to find, install, and manage add-ons for Plasma and all your favorite KDE apps!

[Source](https://invent.kde.org/plasma/discover/-/raw/master/discover/org.kde.discover.appdata.xml)

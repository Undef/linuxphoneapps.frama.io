+++
title = "Kookbook"
description = "Recipe manager"
aliases = []
date = 2019-02-01
updated = 2024-01-02

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Sune Vuorela",]
categories = [ "recipe management",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "pureos_landing",]
freedesktop_categories = [ "Qt", "KDE", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/utilities/kookbook"
homepage = ""
bugtracker = "https://invent.kde.org/utilities/kookbook/-/issues/"
donations = ""
translations = ""
more_information = [ "https://pusling.com/blog/?p=499",]
summary_source_url = "https://invent.kde.org/utilities/kookbook/-/raw/master/src/desktop/org.kde.kookbook.appdata.xml"
screenshots = [ "https://pusling.com/blog/?p=499",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.kookbook"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "kookbook",]
appstream_xml_url = "https://invent.kde.org/utilities/kookbook/-/raw/master/src/desktop/org.kde.kookbook.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

Kookbook is a simplistic recipe manager that will help you to maintain a collection of your favorite recipes.


Recipes can be synced with others using external tools like git repositories, nextcloud or many other services.

[Source](https://invent.kde.org/utilities/kookbook/-/raw/master/src/desktop/org.kde.kookbook.appdata.xml)

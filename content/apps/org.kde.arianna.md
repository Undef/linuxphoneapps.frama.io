+++
title = "Arianna"
description = "EBook Reader"
aliases = []
date = 2023-04-15
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The KDE Community",]
categories = [ "document viewer",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "QtQuick", "Kirigami",]
backends = [ "epub.js",]
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "Network",]
programming_languages = [ "JavaScript", "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/graphics/arianna"
homepage = "https://apps.kde.org/arianna/"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=Arianna"
donations = ""
translations = ""
more_information = [ "https://carlschwan.eu/2023/04/13/announcing-arianna-1.0/",]
summary_source_url = "https://invent.kde.org/graphics/arianna/-/raw/master/org.kde.arianna.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/arianna/library-author.png", "https://cdn.kde.org/screenshots/arianna/library-search.png", "https://cdn.kde.org/screenshots/arianna/library-view.png", "https://cdn.kde.org/screenshots/arianna/reader-search.png", "https://cdn.kde.org/screenshots/arianna/reader.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/org.kde.arianna/1.png", "https://img.linuxphoneapps.org/org.kde.arianna/2.png", "https://img.linuxphoneapps.org/org.kde.arianna/3.png", "https://img.linuxphoneapps.org/org.kde.arianna/4.png", "https://img.linuxphoneapps.org/org.kde.arianna/5.png", "https://img.linuxphoneapps.org/org.kde.arianna/6.png",]
all_features_touch = true
intended_for_mobile = false
app_id = "org.kde.arianna"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.arianna"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "arianna",]
appstream_xml_url = "https://invent.kde.org/graphics/arianna/-/raw/master/org.kde.arianna.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

An ebook reader and library management app supporting ".epub" files. Arianna discovers your books automatically, and sorts them by categories, genres and authors.

[Source](https://invent.kde.org/graphics/arianna/-/raw/master/org.kde.arianna.appdata.xml)

### Notice

Lacks touch controls to easily switch pages currently, so you will have to use the relatively tiny buttons at the bottom to navigate through books.

+++
title = "Biblioteca"
description = "Read documentation"
aliases = []
date = 2023-11-16
updated = 2024-01-04

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Akshay Warrier",]
categories = [ "development",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "GNOME", "GTK", "Documentation",]
programming_languages = [ "JavaScript",]
build_systems = [ "meson",]
requires_internet = [ "requires offline-only",]
tags = []

[extra]
repository = "https://github.com/workbenchdev/Biblioteca"
homepage = "https://github.com/workbenchdev/Biblioteca"
bugtracker = "https://github.com/workbenchdev/Biblioteca/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/workbenchdev/Biblioteca/main/data/app.metainfo.xml"
screenshots = [ "https://raw.githubusercontent.com/workbenchdev/Biblioteca/main/data/screenshot.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "@app_id@"
scale_to_fit = ""
flathub = "https://flathub.org/apps/app.drey.Biblioteca"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/workbenchdev/Biblioteca/main/data/app.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Biblioteca lets you browse and read GNOME documentation.


Among other things, Biblioteca comes with


* Offline documentation
* Dark mode support
* Fuzzy search
* Mobile / adaptive

[Source](https://raw.githubusercontent.com/workbenchdev/Biblioteca/main/data/app.metainfo.xml)
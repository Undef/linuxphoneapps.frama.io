+++
title = "Flare"
description = "Chat with your friends on Signal"
aliases = []
date = 2022-10-08
updated = 2024-01-02

[taxonomies]
project_licenses = [ "AGPL-3.0-only",]
metadata_licenses = [ "FSFAP",]
app_author = [ "Julian Schmidhuber",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "presage",]
services = [ "Signal",]
packaged_in = [ "alpine_3_19", "alpine_edge", "aur", "flathub", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Chat", "InstantMessaging",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = [ "requires first-run", "recommends always",]
tags = []

[extra]
repository = "https://gitlab.com/schmiddi-on-mobile/flare"
homepage = "https://mobile.schmidhuberj.de/flare"
bugtracker = "https://gitlab.com/schmiddi-on-mobile/flare/-/issues"
donations = "https://gitlab.com/schmiddi-on-mobile/flare#donate"
translations = "https://hosted.weblate.org/engage/schmiddi-on-mobile/"
more_information = []
summary_source_url = "https://gitlab.com/schmiddi-on-mobile/flare/-/raw/master/data/de.schmidhuberj.Flare.metainfo.xml"
screenshots = [ "https://gitlab.com/Schmiddiii/flare/-/raw/master/data/screenshots/screenshot.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "de.schmidhuberj.Flare"
scale_to_fit = ""
flathub = "https://flathub.org/apps/de.schmidhuberj.Flare"
flatpak_link = "https://github.com/flathub/de.schmidhuberj.Flare"
flatpak_recipe = "https://raw.githubusercontent.com/flathub/de.schmidhuberj.Flare/master/de.schmidhuberj.Flare.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "flare-signal-client",]
appstream_xml_url = "https://gitlab.com/schmiddi-on-mobile/flare/-/raw/master/data/de.schmidhuberj.Flare.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Flare is an unofficial app that lets you chat with your friends on Signal from Linux. It is still in development and doesn't include as many features as the official Signal applications.


* Translators: Description of Flare in metainfo: Features
* Linking device
* Sending messages
* Receiving messages
* Replying to a message
* Reacting to a message
* Attachments
* Encrypted storage
* Notifications, optionally in the background


More features are planned, blocked, or not-planned. Consult the README for more information about them.


Please note that using this application will probably worsen your security compared to using official Signal applications.
 Use with care when handling sensitive data.
 Look at the projects README for more information about security.

[Source](https://gitlab.com/schmiddi-on-mobile/flare/-/raw/master/data/de.schmidhuberj.Flare.metainfo.xml)

### Notice

Native Signal Desktop alternative, needs [signal-cli](https://github.com/AsamK/signal-cli) or an Android/iOS app as primary signal client. See [this toot for signal-cli paring instructions](https://fosstodon.org/@schmiddionmobile/110865217789552548).

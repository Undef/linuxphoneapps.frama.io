+++
title = "Public Transport"
description = "public transportation app (currently) for Germany"
aliases = []
date = 2021-04-10
updated = 2023-09-30

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Maeve",]
categories = [ "public transport",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/maevemi/publictransport"
homepage = "https://gitlab.com/maevemi/publictransport"
bugtracker = "https://gitlab.com/maevemi/publictransport/-/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/maevemi/publictransport/-/raw/master/data/com.gitlab.maevemi.publictransport.metainfo.xml.in.in"
screenshots = [ "https://gitlab.com/maevemi/publictransport/-/raw/master/data/screenshots/publictransport_screenshot_1.png", "https://gitlab.com/maevemi/publictransport/-/raw/master/data/screenshots/publictransport_screenshot_2.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "com.gitlab.maevemi.publictransport"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.gitlab.maevemi.publictransport"
flatpak_link = ""
flatpak_recipe = "https://gitlab.com/maevemi/publictransport/-/raw/master/build-aux/com.gitlab.maevemi.publictransport.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://gitlab.com/maevemi/publictransport/-/raw/master/data/com.gitlab.maevemi.publictransport.metainfo.xml.in.in"
reported_by = "Maeve"
updated_by = "script"

+++

### Description
Real time information for public transportation (currently) in Germany

[Source](https://gitlab.com/maevemi/publictransport/-/raw/master/data/com.gitlab.maevemi.publictransport.metainfo.xml.in.in)

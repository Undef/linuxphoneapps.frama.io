+++
title = "Pidif"
description = "A lightweight PDF viewer built for touch interfaces, with GTK4 and Rust"
aliases = []
date = 2022-07-20

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "bjesus",]
categories = [ "pdf viewer",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4",]
backends = []
services = []
packaged_in = [ "alpine_edge", "aur",]
freedesktop_categories = [ "GTK", "Office", "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "cargo",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/bjesus/pidif"
homepage = ""
bugtracker = "https://github.com/bjesus/pidif/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/bjesus/pidif"
screenshots = [ "https://user-images.githubusercontent.com/55081/179860375-486574a8-cc29-4126-bd69-d304ea27acae.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "com.yoavmoshe.pidif"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "pidif",]
appstream_xml_url = ""
reported_by = "bjesus"
updated_by = ""

+++


### Description

+++
title = "Jami (GNOME client)"
description = "Privacy-oriented voice, video, chat, and conference platform"
aliases = []
date = 2020-10-27
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC-BY-SA-3.0",]
app_author = [ "savoirfairelinux",]
categories = [ "chat", "telephony",]
mobile_compatibility = [ "4",]
status = [ "inactive",]
frameworks = [ "GTK3", "Clutter",]
backends = []
services = [ "Jami", "SIP",]
packaged_in = [ "archlinuxarm_aarch64",]
freedesktop_categories = [ "Chat", "Communication", "FileTransfer", "GNOME", "GTK", "InstantMessaging", "Network", "Office", "P2P", "Productivity",]
programming_languages = [ "Cpp", "C",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://git.jami.net/savoirfairelinux/jami-client-gnome"
homepage = "https://jami.net/"
bugtracker = "https://git.jami.net/savoirfairelinux/jami-client-gnome/-/issues"
donations = "https://www.paypal.com/donate/?hosted_button_id=MGUDJLQZ4TP5W"
translations = "https://www.transifex.com/savoirfairelinux/jami/"
more_information = [ "https://git.jami.net/savoirfairelinux/jami-client-gnome/-/issues/1299",]
summary_source_url = "https://git.jami.net/savoirfairelinux/jami-client-gnome/-/raw/master/jami-gnome.appdata.xml"
screenshots = [ "https://dl.jami.net/media-resources/screenshots/jami_linux_audiovideo.png", "https://dl.jami.net/media-resources/screenshots/jami_linux_screenshare.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "jami-gnome",]
appstream_xml_url = "https://git.jami.net/savoirfairelinux/jami-client-gnome/-/raw/master/jami-gnome.appdata.xml"
reported_by = "linmob"
updated_by = "script"
app_id = "net.jami.Jami"

+++


### Description

An end-to-end encrypted secure and distributed voice, video, and
 chat communication platform that requires no central server and
 leaves the power of privacy and freedom in the hands of users.


Jami supports the following key features:


* One-to-one conversations
* File sharing
* Audio calls and conferences
* Video calls and conferences
* Screen sharing in video calls and conferences
* Recording and sending audio messages
* Recording and sending video messages
* Functioning as a SIP phone software


Client applications for GNU/Linux, Windows, macOS, iOS, Android,
 and Android TV are available, making Jami an interoperable and
 cross-platform communication framework.

[Source](https://git.jami.net/savoirfairelinux/jami-client-gnome/-/raw/master/jami-gnome.appdata.xml)

### Notice

Adjusts fairly well, uses Xwayland because of Clutter. [Likely deprecated](https://git.jami.net/savoirfairelinux/jami-client-gnome/-/issues/1299) in favour of [Jami-qt](@/apps/net.jami.jami.md).

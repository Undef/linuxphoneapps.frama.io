+++
title = "VVM Player"
description = "Visual Voicemail Player"
aliases = []
date = 2021-07-21
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Chris Talbot",]
categories = [ "visual voicemail",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "vvmd",]
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "aur", "debian_12", "debian_13", "debian_unstable", "devuan_unstable", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "Audio", "Player",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/kop316/vvmplayer"
homepage = "https://gitlab.com/kop316/vvmplayer"
bugtracker = "https://gitlab.com/kop316/vvmplayer/issues"
donations = "https://liberapay.com/kop316/donate"
translations = ""
more_information = [ "https://gitlab.com/kop316/vvmplayer/-/wikis/home",]
summary_source_url = "https://gitlab.com/kop316/vvmplayer"
screenshots = [ "https://gitlab.com/kop316/vvmplayer/-/raw/main/data/metainfo/screenshot.png?inline=false",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.kop316.vvmplayer"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://gitlab.com/kop316/vvmplayer/-/raw/main/org.kop316.vvmplayer.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "vvmplayer",]
appstream_xml_url = "https://gitlab.com/kop316/vvmplayer/-/raw/main/data/metainfo/org.kop316.vvmplayer.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Visual Voicemail Player is a GUI app that plays and deletes Visual voicemails
 retreived by Visual Voicemail Daemon (vvmd).

[Source](https://gitlab.com/kop316/vvmplayer/-/raw/main/data/metainfo/org.kop316.vvmplayer.metainfo.xml.in)

### Notice

Used GTK3/libhandy before release 2.0. Check [Carrier Notes](https://gitlab.com/kop316/vvmplayer/-/wikis/home) to check whether your mobile broadband provider is supported.

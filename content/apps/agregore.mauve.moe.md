+++
title = "Agregore Browser"
description = "A minimal browser for the distributed web"
aliases = []
date = 2021-05-16
updated = 2022-12-19

[taxonomies]
project_licenses = [ "AGPL-3.0-only",]
metadata_licenses = []
app_author = [ "agregoreweb",]
categories = [ "web browser",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Electron",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "Network", "WebBrowser",]
programming_languages = [ "JavaScript",]
build_systems = [ "yarn",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/AgregoreWeb/agregore-browser"
homepage = "https://agregore.mauve.moe/"
bugtracker = "https://github.com/AgregoreWeb/agregore-browser/issues/"
donations = ""
translations = ""
more_information = [ "https://www.youtube.com/watch?v=TnYKvOQB0ts", "https://github.com/AgregoreWeb/agregore-browser/issues/103",]
summary_source_url = "https://github.com/AgregoreWeb/agregore-browser"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "agregore.mauve.moe"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "agregore-browser",]
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"

+++

### Notice

Can be built as described on https://github.com/AgregoreWeb/agregore-browser/issues/103

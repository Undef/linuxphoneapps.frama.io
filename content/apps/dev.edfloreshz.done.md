+++
title = "Done"
description = "To-do lists reimagined"
aliases = []
date = 2024-02-13

[taxonomies]
project_licenses = [ "MPL-2.0",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Eduardo Flores",]
categories = [ "productivity",]
mobile_compatibility = [ "4",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Microsoft Todo",]
packaged_in = [ "aur", "flathub", "nix_stable_23_11", "nix_unstable", "snapcraft",]
freedesktop_categories = [ "Database", "Office",]
programming_languages = [ "Rust", "Fluent",]
build_systems = [ "Meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/done-devs/done/"
homepage = "https://done.edfloreshz.dev/"
bugtracker = "https://github.com/done-devs/done/issues"
donations = "https://github.com/sponsors/edfloreshz"
translations = "https://github.com/done-devs/done/tree/main/i18n"
more_information = []
summary_source_url = "https://flathub.org/apps/dev.edfloreshz.Done"
screenshots = [ "https://raw.githubusercontent.com/edfloreshz/done/main/data/resources/screenshots/about.png", "https://raw.githubusercontent.com/edfloreshz/done/main/data/resources/screenshots/dark.png", "https://raw.githubusercontent.com/edfloreshz/done/main/data/resources/screenshots/dark_details.png", "https://raw.githubusercontent.com/edfloreshz/done/main/data/resources/screenshots/task_details.png", "https://raw.githubusercontent.com/edfloreshz/done/main/data/resources/screenshots/tasks.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "dev.edfloreshz.Done"
scale_to_fit = ""
flathub = "https://flathub.org/apps/dev.edfloreshz.Done"
flatpak_link = "https://flathub.org/apps/dev.edfloreshz.Done.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/done-devs/done/main/build-aux/dev.edfloreshz.Done.json"
snapcraft = "https://snapcraft.io/done"
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "done",]
appstream_xml_url = "https://raw.githubusercontent.com/done-devs/done/main/data/dev.edfloreshz.Done.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = ""

+++

### Description

The ultimate task management solution for seamless organization and efficiency.


Done is a user-friendly app that allows you to effortlessly consolidate your existing task providers into a single application for optimal productivity and organization.


Current features:


* Create tasks and lists
* Add task service providers
* Edit your tasks and lists
* Delete tasks and lists

[Source](https://raw.githubusercontent.com/done-devs/done/main/data/dev.edfloreshz.Done.metainfo.xml.in.in)

### Notice

Does not fit the screen without scale-to-fit, being a few pixels too wide.

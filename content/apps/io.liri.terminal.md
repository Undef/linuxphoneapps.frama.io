+++
title = "Liri Terminal"
description = "Use the command line"
aliases = []
date = 2019-02-01
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Liri",]
categories = [ "terminal emulator",]
mobile_compatibility = [ "3",]
status = []
frameworks = [ "QtQuick", "fluid",]
backends = []
services = []
packaged_in = [ "arch", "archlinuxarm_aarch64", "aur", "manjaro_stable", "manjaro_unstable",]
freedesktop_categories = [ "Qt", "System", "TerminalEmulator",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/lirios/terminal"
homepage = "https://liri.io"
bugtracker = "https://github.com/lirios/terminal/issues/new"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/lirios/terminal/develop/src/app/io.liri.Terminal.appdata.xml"
screenshots = [ "https://liri.io/images/apps/terminal/terminal1.png", "https://liri.io/images/apps/terminal/terminal2.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "io.liri.Terminal"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "liri-terminal",]
appstream_xml_url = "https://raw.githubusercontent.com/lirios/terminal/develop/src/app/io.liri.Terminal.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

Terminal app.


* Basic terminal features
* Warning on pasting commands with sudo
* Settings dialog with the following settings:
* Open new terminal window
* Open multiple tabs

[Source](https://raw.githubusercontent.com/lirios/terminal/develop/src/app/io.liri.Terminal.appdata.xml)

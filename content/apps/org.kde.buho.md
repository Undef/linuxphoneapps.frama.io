+++
title = "Buho"
description = "Note-taking app"
aliases = []
date = 2019-02-01
updated = 2023-09-30

[taxonomies]
project_licenses = [ "LGPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "maui",]
categories = [ "note taking",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "MauiKit", "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "Qt", "Utility", "TextTools",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/maui/buho"
homepage = "https://mauikit.org/"
bugtracker = "https://invent.kde.org/maui/buho/-/issues"
donations = "https://kde.org/community/donations"
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/maui/buho/-/raw/master/src/org.kde.buho.metainfo.xml"
screenshots = [ "https://nxos.org/wp-content/uploads/2021/05/buho-stable-1024x723.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = false
app_id = "org.kde.buho"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "buho",]
appstream_xml_url = "https://invent.kde.org/maui/buho/-/raw/master/src/org.kde.buho.metainfo.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description
Buho is a note-taking app. Buho will let you take quick notes, save links, create books and pages for taking classes notes.

[Source](https://invent.kde.org/maui/buho/-/raw/master/src/org.kde.buho.metainfo.xml)

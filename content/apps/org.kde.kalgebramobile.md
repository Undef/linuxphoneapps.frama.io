+++
title = "KAlgebra Mobile"
description = "Graph Calculator"
aliases = [ "apps/org.kde.kalgebra/",]
date = 2019-02-01
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community",]
categories = [ "education",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Qt", "KDE", "Education", "Science", "Math",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/education/kalgebra"
homepage = "https://apps.kde.org/kalgebra"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?format=guided&product=kalgebra"
donations = "https://www.kde.org/community/donations/?app=kalgebra&source=appdata"
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/education/kalgebra/-/raw/master/mobile/org.kde.kalgebramobile.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/kalgebra/kalgebra-mobile.png", "https://cdn.kde.org/screenshots/kalgebra/kalgebra-plot2d.png", "https://cdn.kde.org/screenshots/kalgebra/kalgebra.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.kalgebramobile"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.kalgebra"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "kalgebra",]
appstream_xml_url = "https://invent.kde.org/education/kalgebra/-/raw/master/mobile/org.kde.kalgebramobile.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

KAlgebra is an application that can replace your graphing calculator. It has numerical, logical, symbolic, and analysis features that let you calculate mathematical expressions on the console and graphically plot the results in 2D or 3D.

[Source](https://invent.kde.org/education/kalgebra/-/raw/master/mobile/org.kde.kalgebramobile.appdata.xml)

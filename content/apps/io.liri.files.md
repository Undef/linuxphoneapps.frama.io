+++
title = "Files"
description = "Access and organize files"
aliases = []
date = 2019-02-01
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Liri",]
categories = [ "file management",]
mobile_compatibility = [ "3",]
status = []
frameworks = [ "QtQuick", "fluid",]
backends = []
services = []
packaged_in = [ "arch", "archlinuxarm_aarch64", "aur", "manjaro_stable", "manjaro_unstable",]
freedesktop_categories = [ "System",]
programming_languages = [ "Cpp", "C", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/lirios/files"
homepage = "https://liri.io"
bugtracker = "https://github.com/lirios/files/issues/new"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/lirios/files/develop/src/app/io.liri.Files.appdata.xml"
screenshots = [ "https://liri.io/images/apps/files/screen_home_01.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "io.liri.Files"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "liri-files",]
appstream_xml_url = "https://raw.githubusercontent.com/lirios/files/develop/src/app/io.liri.Files.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

Liri Files is the default file manager of the Liri desktop.
 It provides a simple way to manage your files and browse your file system.

[Source](https://raw.githubusercontent.com/lirios/files/develop/src/app/io.liri.Files.appdata.xml)

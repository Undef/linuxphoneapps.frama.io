+++
title = "ScrumPoker"
description = "ScrumPoker is a simple and useful scrum planning poker App. It helps you by making your estimation meetings more effective."
aliases = []
date = 2019-02-16
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "dcordero",]
categories = [ "project management tool",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "Utility",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/dcordero/ScrumPoker"
homepage = ""
bugtracker = "https://github.com/dcordero/ScrumPoker/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/dcordero/ScrumPoker"
screenshots = [ "https://github.com/dcordero/ScrumPoker",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "me.dcordero.scrumpoker"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = "script"

+++

+++
title = "G4Music"
description = "Play your music elegantly"
aliases = []
date = 2022-06-21
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Nanling",]
categories = [ "music player",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_edge", "aur", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Audio", "Music", "Player",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/neithern/g4music"
homepage = "https://gitlab.gnome.org/neithern/g4music"
bugtracker = "https://gitlab.gnome.org/neithern/g4music/issues"
donations = ""
translations = "https://l10n.gnome.org/module/g4music"
more_information = []
summary_source_url = "https://gitlab.gnome.org/neithern/g4music/-/raw/master/data/app.appdata.xml.in"
screenshots = [ "https://gitlab.gnome.org/neithern/g4music/-/raw/master/shots/albums.png", "https://gitlab.gnome.org/neithern/g4music/-/raw/master/shots/playing.png", "https://gitlab.gnome.org/neithern/g4music/-/raw/master/shots/playlist.png", "https://gitlab.gnome.org/neithern/g4music/-/raw/master/shots/window.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "com.github.neithern.g4music"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.github.neithern.g4music"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "g4music",]
appstream_xml_url = "https://gitlab.gnome.org/neithern/g4music/-/raw/master/data/app.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

G4Music is a fast fluent lightweight music player written in GTK4, with a beautiful and adaptive user interface, focuses on high performance for large music collection.


Features


* Supports most music file types, samba and any other remote protocols (depends on GIO and GStreamer).
* Fast loading and parsing thousands of music files in very few seconds, monitor local changes.
* Low memory usage for large music collection with album covers (embedded and external), no thumbnail caches to store.
* Group and sorts by album/artist/title, shuffle list, full-text searching.
* Gaussian blurred cover as background, follows GNOME light/dark mode.
* Drag-drop from GNOME Files, showing music in Files.
* Supports audio peaks visualizer.
* Supports gapless playback.
* Supports normalizing volume with ReplayGain.
* Supports pipewire and other audio sink.
* Supports MPRIS control.
* Less than 500KB to install.

[Source](https://gitlab.gnome.org/neithern/g4music/-/raw/master/data/app.appdata.xml.in)

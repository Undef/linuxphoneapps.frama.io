+++
title = "Software"
description = "Install and update apps"
aliases = [ "apps/org.gnome.software.desktop/",]
date = 2021-09-28
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The GNOME Project",]
categories = [ "software center",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "PackageKit",]
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Settings", "PackageManager",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnome-software/"
homepage = "https://apps.gnome.org/Software"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-software/-/issues"
donations = "https://www.gnome.org/donate/"
translations = "https://wiki.gnome.org/TranslationProject"
more_information = [ "https://apps.gnome.org/app/org.gnome.Software/", "https://linmob.net/gnome-software-41-will-be-fine-on-mobile/",]
summary_source_url = "https://gitlab.gnome.org/GNOME/gnome-software/-/raw/main/data/metainfo/org.gnome.Software.metainfo.xml.in"
screenshots = [ "https://gitlab.gnome.org/GNOME/gnome-software/raw/HEAD/data/metainfo/ss-details.png", "https://gitlab.gnome.org/GNOME/gnome-software/raw/HEAD/data/metainfo/ss-details2.png", "https://gitlab.gnome.org/GNOME/gnome-software/raw/HEAD/data/metainfo/ss-installed.png", "https://gitlab.gnome.org/GNOME/gnome-software/raw/HEAD/data/metainfo/ss-overview.png", "https://gitlab.gnome.org/GNOME/gnome-software/raw/HEAD/data/metainfo/ss-up-to-date.png", "https://gitlab.gnome.org/GNOME/gnome-software/raw/HEAD/data/metainfo/ss-updates-details.png", "https://gitlab.gnome.org/GNOME/gnome-software/raw/HEAD/data/metainfo/ss-updates.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.Software"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-software",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-software/-/raw/main/data/metainfo/org.gnome.Software.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Software allows you to find and install new apps and system
 extensions and remove existing installed apps.


Software showcases featured and popular apps with useful
 descriptions and multiple screenshots per app.
 Apps can be found either through browsing the list of categories
 or by searching.
 It also allows you to update your system using an offline update.

[Source](https://gitlab.gnome.org/GNOME/gnome-software/-/raw/main/data/metainfo/org.gnome.Software.metainfo.xml.in)

### Notice

Upstream is mobile compliant from release 41 forward, uses GTK4/libadwaita since 42. Purism maintain a fork called [PureOS Store](https://source.puri.sm/Librem5/pureos-store) for PureOS.

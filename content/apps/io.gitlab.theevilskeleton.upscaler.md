+++
title = "Upscaler"
description = "Upscale and enhance images"
aliases = []
date = 2023-11-18
updated = 2023-12-31

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC-BY-SA-4.0",]
app_author = [ "Upscaler Contributors",]
categories = [ "image processing",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "Real-ESRGAN ncnn Vulkan",]
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GNOME", "GTK", "Graphics", "ImageProcessing",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = [ "requires offline-only",]
tags = []

[extra]
repository = "https://gitlab.gnome.org/World/Upscaler"
homepage = "https://tesk.page/upscaler"
bugtracker = "https://gitlab.gnome.org/World/Upscaler/-/issues"
donations = ""
translations = "https://gitlab.gnome.org/World/Upscaler/po"
more_information = [ "https://matrix.to/#/#Upscaler:gnome.org", "https://gitlab.gnome.org/World/Upscaler/-/blob/main/PRESS.md",]
summary_source_url = "https://gitlab.gnome.org/World/Upscaler/-/raw/main/data/io.gitlab.theevilskeleton.Upscaler.metainfo.xml.in.in"
screenshots = [ "https://tesk.page/assets/upscaler-running-locally.webp", "https://tesk.page/assets/upscaler.webp",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "io.gitlab.theevilskeleton.Upscaler"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.gitlab.theevilskeleton.Upscaler"
flatpak_link = ""
flatpak_recipe = "https://gitlab.gnome.org/World/Upscaler/-/raw/main/build-aux/io.gitlab.theevilskeleton.Upscaler.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "upscaler",]
appstream_xml_url = "https://gitlab.gnome.org/World/Upscaler/-/raw/main/data/io.gitlab.theevilskeleton.Upscaler.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Upscaler enhances and upscales images four times their original resolution. It has optimized modes for cartoons/anime and photos.

[Source](https://gitlab.gnome.org/World/Upscaler/-/raw/main/data/io.gitlab.theevilskeleton.Upscaler.metainfo.xml.in.in)

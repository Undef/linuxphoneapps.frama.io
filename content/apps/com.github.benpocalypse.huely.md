+++
title = "Huely"
description = "Color your space"
aliases = []
date = 2022-03-06
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Ben Foote",]
categories = [ "smart home",]
mobile_compatibility = [ "4",]
status = [ "early",]
frameworks = [ "GTK3", "granite", "libhandy",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "GTK", "Utility",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/benpocalypse/Huely"
homepage = "https://github.com/benpocalypse/Huely"
bugtracker = "https://github.com/benpocalypse/Huely/issues"
donations = ""
translations = ""
more_information = [ "https://mastodon.social/@benpocalypse/107999857976634075",]
summary_source_url = "https://raw.githubusercontent.com/benpocalypse/Huely/main/data/Huely.appdata.xml.in"
screenshots = [ "https://raw.githubusercontent.com/benpocalypse/Huely/main/screenshots/en/dark.png", "https://raw.githubusercontent.com/benpocalypse/Huely/main/screenshots/en/light-and-dark.png", "https://raw.githubusercontent.com/benpocalypse/Huely/main/screenshots/en/light.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "com.github.benpocalypse.Huely"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.github.benpocalypse.Huely"
flatpak_link = ""
flatpak_recipe = "https://raw.githubusercontent.com/benpocalypse/Huely/main/com.github.benpocalypse.Huely.yml"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/benpocalypse/Huely/main/data/Huely.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Control WiFi lights using this simple app.

[Source](https://raw.githubusercontent.com/benpocalypse/Huely/main/data/Huely.appdata.xml.in)

### Notice

Almost a 5 - the app is just a few pixels to wide, and is should be usable without scale-to-fit. Has visual glitches with GNOME Shell Mobile 45 on OnePlus 6.
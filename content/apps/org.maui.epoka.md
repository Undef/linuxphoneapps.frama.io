+++
title = "Epoka"
description = "Simple clock app in development, supporting stopwatch and timer until now"
aliases = []
date = 2019-10-22
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "pontaoski",]
categories = [ "clock",]
mobile_compatibility = [ "5",]
status = [ "inactive", "early", "pre-release",]
frameworks = [ "MauiKit", "Kirigami",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Qt", "KDE", "Utility", "Clock",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://github.com/pontaoski/epoka"
homepage = ""
bugtracker = "https://github.com/pontaoski/epoka/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "no quotation"
screenshots = [ "https://t.me/mauiproject/1508",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.maui.epoka"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = "script"

+++

### Notice

No commits since January 2020, no longer runs after build (submodules no longer recursable).

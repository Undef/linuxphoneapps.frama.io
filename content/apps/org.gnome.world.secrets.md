+++
title = "Secrets"
description = "Manage your passwords"
aliases = []
date = 2019-02-16
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Falk Alexander Seidl",]
categories = [ "password manager",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "pykeepass",]
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gnuguix", "manjaro_stable", "manjaro_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/World/secrets"
homepage = "https://gitlab.gnome.org/World/secrets"
bugtracker = "https://gitlab.gnome.org/World/secrets/-/issues"
donations = ""
translations = "https://l10n.gnome.org/module/Secrets/"
more_information = [ "https://apps.gnome.org/app/org.gnome.World.Secrets/",]
summary_source_url = "https://gitlab.gnome.org/World/secrets/-/raw/master/data/org.gnome.World.Secrets.metainfo.xml.in.in"
screenshots = [ "https://gitlab.gnome.org/World/secrets/-/raw/master/screenshots/browser.png", "https://gitlab.gnome.org/World/secrets/-/raw/master/screenshots/edit.png", "https://gitlab.gnome.org/World/secrets/-/raw/master/screenshots/entry.png", "https://gitlab.gnome.org/World/secrets/-/raw/master/screenshots/search.png", "https://gitlab.gnome.org/World/secrets/-/raw/master/screenshots/start.png", "https://gitlab.gnome.org/World/secrets/-/raw/master/screenshots/unlock.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.World.Secrets"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.World.Secrets"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-passwordsafe", "secrets",]
appstream_xml_url = "https://gitlab.gnome.org/World/secrets/-/raw/master/data/org.gnome.World.Secrets.metainfo.xml.in.in"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

Secrets is a password manager which makes use of the KeePass v.4 format. It integrates perfectly with the GNOME desktop and provides an easy and uncluttered interface for the management of password databases.

[Source](https://gitlab.gnome.org/World/secrets/-/raw/master/data/org.gnome.World.Secrets.metainfo.xml.in.in)

### Notice

Previously based on GTK3/libhandy and called GNOME PasswordSafe. Secrets additionally supports two-factor authentication.

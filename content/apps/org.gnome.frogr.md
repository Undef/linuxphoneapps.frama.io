+++
title = "frogr"
description = "A flickr uploader for GNOME"
aliases = []
date = 2020-10-15
updated = 2024-01-04

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Mario Sanchez Prada",]
categories = [ "photo management",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3",]
backends = []
services = [ "Flickr",]
packaged_in = [ "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "FileTransfer",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/GNOME/frogr/"
homepage = "https://wiki.gnome.org/Apps/Frogr"
bugtracker = "https://gitlab.gnome.org/GNOME/frogr/-/issues"
donations = "https://www.gnome.org/donate"
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/GNOME/frogr/-/raw/master/data/org.gnome.frogr.appdata.xml.in"
screenshots = [ "https://wiki.gnome.org/Apps/Frogr?action=AttachFile&do=get&target=frogr-screenshot.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.gnome.frogr.desktop"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.frogr"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "frogr",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/frogr/-/raw/master/data/org.gnome.frogr.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

frogr allows users to manage their accounts on the Flickr photo hosting
 service with a simple and direct interface.


It supports all the basic Flickr features, including uploading
 pictures, adding descriptions, setting tags and managing sets
 and groups pools.

[Source](https://gitlab.gnome.org/GNOME/frogr/-/raw/master/data/org.gnome.frogr.appdata.xml.in)
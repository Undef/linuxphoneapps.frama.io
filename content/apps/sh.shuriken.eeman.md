+++
title = "Eeman"
description = "Track Salah, Read the Quran"
aliases = []
date = 2024-02-05

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "SHuRiKeN",]
categories = [ "bible",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Python",]
build_systems = [ "Meson",]
requires_internet = []
tags = []

[extra]
repository = "https://codeberg.org/SHuRiKeN/Eeman"
homepage = "https://codeberg.org/SHuRiKeN/Eeman"
bugtracker = "https://codeberg.org/SHuRiKeN/Eeman"
donations = "https://ko-fi.com/shuriken1812"
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/sh.shuriken.Eeman"
screenshots = [ "https://codeberg.org/SHuRiKeN/Eeman/raw/tag/0.1.1/.screenshots/prayer_dark_theme.png", "https://codeberg.org/SHuRiKeN/Eeman/raw/tag/0.1.1/.screenshots/prayer_light_theme.png", "https://codeberg.org/SHuRiKeN/Eeman/raw/tag/0.1.1/.screenshots/preferences.png", "https://codeberg.org/SHuRiKeN/Eeman/raw/tag/0.1.1/.screenshots/quran_dark_theme.png", "https://codeberg.org/SHuRiKeN/Eeman/raw/tag/0.1.1/.screenshots/quran_light_theme.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "sh.shuriken.Eeman"
scale_to_fit = ""
flathub = "https://flathub.org/apps/sh.shuriken.Eeman"
flatpak_link = "https://flathub.org/apps/sh.shuriken.Eeman.flatpakref"
flatpak_recipe = "https://codeberg.org/SHuRiKeN/Eeman/raw/branch/main/sh.shuriken.Eeman.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://codeberg.org/SHuRiKeN/Eeman/raw/branch/main/data/sh.shuriken.Eeman.metainfo.xml.in"
reported_by = "linmob"
updated_by = ""

+++


### Description

An app that lets you track and get notified of your Salah timings, and read the beautiful Quran.


Features:


* Choose calculation methods
* Ability to get location from IP or manually
* Choose school of thought, i.e later Asr time for Hanafi
* read the Quran with popular translation of Saheeh International

[Source](https://codeberg.org/SHuRiKeN/Eeman/raw/branch/main/data/sh.shuriken.Eeman.metainfo.xml.in)

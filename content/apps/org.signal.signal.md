+++
title = "Signal Desktop"
description = "Private messenger"
aliases = []
date = 2022-04-24
updated = 2024-01-02

[taxonomies]
project_licenses = [ "AGPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Signal Foundation",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "Electron",]
backends = []
services = [ "Signal",]
packaged_in = [ "alpine_edge", "arch", "aur", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "Network", "Chat", "InstantMessaging",]
programming_languages = [ "TypeScript", "JavaScript",]
build_systems = [ "yarn",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/signalapp/Signal-Desktop"
homepage = "https://signal.org/"
bugtracker = "https://github.com/flathub/org.signal.Signal/issues"
donations = "https://signal.org/donate/"
translations = ""
more_information = [ "https://web.archive.org/web/20230921192857/https://wiki.mobian.org/doku.php?id=signaldesktop", "https://wiki.mobian.org/doku.php?id=signal", "https://github.com/signalapp/Signal-Desktop/issues/3904#issuecomment-585861062",]
summary_source_url = "https://raw.githubusercontent.com/flathub/org.signal.Signal/master/org.signal.Signal.metainfo.xml"
screenshots = [ "https://signal.org/assets/screenshots/download-desktop-windows-cdc181d589c36f2da3ddb9471a981c73bf7e60f6713a90a376ef731f474ab752.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/org.signal.signal/1.png", "https://img.linuxphoneapps.org/org.signal.signal/2.png", "https://img.linuxphoneapps.org/org.signal.signal/3.png",]
all_features_touch = false
intended_for_mobile = false
app_id = "org.signal.Signal"
scale_to_fit = ""
flathub = ""
flatpak_link = "https://elagost.com/flatpak/"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "signal-desktop",]
appstream_xml_url = "https://raw.githubusercontent.com/flathub/org.signal.Signal/master/org.signal.Signal.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

To use the Signal desktop app, Signal must first be installed on your phone.


Millions of people use Signal every day for free and instantaneous communication 
 anywhere in the world. Send and receive high-fidelity messages, participate in 
 HD voice/video calls, and explore a growing set of new features that help you 
 stay connected. Signal’s advanced privacy-preserving technology is always enabled, 
 so you can focus on sharing the moments that matter with the people who matter to you.


Signal is an independent 501c3 nonprofit. The complete source code for 
 the Signal clients and the Signal server is available on GitHub.


This flatpak is maintained by the Flathub community, and is not necessarily 
 endorsed or officially maintained by the upstream developers.

[Source](https://raw.githubusercontent.com/flathub/org.signal.Signal/master/org.signal.Signal.metainfo.xml)

### Notice

Official Signal app for desktops, works pretty well - except for suffering from the common Electron issues: Blurriness, or non-working virtual keyboards (partially on Phosh, totally on Plasma Mobile) when enabling the ozone wayland backend. Sadly, Signal does not provide official ARM64/aarch64 binaries, so that unofficial builds must do: Builds for Debian are listed on the [Mobian Wiki](https://web.archive.org/web/20230921192857/https://wiki.mobian.org/doku.php?id=signaldesktop), builds for Arch are available on [privacy-shark](https://privacyshark.zero-credibility.net/#howto) and there's also a distribution-independent [flatpak build](https://elagost.com/flatpak/) that should work on every distro. Be sure to examine these before use if your thread model requires it.

+++
title = "MyGNUHealth"
description = "GNU Health Personal Health Record"
aliases = [ "apps/org.gnuhealth.my/",]
date = 2020-10-15
updated = 2024-01-28

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "GNU Solidario",]
categories = [ "health",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "Kirigami",]
backends = []
services = [ "GNU Health",]
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "aur", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gnuguix", "opensuse_tumbleweed",]
freedesktop_categories = [ "Office", "Science",]
programming_languages = [ "Python", "QML",]
build_systems = [ "custom",]
requires_internet = []
tags = []

[extra]
repository = "https://hg.savannah.gnu.org/hgweb/health-mygnuhealth/"
homepage = "https://www.gnuhealth.org"
bugtracker = "https://lists.gnu.org/mailman/listinfo/health"
donations = ""
translations = ""
more_information = [ "https://www.gnuhealth.org/docs/mygnuhealth/",]
summary_source_url = "https://hg.savannah.gnu.org/hgweb/health-mygnuhealth/raw-file/c6b4e9bd3c69/mygnuhealth/org.gnuhealth.mygnuhealth.metainfo.xml"
screenshots = [ "https://www.gnuhealth.org/docs/mygnuhealth/images/book_of_life_list.png", "https://www.gnuhealth.org/docs/mygnuhealth/images/mygnuhealth.png", "https://www.gnuhealth.org/docs/mygnuhealth/images/mygnuhealth_wide_bio.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = false
app_id = "org.gnuhealth.mygnuhealth"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnuhealth.mygnuhealth"
flatpak_link = "https://flathub.org/apps/org.gnuhealth.mygnuhealth.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "mygnuhealth",]
appstream_xml_url = "https://hg.savannah.gnu.org/hgweb/health-mygnuhealth/raw-file/c6b4e9bd3c69/mygnuhealth/org.gnuhealth.mygnuhealth.metainfo.xml"
reported_by = "linmob"
updated_by = "linmob"

+++

### Description

Personal Health Record - PHR.

[Source](https://hg.savannah.gnu.org/hgweb/health-mygnuhealth/raw-file/c6b4e9bd3c69/mygnuhealth/org.gnuhealth.mygnuhealth.metainfo.xml)

### Notice

Can be installed using pip: pip3 install --user --upgrade MyGNUHealth.

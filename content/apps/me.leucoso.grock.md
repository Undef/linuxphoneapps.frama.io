+++
title = "Grock"
description = "Displays geological maps of the UK."
aliases = []
date = 2020-08-25
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Daniel Burgess",]
categories = [ "geology",]
mobile_compatibility = [ "5",]
status = [ "maturing", "inactive",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Science", "Geology",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://codeberg.org/dburgess/Grock"
homepage = "http://leucoso.me/grock.html"
bugtracker = "https://codeberg.org/dburgess/Grock/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/me.leucoso.Grock"
screenshots = [ "https://leucoso.me/grock/demo_screenshots/scr_features.png", "https://leucoso.me/grock/demo_screenshots/scr_grav.png", "https://leucoso.me/grock/demo_screenshots/scr_mag.png", "https://leucoso.me/grock/demo_screenshots/scr_main.png", "https://leucoso.me/grock/demo_screenshots/scr_main2.png", "https://leucoso.me/grock/demo_screenshots/scr_sat.png", "https://leucoso.me/grock/demo_screenshots/scr_search.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "me.leucoso.Grock"
scale_to_fit = ""
flathub = "https://flathub.org/apps/me.leucoso.Grock"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://codeberg.org/dburgess/Grock/raw/branch/master/data/me.leucoso.Grock.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Display geological maps of the UK, using data from the British Geological Survey.
 Allows rock unit names, types and ages to be found easily. Linear features and UK-wide
 gravitational and magnetic anomaly maps can also be shown.

[Source](https://codeberg.org/dburgess/Grock/raw/branch/master/data/me.leucoso.Grock.appdata.xml.in)
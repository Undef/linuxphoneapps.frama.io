+++
title = "Kasts"
description = "Podcast application"
aliases = []
date = 2021-05-05
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The KDE Community",]
categories = [ "podcast client",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "QtQuick", "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_13", "debian_unstable", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "Qt", "KDE", "Network", "Audio", "Feed", "Player",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/multimedia/kasts"
homepage = "https://apps.kde.org/kasts/"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=kasts"
donations = "https://bugs.kde.org/enter_bug.cgi?product=kasts"
translations = ""
more_information = [ "https://plasma-mobile.org/2021/04/27/plasma-mobile-update-march-april/", "https://plasma-mobile.org/2022/11/30/plasma-mobile-gear-22-11/#kasts", "https://plasma-mobile.org/2023/01/30/january-blog-post/#kasts-podcast",]
summary_source_url = "https://invent.kde.org/multimedia/kasts/-/raw/master/org.kde.kasts.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/kasts/kasts-desktop-minimized-header.png", "https://cdn.kde.org/screenshots/kasts/kasts-desktop.png", "https://cdn.kde.org/screenshots/kasts/kasts-mobile-player.png", "https://cdn.kde.org/screenshots/kasts/kasts-mobile.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = false
app_id = "org.kde.kasts"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.kasts"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "kasts",]
appstream_xml_url = "https://invent.kde.org/multimedia/kasts/-/raw/master/org.kde.kasts.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Kasts is a convergent podcast application that looks good on desktop and mobile.


Its main features are:


* Episode management through play queue
* Sync playback positions with other clients through gpodder.net or gpodder-nextcloud
* Variable playback speed
* Search for podcasts
* Full system integration: e.g. inhibit system suspend while listening

[Source](https://invent.kde.org/multimedia/kasts/-/raw/master/org.kde.kasts.appdata.xml)

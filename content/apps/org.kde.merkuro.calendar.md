+++
title = "Merkuro Calendar"
description = "Manage your tasks and events with speed and ease"
aliases = [ "apps/org.kde.kalendar/",]
date = 2021-08-04
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Clau Cambra and Carl Schwan", "KDE Community",]
categories = [ "calendar",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "Kirigami",]
backends = [ "Akonadi",]
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_12", "debian_13", "debian_unstable", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Qt", "KDE", "Office", "Calendar",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/pim/merkuro"
homepage = "https://apps.kde.org/merkuro.calendar/"
bugtracker = "https://invent.kde.org/pim/merkuro/-/issues"
donations = ""
translations = ""
more_information = [ "https://claudiocambra.com/category/kde/", "https://claudiocambra.com/2022/02/12/kalendar-1-0-is-out/", "https://kde.org/announcements/gear/22.04.0/#new-arrivals",]
summary_source_url = "https://invent.kde.org/pim/merkuro/-/raw/master/src/calendar/org.kde.merkuro.calendar.metainfo.xml"
screenshots = [ "https://cdn.kde.org/screenshots/kalendar/mobile_view.png", "https://cdn.kde.org/screenshots/kalendar/schedule_view.png", "https://cdn.kde.org/screenshots/kalendar/task_view.png", "https://cdn.kde.org/screenshots/merkuro/calendar-month.png", "https://cdn.kde.org/screenshots/merkuro/calendar-week.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/org.kde.kalendar/1.png", "https://img.linuxphoneapps.org/org.kde.kalendar/2.png", "https://img.linuxphoneapps.org/org.kde.kalendar/3.png", "https://img.linuxphoneapps.org/org.kde.kalendar/4.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "org.kde.merkuro.calendar"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "kalendar", "merkuro",]
appstream_xml_url = "https://invent.kde.org/pim/merkuro/-/raw/master/src/calendar/org.kde.merkuro.calendar.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Merkuro Calendar is a calendar application that allows you to manage your tasks and events. Kalendar supports both local calendars as well as a multitude of online calendars: Nextcloud, Google® Calendar, Outlook®, Caldav, and many more.


Merkuro gives you many ways to interact with your events. The month view provides an overview of the entire month; the week view presents a detailed hour-by-hour overview of your week; and the schedule view lists all of your upcoming events so that you can easily and quickly plan ahead.


A tasks view is also available, making it possible for you to manage your tasks and subtasks with Merkuro's powerful tree view and its customisable filtering capabilities.


Merkuro was built with the idea to be usable on desktop, on mobile and everything in between.

[Source](https://invent.kde.org/pim/merkuro/-/raw/master/src/calendar/org.kde.merkuro.calendar.metainfo.xml)

### Notice

Great calendar app! Previously called Kalendar.

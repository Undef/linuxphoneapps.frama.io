+++
title = "Video Trimmer"
description = "Trim videos quickly"
aliases = []
date = 2020-10-21
updated = 2023-12-31

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Ivan Molodetskikh",]
categories = [ "video editing",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_edge", "aur", "flathub", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Video", "AudioVideoEditing",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/YaLTeR/video-trimmer"
homepage = "https://gitlab.gnome.org/YaLTeR/video-trimmer"
bugtracker = "https://gitlab.gnome.org/YaLTeR/video-trimmer/-/issues"
donations = ""
translations = "https://l10n.gnome.org/module/video-trimmer/"
more_information = [ "https://apps.gnome.org/app/org.gnome.gitlab.YaLTeR.VideoTrimmer/",]
summary_source_url = "https://gitlab.gnome.org/YaLTeR/video-trimmer"
screenshots = [ "https://gitlab.gnome.org/YaLTeR/video-trimmer/uploads/e840fa093439348448007197d07c8033/image.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.gitlab.YaLTeR.VideoTrimmer"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.gitlab.YaLTeR.VideoTrimmer"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "video-trimmer",]
appstream_xml_url = "https://gitlab.gnome.org/YaLTeR/video-trimmer/-/raw/master/data/org.gnome.gitlab.YaLTeR.VideoTrimmer.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description
Video Trimmer cuts out a fragment of a video given the start and end timestamps. The video is never re-encoded, so the process is very fast and does not reduce the video quality.

[Source](https://gitlab.gnome.org/YaLTeR/video-trimmer/-/raw/master/data/org.gnome.gitlab.YaLTeR.VideoTrimmer.metainfo.xml.in.in)

+++
title = "Eloquens"
description = "Generate the lorem ipsum text"
aliases = []
date = 2024-01-14

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Felipe Kinoshita", "KDE Community",]
categories = [ "development",]
mobile_compatibility = [ "needs testing",]
status = [ "early",]
frameworks = [ "Kirigami", "QtQuick",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Qt", "KDE", "Utility", "TextTools",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/sdk/eloquens"
homepage = "https://invent.kde.org/fhek/eloquens"
bugtracker = "https://bugs.kde.org/buglist.cgi?component=General&product=Eloquens"
donations = ""
translations = ""
more_information = [ "https://apps.kde.org/eloquens/",]
summary_source_url = "https://invent.kde.org/sdk/eloquens/-/raw/master/org.kde.eloquens.metainfo.xml"
screenshots = [ "https://cdn.kde.org/screenshots/eloquens/eloquens.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.kde.eloquens"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://invent.kde.org/sdk/eloquens/-/raw/master/org.kde.eloquens.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "eloquens",]
appstream_xml_url = "https://invent.kde.org/sdk/eloquens/-/raw/master/org.kde.eloquens.metainfo.xml"
reported_by = "linmob"
updated_by = ""

+++


### Description

Eloquens allows you to easily generate the lorem ipsum text, you can add links, unordered/ordered list, headings and much more.

[Source](https://invent.kde.org/sdk/eloquens/-/raw/master/org.kde.eloquens.metainfo.xml)

### Notice

Pre-release, not packaged for any mobile distribution yet. Needs to be evaluated.

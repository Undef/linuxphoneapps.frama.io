+++
title = "Compass"
description = "This is a proof of concept of a simple compass app for Mobile Linux."
aliases = []
date = 2021-06-23
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "lgtrombetta",]
categories = [ "compass",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/lgtrombetta/compass"
homepage = ""
bugtracker = "https://gitlab.com/lgtrombetta/compass/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/lgtrombetta/compass"
screenshots = [ "https://gitlab.com/lgtrombetta/compass/-/raw/main/data/screenshot.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "com.gitlab.lgtrombetta.Compass.desktop"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://gitlab.com/lgtrombetta/compass/-/raw/main/com.gitlab.lgtrombetta.Compass.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "pinephone-compass",]
appstream_xml_url = "https://gitlab.com/lgtrombetta/compass/-/raw/main/data/com.gitlab.lgtrombetta.Compass.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Currently supported devices:

- Pine64 Pinephone v1.0, v1.1, v1.2 (LIS3MDL magnetometer)
- Pine64 Pinephone v1.2b (AF8133J magnetometer)
- Pine64 Pinephone Pro (AF8133J magnetometer, requires Megi's kernel>=6.3 for the correct mount-matrix)
- Purism Librem 5 (LSM9DS1 magnetometer)

Known issues:

- AF8133J is not currently recognized in pmOS

[Source](https://gitlab.com/lgtrombetta/compass)

### Notice

Works great after calibration!

+++
title = "Yishu"
description = "A bespoke and simple Todo.txt client"
aliases = []
date = 2020-12-05
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Lains",]
categories = [ "productivity",]
mobile_compatibility = [ "3",]
status = [ "archived",]
frameworks = [ "GTK3", "libhandy", "granite",]
backends = [ "todo.txt",]
services = []
packaged_in = [ "aur", "opensuse_tumbleweed",]
freedesktop_categories = [ "GTK", "Utility",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/lainsce/yishu"
homepage = "https://github.com/lainsce/yishu/"
bugtracker = "https://github.com/lainsce/yishu/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/lainsce/yishu/master/data/com.github.lainsce.yishu.appdata.xml.in"
screenshots = [ "https://raw.githubusercontent.com/lainsce/yishu/master/data/shot.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "com.github.lainsce.yishu"
scale_to_fit = "com.github.lainsce.yishu"
flathub = ""
flatpak_link = "https://appcenter.elementary.io/com.github.lainsce.yishu/"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "yishu",]
appstream_xml_url = "https://raw.githubusercontent.com/lainsce/yishu/master/data/com.github.lainsce.yishu.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Write a to-do list that will be easy to sync with most known Todo.txt clients


* Quit anytime with the shortcut Ctrl + Q
* Easily shareable with other Todo.txt clients on other platforms
* Searches can be saved, one at a time. No need for more.

[Source](https://raw.githubusercontent.com/lainsce/yishu/master/data/com.github.lainsce.yishu.appdata.xml.in)

### Notice

Works fine after scale-to-fit, repo has been archived.

+++
title = "Iridium"
description = "Friendly IRC Client"
aliases = []
date = 2022-06-29
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Andrew Vojak",]
categories = [ "chat",]
mobile_compatibility = [ "4",]
status = [ "mature",]
frameworks = [ "GTK3", "libhandy", "granite",]
backends = []
services = [ "IRC",]
packaged_in = [ "aur",]
freedesktop_categories = [ "GTK", "Network", "Chat",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/avojak/iridium"
homepage = "https://github.com/avojak/iridium"
bugtracker = "https://github.com/avojak/iridium/issues"
donations = "https://liberapay.com/avojak"
translations = ""
more_information = []
summary_source_url = "https://appcenter.elementary.io/com.github.avojak.iridium/"
screenshots = [ "https://raw.githubusercontent.com/avojak/iridium/master/data/assets/screenshots/iridium-screenshot-01.png", "https://raw.githubusercontent.com/avojak/iridium/master/data/assets/screenshots/iridium-screenshot-02.png", "https://raw.githubusercontent.com/avojak/iridium/master/data/assets/screenshots/iridium-screenshot-03.png", "https://raw.githubusercontent.com/avojak/iridium/master/data/assets/screenshots/iridium-screenshot-04.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/com.github.avojak.iridium/1.png", "https://img.linuxphoneapps.org/com.github.avojak.iridium/2.png", "https://img.linuxphoneapps.org/com.github.avojak.iridium/3.png", "https://img.linuxphoneapps.org/com.github.avojak.iridium/4.png", "https://img.linuxphoneapps.org/com.github.avojak.iridium/5.png", "https://img.linuxphoneapps.org/com.github.avojak.iridium/6.png", "https://img.linuxphoneapps.org/com.github.avojak.iridium/7.png",]
all_features_touch = false
intended_for_mobile = false
app_id = "com.github.avojak.iridium"
scale_to_fit = "com.github.avojak.iridium"
flathub = ""
flatpak_link = "https://appcenter.elementary.io/com.github.avojak.iridium/"
flatpak_recipe = "https://raw.githubusercontent.com/avojak/iridium/master/com.github.avojak.iridium.yml"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "elementary-iridium",]
appstream_xml_url = "https://raw.githubusercontent.com/avojak/iridium/master/data/com.github.avojak.iridium.appdata.xml.in.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Easily connect and chat on any IRC server.


Features include:


* Store your server connections and joined channels between sessions
* Connect to servers securely with SSL support
* Favorite channels for quick and easy access
* Pick up where you left off with automatic server reconnection
* Optionally disable remembered connections for increased privacy

[Source](https://raw.githubusercontent.com/avojak/iridium/master/data/com.github.avojak.iridium.appdata.xml.in.in)

### Notice

The main screens work perfectly, but settings work better with scale-to-fit (although they can be somewhat usable without).

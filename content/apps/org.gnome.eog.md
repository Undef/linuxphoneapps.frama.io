+++
title = "Eye of GNOME"
description = "Browse and rotate images"
aliases = []
date = 2020-12-12
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GFDL-1.3", "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "GNOME Developers",]
categories = [ "image viewer",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Graphics", "Viewer",]
programming_languages = [ "C", "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/GNOME/eog/"
homepage = "https://wiki.gnome.org/Apps/EyeOfGnome"
bugtracker = "https://gitlab.gnome.org/GNOME/eog/issues"
donations = "https://www.gnome.org/donate/"
translations = "https://l10n.gnome.org/module/eog/"
more_information = [ "https://apps.gnome.org/app/org.gnome.eog/",]
summary_source_url = "https://gitlab.gnome.org/GNOME/eog/-/raw/master/data/eog.appdata.xml.in"
screenshots = [ "https://gitlab.gnome.org/GNOME/eog/raw/HEAD/data/screenshot.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.gnome.eog"
scale_to_fit = "eog"
flathub = "https://flathub.org/apps/org.gnome.eog"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "eog",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/eog/-/raw/master/data/eog.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Eye of GNOME is an image viewer designed for the GNOME desktop.
 It integrates with the GTK look and feel of GNOME, and supports many image
 formats for viewing single images or images in a collection.


It also allows to view the images in a fullscreen slideshow mode
 or set an image as the desktop wallpaper.
 It reads the camera tags to automatically rotate your images in the correct
 portrait or landscape orientation.

[Source](https://gitlab.gnome.org/GNOME/eog/-/raw/master/data/eog.appdata.xml.in)

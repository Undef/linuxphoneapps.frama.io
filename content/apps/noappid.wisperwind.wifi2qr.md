+++
title = "Wifi2QR"
description = "Display QR codes to easily connect to any Wifi for which a NetworkManager connection profile exists."
aliases = []
date = 2021-12-18
updated = 2023-09-30

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = []
app_author = [ "wisperwind",]
categories = [ "utilities",]
mobile_compatibility = [ "4",]
status = [ "inactive", "early", "pre-release",]
frameworks = [ "GTK4",]
backends = [ "networkmanager", "qrencode",]
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "Network", "Utility",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://git.sr.ht/~wisperwind/wifi2qr"
homepage = ""
bugtracker = "https://todo.sr.ht/~wisperwind/wifi2qr"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://git.sr.ht/~wisperwind/wifi2qr"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = "com.example.apps.wifi2qr"
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "linmob"

+++

### Notice
WIP Wifi QR-Code generator (works fine despite not being designed for mobile - no desktop file).

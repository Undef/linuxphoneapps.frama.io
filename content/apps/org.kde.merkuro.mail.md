+++
title = "Merkuro Mail"
description = "Read your emails with speed and ease"
aliases = []
date = 2024-01-14

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Clau Cambra and Carl Schwan", "KDE Community",]
categories = [ "email",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "Kirigami", "QtQuick",]
backends = [ "Akonadi",]
services = []
packaged_in = [ "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "fedora_38", "fedora_39", "fedora_rawhide", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "Qt", "KDE", "Office", "Email",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/pim/merkuro/-/tree/master/src/mail"
homepage = "https://apps.kde.org/merkuro.mail/"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?format=guided&product=merkuro"
donations = ""
translations = ""
more_information = [ "https://apps.kde.org/merkuro.mail/",]
summary_source_url = "https://invent.kde.org/pim/merkuro/-/raw/master/src/mail/org.kde.merkuro.mail.metainfo.xml"
screenshots = [ "https://cdn.kde.org/screenshots/merkuro/mail.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.kde.merkuro.mail"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "merkuro",]
appstream_xml_url = "https://invent.kde.org/pim/merkuro/-/raw/master/src/mail/org.kde.merkuro.mail.metainfo.xml"
reported_by = "linmob"
updated_by = ""

+++

### Description

Merkuro Mail is a free and open source email client. Merkuro Mail is part of the Merkuro groupware suite and is currently in beta. For now, it only allows you to read your emails.

[Source](https://invent.kde.org/pim/merkuro/-/raw/master/src/mail/org.kde.merkuro.mail.metainfo.xml)

+++
title = "Feeel"
description = "A simple home workout/exercise app that respects your privacy"
aliases = []
date = 2023-01-07
updated = 2024-01-02

[taxonomies]
project_licenses = [ "AGPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Enjoying FOSS",]
categories = [ "fitness",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "Flutter",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "GTK", "Utility",]
programming_languages = [ "Dart",]
build_systems = [ "flutter",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/enjoyingfoss/feeel"
homepage = "https://gitlab.com/enjoyingfoss/feeel"
bugtracker = "https://gitlab.com/enjoyingfoss/feeel/-/issues"
donations = "https://liberapay.com/Feeel/donate"
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/enjoyingfoss/feeel/-/raw/master/flatpak/com.enjoyingfoss.Feeel.appdata.xml"
screenshots = [ "https://gitlab.com/enjoyingfoss/feeel/-/raw/master/flatpak/screenshots/1-intro.png", "https://gitlab.com/enjoyingfoss/feeel/-/raw/master/flatpak/screenshots/2-exercise.png", "https://gitlab.com/enjoyingfoss/feeel/-/raw/master/flatpak/screenshots/3-exercise-info.png", "https://gitlab.com/enjoyingfoss/feeel/-/raw/master/flatpak/screenshots/4-editor-empty.png", "https://gitlab.com/enjoyingfoss/feeel/-/raw/master/flatpak/screenshots/5-add-exercises.png", "https://gitlab.com/enjoyingfoss/feeel/-/raw/master/flatpak/screenshots/6-editor.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/com.enjoyingfoss.feeel/1.png", "https://img.linuxphoneapps.org/com.enjoyingfoss.feeel/2.png", "https://img.linuxphoneapps.org/com.enjoyingfoss.feeel/3.png", "https://img.linuxphoneapps.org/com.enjoyingfoss.feeel/4.png", "https://img.linuxphoneapps.org/com.enjoyingfoss.feeel/5.png", "https://img.linuxphoneapps.org/com.enjoyingfoss.feeel/6.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "com.enjoyingfoss.feeel"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.enjoyingfoss.feeel"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://gitlab.com/enjoyingfoss/feeel/-/raw/master/flatpak/com.enjoyingfoss.Feeel.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Feeel is an open-source app for doing simple at-home exercises. It holds the acclaimed full body scientific 7-minute workout regime and allows creating custom workouts as well. The app sources exercises from wger, a free and open-source online exercise wiki.


Features:


* Mobile first, runs on Linux and Android
* Private (fully offline, no tracking, no ads)
* Translated into several languages
* Free and open-source. Contribute at https://gitlab.com/enjoyingfoss/feeel/wikis.

[Source](https://gitlab.com/enjoyingfoss/feeel/-/raw/master/flatpak/com.enjoyingfoss.Feeel.appdata.xml)
+++
title = "Font Downloader"
description = "Install fonts from online sources"
aliases = []
date = 2020-10-26
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Gustavo Peredo",]
categories = [ "font downloader",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = [ "Google Fonts",]
packaged_in = [ "flathub", "opensuse_tumbleweed",]
freedesktop_categories = [ "GTK", "GNOME", "TextTools", "Dictionary",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/GustavoPeredo/font-downloader"
homepage = "https://github.com/GustavoPeredo/font-downloader"
bugtracker = "https://github.com/GustavoPeredo/Font-Downloader/issues"
donations = ""
translations = "https://poeditor.com/join/project?hash=hfnXv8Iw4o"
more_information = [ "https://apps.gnome.org/app/org.gustavoperedo.FontDownloader/",]
summary_source_url = "https://raw.githubusercontent.com/GustavoPeredo/Font-Downloader/master/data/org.gustavoperedo.FontDownloader.appdata.xml.in"
screenshots = [ "https://raw.githubusercontent.com/GustavoPeredo/font-downloader/master/data/screenshots/compact.png", "https://raw.githubusercontent.com/GustavoPeredo/font-downloader/master/data/screenshots/dark_compact.png", "https://raw.githubusercontent.com/GustavoPeredo/font-downloader/master/data/screenshots/dark_entire.png", "https://raw.githubusercontent.com/GustavoPeredo/font-downloader/master/data/screenshots/entire.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.gustavoperedo.FontDownloader"
scale_to_fit = "fontdownloader"
flathub = "https://flathub.org/apps/org.gustavoperedo.FontDownloader"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "font-downloader",]
appstream_xml_url = "https://raw.githubusercontent.com/GustavoPeredo/Font-Downloader/master/data/org.gustavoperedo.FontDownloader.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Have you ever wanted to change the font in your terminal, but didn't want to go through the entire process of searching, downloading and installing a font? This simple to use and adaptive GTK application allows you to search and install fonts directly from Google Fonts' website!

[Source](https://raw.githubusercontent.com/GustavoPeredo/Font-Downloader/master/data/org.gustavoperedo.FontDownloader.appdata.xml.in)

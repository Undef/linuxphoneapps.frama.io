+++
title = "Calculator"
description = "Perform arithmetic, scientific or financial calculations"
aliases = []
date = 2021-09-28
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The GNOME Project",]
categories = [ "calculator",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "Calculator",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnome-calculator"
homepage = "https://wiki.gnome.org/Apps/Calculator"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-calculator/issues"
donations = "https://www.gnome.org/donate/"
translations = "https://l10n.gnome.org/module/gnome-calculator/"
more_information = [ "https://apps.gnome.org/app/org.gnome.Calculator/",]
summary_source_url = "https://gitlab.gnome.org/GNOME/gnome-calculator/-/raw/master/data/org.gnome.Calculator.appdata.xml.in"
screenshots = [ "https://static.gnome.org/appdata/gnome-43/calculator/advanced-mode.png", "https://static.gnome.org/appdata/gnome-43/calculator/basic-mode.png", "https://static.gnome.org/appdata/gnome-43/calculator/financial-mode.png", "https://static.gnome.org/appdata/gnome-43/calculator/keyboard-mode.png", "https://static.gnome.org/appdata/gnome-43/calculator/programming-mode.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.Calculator"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Calculator"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-calculator",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-calculator/-/raw/master/data/org.gnome.Calculator.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Calculator is an application that solves mathematical equations.
 Though it at first appears to be a simple calculator with only basic
 arithmetic operations, you can switch into Advanced, Financial, or
 Programming mode to find a surprising set of capabilities.


The Advanced calculator supports many operations, including:
 logarithms, factorials, trigonometric and hyperbolic functions,
 modulus division, complex numbers, random number generation, prime
 factorization and unit conversions.


Financial mode supports several computations, including periodic interest
 rate, present and future value, double declining and straight line
 depreciation, and many others.


Programming mode supports conversion between common bases (binary, octal,
 decimal, and hexadecimal), boolean algebra, one’s and two’s complementation,
 character to character code conversion, and more.

[Source](https://gitlab.gnome.org/GNOME/gnome-calculator/-/raw/master/data/org.gnome.Calculator.appdata.xml.in)

### Notice

Adaptive upstream since release 41, GTK4/libadwaita since release 42.

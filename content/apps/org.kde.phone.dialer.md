+++
title = "Phone"
description = "Send and receive phone calls"
aliases = []
date = 2019-09-30
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community",]
categories = [ "telephony",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "QtQuick", "Kirigami",]
backends = [ "ofono",]
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "aur", "debian_12", "debian_13", "debian_unstable", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "Qt", "KDE", "Utility", "TelephonyTools",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/plasma-mobile/plasma-dialer"
homepage = ""
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=Plasma%20Mobile%20Dialer"
donations = "https://www.kde.org/community/donations/?app=plasmaphonedialer"
translations = ""
more_information = [ "https://phabricator.kde.org/T6935", "https://plasma-mobile.org/2022/11/30/plasma-mobile-gear-22-11/#dialer",]
summary_source_url = "https://invent.kde.org/plasma-mobile/plasma-dialer/-/raw/master/plasma-dialer/org.kde.phone.dialer.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/plasma-dialer/calling-voicemail.png", "https://cdn.kde.org/screenshots/plasma-dialer/plasma-dialer.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = false
app_id = "org.kde.phone.dialer"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "plasma-dialer",]
appstream_xml_url = "https://invent.kde.org/plasma-mobile/plasma-dialer/-/raw/master/plasma-dialer/org.kde.phone.dialer.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

The Phone app lets you easily send and receive phone calls from your smartphone.


Features:


* See your call history and easily call the same number again
* Manage your contacts that have a phone number set

[Source](https://invent.kde.org/plasma-mobile/plasma-dialer/-/raw/master/plasma-dialer/org.kde.phone.dialer.appdata.xml)

+++
title = "OTPClient"
description = "GTK+ application for managing TOTP and HOTP tokens with built-in encryption."
aliases = []
date = 2021-07-24
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC-BY-4.0",]
app_author = [ "Paolo Stivanin",]
categories = [ "multi-factor authentication",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK3",]
backends = [ "libcotp",]
services = []
packaged_in = [ "alpine_edge", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "flathub", "gnuguix", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "C",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/paolostivanin/OTPClient"
homepage = "https://github.com/paolostivanin/OTPClient"
bugtracker = "https://github.com/paolostivanin/OTPClient/issues"
donations = ""
translations = ""
more_information = [ "https://github.com/paolostivanin/OTPClient/wiki",]
summary_source_url = "https://github.com/paolostivanin/OTPClient"
screenshots = [ "https://raw.githubusercontent.com/paolostivanin/OTPClient/master/data/screenshots/addmenu.png", "https://raw.githubusercontent.com/paolostivanin/OTPClient/master/data/screenshots/emptymain.png", "https://raw.githubusercontent.com/paolostivanin/OTPClient/master/data/screenshots/hambmenu.png", "https://raw.githubusercontent.com/paolostivanin/OTPClient/master/data/screenshots/settings.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "com.github.paolostivanin.OTPClient"
scale_to_fit = "otpclient"
flathub = "https://flathub.org/apps/com.github.paolostivanin.OTPClient"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "otpclient",]
appstream_xml_url = "https://raw.githubusercontent.com/paolostivanin/OTPClient/master/data/com.github.paolostivanin.OTPClient.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Highly secure and easy to use OTP client written in C/GTK3 that supports both TOTP and HOTP and has the following features:


* integration with the OS' secret service provider via libsecret
* support both TOTP and HOTP
* support setting custom digits (between 4 and 10 inclusive)
* support setting a custom period (between 10 and 120 seconds inclusive)
* support SHA1, SHA256 and SHA512 algorithms
* support for Steam codes
* import and export encrypted/plain andOTP backup
* import and export encrypted/plain Aegis backup
* import and export plain FreeOTPPlus backup (key URI format only)
* import of Google's migration QR codes
* local database is encrypted using AES256-GCM (PBKDF2 with SHA512 and 100k iterations) and, while decrypted, it's stored in a secure memory area allocated by GCrypt.

[Source](https://raw.githubusercontent.com/paolostivanin/OTPClient/master/data/com.github.paolostivanin.OTPClient.appdata.xml)

### Notice

Despite being 'intended for mobile' according to its metadata, release 3.1.6 from Flathub is far too wide to work great on mobile, while the Alpine package of the same release fits just fine.

Supports importing from many other OTP apps.

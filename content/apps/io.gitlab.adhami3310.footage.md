+++
title = "Footage"
description = "Polish your videos"
aliases = []
date = 2023-06-25
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC-BY-SA-4.0",]
app_author = [ "Khaleel Al-Adhami",]
categories = [ "video editor",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "ffmpeg",]
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GNOME", "GTK", "System", "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/adhami3310/Footage"
homepage = "https://gitlab.com/adhami3310/Footage"
bugtracker = "https://gitlab.com/adhami3310/Footage/-/issues"
donations = ""
translations = ""
more_information = [ "https://gitlab.com/adhami3310/Footage/-/blob/main/PRESS.md",]
summary_source_url = "https://gitlab.com/adhami3310/Footage/-/raw/main/data/io.gitlab.adhami3310.Footage.metainfo.xml.in.in"
screenshots = [ "https://gitlab.com/adhami3310/Footage/-/raw/main/data/resources/screenshots/0.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/io.gitlab.adhami3310.footage/1.png", "https://img.linuxphoneapps.org/io.gitlab.adhami3310.footage/2.png", "https://img.linuxphoneapps.org/io.gitlab.adhami3310.footage/3.png", "https://img.linuxphoneapps.org/io.gitlab.adhami3310.footage/4.png",]
all_features_touch = true
intended_for_mobile = false
app_id = "io.gitlab.adhami3310.Footage"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.gitlab.adhami3310.Footage"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "footage",]
appstream_xml_url = "https://gitlab.com/adhami3310/Footage/-/raw/main/data/io.gitlab.adhami3310.Footage.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Trim, flip, rotate and crop individual clips. Footage is a useful tool for quickly editing short videos and screencasts. It's also capable of exporting any video into a format of your choice.

[Source](https://gitlab.com/adhami3310/Footage/-/raw/main/data/io.gitlab.adhami3310.Footage.metainfo.xml.in.in)

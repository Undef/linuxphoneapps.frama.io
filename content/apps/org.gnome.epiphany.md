+++
title = "Web"
description = "Browse the web"
aliases = []
date = 2019-02-01
updated = 2024-01-14

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The GNOME Project",]
categories = [ "web browser",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "webkit2gtk",]
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_experimental", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing", "snapcraft",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "WebBrowser",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/GNOME/epiphany"
homepage = "https://wiki.gnome.org/Apps/Web"
bugtracker = "https://gitlab.gnome.org/GNOME/epiphany/issues"
donations = "https://www.gnome.org/donate/"
translations = "https://wiki.gnome.org/TranslationProject"
more_information = [ "https://puri.sm/posts/end-of-year-librem-5-update/", "https://apps.gnome.org/app/org.gnome.Epiphany/",]
summary_source_url = "https://gitlab.gnome.org/GNOME/epiphany/-/raw/master/data/org.gnome.Epiphany.appdata.xml.in.in"
screenshots = [ "https://gitlab.gnome.org/GNOME/epiphany/raw/HEAD/data/screenshot.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/org.gnome.epiphany/1.png", "https://img.linuxphoneapps.org/org.gnome.epiphany/2.png", "https://img.linuxphoneapps.org/org.gnome.epiphany/3.png", "https://img.linuxphoneapps.org/org.gnome.epiphany/4.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.Epiphany"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Epiphany"
flatpak_link = ""
flatpak_recipe = "https://gitlab.gnome.org/GNOME/epiphany/-/raw/master/org.gnome.Epiphany.json"
snapcraft = "https://snapcraft.io/epiphany"
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "epiphany-browser",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/epiphany/-/raw/master/data/org.gnome.Epiphany.appdata.xml.in.in"
reported_by = "cahfofpai"
updated_by = "check_via_repology"

+++

### Description

The web browser for GNOME, featuring tight integration with the desktop
 and a simple and intuitive user interface that allows you to focus on your
 web pages. If you’re looking for a simple, clean, beautiful view of the
 web, this is the browser for you.


Web is often referred to by its code name, Epiphany.

[Source](https://gitlab.gnome.org/GNOME/epiphany/-/raw/master/data/org.gnome.Epiphany.appdata.xml.in.in)

### Notice

GTK3/libhandy before release 44.
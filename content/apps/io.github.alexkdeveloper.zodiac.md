+++
title = "Zodiac"
description = "A simple program for plotting horoscopes"
aliases = []
date = 2024-02-10

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Alex Kryuchkov",]
categories = [ "utilities",]
mobile_compatibility = [ "4",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Science",]
programming_languages = [ "Python",]
build_systems = [ "Meson",]
requires_internet = [ "requires always",]
tags = []

[extra]
repository = "https://github.com/alexkdeveloper/zodiac"
homepage = "http://github.com/alexkdeveloper/zodiac"
bugtracker = "http://github.com/alexkdeveloper/zodiac/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/io.github.alexkdeveloper.zodiac"
screenshots = [ "https://raw.githubusercontent.com/alexkdeveloper/zodiac/main/data/screenshot1.png", "https://raw.githubusercontent.com/alexkdeveloper/zodiac/main/data/screenshot2.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "io.github.alexkdeveloper.zodiac"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.alexkdeveloper.zodiac"
flatpak_link = "https://flathub.org/apps/io.github.alexkdeveloper.zodiac.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/alexkdeveloper/zodiac/main/io.github.alexkdeveloper.zodiac.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/alexkdeveloper/zodiac/main/data/io.github.alexkdeveloper.zodiac.appdata.xml.in"
reported_by = "linmob"
updated_by = ""

+++


### Description

The application generates horoscopes in svg and txt format. The user only needs to fill in the required fields. The application can make horoscopes for both one and two people.

[Source](https://raw.githubusercontent.com/alexkdeveloper/zodiac/main/data/io.github.alexkdeveloper.zodiac.appdata.xml.in)

### Notice

Just a few pixels too wide.

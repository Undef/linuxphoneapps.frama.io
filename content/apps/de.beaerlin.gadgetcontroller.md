+++
title = "GadgetController"
description = "Use your PinePhone as a USB gadget"
aliases = []
date = 2022-03-06
updated = 2022-12-19

[taxonomies]
project_licenses = [ "LGPL-3.0-only",]
metadata_licenses = []
app_author = [ "beaerlin",]
categories = [ "system utilities",]
mobile_compatibility = [ "5",]
status = [ "early", "pre-release", "inactive",]
frameworks = [ "GTK3", "libhandy",]
backends = [ "pydbus",]
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "none",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/Beaerlin/gadgetcontroller"
homepage = ""
bugtracker = "https://github.com/Beaerlin/gadgetcontroller/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "no quotation"
screenshots = [ "https://raw.githubusercontent.com/Beaerlin/gadgetcontroller/main/pictures/flash.jpg", "https://raw.githubusercontent.com/Beaerlin/gadgetcontroller/main/pictures/hid.jpg", "https://raw.githubusercontent.com/Beaerlin/gadgetcontroller/main/pictures/iso.jpg", "https://raw.githubusercontent.com/Beaerlin/gadgetcontroller/main/pictures/net.jpg", "https://raw.githubusercontent.com/Beaerlin/gadgetcontroller/main/pictures/start.jpg",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "de.beaerlin.GadgetController"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "linmob"

+++

### Description

A D-Bus service for creating USB widgets in ConfigFS. This project consists of 2 parts: 1. The GadgetController-Service: It takes an xml via D-Bus and creates USB gadgets accordingly, 2. The GadgetController UI: A UI for creating the config files and starting gadgets without the need of root rights. [Source](https://github.com/Beaerlin/gadgetcontroller)

### Notice
Repo contains a PKGBUILD script and an Arch package, it was necessary to reboot the PinePhone to get the app to work.
Inactive since 2022-03-05.

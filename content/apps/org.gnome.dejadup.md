+++
title = "Déjà Dup Backups"
description = "Protect yourself from data loss"
aliases = []
date = 2022-03-06
updated = 2024-01-04

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC-BY-SA-4.0",]
app_author = [ "Michael Terry",]
categories = [ "system utilities", "backup",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "duplicity",]
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Archiving", "GNOME", "GTK", "Utility", "X-GNOME-Utilities",]
programming_languages = [ "Vala", "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/World/deja-dup"
homepage = "https://apps.gnome.org/DejaDup/"
bugtracker = "https://gitlab.gnome.org/World/deja-dup/-/issues"
donations = "https://liberapay.com/DejaDup"
translations = "https://l10n.gnome.org/module/deja-dup/"
more_information = []
summary_source_url = "https://gitlab.gnome.org/World/deja-dup"
screenshots = [ "https://gitlab.gnome.org/World/deja-dup/raw/453b20263528e2a05ba9e8b36bacb8eebe62da70/data/screenshots/1-main.png", "https://gitlab.gnome.org/World/deja-dup/raw/453b20263528e2a05ba9e8b36bacb8eebe62da70/data/screenshots/3-preferences.png", "https://gitlab.gnome.org/World/deja-dup/raw/453b20263528e2a05ba9e8b36bacb8eebe62da70/data/screenshots/4-folders.png", "https://gitlab.gnome.org/World/deja-dup/raw/d2a845c2f8f2890b1050c5a5c0f3fd8b94090720/data/screenshots/2-restore.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.DejaDup"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.DejaDup"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "deja-dup",]
appstream_xml_url = "https://gitlab.gnome.org/World/deja-dup/-/raw/main/data/app.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Déjà Dup is a simple backup tool. It hides the complexity of backing up the Right Way (encrypted, off-site, and regular) and uses duplicity as the backend.


* Support for local, remote, or cloud backup locations such as Google Drive
* Securely encrypts and compresses your data
* Incrementally backs up, letting you restore from any particular backup
* Schedules regular backups
* Integrates well into your GNOME desktop


Déjà Dup focuses on ease of use and recovering from personal, accidental data loss. If you need a full system backup or an archival program, you may prefer other backup apps.

[Source](https://gitlab.gnome.org/World/deja-dup/-/raw/main/data/app.metainfo.xml.in)

### Notice

Mobile ready since release 43. For earlier GTK3 based releases, Purism had created a mobile friendly downstream: https://source.puri.sm/Librem5/deja-dup
+++
title = "Swatch"
description = "Color palette manager"
aliases = []
date = 2022-03-30
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Gabriele Musco",]
categories = [ "development",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/GabMus/swatch"
homepage = "https://swatch.gabmus.org"
bugtracker = "https://gitlab.gnome.org/gabmus/swatch/-/issues"
donations = "https://liberapay.com/gabmus/donate"
translations = "https://gitlab.gnome.org/gabmus/swatch/-/tree/master/po"
more_information = [ "https://gabmus.org/posts/swatch_a_color_palette_manager/",]
summary_source_url = "https://gitlab.gnome.org/GabMus/swatch/-/raw/master/data/org.gabmus.swatch.appdata.xml.in"
screenshots = [ "https://gitlab.gnome.org/GabMus/swatch/-/raw/website/static/screenshots/mainwindow.png", "https://gitlab.gnome.org/GabMus/swatch/-/raw/website/static/screenshots/scrot01.png", "https://gitlab.gnome.org/GabMus/swatch/-/raw/website/static/screenshots/scrot02.png", "https://gitlab.gnome.org/GabMus/swatch/-/raw/website/static/screenshots/scrot03.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.gabmus.swatch"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gabmus.swatch"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://gitlab.gnome.org/GabMus/swatch/-/raw/master/data/org.gabmus.swatch.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Color palette manager.


Create and manage various color palettes for different projects, name colors with significant names, and quickly copy them with a single button.


View your colors as an orderly list or as a grid, depending on your screen size.


Import and export palettes using the GIMP palette format for easier interoperability.

[Source](https://gitlab.gnome.org/GabMus/swatch/-/raw/master/data/org.gabmus.swatch.appdata.xml.in)
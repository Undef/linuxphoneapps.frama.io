+++
title = "Identity"
description = "Compare images and videos"
aliases = []
date = 2020-10-21
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Ivan Molodetskikh",]
categories = [ "image and video comparison",]
mobile_compatibility = [ "4",]
status = [ "maturing",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "GTK", "GNOME", "Video", "Player",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/YaLTeR/identity"
homepage = "https://gitlab.gnome.org/YaLTeR/identity"
bugtracker = "https://gitlab.gnome.org/YaLTeR/identity/issues"
donations = ""
translations = "https://l10n.gnome.org/module/identity/"
more_information = [ "https://apps.gnome.org/app/org.gnome.gitlab.YaLTeR.Identity/",]
summary_source_url = "https://gitlab.gnome.org/YaLTeR/identity"
screenshots = [ "https://gitlab.gnome.org/YaLTeR/identity/uploads/0876907d00c9737d5bb5376a82255baa/row.png", "https://gitlab.gnome.org/YaLTeR/identity/uploads/2c6d515c5a952d14829be99a1bada08c/tab.png", "https://gitlab.gnome.org/YaLTeR/identity/uploads/7c5a7d2191b259f7c846226f7e074826/column.png", "https://gitlab.gnome.org/YaLTeR/identity/uploads/fc69820af95cb95231936b185c4e061f/media-properties.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.gitlab.YaLTeR.Identity"
scale_to_fit = "identity"
flathub = "https://flathub.org/apps/org.gnome.gitlab.YaLTeR.Identity"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "identity",]
appstream_xml_url = "https://gitlab.gnome.org/YaLTeR/identity/-/raw/master/data/org.gnome.gitlab.YaLTeR.Identity.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

A program for comparing multiple versions of an image or video.

[Source](https://gitlab.gnome.org/YaLTeR/identity/-/raw/master/data/org.gnome.gitlab.YaLTeR.Identity.metainfo.xml.in.in)

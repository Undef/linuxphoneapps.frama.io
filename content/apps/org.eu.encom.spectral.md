+++
title = "Spectral"
description = "IM client for the Matrix protocol"
aliases = []
date = 2020-03-02
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Black Hat",]
categories = [ "chat",]
mobile_compatibility = [ "3",]
status = []
frameworks = [ "QtQuick",]
backends = [ "libQuotient",]
services = [ "Matrix",]
packaged_in = [ "aur", "debian_11", "debian_12", "devuan_4_0", "flathub", "pureos_landing",]
freedesktop_categories = [ "Internet", "Matrix",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/spectral-im/spectral"
homepage = "https://gitlab.com/spectral-im/spectral/"
bugtracker = "https://gitlab.com/spectral-im/spectral/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/spectral-im/spectral/-/raw/master/linux/org.eu.encom.spectral.appdata.xml"
screenshots = [ "https://gitlab.com/spectral-im/spectral/raw/master/screenshots/1.png", "https://gitlab.com/spectral-im/spectral/raw/master/screenshots/2.png", "https://gitlab.com/spectral-im/spectral/raw/master/screenshots/3.png", "https://gitlab.com/spectral-im/spectral/raw/master/screenshots/4.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.eu.encom.spectral"
scale_to_fit = "org.eu.encom.spectral"
flathub = "https://flathub.org/apps/org.eu.encom.spectral"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "spectral-matrix",]
appstream_xml_url = "https://gitlab.com/spectral-im/spectral/-/raw/master/linux/org.eu.encom.spectral.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

Spectral is a glossy, cross-platform client for Matrix, the decentralized communication protocol for instant messaging.

[Source](https://gitlab.com/spectral-im/spectral/-/raw/master/linux/org.eu.encom.spectral.appdata.xml)

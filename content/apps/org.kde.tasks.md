+++
title = "Tasks"
description = "Organize your tasks"
aliases = []
date = 2022-04-26
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Felipe Kinoshita", "KDE Community",]
categories = [ "productivity",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Qt", "KDE", "Utility",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/utilities/toad"
homepage = "https://apps.kde.org/tasks"
bugtracker = "https://invent.kde.org/utilities/toad/-/issues"
donations = ""
translations = ""
more_information = [ "http://web.archive.org/web/20220422210752/https://fhek.gitlab.io/en/tasks/", "http://web.archive.org/web/20220602083557/https://fhek.gitlab.io/en/my-week-in-kde-improvements-to-tasks/",]
summary_source_url = "https://invent.kde.org/utilities/toad/-/raw/master/org.kde.tasks.metainfo.xml"
screenshots = [ "https://cdn.kde.org/screenshots/tasks/empty-state.png", "https://cdn.kde.org/screenshots/tasks/tasks.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/org.kde.tasks/1.png", "https://img.linuxphoneapps.org/org.kde.tasks/2.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "org.kde.tasks"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://invent.kde.org/utilities/toad/-/raw/master/org.kde.tasks.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Tasks is a to-do application, you can edit tasks on the fly and it saves all your work automatically.

[Source](https://invent.kde.org/utilities/toad/-/raw/master/org.kde.tasks.metainfo.xml)
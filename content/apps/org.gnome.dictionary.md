+++
title = "Dictionary"
description = "Check word definitions and spellings in an online dictionary"
aliases = []
date = 2020-10-15
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GFDL-1.3", "GPL-2.0-or-later", "LGPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Emmanuele Bassi",]
categories = [ "dictionary",]
mobile_compatibility = [ "5",]
status = [ "archived",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "TextTools", "Dictionary",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/Archive/gnome-dictionary"
homepage = "https://wiki.gnome.org/Apps/Dictionary"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-dictionary/issues"
donations = "http://www.gnome.org/friends/"
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/Archive/gnome-dictionary"
screenshots = [ "https://gitlab.gnome.org/GNOME/gnome-dictionary/-/raw/master/data/appdata/gnome-dictionary-main.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.gnome.Dictionary"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-dictionary",]
appstream_xml_url = "https://gitlab.gnome.org/Archive/gnome-dictionary/-/raw/master/data/appdata/org.gnome.Dictionary.appdata.xml.in.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Dictionary is a simple dictionary application that looks up
 definitions of words online.
 Though it looks up English definitions by default, you can easily switch to
 Spanish or add other online dictionaries using the DICT protocol to suit your
 needs.

[Source](https://gitlab.gnome.org/Archive/gnome-dictionary/-/raw/master/data/appdata/org.gnome.Dictionary.appdata.xml.in.in)

### Notice

Archived in November 2022.

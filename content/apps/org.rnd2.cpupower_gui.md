+++
title = "CPU frequency settings"
description = "Adjust the frequency of the CPU"
aliases = [ "apps/org.rnd2.cpupower_gui.desktop/",]
date = 2022-03-13
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Evangelos Rigas",]
categories = [ "system utilities",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "aur", "debian_11", "debian_unstable", "devuan_4_0", "devuan_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "System",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/vagnum08/cpupower-gui"
homepage = "https://github.com/vagnum08/cpupower-gui"
bugtracker = "https://github.com/vagnum08/cpupower-gui/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/vagnum08/cpupower-gui/master/data/org.rnd2.cpupower_gui.appdata.xml.in"
screenshots = [ "https://raw.githubusercontent.com/vagnum08/cpupower-gui/media/screenshots/v1_prefs.png", "https://raw.githubusercontent.com/vagnum08/cpupower-gui/media/screenshots/v1_profiles.png", "https://raw.githubusercontent.com/vagnum08/cpupower-gui/media/screenshots/v1_settings.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.rnd2.cpupower_gui.desktop"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "cpupower-gui",]
appstream_xml_url = "https://raw.githubusercontent.com/vagnum08/cpupower-gui/master/data/org.rnd2.cpupower_gui.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

A graphical utility for changing the CPU frequencies and governor.


This utility can change the operating frequency of the CPU for each core separately.
 The program can detect offline cores and only present the available ones.
 Furthermore, if a core doesn't support frequency scaling is not shown.
 There is also an option to apply the same settings to all cores simultaneously.


To apply the settings, the user must be local and active, as well as in group sudo.
 Local and active users not in this group must authenticate themselves as admin in order to apply the settings.

[Source](https://raw.githubusercontent.com/vagnum08/cpupower-gui/master/data/org.rnd2.cpupower_gui.appdata.xml.in)

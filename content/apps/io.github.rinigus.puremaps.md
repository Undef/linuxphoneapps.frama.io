+++
title = "Pure Maps"
description = "Maps and navigation"
aliases = []
date = 2019-02-01
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC-BY-SA-3.0",]
app_author = [ "rinigus",]
categories = [ "maps and navigation",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "QtQuick", "Kirigami", "Silica",]
backends = []
services = [ "openstreetmap",]
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "aur", "flathub", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "Maps", "Qt", "Science",]
programming_languages = [ "QML", "Python", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/rinigus/pure-maps"
homepage = "https://github.com/rinigus/pure-maps"
bugtracker = "https://github.com/rinigus/pure-maps/issues"
donations = "https://rinigus.github.io/donate"
translations = "https://www.transifex.com/rinigus/pure-maps"
more_information = []
summary_source_url = "https://github.com/rinigus/pure-maps"
screenshots = [ "https://raw.githubusercontent.com/rinigus/pure-maps/master/screenshots/main.png", "https://raw.githubusercontent.com/rinigus/pure-maps/master/screenshots/menu.png", "https://raw.githubusercontent.com/rinigus/pure-maps/master/screenshots/navigation.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "io.github.rinigus.PureMaps"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.rinigus.PureMaps"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "pure-maps",]
appstream_xml_url = "https://raw.githubusercontent.com/rinigus/pure-maps/91e155d4ce580d0f0dd8bc08cbdf010925ddf4ac/packaging/pure-maps.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

Pure Maps is an application to display vector and raster maps, places, routes, and provide navigation instructions with a flexible selection of data and service providers.


Depending on the used service provider, Pure Maps can rely fully either on online services or, together with OSM Scout Server, provide fully offline maps

[Source](https://raw.githubusercontent.com/rinigus/pure-maps/91e155d4ce580d0f0dd8bc08cbdf010925ddf4ac/packaging/pure-maps.appdata.xml)

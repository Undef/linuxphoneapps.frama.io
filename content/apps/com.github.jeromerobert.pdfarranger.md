+++
title = "PDF Arranger"
description = "PDF Merging, Rearranging, Splitting, Rotating and Cropping"
aliases = []
date = 2020-10-15
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "pdfarranger",]
categories = [ "office",]
mobile_compatibility = [ "4",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Office",]
programming_languages = [ "Python",]
build_systems = [ "setup.py",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/pdfarranger/pdfarranger"
homepage = "https://github.com/pdfarranger/pdfarranger"
bugtracker = "https://github.com/pdfarranger/pdfarranger/issues"
donations = ""
translations = "https://github.com/pdfarranger/pdfarranger#for-translators"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/pdfarranger/pdfarranger/main/data/com.github.jeromerobert.pdfarranger.metainfo.xml"
screenshots = [ "https://github.com/pdfarranger/pdfarranger/raw/main/data/screenshot.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = false
app_id = "com.github.jeromerobert.pdfarranger"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.github.jeromerobert.pdfarranger"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "pdfarranger",]
appstream_xml_url = "https://raw.githubusercontent.com/pdfarranger/pdfarranger/main/data/com.github.jeromerobert.pdfarranger.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

PDF Arranger is a small application, which helps the user to merge or split pdf
 documents and rotate, crop and rearrange their pages using an interactive and
 intuitive graphical interface.


It is a frontend for pikepdf.

[Source](https://raw.githubusercontent.com/pdfarranger/pdfarranger/main/data/com.github.jeromerobert.pdfarranger.metainfo.xml)

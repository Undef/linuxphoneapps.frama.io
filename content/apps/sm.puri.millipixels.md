+++
title = "Millipixels"
description = "A gnome camera application for phones"
aliases = []
date = 2021-12-14
updated = 2023-09-30

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Purism",]
categories = [ "camera",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3",]
backends = [ "libcamera",]
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "aur", "nix_stable_23_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Graphics", "Photography",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://source.puri.sm/Librem5/millipixels"
homepage = "https://source.puri.sm/Librem5/megapixels/"
bugtracker = "https://source.puri.sm/Librem5/millipixels/-/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://source.puri.sm/Librem5/millipixels/-/raw/master/data/sm.puri.Millipixels.metainfo.xml"
screenshots = [ "http://brixitcdn.net/metainfo/megapixels.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "sm.puri.Millipixels"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "millipixels",]
appstream_xml_url = "https://source.puri.sm/Librem5/millipixels/-/raw/master/data/sm.puri.Millipixels.metainfo.xml"
reported_by = "linmob"
updated_by = "linmob"

+++


### Description
Millipixels is a fork of Megapixels for the Librem 5.
 Its goal is to use libcamera.
 Currently, it uses the v4l2 and media-request apis to set up camera pipelines.

[Source](https://source.puri.sm/Librem5/millipixels/-/raw/master/data/sm.puri.Millipixels.metainfo.xml)

### Notice

Megapixels (GTK3 branch) fork by Purism for the Librem 5. Uses libcamera.

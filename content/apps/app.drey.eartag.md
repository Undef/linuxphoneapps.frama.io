+++
title = "Ear Tag"
description = "Edit audio file tags"
aliases = []
date = 2022-09-08
updated = 2024-01-02

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "knuxify",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "aur", "debian_13", "debian_unstable", "devuan_unstable", "flathub", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "AudioVideo",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/World/eartag/"
homepage = "https://gitlab.gnome.org/World/eartag"
bugtracker = "https://gitlab.gnome.org/World/eartag/-/issues"
donations = ""
translations = "https://gitlab.gnome.org/World/eartag/-/tree/main/po"
more_information = []
summary_source_url = "https://gitlab.gnome.org/World/eartag/-/raw/main/data/app.drey.EarTag.metainfo.xml.in"
screenshots = [ "https://gitlab.gnome.org/World/eartag/-/raw/0.5.1/data/screenshot-advanced.png", "https://gitlab.gnome.org/World/eartag/-/raw/0.5.1/data/screenshot-empty.png", "https://gitlab.gnome.org/World/eartag/-/raw/0.5.1/data/screenshot-identify.png", "https://gitlab.gnome.org/World/eartag/-/raw/0.5.1/data/screenshot-mobile.png", "https://gitlab.gnome.org/World/eartag/-/raw/0.5.1/data/screenshot-rename.png", "https://gitlab.gnome.org/World/eartag/-/raw/0.5.1/data/screenshot-scaled.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "app.drey.EarTag"
scale_to_fit = ""
flathub = "https://flathub.org/apps/app.drey.EarTag"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "eartag",]
appstream_xml_url = "https://gitlab.gnome.org/World/eartag/-/raw/main/data/app.drey.EarTag.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Ear Tag is a simple audio file tag editor. It is primarily geared towards making quick edits or bulk-editing tracks in albums/EPs. Unlike other tagging programs, Ear Tag does not require the user to set up a music library folder. It can:


* Edit tags of MP3, WAV, M4A, FLAC, OGG and WMA files
* Modify metadata of multiple files at once
* Rename files using information from present tags
* Identify files using AcoustID


Network access is only used for the "Identify selected files" option.

[Source](https://gitlab.gnome.org/World/eartag/-/raw/main/data/app.drey.EarTag.metainfo.xml.in)

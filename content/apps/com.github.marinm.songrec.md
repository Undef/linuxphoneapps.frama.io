+++
title = "SongRec"
description = "An open-source, unofficial Shazam client for Linux, written in Rust."
aliases = []
date = 2022-06-06
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Marin",]
categories = [ "multimedia",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK3",]
backends = []
services = [ "shazam",]
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Audio",]
programming_languages = [ "Rust",]
build_systems = [ "cargo",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/marin-m/SongRec"
homepage = "https://github.com/marin-m/SongRec"
bugtracker = "https://github.com/marin-m/SongRec/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/com.github.marinm.songrec"
screenshots = [ "https://raw.githubusercontent.com/marin-m/SongRec/master/packaging/Screenshot.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/com.github.marinm.songrec/1.png", "https://img.linuxphoneapps.org/com.github.marinm.songrec/2.png",]
all_features_touch = false
intended_for_mobile = false
app_id = "com.github.marinm.songrec"
scale_to_fit = "songrec"
flathub = "https://flathub.org/apps/com.github.marinm.songrec"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "songrec",]
appstream_xml_url = "https://raw.githubusercontent.com/marin-m/SongRec/master/packaging/rootfs/usr/share/metainfo/com.github.marinm.songrec.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Features:


- Recognize audio from an arbitrary audio file.


- Recognize audio from the microphone.


- Usage from both GUI and command line (for the file recognition part).


- Provide an history of the recognized songs on the GUI, exportable to CSV.


- Continuous song detection from the microphone, with the ability to choose your input device.


- Ability to recognize songs from your speakers rather than your microphone (on compatible PulseAudio setups).


- Generate a lure from a song that, when played, will fool Shazam into thinking that it is the concerned song.

[Source](https://raw.githubusercontent.com/marin-m/SongRec/master/packaging/rootfs/usr/share/metainfo/com.github.marinm.songrec.metainfo.xml)

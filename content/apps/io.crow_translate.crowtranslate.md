+++
title = "Crow Translate"
description = "A simple and lightweight translator"
aliases = []
date = 2021-02-28
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "crow-translate",]
categories = [ "translation tool",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "QtWidgets",]
backends = []
services = [ "Google Translate", "bing translate",]
packaged_in = [ "aur", "debian_13", "debian_unstable", "devuan_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "Qt", "Utility",]
programming_languages = [ "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/crow-translate/crow-translate"
homepage = "https://crow-translate.github.io"
bugtracker = "https://github.com/crow-translate/crow-translate/issues"
donations = ""
translations = "https://crowdin.com/project/crow-translate"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/crow-translate/crow-translate/master/data/io.crow_translate.CrowTranslate.metainfo.xml"
screenshots = [ "https://raw.githubusercontent.com/crow-translate/crow-translate.github.io/master/static/screenshots/plasma/main.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "io.crow_translate.CrowTranslate"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "crow-translate",]
appstream_xml_url = "https://raw.githubusercontent.com/crow-translate/crow-translate/master/data/io.crow_translate.CrowTranslate.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Crow Translate is a simple and lightweight translator written in C++ / Qt
 that allows you to translate and speak text using Google, Yandex, Bing,
 LibreTranslate and Lingva translate API.


Features:


* Translate and speak text from screen or selection
* Support 125 different languages
* Low memory consumption (~20MB)
* Highly customizable shortcuts
* Command-line interface with rich options
* D-Bus API
* Available for Linux and Windows

[Source](https://raw.githubusercontent.com/crow-translate/crow-translate/master/data/io.crow_translate.CrowTranslate.metainfo.xml)

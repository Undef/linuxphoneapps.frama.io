+++
title = "Satellite"
description = "Check your GPS reception and save your tracks"
aliases = [ "apps/org.codeberg.tpikonen.satellite/",]
date = 2021-08-02
updated = 2023-12-31

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "tpikonen",]
categories = [ "geography",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK3", "libhandy",]
backends = [ "ModemManager", "gnss-share",]
services = []
packaged_in = [ "aur", "debian_12", "debian_13", "debian_unstable", "devuan_unstable", "flathub", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "Geoscience", "Monitor",]
programming_languages = [ "Python",]
build_systems = [ "setup.py",]
requires_internet = []
tags = []

[extra]
repository = "https://codeberg.org/tpikonen/satellite"
homepage = "https://codeberg.org/tpikonen/satellite"
bugtracker = "https://codeberg.org/tpikonen/satellite/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://codeberg.org/tpikonen/satellite"
screenshots = [ "https://github.com/flathub/page.codeberg.tpikonen.satellite/raw/master/screenshot-fix.png", "https://github.com/flathub/page.codeberg.tpikonen.satellite/raw/master/screenshot-log.png", "https://github.com/flathub/page.codeberg.tpikonen.satellite/raw/master/screenshot-snr.png", "https://github.com/flathub/page.codeberg.tpikonen.satellite/raw/master/screenshot-track.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = false
app_id = "page.codeberg.tpikonen.satellite"
scale_to_fit = ""
flathub = "https://flathub.org/apps/page.codeberg.tpikonen.satellite"
flatpak_link = ""
flatpak_recipe = "https://codeberg.org/tpikonen/satellite/raw/branch/main/flatpak/page.codeberg.tpikonen.satellite.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "satellite-gtk",]
appstream_xml_url = "https://codeberg.org/tpikonen/satellite/raw/branch/main/data/appdata.xml"
reported_by = "tpikonen"
updated_by = "script"

+++


### Description
Satellite displays global navigation satellite system (GNSS: that's GPS,
 Galileo, Glonass etc.) data obtained from an NMEA source in your device.
 Currently the ModemManager and gnss-share APIs are supported. You can use
 it to check the navigation satellite signal strength and see your speed,
 coordinates and other parameters once a fix is obtained. It can also save
 GPX-tracks of your travels.

[Source](https://codeberg.org/tpikonen/satellite/raw/branch/main/data/appdata.xml)

+++
title = "Goguma"
description = "An IRC client for mobile devices."
aliases = []
date = 2022-06-30
updated = 2023-09-30

[taxonomies]
project_licenses = [ "AGPL-3.0-only",]
metadata_licenses = []
app_author = [ "goguma",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "Flutter",]
backends = []
services = [ "IRC",]
packaged_in = []
freedesktop_categories = [ "Network", "Chat",]
programming_languages = [ "Dart",]
build_systems = [ "flutter",]
requires_internet = []
tags = []

[extra]
repository = "https://sr.ht/~emersion/goguma/"
homepage = ""
bugtracker = "https://todo.sr.ht/~emersion/goguma"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://sr.ht/~emersion/goguma/"
screenshots = [ "https://cdn.fosstodon.org/media_attachments/files/108/520/770/426/569/289/original/28373dce62d64710.jpeg", "https://l.sr.ht/5NNh.png", "https://l.sr.ht/7tDh.png", "https://l.sr.ht/VoM9.png", "https://l.sr.ht/ah3N.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "linmob"

+++

### Description

Goals: 
- Modern: support for many IRCv3 extensions, plus some special support for IRC bouncers 
- Easy to use: offer a simple, straightforward interface 
- Offline-first: users should be able to read past conversations while offline, and network disruptions should be handled transparently 
- Lightweight: go easy on resource usage to run smoothly on older phones and save battery power. 
- Cross-platform: the main target platforms are Linux and Android. 

[Source](https://sr.ht/~emersion/goguma/)

### Notice
As of September 2023, you will have to compile this yourself. If you have set up flutter correctly (make sure to read and follow all the setup instructions), the instructions for compiling for the Linux platform work fine.

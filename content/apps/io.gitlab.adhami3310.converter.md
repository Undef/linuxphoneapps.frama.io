+++
title = "Switcheroo"
description = "Convert and manipulate images"
aliases = []
date = 2023-04-16
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC-BY-SA-4.0",]
app_author = [ "Khaleel Al-Adhami",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "ImageMagick",]
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GNOME", "GTK", "Graphics", "ImageProcessing", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/adhami3310/Switcheroo"
homepage = "https://gitlab.com/adhami3310/Switcheroo"
bugtracker = "https://gitlab.com/adhami3310/Switcheroo/-/issues"
donations = ""
translations = "https://gitlab.com/adhami3310/Switcheroo/-/tree/main/po"
more_information = []
summary_source_url = "https://gitlab.com/adhami3310/Switcheroo/-/raw/main/data/io.gitlab.adhami3310.Switcheroo.metainfo.xml.in"
screenshots = [ "https://gitlab.com/adhami3310/Switcheroo/-/raw/main/data/resources/screenshots/0.png", "https://gitlab.com/adhami3310/Switcheroo/-/raw/main/data/resources/screenshots/1.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/io.gitlab.adhami3310.converter/1.png", "https://img.linuxphoneapps.org/io.gitlab.adhami3310.converter/2.png", "https://img.linuxphoneapps.org/io.gitlab.adhami3310.converter/3.png", "https://img.linuxphoneapps.org/io.gitlab.adhami3310.converter/4.png",]
all_features_touch = true
intended_for_mobile = false
app_id = "io.gitlab.adhami3310.Converter"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.gitlab.adhami3310.Converter"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "converter",]
appstream_xml_url = "https://gitlab.com/adhami3310/Switcheroo/-/raw/main/data/io.gitlab.adhami3310.Converter.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Convert between different image filetypes and resize them easily.

[Source](https://gitlab.com/adhami3310/Switcheroo/-/raw/main/data/io.gitlab.adhami3310.Switcheroo.metainfo.xml.in.in)

### Notice

Previously named Converter (until release 2.0).

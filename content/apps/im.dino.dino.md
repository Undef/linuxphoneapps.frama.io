+++
title = "Dino"
description = "Modern XMPP Chat Client"
aliases = []
date = 2021-01-19
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Dino Development Team",]
categories = [ "chat",]
mobile_compatibility = [ "4",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "XMPP",]
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_experimental", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Chat", "InstantMessaging",]
programming_languages = [ "Vala",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/dino/dino/"
homepage = "https://dino.im"
bugtracker = "https://github.com/dino/dino/issues"
donations = "https://dino.im/#donate"
translations = "https://hosted.weblate.org/projects/dino/"
more_information = [ "https://wiki.postmarketos.org/wiki/Dino", "https://github.com/dino/dino/issues/178", "https://web.archive.org/web/20231001011503/https://wiki.mobian.org/doku.php?id=dino", "https://github.com/dino/dino/wiki/Distribution-Packages",]
summary_source_url = "https://github.com/dino/dino"
screenshots = [ "https://dino.im/img/appdata/2022-02_screenshot-call.png", "https://dino.im/img/appdata/2022-02_screenshot-main.png", "https://dino.im/img/appdata/start_chat.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "im.dino.Dino"
scale_to_fit = "dino"
flathub = "https://flathub.org/apps/im.dino.Dino"
flatpak_link = ""
flatpak_recipe = "https://raw.githubusercontent.com/flathub/im.dino.Dino/master/im.dino.Dino.yml"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "dino-im",]
appstream_xml_url = "https://raw.githubusercontent.com/dino/dino/master/main/data/im.dino.Dino.appdata.xml.in"
reported_by = "esu23"
updated_by = "script"

+++


### Description

Dino is a modern open-source chat client for the desktop. It focuses on providing a clean and reliable Jabber/XMPP experience while having your privacy in mind.


It supports end-to-end encryption with OMEMO and OpenPGP and allows configuring privacy-related features such as read receipts and typing notifications.


Dino fetches history from the server and synchronizes messages with other devices.

[Source](https://raw.githubusercontent.com/dino/dino/master/main/data/im.dino.Dino.appdata.xml.in)

### Notice

GTK4/libadwaita since release 0.4. The main window adapts perfectly, but account and conversation settings do not (can be fixed on Phosh with `scale-to-fit dino on`), which can make [starting conversations difficult](https://github.com/dino/dino/issues/1371), and the [emoji picker does not work well yet](https://github.com/dino/dino/issues/1360). For earlier releases, the [feature/handy branch](https://github.com/dino/dino/tree/feature/handy) had good mobile compatibility.

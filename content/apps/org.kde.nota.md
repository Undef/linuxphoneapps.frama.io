+++
title = "Nota"
description = "Browse, create and edit text files"
aliases = []
date = 2019-02-01
updated = 2024-01-02

[taxonomies]
project_licenses = [ "LGPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "maui",]
categories = [ "text editor",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "MauiKit", "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "Qt", "KDE", "Utility", "TextEditor",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/maui/nota"
homepage = "https://mauikit.org/"
bugtracker = "https://invent.kde.org/maui/nota/-/issues"
donations = "https://kde.org/community/donations"
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/maui/nota/-/raw/master/org.kde.nota.metainfo.xml"
screenshots = [ "https://cdn.kde.org/screenshots/nota/nota-stable.png", "https://cdn.kde.org/screenshots/nota/nota-tabs-find-dialog.png", "https://cdn.kde.org/screenshots/nota/nota.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = false
app_id = "org.kde.nota"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "maui-nota",]
appstream_xml_url = "https://invent.kde.org/maui/nota/-/raw/master/org.kde.nota.metainfo.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

Nota is a simple text editor for desktop and mobile computers.

[Source](https://invent.kde.org/maui/nota/-/raw/master/org.kde.nota.metainfo.xml)

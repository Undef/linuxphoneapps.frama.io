+++
title = "Jupii"
description = "Play audio, video and images on UPnP/DLNA devices"
aliases = []
date = 2024-02-08

[taxonomies]
project_licenses = [ "MPL-2.0",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Michal Kosciesza",]
categories = [ "multimedia",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "Kirigami", "QtQuick",]
backends = [ "QHTTPServer", "npupnp", "Libupnpp", "TagLib", "FFmpeg", "Lame", "x264", "Gumbo", "yt-dlp", "ytmusicapi", "EasyEXIF",]
services = [ "UPnP", "DLNA",]
packaged_in = [ "flathub",]
freedesktop_categories = [ "AudioVideo",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "CMake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/mkiol/Jupii"
homepage = "https://github.com/mkiol/Jupii"
bugtracker = "https://github.com/mkiol/Jupii/issues"
donations = "https://github.com/mkiol/Jupii#how-to-support"
translations = "https://app.transifex.com/mkiol/jupii"
more_information = [ "https://openrepos.net/content/mkiol/jupii",]
summary_source_url = "https://flathub.org/apps/net.mkiol.Jupii"
screenshots = [ "https://raw.githubusercontent.com/mkiol/Jupii/master/desktop/screenshots/jupii-screenshot-dark.png", "https://raw.githubusercontent.com/mkiol/Jupii/master/desktop/screenshots/jupii-screenshot-light.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "net.mkiol.Jupii"
scale_to_fit = ""
flathub = "https://flathub.org/apps/net.mkiol.Jupii"
flatpak_link = "https://flathub.org/apps/net.mkiol.Jupii.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/mkiol/Jupii/master/flatpak/net.mkiol.Jupii.yaml"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/mkiol/Jupii/master/desktop/jupii.metainfo.xml.in"
reported_by = "linmob"
updated_by = ""

+++


### Description

Jupii let you play audio, video and image files on any device on your local network that supports UPnP/DLNA, such as smart speaker, smart TVs, gaming consoles, and more.


In addition to the typical features you might expect from this type of application, Jupii also has some unique functionalities such as:


* support many different internet services as media source (Bandcamp, SoundCloud, YouTube and many more)
* live casting of video/audio from camera or microphone
* screen mirroring
* recorder that let you to extract music from Internet radio streams


This app can be used in two different UPnP/DLNA modes:


* Playback control mode: Using Jupii, you connect to the player device (e.g. smart speaker) and transfer media from your phone/computer to this device.
* Media server mode: Using your playback device (e.g. smart TV), you browse and play media files shared by Jupii

[Source](https://raw.githubusercontent.com/mkiol/Jupii/master/desktop/jupii.metainfo.xml.in)

+++
title = "Tootle"
description = "Lightning fast client for Mastodon"
aliases = []
date = 2020-08-25
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "bleak_grey",]
categories = [ "social media",]
mobile_compatibility = [ "5",]
status = [ "archived",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = [ "Mastodon",]
packaged_in = [ "aur", "debian_11", "devuan_4_0", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Feed",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/bleakgrey/tootle"
homepage = "https://github.com/bleakgrey"
bugtracker = "https://github.com/bleakgrey/tootle/issues"
donations = "https://liberapay.com/bleakgrey/donate"
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/bleakgrey/tootle/master/data/com.github.bleakgrey.tootle.appdata.xml.in"
screenshots = [ "https://raw.githubusercontent.com/bleakgrey/tootle/master/data/screenshot1.png", "https://raw.githubusercontent.com/bleakgrey/tootle/master/data/screenshot2.png", "https://raw.githubusercontent.com/bleakgrey/tootle/master/data/screenshot3.png", "https://raw.githubusercontent.com/bleakgrey/tootle/master/data/screenshot4.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "com.github.bleakgrey.tootle"
scale_to_fit = "com.github.bleakgrey.tootle"
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://raw.githubusercontent.com/bleakgrey/tootle/master/com.github.bleakgrey.tootle.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "tootle",]
appstream_xml_url = "https://raw.githubusercontent.com/bleakgrey/tootle/master/data/com.github.bleakgrey.tootle.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Tootle is a GTK client for the world's largest, decentralized, microblogging platform, Mastodon. With the user experience in mind, it provides a clean, native interface that allows you to integrate Mastodon's social experience seamlessly with your desktop environment.


Mastodon is a free and open source microblogging platform similar to Twitter, but with user privacy and decentralization in mind. It is one of many protocols that interacts with the Fediverse of protocols like Pleroma, GNU Social, and others. The power of federated microblogging that Mastodon brings and the agility of Tootle creates a fantastic duo for all of your social media needs.

[Source](https://raw.githubusercontent.com/bleakgrey/tootle/master/data/com.github.bleakgrey.tootle.appdata.xml.in)

### Notice

Archived. Version 2.0 was going to be GTK4 and libadwaita, its development has been continued as [Tuba](https://linuxphoneapps.org/apps/dev.geopjr.tuba/).

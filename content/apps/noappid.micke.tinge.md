+++
title = "Tinge"
description = "Tinge is a mobile first application for controlling Philips Hue lights on Linux"
aliases = []
date = 2021-05-06
updated = 2023-09-30

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "micke",]
categories = [ "smart home",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "WxWidgets",]
backends = []
services = [ "Hue",]
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "custom",]
requires_internet = []
tags = []

[extra]
repository = "https://code.smolnet.org/micke/tinge"
homepage = ""
bugtracker = "https://code.smolnet.org/micke/tinge/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://code.smolnet.org/micke/tinge"
screenshots = [ "https://code.smolnet.org/micke/tinge/raw/branch/master/screenshots/scrot1.png", "https://code.smolnet.org/micke/tinge/raw/branch/master/screenshots/scrot2.png", "https://code.smolnet.org/micke/tinge/raw/branch/master/screenshots/scrot3.png", "https://code.smolnet.org/micke/tinge/raw/branch/master/screenshots/scrot4.png", "https://code.smolnet.org/micke/tinge/raw/branch/master/screenshots/scrot5.png", "https://code.smolnet.org/micke/tinge/raw/branch/master/screenshots/scrot6.png", "https://code.smolnet.org/micke/tinge/raw/branch/master/screenshots/scrot7.png", "https://code.smolnet.org/micke/tinge/raw/branch/master/screenshots/scrot8.png", "https://code.smolnet.org/micke/tinge/raw/branch/master/screenshots/scrot9.png", "https://code.smolnet.org/micke/tinge/raw/branch/master/screenshots/scrot10.png", "https://code.smolnet.org/micke/tinge/raw/branch/master/screenshots/scrot11.png", "https://code.smolnet.org/micke/tinge/raw/branch/master/screenshots/scrot12.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "linmob"

+++

### Notice
Could not test whether this actually works with Hue lights because I don't have any.

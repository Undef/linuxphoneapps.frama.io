+++
title = "Subsurface"
description = "Manage and display dive computer data"
aliases = [ "apps/subsurface/",]
date = 2019-02-01
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-2.0-only",]
metadata_licenses = [ "CC-BY-SA-3.0",]
app_author = [ "The Subsurface development team",]
categories = [ "sports",]
mobile_compatibility = [ "needs testing",]
status = [ "mature",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "Qt", "Education", "Geography",]
programming_languages = [ "Cpp", "C",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/subsurface/subsurface"
homepage = "https://subsurface-divelog.org"
bugtracker = "https://github.com/Subsurface/subsurface/issues"
donations = ""
translations = "https://www.transifex.com/subsurface/subsurface/"
more_information = []
summary_source_url = "https://subsurface-divelog.org/"
screenshots = [ "https://raw.githubusercontent.com/Subsurface/subsurface/master/appdata/diveplanner.png", "https://raw.githubusercontent.com/Subsurface/subsurface/master/appdata/main.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.subsurface_divelog.subsurface"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.subsurface_divelog.Subsurface"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "subsurface",]
appstream_xml_url = "https://raw.githubusercontent.com/subsurface/subsurface/master/appdata/subsurface.appdata.xml.in"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

Subsurface is an opensource diving logbook that runs on Linux, Windows and Mac.
 With this program, users can keep track of their dives by logging dive locations (with GPS coordinates),
 weights and exposure protection used, divemasters and dive buddies, etc.
 Subsurface also enables the users to rate their dives and provide additional dive notes.


Dives can be downloaded from a variety of dive computers, inserted manually or imported from other programs.
 A wide array of diving statistics and information is calculated and displayed, like the user’s SAC rate,
 partial pressures of O2, N2 and He, calculated deco information, and many more.


The dive profiles (and the tank pressure curves) can be visualized in a comprehensive and clean way, that
 provides the user with additional information on relative velocity (and momentary air consumption) during the dive.
 Subsurface also allows the user to print out a detailed log book that includes dive profiles and other relevant information.
 The program is localized in about 20 languages and well supported by an active developer community.

[Source](https://raw.githubusercontent.com/subsurface/subsurface/master/appdata/subsurface.appdata.xml.in)

### Notice

Packaging on AUR and Flathub contains the QWidget-based desktop app, that still somewhat fits the screen and does somewhat work. The mobile, Kirigami-based interface (created for the Android app) needs to be build manually and needs evaluation on Mobile Linux.

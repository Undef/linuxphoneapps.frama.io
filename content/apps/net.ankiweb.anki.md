+++
title = "Anki"
description = "Powerful, intelligent flash cards"
aliases = []
date = 2020-12-19
updated = 2024-01-02

[taxonomies]
project_licenses = [ "AGPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Damien Elmes",]
categories = [ "education",]
mobile_compatibility = [ "4",]
status = [ "mature",]
frameworks = [ "QtWidgets",]
backends = []
services = []
packaged_in = [ "alpine_edge", "aur", "debian_11", "debian_unstable", "devuan_4_0", "devuan_unstable", "flathub", "gentoo", "gnuguix", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "Qt", "Education",]
programming_languages = [ "Python", "Rust", "TypeScript",]
build_systems = [ "custom",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/ankitects/anki"
homepage = "https://apps.ankiweb.net/"
bugtracker = "https://forums.ankiweb.net/"
donations = "https://apps.ankiweb.net/support/"
translations = "https://translating.ankiweb.net/"
more_information = [ "https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/268",]
summary_source_url = "https://raw.githubusercontent.com/flathub/net.ankiweb.Anki/master/net.ankiweb.Anki.appdata.xml"
screenshots = [ "https://iili.io/HU8RpwX.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/net.ankiweb.anki/1.png", "https://img.linuxphoneapps.org/net.ankiweb.anki/2.png", "https://img.linuxphoneapps.org/net.ankiweb.anki/3.png", "https://img.linuxphoneapps.org/net.ankiweb.anki/4.png",]
all_features_touch = false
intended_for_mobile = false
app_id = "net.ankiweb.Anki"
scale_to_fit = "anki"
flathub = "https://flathub.org/apps/net.ankiweb.Anki"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "anki",]
appstream_xml_url = "https://raw.githubusercontent.com/flathub/net.ankiweb.Anki/master/net.ankiweb.Anki.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Anki is a program which makes remembering things easy. Because it's a lot more efficient than traditional study methods, you can either greatly decrease your time spent studying, or greatly increase the amount you learn.


Anyone who needs to remember things in their daily life can benefit from Anki. Since it is content-agnostic and supports images, audio, videos and scientific markup (via LaTeX), the possibilities are endless.


Optional LaTeX support is provided by the TeX Live Flatpak extension org.freedesktop.Sdk.Extension.texlive.

[Source](https://raw.githubusercontent.com/flathub/net.ankiweb.Anki/master/net.ankiweb.Anki.appdata.xml)

### Notice

Because of added complexity (requiring a number of language bindings and build systems), distribution packaging of Anki on ARM/aarch64 may be outdated, even on Flathub.

+++
title = "Portfolio"
description = "Manage files on the go"
aliases = []
date = 2020-12-22
updated = 2023-12-31

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Martin Abente Lahaye",]
categories = [ "file management",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "aur", "debian_12", "debian_13", "debian_unstable", "devuan_unstable", "flathub", "gnuguix", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "System", "FileManager",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "editors choice",]

[extra]
repository = "https://github.com/tchx84/Portfolio"
homepage = "https://github.com/tchx84/Portfolio"
bugtracker = "https://github.com/tchx84/Portfolio/issues"
donations = ""
translations = ""
more_information = [ "https://blogs.gnome.org/tchx84/2020/12/21/portfolio-manage-files-in-your-phone/", "https://blogs.gnome.org/tchx84/2023/08/29/portfolio-1-0-0/",]
summary_source_url = "https://github.com/tchx84/Portfolio"
screenshots = [ "https://raw.githubusercontent.com/tchx84/Portfolio/master/screenshots/en/1.png", "https://raw.githubusercontent.com/tchx84/Portfolio/master/screenshots/en/2.png", "https://raw.githubusercontent.com/tchx84/Portfolio/master/screenshots/en/3.png", "https://raw.githubusercontent.com/tchx84/Portfolio/master/screenshots/en/4.png", "https://raw.githubusercontent.com/tchx84/Portfolio/master/screenshots/en/5.png", "https://raw.githubusercontent.com/tchx84/Portfolio/master/screenshots/en/6.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "dev.tchx84.Portfolio"
scale_to_fit = ""
flathub = "https://flathub.org/apps/dev.tchx84.Portfolio"
flatpak_link = ""
flatpak_recipe = "https://raw.githubusercontent.com/tchx84/Portfolio/master/dev.tchx84.Portfolio.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "portfolio-file-manager",]
appstream_xml_url = "https://raw.githubusercontent.com/tchx84/Portfolio/master/data/dev.tchx84.Portfolio.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description
A minimalist file manager for those who want to use Linux mobile devices.

[Source](https://raw.githubusercontent.com/tchx84/Portfolio/master/data/dev.tchx84.Portfolio.metainfo.xml.in)

### Notice
Used GTK3/libhandy before its 1.0 release.

+++
title = "Alligator"
description = "Feed reader for mobile devices"
aliases = []
date = 2020-08-25
updated = 2023-12-31

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community",]
categories = [ "feed reader",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "Qt", "KDE", "Network", "Feed", "News",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/network/alligator"
homepage = "https://apps.kde.org/alligator"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=Alligator"
donations = "https://kde.org/community/donations"
translations = ""
more_information = [ "https://plasma-mobile.org/2021/04/27/plasma-mobile-update-march-april/",]
summary_source_url = "https://invent.kde.org/network/alligator/-/raw/master/org.kde.alligator.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/alligator/alligator-desktop.png", "https://cdn.kde.org/screenshots/alligator/alligator-mobile.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.alligator"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.alligator"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "alligator", "kde5-alligator",]
appstream_xml_url = "https://invent.kde.org/network/alligator/-/raw/master/org.kde.alligator.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description
Alligator is a mobile feed reader

[Source](https://invent.kde.org/network/alligator/-/raw/master/org.kde.alligator.appdata.xml)

+++
title = "Lith"
description = "WeeChat Relay Client"
aliases = []
date = 2021-04-18
updated = 2023-12-31

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "lithapp",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "QtQuick",]
backends = [ "WeeChat",]
services = [ "IRC",]
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "aur", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "IRCClient",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "qmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/LithApp/Lith"
homepage = "https://lith.app"
bugtracker = "https://github.com/LithApp/Lith/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/LithApp/Lith/master/dist/linux/app.lith.Lith.appdata.xml"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "app.lith.Lith"
scale_to_fit = ""
flathub = "https://flathub.org/apps/app.lith.Lith"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "lith",]
appstream_xml_url = "https://raw.githubusercontent.com/LithApp/Lith/master/dist/linux/app.lith.Lith.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++

### Description
With Lith, you can connect to your WeeChat instance from any device.

[Source](https://raw.githubusercontent.com/LithApp/Lith/master/dist/linux/app.lith.Lith.appdata.xml)

### Notice
WeeChat client – you will need to set up and configure WeeChat to use this.

+++
title = "NeoChat"
description = "Chat with your friends on matrix"
aliases = []
date = 2020-11-03
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The KDE Community",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "Kirigami", "QtQuick",]
backends = []
services = [ "Matrix",]
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_12", "debian_13", "debian_unstable", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Network",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/network/neochat"
homepage = "https://apps.kde.org/neochat"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=NeoChat"
donations = ""
translations = ""
more_information = [ "https://plasma-mobile.org/2022/11/30/plasma-mobile-gear-22-11/#neochat",]
summary_source_url = "https://invent.kde.org/network/neochat/-/raw/master/org.kde.neochat.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/neochat/NeoChat-Windows-Login.png", "https://cdn.kde.org/screenshots/neochat/NeoChat-Windows-Timeline.png", "https://cdn.kde.org/screenshots/neochat/application-mobile.png", "https://cdn.kde.org/screenshots/neochat/application.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.neochat"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "neochat",]
appstream_xml_url = "https://invent.kde.org/network/neochat/-/raw/master/org.kde.neochat.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

NeoChat is a client for Matrix, the decentralized communication protocol for instant messaging. It allows you to send text messages, videos and audio files to your family, colleagues and friends. It uses KDE frameworks and most notably Kirigami
to provide a convergent experience across multiple platforms.


NeoChat aims to be a fully featured application for the Matrix specification. As such everything in the current stable specification with the notable exceptions of VoIP, threads and some aspects of End-to-End Encryption are supported. There are a few other smaller omissions due to the fact that the Matrix spec is constantly evolving but the aim remains to provide eventual support for the entire spec.


Due to the nature of the Matrix specification development NeoChat also supports numerous unstable features. Currently these are:


* Polls - MSC3381
* Sticker Packs - MSC2545
* Location Events - MSC3488

[Source](https://invent.kde.org/network/neochat/-/raw/master/org.kde.neochat.appdata.xml)

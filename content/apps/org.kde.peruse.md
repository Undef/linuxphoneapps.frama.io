+++
title = "Peruse"
description = "Comic Book Reader"
aliases = []
date = 2019-02-01
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community",]
categories = [ "document viewer",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "pureos_landing", "snapcraft",]
freedesktop_categories = [ "Qt", "KDE", "Graphics", "Viewer",]
programming_languages = [ "Cpp", "QML", "C",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/graphics/peruse"
homepage = "https://peruse.kde.org/"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?format=guided&product=peruse"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/graphics/peruse/-/raw/master/src/app/org.kde.peruse.appdata.xml"
screenshots = [ "https://peruse.kde.org/screenshots/peruse-book-details.jpg", "https://peruse.kde.org/screenshots/peruse-reading-sidebar.jpg", "https://peruse.kde.org/screenshots/peruse-welcome.jpg",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.peruse.desktop"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.peruse"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = "https://snapcraft.io/peruse"
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "peruse",]
appstream_xml_url = "https://invent.kde.org/graphics/peruse/-/raw/master/src/app/org.kde.peruse.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

There are many ways to read comic books, and one of those that has become more
 common in recent years is on a computer. Peruse was created as a way to make
 exactly that as easy and pleasant as possible, and to simply get out of the
 way and let you read your comic books. One could say that it allows you to
 peruse your comic books at your leisure, but that would be a silly way of
 putting it - so, peruse your comic books at your leisure!


One of the ways in which Peruse ensures your reading experience is as pleasant
 and effortless as possible is to allow you to simply pick up where you last left
 off. The welcome page in the application (first screenshot below) shows you your
 most recently read comics, and shows your progress through them. To pick up
 where you left off, simply tap the top left item and you will be taken right to
 the page you were reading before you had to stop.


Features:


* Comic Book Archive (cbz, cbr, cb7, cbt, cba)
* Portable Document Format (pdf)
* ePub Books (epub)
* DeVice Independent files (dvi)
* DeJaVu (djvu)
* Compiled Help (chm)

[Source](https://invent.kde.org/graphics/peruse/-/raw/master/src/app/org.kde.peruse.appdata.xml)

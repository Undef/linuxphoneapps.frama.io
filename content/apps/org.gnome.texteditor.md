+++
title = "Text Editor"
description = "Edit text files"
aliases = []
date = 2023-01-22
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The GNOME Project",]
categories = [ "text editor",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_12", "debian_13", "debian_unstable", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "TextEditor",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnome-text-editor"
homepage = "https://gitlab.gnome.org/GNOME/gnome-text-editor"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-text-editor/issues"
donations = "https://www.gnome.org/donate/"
translations = "https://wiki.gnome.org/TranslationProject"
more_information = [ "https://apps.gnome.org/app/org.gnome.TextEditor/",]
summary_source_url = "https://gitlab.gnome.org/GNOME/gnome-text-editor/-/raw/main/data/org.gnome.TextEditor.appdata.xml.in.in"
screenshots = [ "https://static.gnome.org/appdata/gnome-43/text-editor/text-editor-1.png", "https://static.gnome.org/appdata/gnome-43/text-editor/text-editor-2.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/org.gnome.text-editor/1.png", "https://img.linuxphoneapps.org/org.gnome.text-editor/2.png", "https://img.linuxphoneapps.org/org.gnome.text-editor/3.png",]
all_features_touch = false
intended_for_mobile = false
app_id = "org.gnome.TextEditor"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.TextEditor"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-text-editor",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-text-editor/-/raw/main/data/org.gnome.TextEditor.appdata.xml.in.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

GNOME Text Editor is a simple text editor focused on a pleasing default experience.

[Source](https://gitlab.gnome.org/GNOME/gnome-text-editor/-/raw/main/data/org.gnome.TextEditor.appdata.xml.in.in)

### Notice

Mobile friendly since release 43.

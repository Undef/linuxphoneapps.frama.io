+++
title = "Fiery"
description = "Web browser"
aliases = []
date = 2021-06-12
updated = 2023-09-30

[taxonomies]
project_licenses = [ "LGPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "maui",]
categories = [ "web browser",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "MauiKit", "Kirigami",]
backends = [ "QtWebEngine",]
services = []
packaged_in = [ "alpine_edge", "arch", "archlinuxarm_aarch64", "manjaro_stable", "manjaro_unstable",]
freedesktop_categories = [ "Network", "Qt", "WebBrowser",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/maui/fiery"
homepage = "https://mauikit.org/"
bugtracker = "https://invent.kde.org/maui/fiery/-/issues"
donations = "https://kde.org/community/donations"
translations = ""
more_information = [ "https://www.youtube.com/watch?v=JS8qZwKoMFk?t=515", "https://mauikit.org/blog/maui-2-2-0-release/",]
summary_source_url = "https://invent.kde.org/maui/fiery/-/raw/master/org.kde.fiery.metainfo.xml"
screenshots = [ "https://mauikit.org/wp-content/uploads/2022/11/fiery.jpg",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = false
app_id = "org.kde.fiery"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "fiery",]
appstream_xml_url = "https://invent.kde.org/maui/fiery/-/raw/master/org.kde.fiery.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description
Fiery is a convergent web browser.

[Source](https://invent.kde.org/maui/fiery/-/raw/master/org.kde.fiery.metainfo.xml)

### Notice
Used to be called Sol before.

+++
title = "Strike"
description = "Simple and convergent IDE"
aliases = []
date = 2022-01-23
updated = 2023-09-30

[taxonomies]
project_licenses = [ "LGPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "maui",]
categories = [ "development",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "MauiKit", "Kirigami",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "Qt", "Development", "IDE",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/maui/strike"
homepage = "https://mauikit.org/"
bugtracker = "https://invent.kde.org/maui/strike/-/issues"
donations = "https://kde.org/community/donations"
translations = ""
more_information = [ "https://mauikit.org/blog/maui-report-13/",]
summary_source_url = "https://invent.kde.org/maui/strike/-/raw/master/org.kde.strike.metainfo.xml"
screenshots = [ "https://cdn.kde.org/screenshots/nota/nota.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = false
app_id = "org.kde.strike"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "maui-strike",]
appstream_xml_url = "https://invent.kde.org/maui/strike/-/raw/master/org.kde.strike.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description
Strike is a simple and covergent IDE that works on Linux desktop and mobile computers.

[Source](https://invent.kde.org/maui/strike/-/raw/master/org.kde.strike.metainfo.xml)

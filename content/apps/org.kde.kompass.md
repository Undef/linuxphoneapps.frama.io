+++
title = "Kompass"
description = "Compass application"
aliases = []
date = 2021-12-18
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Sylvain Migaud",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "inactive", "pre-release", "inactive",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Compass", "KDE", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/smigaud/kompass"
homepage = "https://invent.kde.org/plasma-mobile/kompass"
bugtracker = "https://invent.kde.org/plasma-mobile/kompass/-/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/smigaud/kompass/-/raw/master/org.kde.kompass.appdata.xml"
screenshots = []
screenshots_img = []
all_features_touch = true
intended_for_mobile = false
app_id = "org.kde.kompass"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://invent.kde.org/smigaud/kompass/-/raw/master/org.kde.kompass.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Compass application for the PinePhone and other Linux device with a magnetometer.

[Source](https://invent.kde.org/smigaud/kompass/-/raw/master/org.kde.kompass.appdata.xml)
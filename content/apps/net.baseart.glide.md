+++
title = "Glide"
description = "Play movies and audio files"
aliases = []
date = 2020-11-07
updated = 2024-01-02

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "CC-BY-3.0",]
app_author = [ "The Glide Team",]
categories = [ "video player",]
mobile_compatibility = [ "4",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "AudioVideo", "Video",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/philn/glide"
homepage = "https://github.com/philn/glide"
bugtracker = "https://github.com/philn/glide/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/philn/glide/master/data/net.baseart.Glide.metainfo.xml"
screenshots = [ "https://github.com/philn/glide/raw/master/audio-screenshot.png", "https://github.com/philn/glide/raw/master/screenshot.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = false
app_id = "net.baseart.Glide"
scale_to_fit = "glide"
flathub = "https://flathub.org/apps/net.baseart.Glide"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/philn/glide/master/data/net.baseart.Glide.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Glide is a simple and minimalistic media player relying on GStreamer for the multimedia support
 and GTK for the user interface. Glide should be able to play any multimedia format supported by
 GStreamer, locally or remotely hosted.


Once Glide is running you can use some menus to switch the subtitle and audio
 tracks, play, pause, seek and switch the window to fullscreen.

[Source](https://raw.githubusercontent.com/philn/glide/master/data/net.baseart.Glide.metainfo.xml)
+++
title = "Lobjur"
description = "A simple lobste.rs client"
aliases = []
date = 2022-08-03
updated = 2023-09-30

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Lorenzo Miglietta",]
categories = [ "news",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Lobste.rs",]
packaged_in = [ "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "News",]
programming_languages = [ "Clojure",]
build_systems = [ "nix",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/ranfdev/Lobjur"
homepage = "https://github.com/ranfdev/Lobjur"
bugtracker = "https://github.com/ranfdev/Lobjur/issues/"
donations = "https://github.com/sponsors/ranfdev"
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/ranfdev/Lobjur/master/data/com.ranfdev.Lobjur.metainfo.xml"
screenshots = [ "https://user-images.githubusercontent.com/23294184/188264118-76c8df44-10a4-4894-902e-66a006037d7f.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "com.ranfdev.Lobjur"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.ranfdev.Lobjur"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/ranfdev/Lobjur/master/data/com.ranfdev.Lobjur.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description
Simple client for lobste.rs online community. From the website: "Lobsters is a computing-focused community centered around link aggregation and discussion, launched on July 3rd, 2012".

[Source](https://raw.githubusercontent.com/ranfdev/Lobjur/master/data/com.ranfdev.Lobjur.metainfo.xml)

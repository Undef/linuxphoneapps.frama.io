+++
title = "Anti-Spam"
description = "A GTK template application"
aliases = []
date = 2021-10-25
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Chris Talbot",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "gnome-calls",]
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "aur", "debian_12", "debian_13", "debian_unstable", "devuan_unstable", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/kop316/phosh-antispam"
homepage = "https://gitlab.com/kop316/phosh-antispam"
bugtracker = "https://gitlab.com/kop316/phosh-antispam/issues"
donations = "https://liberapay.com/kop316/donate"
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/kop316/phosh-antispam/-/raw/master/data/metainfo/org.kop316.antispam.metainfo.xml.in"
screenshots = [ "https://gitlab.com/kop316/phosh-antispam/-/raw/master/data/metainfo/screenshot.png?inline=false",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.kop316.antispam"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://gitlab.com/kop316/phosh-antispam/-/raw/master/org.kop316.antispam.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "phosh-antispam",]
appstream_xml_url = "https://gitlab.com/kop316/phosh-antispam/-/raw/master/data/metainfo/org.kop316.antispam.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Phosh Anti-Spam is a program that monitors Gnome calls and automatically hangs up depending on the user's preferences.


By default, if the number calling you is not in your contact list, or you have the contact listed as "Spam", Phosh Anti-Spam will automatically tell GNOME Calls to hang up on the call. There are additionally these user configuration options:


* Whether to only block certain calls vs only allowing certain calls.
* Whether ot not to allow blocked numbers (also called "Anonymous Number" in GNOME Calls)
* If the caller calls back within a user defined time (min) of the first (blocked) call, whether to allow the call through (in case it is a human trying to call back). Note this does not apply to contacts named "Spam", they will always be hung up on.
* If you would like to match a certain type (or types) of number (for example, an area code or a number prefix) to let them through. For example, if you want to allow the area code `201` and the number prefix `312-555`, you could allow both combinations (and add others as well).
* NOTE: GNOME Calls has all of the numbers to compare in the E.164 format.
* The patterm match for numbers is located in a csv in `$XDG\_CONFIG\_DIR/phoshantispam/whitelist.csv` (`$HOME/.config/phoshantispam/whitelist.csv` in most cases). On start up, `phosh-antispam` loads all of the patterm values from this file (and saves to it if the list changes in the program). If one does not exist, `phosh-antispam` will create a file there on first run.

[Source](https://gitlab.com/kop316/phosh-antispam/-/raw/master/data/metainfo/org.kop316.antispam.metainfo.xml.in)

### Notice

Was GTK3/libhandy before release 3.0.

Read the README for the required version of Gnome Calls required to make this app work if you are using a stable/slow-moving distribution.

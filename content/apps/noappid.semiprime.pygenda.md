+++
title = "Pygenda"
description = "Pygenda is a calendar/agenda application written in Python3/GTK3 and designed for \"PDA\" devices such as Planet Computers' Gemini."
aliases = []
date = 2023-10-28

[taxonomies]
project_licenses = ["GPL-3.0-only"]
metadata_licenses = []
app_author = ["Matt Lewis"]
categories = ["calendar"]
mobile_compatibility = ["4"]
status = ["maturing"]
frameworks = ["GTK3", "Cairo"]
backends = []
services = []
packaged_in = []
freedesktop_categories = ["GTK", "Office", "Calendar"]
programming_languages = ["Python"]
build_systems = ["setup.py"]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/semiprime/pygenda"
homepage = ""
bugtracker = "https://github.com/semiprime/pygenda/issues"
donations = ""
translations = ""
more_information = ["https://github.com/semiprime/pygenda/blob/main/docs/quickstart-geminipda.md", "https://github.com/semiprime/pygenda/blob/main/docs/quickstart-postmarketOS.md"]
summary_source_url = "https://github.com/semiprime/pygenda"
screenshots = []
screenshots_img = []
all_features_touch = false 
intended_for_mobile = false 
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "linmob"
updated_by = ""

+++

### Description 
	   
 An agenda application inspired by Agenda programs on Psion PDAs, written in Python/GTK. Targeting Planet Computers' Gemini PDA (running Linux) in particular, but should work on similar devices (after providing a user CSS file to set font sizes etc.). 

[Source](https://github.com/semiprime/pygenda)
	   

### Notice

This application is made for landscape use and is customizable, so it should be possible to also make it work on portrait.

The easiest way to install may be via pip and PyPI.

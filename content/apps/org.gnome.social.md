+++
title = "Social"
description = "Federated microblogging client"
aliases = []
date = 2020-09-23
updated = 2023-09-30

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Christopher Davis",]
categories = [ "social media",]
mobile_compatibility = [ "5",]
status = [ "inactive",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Mastodon", "Pleroma",]
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Network", "Feed", "Chat",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/World/Social"
homepage = "https://gitlab.gnome.org/World/Social"
bugtracker = "https://gitlab.gnome.org/World/Social/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/World/Social/-/raw/main/data/org.gnome.Social.appdata.xml.in.in"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.gnome.Social"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://gitlab.gnome.org/World/Social/-/raw/main/data/org.gnome.Social.appdata.xml.in.in"
reported_by = "linmob"
updated_by = "script"

+++

### Description
Social is a federated microblogging client for GNOME written in Rust.

[Source](https://gitlab.gnome.org/World/Social/-/raw/main/data/org.gnome.Social.appdata.xml.in.in)

### Notice
Work in progress, login does not work for me yet. No commits (except for translations) since late 2020.

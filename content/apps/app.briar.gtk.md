+++
title = "Briar"
description = "Secure messaging, anywhere"
aliases = []
date = 2020-12-01
updated = 2024-01-02

[taxonomies]
project_licenses = [ "AGPL-3.0-only",]
metadata_licenses = [ "CC-BY-4.0",]
app_author = [ "The Briar Project",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = [ "archived",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = [ "briar",]
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Network", "Chat", "P2P",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://code.briarproject.org/briar/briar-gtk"
homepage = "https://code.briarproject.org/briar/briar-gtk"
bugtracker = "https://code.briarproject.org/briar/briar-gtk/issues"
donations = "https://liberapay.com/Briar/donate"
translations = "https://www.transifex.com/otf/briar/"
more_information = [ "https://nico.dorfbrunnen.eu/posts/2020/briar-beta/", "https://nico.dorfbrunnen.eu/posts/2020/briar-international/",]
summary_source_url = "https://code.briarproject.org/briar/briar-gtk"
screenshots = [ "https://code.briarproject.org/briar/briar-gtk/raw/fff8cbe97dc8e6c759b978e6d7d8608a3105b9c0/tools/screenshots/briar-gtk-screenshot-1.png", "https://code.briarproject.org/briar/briar-gtk/raw/fff8cbe97dc8e6c759b978e6d7d8608a3105b9c0/tools/screenshots/briar-gtk-screenshot-2.png", "https://code.briarproject.org/briar/briar-gtk/raw/fff8cbe97dc8e6c759b978e6d7d8608a3105b9c0/tools/screenshots/briar-gtk-screenshot-3.png", "https://code.briarproject.org/briar/briar-gtk/raw/fff8cbe97dc8e6c759b978e6d7d8608a3105b9c0/tools/screenshots/briar-gtk-screenshot-4.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "app.briar.gtk"
scale_to_fit = "Briar"
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://code.briarproject.org/briar/briar-gtk/-/raw/main/app.briar.gtk.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://code.briarproject.org/briar/briar-gtk/-/raw/main/briar-gtk/data/app.briar.gtk.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Briar is a messaging app designed for activists, journalists, and anyone
 else who needs a safe, easy and robust way to communicate. Unlike
 traditional messaging apps, Briar doesn't rely on a central server -
 messages are synchronized directly between the users' devices. If the
 internet's down, Briar can sync via Bluetooth or Wi-Fi, keeping the
 information flowing in a crisis. If the internet's up, Briar can sync via
 the Tor network, protecting users and their relationships from
 surveillance.


Note: Currently only private chats via Tor are supported in Briar GTK.

[Source](https://code.briarproject.org/briar/briar-gtk/-/raw/main/briar-gtk/data/app.briar.gtk.metainfo.xml.in)

### Notice

Could not chat with Android client in initial test (2020/12/01), deprecated and replaced by [Briar Desktop](https://code.briarproject.org/briar/briar-desktop/) (2021/09/21)
+++
title = "SciTEQt"
description = "A port of the SciTE text editor to the Qt QML/Quick, which is optimized for touch devices."
aliases = []
date = 2021-05-11
updated = 2022-12-19

[taxonomies]
project_licenses = [ "Custom (see https://github.com/mneuroth/SciTEQt/blob/master/scite/License.txt)",]
metadata_licenses = []
app_author = [ "mneuroth",]
categories = [ "text editor",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "QtQuick",]
backends = [ "Scintilla",]
services = []
packaged_in = []
freedesktop_categories = [ "Qt", "Utility", "TextEditor",]
programming_languages = [ "Cpp",]
build_systems = [ "qmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/mneuroth/SciTEQt"
homepage = ""
bugtracker = "https://github.com/mneuroth/SciTEQt/issues/"
donations = ""
translations = ""
more_information = [ "https://play.google.com/store/apps/details?id=org.scintilla.sciteqt",]
summary_source_url = "https://github.com/mneuroth/SciTEQt"
screenshots = [ "https://twitter.com/linuxphoneapps/status/1392221878199234571",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.scintilla.sciteqt"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"

+++

### Notice

Does not come with a desktop file, has to be started manually with sciteqt from the terminal. Toggle mobile view via View - Mobile Platform UI.

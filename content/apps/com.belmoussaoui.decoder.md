+++
title = "Decoder"
description = "Scan and Generate QR Codes"
aliases = []
date = 2021-01-04
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Bilal Elmoussaoui",]
categories = [ "qr code",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "libaperture",]
services = []
packaged_in = [ "aur", "flathub", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/World/decoder/"
homepage = "https://gitlab.gnome.org/World/decoder/"
bugtracker = "https://gitlab.gnome.org/World/decoder/-/issues"
donations = "https://liberapay.com/bielmoussaoui"
translations = "https://l10n.gnome.org/module/decoder/"
more_information = [ "https://blogs.gnome.org/msandova/2021/08/07/decoder-0-2-0-released-%f0%9f%9a%80/", "https://apps.gnome.org/app/com.belmoussaoui.Decoder/",]
summary_source_url = "https://gitlab.gnome.org/World/decoder/"
screenshots = [ "https://gitlab.gnome.org/World/decoder/raw/master/data/screenshots/screenshot1.png", "https://gitlab.gnome.org/World/decoder/raw/master/data/screenshots/screenshot2.png", "https://gitlab.gnome.org/World/decoder/raw/master/data/screenshots/screenshot3.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "com.belmoussaoui.Decoder"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.belmoussaoui.Decoder"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "decoder",]
appstream_xml_url = "https://gitlab.gnome.org/World/decoder/-/raw/master/data/com.belmoussaoui.Decoder.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Fancy yet simple QR Codes scanner and generator.


Features:


* QR Code generation
* Scanning with a camera
* Scanning from a screenshot
* Parses and displays QR code content when possible

[Source](https://gitlab.gnome.org/World/decoder/-/raw/master/data/com.belmoussaoui.Decoder.metainfo.xml.in.in)

### Notice

Note: Use Megapixels to scan QR codes on your Linux Phone, and use Decoder as a QR Code decoder and creator - most Linux Phones do not expose the camera to every app yet.

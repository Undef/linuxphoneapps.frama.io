+++
title = "FreeTube"
description = "An Open Source YouTube app for privacy"
aliases = []
date = 2020-11-28
updated = 2023-12-31

[taxonomies]
project_licenses = [ "AGPL-3.0-only",]
metadata_licenses = []
app_author = [ "freetubeapp",]
categories = [ "video player",]
mobile_compatibility = [ "4",]
status = []
frameworks = [ "Electron",]
backends = []
services = []
packaged_in = [ "alpine_edge", "aur", "flathub", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "Video", "Network", "Player",]
programming_languages = [ "JavaScript",]
build_systems = [ "yarn",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/FreeTubeApp/FreeTube"
homepage = "https://freetubeapp.io/"
bugtracker = "https://github.com/FreeTubeApp/FreeTube/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://freetubeapp.io/"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "io.freetubeapp.FreeTube"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.freetubeapp.FreeTube"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "freetube",]
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"

+++

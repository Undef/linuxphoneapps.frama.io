+++
title = "Marble Maps"
description = "Find your way"
aliases = []
date = 2019-02-01
updated = 2024-01-02

[taxonomies]
project_licenses = [ "LGPL-2.1-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community",]
categories = [ "maps and navigation", "geography",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Qt", "KDE", "Education", "Geography",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/education/marble"
homepage = "https://marble.kde.org/"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?format=guided&product=marble"
donations = "https://www.kde.org/community/donations/?app=marble&source=appdata"
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/education/marble/-/raw/master/src/apps/marble-maps/org.kde.marble.maps.appdata.xml"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.marble.maps.desktop"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "marble",]
appstream_xml_url = "https://invent.kde.org/education/marble/-/raw/master/src/apps/marble-maps/org.kde.marble.maps.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

OpenStreetMap Navigation.

[Source](https://invent.kde.org/education/marble/-/raw/master/src/apps/marble-maps/org.kde.marble.maps.appdata.xml)

### Notice

Marble maps is sadly not working well, at least on wayland.

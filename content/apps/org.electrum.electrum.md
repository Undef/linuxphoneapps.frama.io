+++
title = "Electrum"
description = "Bitcoin Wallet"
aliases = []
date = 2020-10-21
updated = 2023-12-31

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "MIT",]
app_author = [ "spesmilo",]
categories = [ "bitcoin wallet",]
mobile_compatibility = [ "3",]
status = []
frameworks = [ "QtWidgets", "QtQuick",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "Qt", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "setup.py",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/spesmilo/electrum"
homepage = "https://electrum.org"
bugtracker = "https://github.com/spesmilo/electrum/issues/"
donations = ""
translations = ""
more_information = [ "https://github.com/spesmilo/electrum/issues/6835",]
summary_source_url = "https://raw.githubusercontent.com/spesmilo/electrum/master/org.electrum.electrum.metainfo.xml"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.electrum.electrum"
scale_to_fit = "electrum"
flathub = "https://flathub.org/apps/org.electrum.electrum"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "electrum",]
appstream_xml_url = "https://raw.githubusercontent.com/spesmilo/electrum/master/org.electrum.electrum.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description
Electrum is a lightweight Bitcoin wallet focused is speed, with low resource usage and simplifying Bitcoin.
 Startup times are instant because it operates in conjunction with high-performance servers that handle the most complicated parts of the Bitcoin system.

[Source](https://raw.githubusercontent.com/spesmilo/electrum/master/org.electrum.electrum.metainfo.xml)

### Notice
Ok after scale-to-fit. The app has a Kivy GUI for Android, which should do better - but needs custom building. If you manage to run the Kivy GUI on mobile GNU/Linux, please report back :-)

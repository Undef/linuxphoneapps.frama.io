+++
title = "Metadata Cleaner"
description = "View and clean metadata in files"
aliases = []
date = 2021-01-04
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC-BY-SA-4.0",]
app_author = [ "Romain Vigier",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "mat2",]
services = []
packaged_in = [ "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "flathub", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/rmnvgr/metadata-cleaner/"
homepage = "https://metadatacleaner.romainvigier.fr"
bugtracker = "https://gitlab.com/rmnvgr/metadata-cleaner/-/issues"
donations = ""
translations = "https://hosted.weblate.org/engage/metadata-cleaner/"
more_information = [ "https://apps.gnome.org/app/fr.romainvigier.MetadataCleaner/",]
summary_source_url = "https://gitlab.com/rmnvgr/metadata-cleaner/-/raw/main/application/data/fr.romainvigier.MetadataCleaner.metainfo.xml"
screenshots = [ "https://metadatacleaner.romainvigier.fr/screenshots/1.png", "https://metadatacleaner.romainvigier.fr/screenshots/2.png", "https://metadatacleaner.romainvigier.fr/screenshots/3.png", "https://metadatacleaner.romainvigier.fr/screenshots/4.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "fr.romainvigier.MetadataCleaner"
scale_to_fit = ""
flathub = "https://flathub.org/apps/fr.romainvigier.MetadataCleaner"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "metadata-cleaner", "python:metadata-cleaner",]
appstream_xml_url = "https://gitlab.com/rmnvgr/metadata-cleaner/-/raw/main/application/data/fr.romainvigier.MetadataCleaner.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Metadata within a file can tell a lot about you. Cameras record data about when and where a picture was taken and which camera was used. Office applications automatically add author and company information to documents and spreadsheets. This is sensitive information and you may not want to disclose it.


This tool allows you to view metadata in your files and to get rid of it, as much as possible.

[Source](https://gitlab.com/rmnvgr/metadata-cleaner/-/raw/main/application/data/fr.romainvigier.MetadataCleaner.metainfo.xml)

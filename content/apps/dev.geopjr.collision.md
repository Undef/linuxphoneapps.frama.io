+++
title = "Collision"
description = "Check hashes for your files"
aliases = []
date = 2023-04-17
updated = 2024-01-02

[taxonomies]
project_licenses = [ "BSD-2-Clause",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Evangelos \"GeopJr\" Paterakis",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = []
programming_languages = [ "Crystal",]
build_systems = [ "make",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/GeopJr/Collision/"
homepage = "https://collision.geopjr.dev/"
bugtracker = "https://github.com/GeopJr/Collision/issues"
donations = "https://geopjr.dev/donate"
translations = "https://hosted.weblate.org/engage/collision/"
more_information = [ "https://apps.gnome.org/de/app/dev.geopjr.Collision/",]
summary_source_url = "https://raw.githubusercontent.com/GeopJr/Collision/main/data/dev.geopjr.Collision.metainfo.xml.in"
screenshots = [ "https://media.githubusercontent.com/media/GeopJr/Collision/main/data/screenshots/screenshot-1.png", "https://media.githubusercontent.com/media/GeopJr/Collision/main/data/screenshots/screenshot-2.png", "https://media.githubusercontent.com/media/GeopJr/Collision/main/data/screenshots/screenshot-3.png", "https://media.githubusercontent.com/media/GeopJr/Collision/main/data/screenshots/screenshot-4.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/dev.geopjr.collision/1.png", "https://img.linuxphoneapps.org/dev.geopjr.collision/2.png", "https://img.linuxphoneapps.org/dev.geopjr.collision/3.png", "https://img.linuxphoneapps.org/dev.geopjr.collision/4.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "dev.geopjr.Collision"
scale_to_fit = ""
flathub = "https://flathub.org/apps/dev.geopjr.Collision"
flatpak_link = ""
flatpak_recipe = "https://raw.githubusercontent.com/GeopJr/Collision/main/data/dev.geopjr.Collision.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "collision",]
appstream_xml_url = "https://raw.githubusercontent.com/GeopJr/Collision/main/data/dev.geopjr.Collision.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Verifying that a file you downloaded or received is actually the one you were
 expecting is often overlooked or too time-consuming to do. At the same time, it
 has become very easy to get your hands on a file that has been tampered with, due
 to the mass increase of malicious webpages and other actors.


This tool aims to solve that. Collision comes with a simple & clean UI, allowing
 anyone, from any age and experience group, to generate, compare and verify MD5,
 SHA-256, SHA-512, SHA-1, Blake3, CRC32 and Adler32 hashes.

[Source](https://raw.githubusercontent.com/GeopJr/Collision/main/data/dev.geopjr.Collision.metainfo.xml.in)

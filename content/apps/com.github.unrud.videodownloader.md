+++
title = "Video Downloader"
description = "Download videos from websites like YouTube and many others"
aliases = []
date = 2020-10-15
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Unrud",]
categories = [ "video downloader",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "aur", "debian_13", "debian_unstable", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/Unrud/video-downloader"
homepage = "https://github.com/Unrud/video-downloader"
bugtracker = "https://github.com/Unrud/video-downloader/issues"
donations = ""
translations = "https://hosted.weblate.org/engage/video-downloader/"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/Unrud/video-downloader/master/data/com.github.unrud.VideoDownloader.metainfo.xml.in"
screenshots = [ "https://raw.githubusercontent.com/Unrud/video-downloader/v0.12.10/screenshots/1.png", "https://raw.githubusercontent.com/Unrud/video-downloader/v0.12.10/screenshots/2.png", "https://raw.githubusercontent.com/Unrud/video-downloader/v0.12.10/screenshots/3.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "com.github.unrud.VideoDownloader"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.github.unrud.VideoDownloader"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "video-downloader",]
appstream_xml_url = "https://raw.githubusercontent.com/Unrud/video-downloader/master/data/com.github.unrud.VideoDownloader.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Download videos from websites with an easy-to-use interface.
 Provides the following features:


* Convert videos to MP3
* Supports password-protected and private videos
* Download single videos or whole playlists
* Automatically selects a video format based on your quality demands


Based on yt-dlp.

[Source](https://raw.githubusercontent.com/Unrud/video-downloader/master/data/com.github.unrud.VideoDownloader.metainfo.xml.in)

+++
title = "Kontrast"
description = "A Color Contrast Checker"
aliases = []
date = 2020-09-10
updated = 2023-12-31

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Carl Schwan",]
categories = [ "accessibility",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "QtQuick", "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Qt", "KDE", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/accessibility/kontrast"
homepage = "https://kde.org/applications/en/kontrast"
bugtracker = "https://invent.kde.org/accessibility/kontrast/-/issues"
donations = "https://liberapay.com/Carl/"
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/accessibility/kontrast/-/raw/master/org.kde.kontrast.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/kontrast/kontrast.png", "https://cdn.kde.org/screenshots/kontrast/kontrat_mobile.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.kontrast"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.kontrast"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "kontrast",]
appstream_xml_url = "https://invent.kde.org/accessibility/kontrast/-/raw/master/org.kde.kontrast.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description
Kontrast is a color contrast checker and tells you if your color combinations are distinct enough to be readable and accessible.

[Source](https://invent.kde.org/accessibility/kontrast/-/raw/master/org.kde.kontrast.appdata.xml)

+++
title = "Saber: Handwritten Notes"
description = "The notes app built for handwriting"
aliases = []
date = 2023-12-02
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "MIT",]
app_author = [ "Adil Hanney",]
categories = [ "note taking", "drawing",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "Flutter",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "snapcraft",]
freedesktop_categories = [ "Education", "Graphics", "Office",]
programming_languages = [ "Dart",]
build_systems = [ "flutter",]
requires_internet = [ "recommends always",]
tags = []

[extra]
repository = "https://github.com/saber-notes/saber"
homepage = "https://github.com/saber-notes/saber"
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/saber-notes/saber/main/flatpak/com.adilhanney.saber.metainfo.xml"
screenshots = [ "https://raw.githubusercontent.com/saber-notes/saber/main/metadata/en-US/images/tenInchScreenshots/editor-desktop.png", "https://raw.githubusercontent.com/saber-notes/saber/main/metadata/en-US/images/tenInchScreenshots/home-desktop.png", "https://raw.githubusercontent.com/saber-notes/saber/main/metadata/en-US/images/tenInchScreenshots/login-desktop.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = false 
app_id = "com.adilhanney.saber"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.adilhanney.saber"
flatpak_link = "https://flathub.org/apps/com.adilhanney.saber.flatpakref"
flatpak_recipe = ""
snapcraft = "https://snapcraft.io/saber"
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = "https://github.com/saber-notes/saber/releases/"
appimage_aarch64_url = ""
repology = [ "saber",]
appstream_xml_url = "https://raw.githubusercontent.com/saber-notes/saber/main/flatpak/com.adilhanney.saber.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Saber is the notes app built for handwriting.


It's designed to be as simple and intuitive as possible, while still delivering unique features that you'll actually use. Additionally, Saber is available across all your devices, large and small, and syncs between them seamlessly.


Notably, it can invert your notes when you're in dark mode. This allows you to write with white ink on a black background, which is much easier on the eyes in low-light environments like when the teacher turns off the lights in class.
 Images and PDFs are also inverted, so you can still use a digital printout or a textbook without the fuss.


Saber uses a dual-password system to protect your notes from anyone but you, even if they have complete control over the server. You can safely store your notes on the official Saber server, another server, or even host your own!


The app is completely open-source so that anyone can view the source code and see exactly what it's doing and how it handles your data. Many other note-taking apps are closed-source and proprietary, meaning that their inner workings are a mystery to the public.


As someone who studies maths, highlighting multi-line equations was always a hassle with other apps, where the highlighter would change color when it overlapped with itself. Another problem I had was that in some apps, the highlighter would render on top of the text, fading it out and making it hard to read.
 Saber's highlighter has no such issues. It utilizes canvas compositing to render the highlighter in a way that is consistent with/better than traditional paper, where it handles overlaps and maintains color consistency.


Saber has everything you need to keep your notes organized. Create folders inside folders inside folders to your heart's content with no limit on the number of nested folders. And even though a note may be buried deep within a nested folder, you can still access it easily with your most recent notes always available on the home screen.


Discover a whole new way to capture and organize your thoughts
 with Saber. Whether you're a student, professional, or creative
 mind, Saber is your trusted companion for digital handwriting.
 Download now and let your ideas flow freely!

[Source](https://raw.githubusercontent.com/saber-notes/saber/main/flatpak/com.adilhanney.saber.metainfo.xml)

### Notice

To make the app launch on GLES 2 devices, make sure to set the environment variable `LIBGL_ALWAYS_SOFTWARE=1`.

+++
title = "Mepo"
description = "Fast, simple, and hackable OSM map viewer for Linux"
aliases = []
date = 2022-03-25
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Miles Alan",]
categories = [ "maps and navigation", "geography",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "SDL",]
backends = []
services = [ "openstreetmap",]
packaged_in = [ "alpine_edge", "aur", "flathub", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "Utility", "Maps",]
programming_languages = [ "Zig",]
build_systems = [ "zig",]
requires_internet = []
tags = []

[extra]
repository = "https://git.sr.ht/~mil/mepo"
homepage = "https://sr.ht/~mil/mepo"
bugtracker = "https://todo.sr.ht/~mil/mepo-tickets"
donations = ""
translations = ""
more_information = [ "https://mepo.milesalan.com/guides.html", "https://mepo.milesalan.com/userguide.html", "https://mepo.milesalan.com/demos.html", "https://mepo.milesalan.com/demos.html",]
summary_source_url = "https://raw.githubusercontent.com/flathub/com.milesalan.mepo/master/com.milesalan.mepo.metainfo.xml"
screenshots = [ "https://media.lrdu.org/mepo_demos/mepo_demo_0.3_pois.png", "https://media.lrdu.org/mepo_demos/mepo_demo_0.3_routing.png", "https://media.lrdu.org/mepo_demos/mepo_demo_1.0_desktop_pois.png", "https://media.lrdu.org/mepo_demos/mepo_demo_1.0_desktop_routing.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "com.milesalan.mepo"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.milesalan.mepo"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "mepo",]
appstream_xml_url = "https://raw.githubusercontent.com/flathub/com.milesalan.mepo/master/com.milesalan.mepo.metainfo.xml"
reported_by = "milesalan"
updated_by = "script"

+++


### Description

Mepo is a fast, simple, and hackable OSM map viewer for
 Linux. Designed with the Pinephone & mobile linux in mind, works
 both offline and online.

[Source](https://raw.githubusercontent.com/flathub/com.milesalan.mepo/master/com.milesalan.mepo.metainfo.xml)

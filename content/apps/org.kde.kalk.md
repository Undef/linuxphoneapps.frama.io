+++
title = "Calculator"
description = "A feature rich calculator"
aliases = []
date = 2020-03-02
updated = 2024-01-14

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community",]
categories = [ "calculator",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "Kirigami",]
backends = [ "Math.js",]
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_13", "debian_unstable", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "Calculator", "KDE", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake", "ninja",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/utilities/kalk"
homepage = "https://invent.kde.org/plasma-mobile/kalk"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=Kalk"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/utilities/kalk/-/raw/master/org.kde.kalk.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/kalk/kalk.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.kde.kalk"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.kalk"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "kalk",]
appstream_xml_url = "https://invent.kde.org/utilities/kalk/-/raw/master/org.kde.kalk.appdata.xml"
reported_by = "cahfofpai"
updated_by = "check_via_repology"

+++

### Description

Kalk is a convergent calculator for Plasma.

[Source](https://invent.kde.org/utilities/kalk/-/raw/master/org.kde.kalk.appdata.xml)
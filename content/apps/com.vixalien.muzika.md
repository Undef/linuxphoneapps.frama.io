+++
title = "Muzika"
description = "Elegant music streaming application"
aliases = []
date = 2024-02-08

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Angelo Verlain",]
categories = [ "music player",]
mobile_compatibility = [ "5",]
status = [ "early", "pre-release",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "gstreamer-rs",]
services = [ "YouTube",]
packaged_in = [ "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "GNOME", "GTK", "Audio", "Network", "Player",]
programming_languages = [ "TypeScript",]
build_systems = [ "Meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/vixalien/muzika"
homepage = "https://github.com/vixalien/muzika"
bugtracker = "https://github.com/vixalien/muzika/issues"
donations = "https://www.buymeacoffee.com/vixalien"
translations = "https://github.com/vixalien/muzika/tree/main/po"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/vixalien/muzika/main/data/com.vixalien.muzika.appdata.xml.in.in"
screenshots = [ "https://raw.githubusercontent.com/vixalien/muzika/main/data/resources/screenshots/home.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "com.vixalien.muzika"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://raw.githubusercontent.com/vixalien/muzika/main/build-aux/flatpak/com.vixalien.muzika.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "muzika",]
appstream_xml_url = "https://raw.githubusercontent.com/vixalien/muzika/main/data/com.vixalien.muzika.appdata.xml.in.in"
reported_by = "linmob"
updated_by = ""

+++

### Description

Play music from millions of songs available on the internet for free.
 Search for songs, albums, artists, and more. Discover new music from
 playlists and get a personalized & curated experience.


Current features:


* play any music from YouTube Music
* allows sign in through OAuth with Google to play your playlists and more from your library or uploads
* search for songs, albums, artists, radios, community & featured playlists and more
* access personalised music radios & mixes

[Source](https://raw.githubusercontent.com/vixalien/muzika/main/data/com.vixalien.muzika.appdata.xml.in.in)

### Notice

Nightly aarch64 flatpak can be found on [GitHub](https://github.com/vixalien/muzika/actions/workflows/build-nighly.yml)

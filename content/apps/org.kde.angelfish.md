+++
title = "Angelfish Web Browser"
description = "Webbrowser for mobile devices"
aliases = []
date = 2019-02-01
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community",]
categories = [ "web browser",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_12", "debian_13", "debian_unstable", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Network", "KDE", "Qt", "WebBrowser",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/network/angelfish"
homepage = "https://apps.kde.org/angelfish"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=angelfish"
donations = "https://www.kde.org/community/donations/?app=angelfish&source=appdata"
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/network/angelfish/-/raw/master/org.kde.angelfish.metainfo.xml"
screenshots = [ "https://cdn.kde.org/screenshots/plasma-angelfish/actions.png", "https://cdn.kde.org/screenshots/plasma-angelfish/desktop.png", "https://cdn.kde.org/screenshots/plasma-angelfish/homepage.png", "https://cdn.kde.org/screenshots/plasma-angelfish/tabs.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = true
app_id = "org.kde.angelfish"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.angelfish"
flatpak_link = ""
flatpak_recipe = "https://invent.kde.org/network/angelfish/-/raw/master/org.kde.angelfish.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "angelfish", "angelfish",]
appstream_xml_url = "https://invent.kde.org/network/angelfish/-/raw/master/org.kde.angelfish.metainfo.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

Angelfish is a modern mobile webbrowser


It supports typical browser features, such as


* bookmarks
* history
* tabs

[Source](https://invent.kde.org/network/angelfish/-/raw/master/org.kde.angelfish.metainfo.xml)

+++
title = "Workflow"
description = "Screen time monitor app based on ActivityWatch"
aliases = []
date = 2020-09-23
updated = 2023-09-30

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Raffaele T.",]
categories = [ "health",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/cunidev/workflow"
homepage = "https://gitlab.com/cunidev/workflow"
bugtracker = "https://gitlab.com/cunidev/workflow/issues"
donations = "https://paypal.me/tranquillini"
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/cunidev/workflow/-/raw/master/data/com.gitlab.cunidev.Workflow.appdata.xml.in"
screenshots = [ "https://i.imgur.com/Uk3XUQU.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "com.gitlab.cunidev.Workflow"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.gitlab.cunidev.Workflow"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://gitlab.com/cunidev/workflow/-/raw/master/data/com.gitlab.cunidev.Workflow.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++

### Description
Workflow is a basic Linux screen time monitor app.

It is based on ActivityWatch, which is not included and should be installed and running following the instructions on their website.

[Source](https://gitlab.com/cunidev/workflow/-/raw/master/data/com.gitlab.cunidev.Workflow.appdata.xml.in)

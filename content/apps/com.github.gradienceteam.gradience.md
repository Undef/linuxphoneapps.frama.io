+++
title = "Gradience"
description = "Change the look of Adwaita, with ease"
aliases = []
date = 2022-09-17
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC-BY-SA-4.0",]
app_author = [ "Gradience Team",]
categories = [ "utilities",]
mobile_compatibility = [ "4",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/GradienceTeam/Gradience"
homepage = "https://gradience.atrophaneura.tech/"
bugtracker = "https://github.com/GradienceTeam/Gradience/issues/"
donations = ""
translations = "https://hosted.weblate.org/projects/GradienceTeam/gradience/"
more_information = [ "https://github.com/orgs/GradienceTeam/discussions",]
summary_source_url = "https://github.com/GradienceTeam/Gradience"
screenshots = [ "https://raw.githubusercontent.com/GradienceTeam/Design/main/Screenshots/Appdata/main-ui-advanced.png", "https://raw.githubusercontent.com/GradienceTeam/Design/main/Screenshots/Appdata/main-ui-colors.png", "https://raw.githubusercontent.com/GradienceTeam/Design/main/Screenshots/Appdata/main-ui-monet.png", "https://raw.githubusercontent.com/GradienceTeam/Design/main/Screenshots/Appdata/preset-manager-explore.png", "https://raw.githubusercontent.com/GradienceTeam/Design/main/Screenshots/Appdata/preset-manager-installed.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/com.github.gradienceteam.gradience/1.png", "https://img.linuxphoneapps.org/com.github.gradienceteam.gradience/2.png", "https://img.linuxphoneapps.org/com.github.gradienceteam.gradience/3.png", "https://img.linuxphoneapps.org/com.github.gradienceteam.gradience/4.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "com.github.GradienceTeam.Gradience"
scale_to_fit = "com.github.GradienceTeam.Gradience gradience"
flathub = "https://flathub.org/apps/com.github.GradienceTeam.Gradience"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gradience",]
appstream_xml_url = "https://raw.githubusercontent.com/GradienceTeam/Gradience/main/data/com.github.GradienceTeam.Gradience.appdata.xml.in.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Gradience is a tool for customizing Libadwaita applications and the adw-gtk3 theme.


The main features of Gradience include the following:


* 🎨️ Changing any color of Adwaita theme
* 🖼️ Applying Material 3 color scheme from wallpaper
* 🎁️ Usage of other users presets
* ⚙️ Changing advanced options with CSS
* 🧩️ Extending functionality using plugins

[Source](https://raw.githubusercontent.com/GradienceTeam/Gradience/main/data/com.github.GradienceTeam.Gradience.appdata.xml.in.in)

### Notice

Release 0.2.2 is just a few pixels to wide to fit the screen and is usable without adjustments. To fit the color picker, make sure to run `scale-to-fit gradience true`.

+++
title = "Shipments"
description = "Postal package tracking application"
aliases = []
date = 2021-12-01
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "martijnbraam",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "alpine_edge", "aur", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://git.sr.ht/~martijnbraam/shipments"
homepage = "https://sr.ht/~martijnbraam/shipments"
bugtracker = "https://todo.sr.ht/~martijnbraam/shipments"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://git.sr.ht/~martijnbraam/shipments/blob/master/data/org.postmarketos.Shipments.appdata.xml"
screenshots = [ "http://brixitcdn.net/metainfo/shipments.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.postmarketos.Shipments"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "shipments",]
appstream_xml_url = "https://git.sr.ht/~martijnbraam/shipments/blob/master/data/org.postmarketos.Shipments.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Application to track your shipments. Supported carriers:


* 4PX
* DHL (needs api code)
* InPost
* PostNL
* Russian Post
* UPS

[Source](https://git.sr.ht/~martijnbraam/shipments/blob/master/data/org.postmarketos.Shipments.appdata.xml)

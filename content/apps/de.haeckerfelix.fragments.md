+++
title = "Fragments"
description = "A BitTorrent Client"
aliases = []
date = 2019-03-04
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Felix Häcker",]
categories = [ "bittorrent client",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "BitTorrent",]
packaged_in = [ "arch", "archlinuxarm_aarch64", "aur", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "FileTransfer", "GNOME", "GTK", "P2P", "Utility",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/World/Fragments"
homepage = "https://gitlab.gnome.org/World/Fragments"
bugtracker = "https://gitlab.gnome.org/World/Fragments/issues"
donations = "https://liberapay.com/haecker-felix"
translations = "https://l10n.gnome.org/module/Fragments/"
more_information = [ "https://puri.sm/posts/fragments-app-for-the-librem-5/", "https://apps.gnome.org/app/de.haeckerfelix.Fragments/", "https://blogs.gnome.org/haeckerfelix/2022/02/07/the-road-to-fragments-2-0/",]
summary_source_url = "https://gitlab.gnome.org/World/Fragments/-/raw/main/data/de.haeckerfelix.Fragments.metainfo.xml.in.in"
screenshots = [ "https://gitlab.gnome.org/World/Fragments/-/raw/main/data/screenshots/1.png", "https://gitlab.gnome.org/World/Fragments/-/raw/main/data/screenshots/2.png", "https://gitlab.gnome.org/World/Fragments/-/raw/main/data/screenshots/3.png", "https://gitlab.gnome.org/World/Fragments/-/raw/main/data/screenshots/4.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "de.haeckerfelix.Fragments"
scale_to_fit = ""
flathub = "https://flathub.org/apps/de.haeckerfelix.Fragments"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "fragments",]
appstream_xml_url = "https://gitlab.gnome.org/World/Fragments/-/raw/main/data/de.haeckerfelix.Fragments.metainfo.xml.in.in"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

Fragments is an easy to use BitTorrent client. It can be used to transfer files via the BitTorrent protocol, such as videos, music or installation images for Linux distributions.

[Source](https://gitlab.gnome.org/World/Fragments/-/raw/main/data/de.haeckerfelix.Fragments.metainfo.xml.in.in)

### Notice

Ported to GTK4/libadwaita for release 2.0.

+++
title = "Key Rack"
description = "View and edit app secrets"
aliases = []
date = 2024-01-14

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC-BY-SA-4.0",]
app_author = [ "Sophie Herold",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GNOME", "GTK", "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "Meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/sophie-h/key-rack/"
homepage = "https://gitlab.gnome.org/sophie-h/key-rack"
bugtracker = "https://gitlab.gnome.org/sophie-h/key-rack/-/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/sophie-h/key-rack/-/raw/main/data/app.drey.KeyRack.metainfo.xml.in"
screenshots = [ "https://gitlab.gnome.org/sophie-h/key-rack/uploads/f129bd26906266c9533790dce11f0f9f/key-rack.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "app.drey.KeyRack"
scale_to_fit = ""
flathub = "https://flathub.org/apps/app.drey.KeyRack"
flatpak_link = "https://flathub.org/apps/app.drey.KeyRack.flatpakref"
flatpak_recipe = "https://gitlab.gnome.org/sophie-h/key-rack/-/raw/main/build-aux/app.drey.KeyRack.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "key-rack",]
appstream_xml_url = "https://gitlab.gnome.org/sophie-h/key-rack/-/raw/main/data/app.drey.KeyRack.metainfo.xml.in"
reported_by = "linmob"
updated_by = ""

+++

### Description

Key Rack allows to view and edit keys, like passwords or tokens, stored by apps. It supports Flatpak secrets as well as system wide secrets.

[Source](https://gitlab.gnome.org/sophie-h/key-rack/-/raw/main/data/app.drey.KeyRack.metainfo.xml.in)

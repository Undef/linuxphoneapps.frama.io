+++
title = "Kaidan"
description = "User-friendly and modern chat app for every device"
aliases = []
date = 2019-02-01
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Kaidan Developers & Contributors",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = []
services = [ "XMPP",]
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Network",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/network/kaidan"
homepage = "https://kaidan.im"
bugtracker = "https://invent.kde.org/network/kaidan/-/issues"
donations = ""
translations = "https://l10n.kde.org/stats/gui/trunk-kf5/package/kaidan/"
more_information = [ "https://www.kaidan.im/2021/05/28/kaidan-0.8.0/",]
summary_source_url = "https://invent.kde.org/network/kaidan/-/raw/master/misc/im.kaidan.kaidan.appdata.xml"
screenshots = [ "https://www.kaidan.im/images/screenshot.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "im.kaidan.kaidan"
scale_to_fit = ""
flathub = "https://flathub.org/apps/im.kaidan.kaidan"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "kaidan",]
appstream_xml_url = "https://invent.kde.org/network/kaidan/-/raw/master/misc/im.kaidan.kaidan.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

Kaidan is a user-friendly and modern chat app for every device. It uses the open communication protocol XMPP (Jabber). Unlike other chat apps, you are not dependent on one specific service provider.


Kaidan does not have all basic features yet and has still some stability issues. But we do our best to improve it!

[Source](https://invent.kde.org/network/kaidan/-/raw/master/misc/im.kaidan.kaidan.appdata.xml)

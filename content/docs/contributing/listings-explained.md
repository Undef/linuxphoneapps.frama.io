+++
title = "Listings explained"
description = "Adding or editing existing listings"
date = 2022-04-07T19:30:00+02:00
draft = false
weight = 40
sort_by = "weight"
template = "docs/page.html"

[extra]
toc = true
top = false
+++

### Introduction

This page explains how to fill out the template to add or edit an app.

#### TOML values and contents

No matter whether you edit or add a listing, here's what kind of contents is supposed to go into the different columns (if you prefer living examples, the [listing for Tuba](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/blob/main/content/apps/dev.geopjr.tuba.md) is a good starting point): 

__Please note: 

| frontmatter value               | type / form      | note                                                                          |
|---------------------------------|------------------|-------------------------------------------------------------------------------|
| title = ""                      | string           | name of the app, make sure to write it as it's written on its project page, **mandatory** |
| description = ""                | string           | app summary, keep it short **mandatory** |
| aliases = ["",]                 | array of strings | optional, only use when name or app id changes, keeps incoming links from breaking |
| date =                          | date, yyyy-mm-dd | date that you are adding the app listing, don't change it later **mandatory**.
| updated =                       | date, yyyy-mm-dd | optional, date of the latest update to the app listing, add line on first update, **don't include while still empty** |
|[taxonomies]                     | section start    | **mandatory**, don't change please! |
| project_licenses = [ "",]       | array of strings | [SPDX 3 identifier](https://spdx.org/licenses/),**mandatory** |
| metadata_licenses = [ "",]      | array of strings | [SPDX 3 identifier](https://spdx.org/licenses/), optionalrepository = ""
| app_author = [ "",]             | array of strings | optional, fill with nick name or self-description from metainfo.xml, [existing values](https://linuxphoneapps.org/app-author/) |
| categories = [ "",]             | array of strings | **mandatory**, fill with value from metainfo.xml or something fitting, use [existing values](https://linuxphoneapps.org/categories/) for reference |
| mobile_compatibility = [ "",]   | array of strings | **mandatory**, see [existing values](https://linuxphoneapps.org/mobile-compatibility/) for reference |
| status = [ "",]                 | array of strings | optional, see [existing values](https://linuxphoneapps.org/status/) for reference |
| frameworks = [ "", ]            | array of strings | recommended, see [existing values](https://linuxphoneapps.org/frameworks/) for reference |
| backends = ["",]                | array of strings | optional, see [existing values](https://linuxphoneapps.org/backends/) for reference |
| services = [ "",]               | array of strings | optional, see [existing values](https://linuxphoneapps.org/services/) for reference |
| packaged_in = [ "",]            | array of strings | optional, see [existing values](https://linuxphoneapps.org/packaged-in/) for reference |
| freedesktop_categories = [ "",] | array of strings | optional, see [existing values](https://linuxphoneapps.org/freedesktop-categories/) for reference, see [the spec](https://specifications.freedesktop.org/menu-spec/menu-spec-latest.html) for allowed values |
| programming_languages = [ "",]  | array of strings | recommended, see [existing values](https://linuxphoneapps.org/programming-languages/) for reference, __write "Cpp" for C++__  |
| build_systems = []          | array of strings | recommended, see [existing values](https://linuxphoneapps.org/build-systems/) for reference |
| requires\_internet = []         | array of strings | recommended, but values [still need to be defined](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/issues/55), see [existing values](https://linuxphoneapps.org/requires-internet/) for reference |
| tags = []                       | array of strings | leave empty |
| [extra]                         | section start    | **mandatory**, don't change please! |
| repository = ""                 | string, URL      | **mandatory**, URL of the apps repository |
| homepage = ""                   | string, URL      | optional, URL of the apps homepage |
| bugtracker = ""                 | string, URL      | recommended, URL of the apps bug tracker |
| donations = ""                  | string, URL      | optional, URL of donation page |
| translations = ""               | string, URL      | optional, URL of translation page |
| more_information = []           | array of strings, URLs | optional, links with helpful additional info about the app, e.g. app reviews | 
| summary\_source\_url = ""       | string, URL      | optional, source URL of the apps description/summary (see above) |
| screenshots = []            | array of strings, URLs | optional, links to screenshots (will not be hot-linked) |
| screenshots_img = []        | array of strings, URLs | optional, links to screenshots, will be displayed - make sure hot-linking is allowed or [send by mail for upload to 1peter10](mailto:screenshots@linuxphoneapps.org) |
| all\_features\_touch = ""       | boolean           | optional, set to true (if true) or false (if some features require other input measures), no quotes! | 
| intended\_for\_mobile = ""      | boolean           | optional, set to true (no quotes!) if the apps descriptions or metadata makes it obvious that the app is intended for mobile and does not just work accidentally |
| app_id = ""                     | string            | e.g. org.gnome.GHex |
| scale\_to\_fit = ""             | string            | value that needs to be used with scale-to-fit on Phosh to make the app fit the screen |
| flathub = ""                    | string, URL       | link to [Flathub](https://flathub.org/) listing (only if aarch64 is supported, check beta.flathub.org when in doubt) |
| flatpak_link = ""               | string, URL       | link to other flatpak source/repository (only if repo provides aarch64 builds) |
| flatpak_recipe = ""             | string, URL       | link to .json to build a flatpak, please use RAW/plaintext not blob view for Github/Gitlab | 
| snapcraft = ""                  | string, URL       | link to [Snapcraft](https://snapcraft.io/) listing (only if aarch64 is supported) |
| snap_link = ""                  | string, URL       | link to other Snap build (only if aarch64 build is provided)
| snap_recipe = ""                | string, URL       | link to snapcraft.yml to build a Snap, please use RAW/plaintext not blob view for Github/Gitlab |
| appimage\_x86\_64\_url = ""     | string, URL       | link to an appimage to download (for x86_64) |
| appimage\_aarch64\_url = ""     | string, URL       | link to an appimage to download (for aarch64 = 64bit ARM) |
| repology = [ "",]               | array of strings  | package name as found in [repology.org](https://repology.org) url (e.g., `tuba` in `https://repology.org/project/tuba/versions`), multiple if applicable |
| appstream\_xml\_url = ""        | string, URL       | appdata.xml or metainfo.xml url, please use RAW/plaintext not blob view for Github/Gitlab |
| reported_by = ""                | string            | **mandatory** your nickname |
| updated_by = ""                 | string            | optional, your nickname, only fill when updating a listing |
| verified = ""                   | ✅ or ❎          | **game list only**: legacy value marking whether you have checked that the game actually works. Please do! |


### Markdown bits and contents (optional)

Zola supports [CommonMark](https://commonmark.org/) ([spec](https://spec.commonmark.org/)), here's an example for the Markdown bits of a listing:

~~~~
    
### Description

Explore the federated social web with Tuba for GNOME. Stay connected to your favorite communities, family and friends with support for popular Fediverse platforms like Mastodon, GoToSocial, Akkoma & more! The Fediverse is a decentralized social network that consists of multiple interconnected platforms and communities, allowing users to communicate and share content across different networks and servers. It promotes user privacy and data ownership, and offers an alternative to centralized social media platforms. [Source](https://raw.githubusercontent.com/GeopJr/Tuba/main/data/dev.geopjr.Tuba.appdata.xml.in)


### Notice

Continuation of [Tootle](https://linuxphoneapps.org/apps/com.github.bleakgrey.tootle/), was called Tooth before its initial release.
~~~~
#### Description
**Important:** If you copy a description (can be lengthy), please make sure to add a link to your source at the end.

#### Notice
Notice can be filled with additional information detailing how well the app works/its limitations, where to get the app (if not in repos, etc.) if you assume this to be helpful.




+++
title = "Developer Information"
description = "Some hints for developers."
date = 2022-04-06T22:30:00+02:00
draft = false
weight = 40
sort_by = "weight"
template = "docs/page.html"

[extra]
toc = true
top = false
+++

<mark>Please note: This is an early draft, and the authoring person is not a developer, so take the following with a grain of salt. Feedback and MRs are most welcome.</mark>

## Tutorials

### Plasma Mobile ecosystem

* dimitris.cc: [Linux applications with Python and QML](https://dimitris.cc/kde/2022/02/26/pyside-blog-post.html)

### Phosh/GNOME on Mobile ecosystem

* Phosh.mobi: [Developing for Mobile Linux with Phosh - Part 0: Running nested](https://phosh.mobi/posts/phosh-dev-part-0/)
* TuxPhones.com: [Building responsive Linux mobile apps with libhandy 1.0 and Gtk3](https://tuxphones.com/tutorial-developing-responsive-linux-smartphone-apps-gnome-builder-gtk-libhandy-gtk-part-1/)
* Radu Zaharia for devgenius.com: [Using the libadwaita Leaflet widget for a responsive GTK4 UI in Rust](https://blog.devgenius.io/using-the-libadwaita-leaflet-widget-for-a-responsive-gtk4-ui-in-rust-73bbc2f4025)
* Lupyuen: [Build a PinePhone App with Zig and zgt](https://lupyuen.github.io/articles/pinephone?1) 

### Generic Linux app development resources 
* [Make Apps for Linux](https://makealinux.app/)

## Templates (as a starting point for app development)

### GTK3/GTK4 apps
* sadiq: [my-gtemplate](https://gitlab.com/sadiq/my-gtemplate/)


## Hardware specs to consider

In order to reach a [good rating](https://linuxphoneapps.org/mobile-compatibility/5) it‘s important to consider the screen size and resolution when designing the app UI:

| Phone           | Pixels       | Points[^1] |
|-----------------|--------------|------------|
| PinePhone (Pro) | 720 × 1440   | 360 × 720  |
| Librem 5        | 720 × 1440   | 360 × 720  |
| Moto G4 Play    | 720 × 1280   | 360 × 640  |
| OnePlus 6       | 1080 × 2280  | 360 × 760  |
| Xiaomi Poco F1  | 1080 × 2246  | 360 × 752  |
| Shift 6mq       | 1080 × 2160  | 360 × 720  |
| Fairphone 5     | 1224 × 2700  | 408 × 900 [^2] |

When landscape use and the mobile shell's own UI are also considered, this leads to a recommended dimension of 360×294px in [GNOME's HIG](https://developer.gnome.org/hig/guidelines/adaptive.html ) for a fully mobile compliant app.[^3]

By using [nested Phosh for app development](https://phosh.mobi/posts/phosh-dev-part-0/), you can check how your app fits a mobile screen without owning or having to use a mobile device.


## Finding a project

If you have time on your hands, but don't have an idea (and looking through the app list has not helped you with coming up with what is not there nor inspired you to help out a listed project), you could do the following:

* Pick up an existing project that's
  * [archived](https://linuxphoneapps.org/status/archived/),
  * [unmaintained](https://linuxphoneapps.org/status/unmaintained/), or
  * [inactive](https://linuxphoneapps.org/status/inactive/),

  and go ahead and revive it!

* Make adjustments to an app that mostly works, but does not really fit the screen and thus has a mobile compatibility rating of
  * [4](https://linuxphoneapps.org/mobile-compatibility/4/),
  * [3](https://linuxphoneapps.org/mobile-compatibility/3/), or
  * [2](https://linuxphoneapps.org/mobile-compatibility/2/).

If you rather want to start your own project, maybe the [Wishlist on the Mobian Wiki](https://web.archive.org/web/20230921194031/https://wiki.mobian.org/doku.php?id=wishlist) can be a source of inspiration. If that's not it, maybe go through related subreddits, or the [PINE64](https://forum.pine64.org) or [Purism forums](https://forums.puri.sm/) to find out what people are longing for.

## When it you have something that works and want others to use or try it

Don't forget to add appstream metadata! [The Appstream MetaInfo Creator](https://www.freedesktop.org/software/appstream/metainfocreator/#/guiapp) makes this easy! For extra credit, check the [Appstream docs](https://www.freedesktop.org/software/appstream/docs/index.html).

Tag a release! Otherwise, your app is unlikely to land in distributions (beyond, maybe, the AUR or NixOS).

Make it easy to try - by publishing it, e.g., on [flathub.org](https://docs.flathub.org/docs/for-app-authors/submission/), or, if you don't feel ready for that, by fulfilling parts of the [requirements](https://docs.flathub.org/docs/for-app-authors/requirements) by putting a working flatpak manifest in your repository. 



## Mobile App Testing

If you don't have a device, get in touch by [opening an issue](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/issues/new) or sending an email to our discuss list: <https://lists.sr.ht/~linuxphoneapps/linuxphoneapps.org-discuss> 


[^1]: Points at default scaling of 2x on low-end devices, and 3x on Snapdragon 845 devices. Fortunately, all phones with good mobile Linux support default to a width of 360 points.i

[^2]: Assuming 300% scaling, the Fairphone 5 unfortunately does not fit the 'standard' 360 point/pixels schema. At 340% scaling, it would come in at 360 × 794 points - at the price of the issues fractional scaling brings.

[^3]: That said, few of our 5/5 rated apps actually fulfill the landscape criterion.

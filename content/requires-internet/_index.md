+++
title = "Requires Internet"
description = "Whether and how often an app requires a connection to the internet."
date = 2021-04-01T08:00:00+00:00
updated = 2021-04-01T08:00:00+00:00
draft = false
sort_by = "title"

[extra.requiresinternet_pages]
+++

Use these pages to see apps and games that do or don't require an internet connection to work.

Note: This is a new feature of LinuxPhoneApps.org and currently the metadata is added on a purely manual basis - thus only a few apps show up here.


+++
title = "New Apps of LinuxPhoneApps.org, Q4/2023: 21 new apps, and a simpler way to add new apps!"
date = 2024-02-10T11:30:00+02:00
template = "blog/page.html"
draft = false

[taxonomies]
authors = ["linmob"]

+++

It feels like it is almost March, so let's have a long overdue look at what happened in the final quarter of 2023!

<!-- more -->
## New apps

The following 21 apps where added to LinuxPhoneApps.org in the fourth quarter of 2023:

### October

* [Kalm](https://linuxphoneapps.org/apps/org.kde.kalm/) - Breathing techniques. _Thank you, plata, for creating and adding the app!_
* [Metronomek](https://linuxphoneapps.org/apps/net.sf.metronomek/) - Trivial looking metronome with natural sounds and sophisticated possibilities. _Thank you, Agent Smith, for adding the app!_
* [Pygenda](https://linuxphoneapps.org/apps/noappid.semiprime.pygenda/) - Pygenda is a calendar/agenda application written in Python3/GTK3 and designed for "PDA" devices such as Planet Computers' Gemini.. 
* [Fretboard](https://linuxphoneapps.org/apps/dev.bragefuglseth.fretboard/) - Look up guitar chords. 

### November

* [Biblioteca](https://linuxphoneapps.org/apps/app.drey.biblioteca/) - Read documentation. 
* [Camera](https://linuxphoneapps.org/apps/org.gnome.snapshot/) - Take pictures and videos. 
* [Jellybean](https://linuxphoneapps.org/apps/garden.turtle.jellybean/) - Manage inventories of various things. 
* [Dosage](https://linuxphoneapps.org/apps/io.github.diegopvlk.dosage/) - Keep track of your treatments. 
* [Halftone](https://linuxphoneapps.org/apps/io.github.tfuxu.halftone/) - Give your images that pixel art-like style. 
* [Upscaler](https://linuxphoneapps.org/apps/io.gitlab.theevilskeleton.upscaler/) - Upscale and enhance images. 
* [KOReader](https://linuxphoneapps.org/apps/rocks.koreader.koreader/) - A document viewer for DjVu, PDF, EPUB and more. _Thank you, colinsane, for adding the app!_
* [Decibels](https://linuxphoneapps.org/apps/com.vixalien.decibels/) - Play audio files. 
* [Resources](https://linuxphoneapps.org/apps/net.nokyan.resources/) - Monitor your system resources and processes. 
* [Merkuro Contact](https://linuxphoneapps.org/apps/org.kde.merkuro.contact/) - Manage your contacts with speed and ease. 

### December

* [Saber: Handwritten Notes](https://linuxphoneapps.org/apps/com.adilhanney.saber/) - The notes app built for handwriting. 
* [Delfin](https://linuxphoneapps.org/apps/cafe.avery.delfin/) - Stream movies and TV shows from Jellyfin. 
* [Pomodoro](https://linuxphoneapps.org/apps/io.gitlab.idevecore.pomodoro/) - Pomodoro is a productivity-focused timer. 
* [Exercise Timer](https://linuxphoneapps.org/apps/xyz.safeworlds.hiit/) - Timer clock for high intensity interval training. 
* [Wifi Connect](https://linuxphoneapps.org/apps/noappid.andym48.pinephone-wifi/) - Scan for Wifi  SSIDs, connect, disconnect, and restart services on your linux phone. _Thank you, AndyM48, for creating and adding the app!_
* [Televido](https://linuxphoneapps.org/apps/de.k_bo.televido/) - Access German-language public TV. 
* [KWordQuiz](https://linuxphoneapps.org/apps/org.kde.kwordquiz/) - Flash Card Trainer. 

## New Games

As the games list remains unmaintained, it did not see much action. _Please step up if you're into games, [here are what's on the agenda (aside from adding/reviewing games)](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/issues/?label_name%5B%5D=games)!_ 

### Maintenance

Most listings were automatically updated, and many of them were additionally edited by hand and brought up to date. 
And because information regarding services or frameworks can't be updated automatically ...


## Please contribute

Since April 14th, 2023, the way this site is generated and thus also the ways to contribute to it have been simplified. All relevant parts now live in [one repository](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io).

As a consequence, fixing existing app or game listings is now just one click away (provided you have a framagit account) - every listing has an edit link at the bottom of the page.

![Footer on the bottom of the page](https://linuxphoneapps.org/img/app-list-footer.png)

So if you see that something is wrong, take the time and help fix it!

### Adding apps

If you want to add an app (if you don't know any, check [Apps to be added](https://linuxphoneapps.org/lists/apps-to-be-added/) ;-) ), 

Fortunately, we now have a new and easy way to do this:

 [LPA helper](https://linuxphoneapps.org/lpa_helper.html) is ugly, but it works. We hope to integrate it properly and overhaul its UI soon - if you have questions or suggestions, do not hesitate to ask/get in touch!

See also:
- [Submit apps via GitLab](https://linuxphoneapps.org/docs/contributing/submit-app-on-gitlab/)
- [Listings explained](https://linuxphoneapps.org/docs/contributing/listings-explained/)

You can also send your prepared listing via email to the [devel-list](https://lists.sr.ht/~linuxphoneapps/linuxphoneapps.org-devel).

## What's next?

Stay tuned for a post with our plans and goals for 2024. If you can't wait for that - we do have a [public issue tracker](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/issues)!

### Feedback, thoughts?

Thank you for reading this post! If you enjoyed it, please spread the word about this post and LinuxPhoneApps.org - it can also be useful for your friends or yourself if you only use the Linux Desktop, as we constantly check for new, awesome adaptive Linux apps!

If you want to contribute an/your app or work on the website itself, please see above or check the [FAQ](@/docs/help/faq.md#join-the-effort)![^1] 

And if you want to discuss apps with like minded persons, make sure to join [our Matrix group](https://matrix.to/#/#linuxphoneapps:matrix.org) or subscribe/post to our [discuss mailing list](https://lists.sr.ht/~linuxphoneapps/linuxphoneapps.org-discuss)!


[^1]: If you are not aware of any new apps, check out apps listed in [apps-to-be-added](https://linuxphoneapps.org/lists/apps-to-be-added/), test the app and add it!



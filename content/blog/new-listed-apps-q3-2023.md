+++
title = "New Apps of LinuxPhoneApps.org, Q3/2023: 15 new apps, maintenance and more!"
date = 2023-10-08T19:40:00+02:00
template = "blog/page.html"
draft = false

[taxonomies]
authors = ["linmob"]

+++

It's October (and thus Q4): Let's have a look at what happened in the third quarter of 2023!

<!-- more -->
## New apps

The following fifteen apps where added in the third quarter of 2023:

### Juli

No new apps :(

### August

- [Syncthing GTK](https://linuxphoneapps.org/apps/me.kozec.syncthingtk/), a GUI for Syncthing. _Thank you for adding the app, Guido!_ 
- [Lemoa](https://linuxphoneapps.org/apps/io.github.lemmygtk.lemoa/), a Gtk client for Lemmy.
- [Lemonade](https://linuxphoneapps.org/apps/ml.mdwalters.lemonade/), follow discussions on Lemmy. _Despite these additions, I think we need more good Lemmy/kbin clients for #MobileLinux.
- [Pinephone MPV](https://linuxphoneapps.org/apps/noappid.andym48.pinephone_mpv/), a simple wrapper for MPV for the Pinephone and other Linux mobile devices. _Thank you, AndyM48 for creating and adding the app!_

### September

- [Bluetooth Connect](https://linuxphoneapps.org/apps/noappid.andym48.pinephone_btconnect/). Scan, add, edit and connect bluetooth devices on your linux phone. _Thank you AndyM48, for creating and adding the app to LinuxPhoneApps.org!_
- [Flatsweep](https://linuxphoneapps.org/apps/io.github.giantpinkrobots.flatsweep/). Flatpak leftover cleaner.
- [Letterpress](https://linuxphoneapps.org/apps/io.gitlab.gregorni.letterpress/), create beautiful ASCII art.
- [Francis](https://linuxphoneapps.org/apps/org.kde.francis/). Track your time
- [Jogger](https://linuxphoneapps.org/apps/xyz.slothlife.jogger/): Track and view your runs, walks, cycles, swims, and other workouts on mobile Linux.

### Maintenance

Also, at the very end of the quarter, most listings were automatically updated, and many of them were additionally edited by hand and brought up to date. 
Also, we prepared the site to list even more information: 

- First, we are [working on a new taxonomy](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/issues/55) to also list whether/how well and app can work without a connection to the internet. 
- Secondly, but way more immidiate, App Images can now be linked to in our listings, so if you find one, let us know!

And because information regarding services or frameworks can't be updated automatically ...


## Please contribute

Since April 14th, 2023, the way this site is generated and thus also the ways to contribute to it have been simplified. All relevant parts now live in [one repository](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io).

As a consequence, fixing existing app or game listings is now just one click away (provided you have a framagit account) - every listing has an edit link at the bottom of the page.

![Footer on the bottom of the page](https://linuxphoneapps.org/img/app-list-footer.png)

So if you see that something is wrong, take the time and help fix it!

### New apps 

If you want to add an app (if you don't know any, check [Apps to be added](https://linuxphoneapps.org/lists/apps-to-be-added/) ;-) ), 

- [Submit apps via GitLab](https://linuxphoneapps.org/docs/contributing/submit-app-on-gitlab/)
- [Listings explained](https://linuxphoneapps.org/docs/contributing/listings-explained/)

You can also send your prepared listing via email to the [devel-list](https://lists.sr.ht/~linuxphoneapps/linuxphoneapps.org-devel).

## What's next?

Goals for the Q4 are the following:

- Making it easier to add new apps: [Integrate lpa_helper.html into the site](https://framagit.org/1peter10/lpa-new-app-helper/-/blob/main/lpa_helper.html?ref_type=heads), [make it possible to generate listings from [metadata](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/merge_requests/51) or [flathub.org](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/merge_requests/52).
- Improve information and UI - make listings better (more readable) and improve listings findability.

### Still to be considered and fixed 

Talking about ratings: The way apps are rated a thing we still are thinking about. They are a hard topic, not just because changes to the rating system would then have to be applied to more than 400 apps. Among the considerations for the future are new separate sections (apps and games are sections in [Zola](https://getzola.org) terminology) for Tablet apps (how to rate those with form factors ranging from 7" to 13"?) or TUI apps (think [Sxmo](https://sxmo.org)).

If you are willing to help with the concept, please describe it in an [issue](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/issues) - be prepared to contribute to the MR that adds it, though - also we should start out with at least 5 listings to make the section worthwhile.

### Feedback, thoughts?

Thank you for reading this post! If you enjoyed it, please spread the word about this post and LinuxPhoneApps.org - it can also be useful for your friends or yourself if you only use the Linux Desktop!

If you want to contribute an/your app or work on the website itself, please see above or check the [FAQ](@/docs/help/faq.md#join-the-effort)![^1] 

And if you want to discuss apps with like minded persons, make sure to join [our Matrix group](https://matrix.to/#/#linuxphoneapps:matrix.org) or subscribe/post to our [discuss mailing list](https://lists.sr.ht/~linuxphoneapps/linuxphoneapps.org-discuss)!


[^1]: If you are not aware of any new apps, check out apps listed in [apps-to-be-added](https://linuxphoneapps.org/lists/apps-to-be-added/), test the app and add it!



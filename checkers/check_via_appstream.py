#!/usr/bin/env python3

import asyncio
import datetime
import pathlib
import re
import sys
import traceback

import aiofiles
import appstream_python
import frontmatter
import httpx
import markdownify
import utils


async def load_appstream(client, url):
    if not url:
        return None
    if "/blob/" in url:
        print(f"Waring: /blob/ URL fragment detected in {url}, you probably want to use the raw URL.", file=sys.stderr)
    app = appstream_python.AppstreamComponent()
    try:
        response = await client.get(url)
        if response.status_code != httpx.codes.OK:
            print(f"Error loading {url}", file=sys.stderr)
            return None
        app.load_bytes(response.content, encoding=response.encoding)
    except Exception as e:
        print(f"Error loading {url}:", file=sys.stderr)
        traceback.print_exception(e, file=sys.stderr)
        return None
    return app


def strip_app_id(app):
    # Legacy app ids have been used to link to a .desktop file
    # https://www.freedesktop.org/software/appstream/docs/sect-Metadata-Application.html#tag-id-desktopapp
    if not app.id.endswith(".desktop"):
        return app.id

    if launchable := app.launchables.get("desktop-id"):
        if app.id == launchable:
            stripped_app_id = app.id[: -len(".desktop")]
            print(f'{stripped_app_id}: Stripping ".desktop" suffix from legacy AppStream ID.', file=sys.stderr)
            return stripped_app_id

    return app.id


def get_appstream_app_id(app):
    return strip_app_id(app)


def get_appstream_name(app):
    return app.name.get_default_text()


# possible URL types: https://www.freedesktop.org/software/appstream/docs/chap-Metadata.html#tag-url
def get_appstream_url(app, url_type):
    return app.urls.get(url_type, "") or ""  # filter out empty <url type="TYPE"></url>


def get_appstream_categories(app):
    return app.categories


def get_appstream_app_author(app):
    return app.developer_name.get_default_text().split(",")


def sanitize_licenses(licenses_str):
    return [utils.convert_to_spdx_license(license) for license in utils.multisplit(licenses_str, [" AND ", " and ", " OR ", " or "])]


def get_appstream_metadata_licenses(app):
    return sanitize_licenses(app.metadata_license)


def get_appstream_project_licenses(app):
    return sanitize_licenses(app.project_license)


def get_appstream_summary(app):
    return app.summary.get_default_text()


def get_appstream_description(app):
    return markdownify.markdownify(app.description.to_html(lang=None), heading_style="ATX")


def get_appstream_screenshots(app):
    return [screenshot.get_source_image().url.strip() for screenshot in app.screenshots if screenshot.get_source_image() is not None]


def get_appstream_display_compatible(app):
    if app.custom.get("Purism::form_factor", "") == "mobile":
        return True

    def is_display_incompatible(app, px):
        if not app.display_length:
            return True  # display_length undefined means "desktop with big screen": https://github.com/ximion/appstream/issues/481#issuecomment-1505287662

        for relation in ["requires", "recommends", "supports"]:
            props = app.display_length.get(relation, [])
            if not props:
                continue

            for prop in props:
                if not prop.compare_px(px):
                    return True

        return False

    return not is_display_incompatible(app, 360)


def get_appstream_touch_compatible(app):
    if app.custom.get("Purism::form_factor", "") == "mobile":
        return True

    def is_touch_compatible(app):
        if all(x is None for x in app.controls.values()):
            return False  # control undefined means "desktop with mouse & keyboard only": https://github.com/ximion/appstream/issues/481#issuecomment-1505287662

        if app.controls["touch"] in ["requires", "recommends", "supports"]:
            return True

        return False

    return is_touch_compatible(app)


async def check(client, item, update=False):
    item_name = utils.get_recursive(item, "extra.app_id") or utils.get_recursive(item, "title", "")
    appstream_xml_url = utils.get_recursive(item, "extra.appstream_xml_url")
    app = await load_appstream(client, appstream_xml_url)
    if not app:
        return False

    properties = [
        {"apps_key": "title", "handler": get_appstream_name},
        {"apps_key": "extra.app_id", "handler": get_appstream_app_id},
        {"apps_key": "extra.homepage", "handler": lambda app: get_appstream_url(app, "homepage")},
        {"apps_key": "extra.bugtracker", "handler": lambda app: get_appstream_url(app, "bugtracker")},
        {"apps_key": "extra.donations", "handler": lambda app: get_appstream_url(app, "donation")},
        {"apps_key": "extra.translations", "handler": lambda app: get_appstream_url(app, "translate")},
        {"apps_key": "extra.repository", "handler": lambda app: get_appstream_url(app, "vcs-browser")},
        {"apps_key": "taxonomies.freedesktop_categories", "handler": get_appstream_categories},
        {"apps_key": "taxonomies.app_author", "handler": get_appstream_app_author},
        {"apps_key": "taxonomies.metadata_licenses", "handler": get_appstream_metadata_licenses},
        {"apps_key": "taxonomies.project_licenses", "handler": get_appstream_project_licenses},
        {"apps_key": "description", "handler": get_appstream_summary},
        {"apps_key": "content", "handler": get_appstream_description},
        {"apps_key": "extra.screenshots", "handler": get_appstream_screenshots},
        {"apps_key": "extra.intended_for_mobile", "handler": get_appstream_display_compatible},
        {"apps_key": "extra.all_features_touch", "handler": get_appstream_touch_compatible},
    ]
    found = False
    for property in properties:
        try:
            found_entry = utils.sanitize(property["handler"](app))
        except Exception as e:
            print(f'{item_name}: Error handling {property["apps_key"]}:', file=sys.stderr)
            traceback.print_exception(e, file=sys.stderr)
            continue

        if appstream_xml_url.endswith(".in"):
            is_autotools_template = lambda entry: bool(re.search(r"@.*@", entry))
            if (isinstance(found_entry, str) and is_autotools_template(found_entry)) or (isinstance(found_entry, list) and any([is_autotools_template(item) for item in found_entry])):
                print(f'{item_name}: Ignoring autotools template variable for {property["apps_key"]}: {found_entry}', file=sys.stderr)
                continue

        if utils.get_recursive(item, property["apps_key"]) and not found_entry:
            print(f'{item_name}: {property["apps_key"]} missing in upstream AppStream file. Consider contributing it upstream: {utils.get_recursive(item, property["apps_key"])}', file=sys.stderr)
        if not found_entry or found_entry == utils.get_recursive(item, property["apps_key"]):
            continue  # already up to date

        message = f'{item_name}: {property["apps_key"]} '
        if not utils.get_recursive(item, property["apps_key"]):
            message += "new: "
        else:
            message += f'outdated "{utils.get_recursive(item, property["apps_key"])}" -> '
        message += f'"{found_entry}"'
        print(message, file=sys.stderr)

        found = True
        if update:
            utils.set_recursive(item, property["apps_key"], found_entry)

            source_column = property["apps_key"] + "_source"
            if property["apps_key"] == "description":
                source_column = "extra.summary_source_url"
            if utils.isset_recursive(item, source_column):
                if utils.get_recursive(item, source_column) != appstream_xml_url:
                    print(f"{item_name}: {source_column} {utils.get_recursive(item, source_column)} -> {appstream_xml_url}", file=sys.stderr)
                utils.set_recursive(item, source_column, appstream_xml_url)

            utils.set_recursive(item, "updated", datetime.date.today())
            utils.set_recursive(item, "extra.updated_by", "script")

    return found


async def new(folder: pathlib.Path, appstream_xml_url: str):
    async with httpx.AsyncClient(timeout=30.0) as client:
        doc = frontmatter.Post(content="")
        doc.metadata = {"extra": {"appstream_xml_url": appstream_xml_url}}
        found = await check(client, doc.metadata, True)

        if found:
            utils.set_recursive(doc.metadata, "date", datetime.date.today())
            utils.set_recursive(doc.metadata, "extra.reported_by", "script")
            doc.content = utils.format_markdown(
                {
                    "description": doc.metadata.pop("content"),
                    "description_source": doc.metadata.pop("content_source"),
                }
            )

            folder.mkdir(parents=True, exist_ok=True)
            filename = folder / utils.generate_filename(doc.metadata)
            print(f"Writing changes to {filename}")
            async with aiofiles.open(filename, mode="w", encoding="utf-8") as f:
                await f.write(frontmatter.dumps(doc, handler=frontmatter.default_handlers.TOMLHandler()))

        return found


async def check_file(client: httpx.AsyncClient, filename: pathlib.Path, update: bool = False):
    async with aiofiles.open(filename, mode="r", encoding="utf-8") as f:
        doc = frontmatter.loads(await f.read())

    page_content = utils.parse_markdown(doc.content)
    doc.metadata |= {
        "content": page_content["description"] or "",
        "content_source": page_content["description_source"] or "",
    }

    found = await check(client, doc.metadata, update)

    if found and update:
        doc.content = utils.format_markdown(
            {
                "description": doc.metadata.pop("content"),
                "description_source": doc.metadata.pop("content_source"),
                "notice": page_content["notice"],
            }
        )

        print(f"Writing changes to {filename}")
        async with aiofiles.open(filename, mode="w", encoding="utf-8") as f:
            await f.write(frontmatter.dumps(doc, handler=frontmatter.default_handlers.TOMLHandler()))

    return found


async def run(path: pathlib.Path, update: bool = False):
    async with httpx.AsyncClient(timeout=30.0) as client:
        tasks = []
        files = path.glob("**/*.md") if path.is_dir() else [path]
        for filename in files:
            if filename.name == "_index.md":
                continue
            tasks.append(asyncio.ensure_future(check_file(client, filename, update)))
        found = any(await asyncio.gather(*tasks))
        return found


async def main():
    if len(sys.argv) < 3:
        print(f"Syntax:\n {sys.argv[0]} check|fix PATH\n {sys.argv[0]} new PATH APPSTREAM_XML_URL", file=sys.stderr)
        sys.exit(1)

    mode = sys.argv[1]
    apps_path = pathlib.Path(sys.argv[2])

    if mode == "new":
        appstream_xml_url = sys.argv[3]
        found = await new(apps_path, appstream_xml_url)
        if not found:
            print(f"Error importing app from {appstream_xml_url}.", file=sys.stderr)
            sys.exit(1)
    else:
        update = mode == "fix"
        found = await run(apps_path, update)
        if found and not update:
            print(f'Errors found! Run "{sys.argv[0]} fix {apps_path}" to apply suggested changes.', file=sys.stderr)
            sys.exit(1)


if __name__ == "__main__":
    asyncio.run(main())

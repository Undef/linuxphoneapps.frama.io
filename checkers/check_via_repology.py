#!/usr/bin/env python3

import datetime
import pathlib
import sys
import traceback

import frontmatter
import httpx

import utils

# List of valid distributions
valid_distributions = [
    "alpine_3_18",
    "alpine_3_19",
    "alpine_edge",
    "archlinuxarm_aarch64",
    "archlinuxarm_armv7h",
    "arch",
    "aur",
    "debian_11",
    "debian_12",
    "debian_13",
    "debian_unstable",
    "debian_experimental",
    "devuan_4_0",
    "devuan_unstable",
    "fedora_38",
    "fedora_39",
    "fedora_40",
    "fedora_rawhide",
    "gentoo",
    "gnuguix",
    "nix_stable_23_05",
    "nix_stable_23_11",
    "nix_unstable",
    "manjaro_stable",
    "manjaro_unstable",
    "opensuse_tumbleweed",
    "postmarketos_master",
    "postmarketos_23.06",
    "postmarketos_23.12",
    "pureos_landing",
]


def load_repology(client, package_name):
    if not package_name:
        return None
    url = f"https://repology.org/api/v1/project/{package_name}"
    try:
        response = client.get(url)
        if response.status_code != httpx.codes.OK:
            print(f"Error loading {url}", file=sys.stderr)
            return None
        return response.json()
    except Exception as e:
        print(f"Error loading {url}:", file=sys.stderr)
        traceback.print_exception(e, file=sys.stderr)
        return None


def get_repology_packaged_in(client, item):
    repology_package_names = utils.get_recursive(item, "extra.repology")
    if not repology_package_names:
        return False

    packaged_in = set()
    for package_name in repology_package_names:
        repology_packages = load_repology(client, package_name)
        if not repology_packages:
            print(f"No data returned for package {package_name} from Repology API", file=sys.stderr)
            continue

        for package in repology_packages:
            if (repo := package.get("repo")) in valid_distributions:
                packaged_in.add(repo)

    if utils.get_recursive(item, "extra.flathub"):
        packaged_in.add("flathub")

    if utils.get_recursive(item, "extra.snapcraft"):
        packaged_in.add("snapcraft")

    return list(packaged_in)


def check(client, item, update=False):
    item_name = utils.get_recursive(item, "extra.app_id") or utils.get_recursive(item, "title", "")

    properties = [
        {"apps_key": "taxonomies.packaged_in", "handler": get_repology_packaged_in},
    ]
    found = False
    for property in properties:
        try:
            found_entry = utils.sanitize(property["handler"](client, item))
        except Exception as e:
            print(f'{item_name}: Error handling {property["apps_key"]}:', file=sys.stderr)
            traceback.print_exception(e, file=sys.stderr)
            continue

        if not found_entry or found_entry == utils.get_recursive(item, property["apps_key"]):
            continue  # already up to date

        message = f'{item_name}: {property["apps_key"]} '
        if not utils.get_recursive(item, property["apps_key"]):
            message += "new: "
        else:
            message += f'outdated "{utils.get_recursive(item, property["apps_key"])}" -> '
        message += f'"{found_entry}"'
        print(message, file=sys.stderr)

        found = True
        if update:
            utils.set_recursive(item, property["apps_key"], found_entry)

            source_column = property["apps_key"] + "_source"
            if property["apps_key"] == "description":
                source_column = "extra.summary_source_url"
            if utils.get_recursive(item, source_column):
                if utils.get_recursive(item, source_column) != utils.get_recursive(item, "extra.appstream_xml_url"):
                    print(f'{item_name}: {source_column} {utils.get_recursive(item, source_column)} -> {utils.get_recursive(item, "extra.appstream_xml_url")}', file=sys.stderr)
                utils.set_recursive(item, source_column, utils.get_recursive(item, "extra.appstream_xml_url"))

            utils.set_recursive(item, "updated", datetime.date.today())
            utils.set_recursive(item, "extra.updated_by", "check_via_repology")

    return found


def check_file(client: httpx.Client, filename: pathlib.Path, update: bool = False):
    with open(filename, mode="r", encoding="utf-8") as f:
        doc = frontmatter.load(f)  # Use `load` instead of `loads`
    found = check(client, doc.metadata, update)

    if found and update:
        print(f"Writing changes to {filename}")
        with open(filename, mode="w", encoding="utf-8") as f:
            f.write(frontmatter.dumps(doc, handler=frontmatter.default_handlers.TOMLHandler()))

    return found


def run(path: pathlib.Path, update: bool = False):
    with httpx.Client(timeout=30.0) as client:
        found = False
        files = path.glob("**/*.md") if path.is_dir() else [path]
        for filename in files:
            if filename.name == "_index.md":
                continue
            if check_file(client, filename, update):
                found = True
        return found


def main():
    if len(sys.argv) < 3:
        print(f"Syntax: {sys.argv[0]} check|fix PATH")
        sys.exit(1)
    update = sys.argv[1] == "fix"
    apps_path = pathlib.Path(sys.argv[2])
    found = run(apps_path, update)
    if found and not update:
        print(f'Errors found! Run "{sys.argv[0]} fix {apps_path}" to apply suggested changes.', file=sys.stderr)
        sys.exit(1)


if __name__ == "__main__":
    main()

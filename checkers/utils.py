import re


def get_recursive(obj, path, default=None):
    if isinstance(path, str):
        path = path.split(".")

    if len(path) == 0:
        return obj

    key, *path = path
    if key not in obj:
        return default

    return get_recursive(obj[key], path, default)


def isset_recursive(obj, path):
    DEFAULT_MARKER = object()
    return get_recursive(obj, path, DEFAULT_MARKER) is not DEFAULT_MARKER


def set_recursive(obj, path, value):
    if isinstance(path, str):
        path = path.split(".")

    if len(path) == 0:
        raise KeyError("Empty path")

    key, *path = path
    if len(path) > 0:
        if key not in obj:
            obj[key] = {}
        set_recursive(obj[key], path, value)
    else:
        obj[key] = value


def parse_markdown(content):
    PAGE_PATTERN = re.compile(
        r"""\s*
(?:### Description\s*(?P<description>.*?)(?:\s*\[Source\]\((?P<description_source>.*?)\))?)?
\s*
(?:### Notice\s*(?P<notice>.*))?
\s*""".replace(
            "\n", ""
        ),
        re.MULTILINE | re.DOTALL,
    )
    return PAGE_PATTERN.fullmatch(content).groupdict()


def format_markdown(data):
    content = ""
    if data.get("description"):
        content += f"""
### Description

{data["description"]}
"""
        if data.get("description_source"):
            content += f"""
[Source]({data["description_source"]})
"""
    if data.get("notice"):
        content += f"""
### Notice

{data["notice"]}
"""
    return content


def sanitize(data):
    if isinstance(data, str):
        return data.strip()
    elif isinstance(data, list):
        return sorted(filter(None, [sanitize(e) for e in data]))
    else:
        return data


def multisplit(data, seps=None):
    if not isinstance(data, list):
        data = [data]

    if not seps:
        sep = None
    elif isinstance(seps, str):
        sep = seps
        seps = []  # break recursion
    elif isinstance(seps, list):
        sep = seps.pop()
    else:
        raise TypeError(f"Invalid separator {seps}")

    if seps:
        data = multisplit(data, seps)

    return [e for d in data for e in d.split(sep)]


def convert_to_spdx_license(license):
    # Conversion for SPDX-2 to SPDX-3
    # Optional "+" at the end
    if m := re.match(r"(?P<license>(?:A|L)?GPL-[0-9]\.[0-9])(?P<plus>\+)?$", license):
        d = m.groupdict()
        return d["license"] + ("-or-later" if d["plus"] == "+" else "-only")
    # Implicit license version
    spdx2_to_spdx3 = {
        "CC0": "CC0-1.0",
    }
    if license in spdx2_to_spdx3:
        return spdx2_to_spdx3[license]

    return license


def generate_filename(item):
    if app_id := get_recursive(item, "extra.app_id"):
        return app_id.lower() + ".md"

    m = re.match(r"^(?:https?://)(?P<domain>[^/]+)/(?P<path>.*)$", item["extra"]["repository"]).groupdict()
    path_name = ".".join([name.lower().replace("~", "").replace("_", "-") for name in m["path"].split("/") if name])
    return f"noappid.{path_name}.md"

#!/usr/bin/env python3

import asyncio
import datetime
import pathlib
import sys

import aiofiles
import check_via_repology as repology
import frontmatter
import httpx
import utils
import validators
from schema import And, Literal, Optional, Or, Regex, Schema, SchemaError, Use


# TODO upstream to schema
class Category(object):
    def __init__(self, *args):
        self.values = args

    def validate(self, s):
        if s not in self.values:
            raise SchemaError(f"{s} not in {self.values}")
        return s


# AppStream categories should conform to Freedesktop categories: https://www.freedesktop.org/software/appstream/docs/chap-Metadata.html#tag-categories
freedesktop_categories = [
    # Main categories: https://specifications.freedesktop.org/menu-spec/latest/apa.html#main-category-registry
    "AudioVideo",
    "Audio",
    "Video",
    "Development",
    "Education",
    "Game",
    "Graphics",
    "Network",
    "Office",
    "Science",
    "Settings",
    "System",
    "Utility",
    # Additional categories: https://specifications.freedesktop.org/menu-spec/latest/apas02.html
    "Building",
    "Debugger",
    "IDE",
    "GUIDesigner",
    "Profiling",
    "RevisionControl",
    "Translation",
    "Calendar",
    "ContactManagement",
    "Database",
    "Dictionary",
    "Chart",
    "Email",
    "Finance",
    "FlowChart",
    "PDA",
    "ProjectManagement",
    "Presentation",
    "Spreadsheet",
    "WordProcessor",
    "2DGraphics",
    "VectorGraphics",
    "RasterGraphics",
    "3DGraphics",
    "Scanning",
    "OCR",
    "Photography",
    "Publishing",
    "Viewer",
    "TextTools",
    "DesktopSettings",
    "HardwareSettings",
    "Printing",
    "PackageManager",
    "Dialup",
    "InstantMessaging",
    "Chat",
    "IRCClient",
    "Feed",
    "FileTransfer",
    "HamRadio",
    "News",
    "P2P",
    "RemoteAccess",
    "Telephony",
    "TelephonyTools",
    "VideoConference",
    "WebBrowser",
    "WebDevelopment",
    "Midi",
    "Mixer",
    "Sequencer",
    "Tuner",
    "TV",
    "AudioVideoEditing",
    "Player",
    "Recorder",
    "DiscBurning",
    "ActionGame",
    "AdventureGame",
    "ArcadeGame",
    "BoardGame",
    "BlocksGame",
    "CardGame",
    "KidsGame",
    "LogicGame",
    "RolePlaying",
    "Shooter",
    "Simulation",
    "SportsGame",
    "StrategyGame",
    "Art",
    "Construction",
    "Music",
    "Languages",
    "ArtificialIntelligence",
    "Astronomy",
    "Biology",
    "Chemistry",
    "ComputerScience",
    "DataVisualization",
    "Economy",
    "Electricity",
    "Geography",
    "Geology",
    "Geoscience",
    "History",
    "Humanities",
    "ImageProcessing",
    "Literature",
    "Maps",
    "Math",
    "NumericalAnalysis",
    "MedicalSoftware",
    "Physics",
    "Robotics",
    "Spirituality",
    "Sports",
    "ParallelComputing",
    "Amusement",
    "Archiving",
    "Compression",
    "Electronics",
    "Emulator",
    "Engineering",
    "FileTools",
    "FileManager",
    "TerminalEmulator",
    "Filesystem",
    "Monitor",
    "Security",
    "Accessibility",
    "Calculator",
    "Clock",
    "TextEditor",
    "Documentation",
    "Adult",
    "Core",
    "KDE",
    "GNOME",
    "XFCE",
    "DDE",
    "GTK",
    "Qt",
    "Motif",
    "Java",
    "ConsoleOnly",
    # Reserved categories: https://specifications.freedesktop.org/menu-spec/latest/apas03.html
    "Screensaver",
    "TrayIcon",
    "Applet",
    "Shell",
]

build_systems = [
    "cargo",
    "clickable",
    "cmake",
    "CMake",
    "custom",
    "flit",
    "flutter",
    "go",
    "make",
    "meson",
    "Meson",
    "ninja",
    "nix",
    "none",
    "npm",
    "poetry",
    "pyproject",
    "qbs",
    "qmake",
    "setup.py",
    "various",
    "yarn",
    "zig",
]

programming_languages = [
    "C",
    "Clojure",
    "Cpp",
    "Crystal",
    "Dart",
    "Go",
    "HTML",
    "Java",
    "JavaScript",
    "Lua",
    "Objective-C",
    "Python",
    "QML",
    "Rust",
    "Shell",
    "Tcl",
    "TypeScript",
    "Vala",
    "Vue",
    "XSLT",
    "Zig",
]

app_schema = {
    Optional("title", default=""): str,
    Optional("aliases", default=[]): [str],
    Optional("description", default=""): str,
    Optional("description_long", default=""): str,
    Optional("notice", default=""): str,
    Optional("taxonomies", default={}): {
        Optional("project_licenses", default=[]): [Use(utils.convert_to_spdx_license)],
        Optional("metadata_licenses", default=[]): [Use(utils.convert_to_spdx_license)],
        Optional("app_author", default=[]): [str],
        Optional("categories", default=[]): [str],
        Optional("status", default=[]): [Category("archived", "early", "gone", "inactive", "mature", "maturing", "pre-release", "unmaintained")],
        Optional("mobile_compatibility", default=[]): [Category("1", "2", "3", "4", "5", "needs testing")],
        Optional("frameworks", default=[]): [str],
        Optional("backends", default=[]): [str],
        Optional("services", default=[]): [str],
        Optional("packaged_in", default=[]): [Category(*(repology.valid_distributions + ["flathub", "snapcraft"]))],
        Optional("freedesktop_categories", default=[]): [str],  # TODO fix invalid categories: Category(*freedesktop_categories)
        Optional("programming_languages", default=[]): [str],
        Optional("build_systems", default=[]): [Category(*build_systems)],
        Optional("requires_internet", default=[]): [Category("recommends always", "requires always", "requires first-run", "requires offline-only")],
        Optional("tags", default=[]): [Category("editors choice")],
    },
    Optional("extra", default={}): {
        Optional("repository", default=""): Or(validators.url, ""),
        Optional("homepage", default=""): Or(validators.url, ""),
        Optional("bugtracker", default=""): Or(validators.url, ""),
        Optional("donations", default=""): Or(validators.url, ""),
        Optional("translations", default=""): Or(validators.url, ""),
        Optional("more_information", default=[]): [validators.url],
        Optional("summary_source_url", default=""): str,  # TODO wait for release of https://github.com/python-validators/validators/pull/305
        Optional("screenshots", default=[]): [validators.url],
        Optional("screenshots_img", default=[]): [validators.url],
        Optional("app_id", default=""): str,
        Optional("scale_to_fit", default=""): str,
        Optional("all_features_touch", default=False): bool,
        Optional("intended_for_mobile", default=False): bool,
        Optional("appstream_xml_url", default=""): Or(validators.url, ""),
        Optional("repology", default=[]): [str],
        Optional("appimage_aarch64_url", default=""): Or(validators.url, ""),
        Optional("appimage_x86_64_url", default=""): Or(validators.url, ""),
        Optional("flathub", default=""): Or(validators.url, ""),
        Optional("flatpak_link", default=""): Or(validators.url, ""),
        Optional("flatpak_recipe", default=""): Or(validators.url, ""),
        Optional("snap_link", default=""): Or(validators.url, ""),
        Optional("snap_recipe", default=""): Or(validators.url, ""),
        Optional("snapcraft", default=""): Or(validators.url, ""),
        Optional("reported_by", default=""): str,
        Optional("updated_by", default=""): str,
    },
    Optional("date", default=None): datetime.date,
    Optional("updated", default=None): datetime.date,
}


async def check(client, item, update=False):
    item_name = utils.get_recursive(item, "extra.app_id") or utils.get_recursive(item, "title", "")

    try:
        return Schema(app_schema, ignore_extra_keys=False).validate(item)
    except SchemaError as e:
        print(f"{item_name}: Invalid schema: {e}", file=sys.stderr)
        return None


async def check_file(client, filename, update=False):
    async with aiofiles.open(filename, mode="r", encoding="utf-8") as f:
        doc = frontmatter.loads(await f.read())

    found_item = await check(client, doc.metadata, update)
    found = found_item == doc.metadata
    doc.metadata = found_item

    if found and update:
        print(f"Writing changes to {filename}")
        async with aiofiles.open(filename, mode="w", encoding="utf-8") as f:
            await f.write(frontmatter.dumps(doc, handler=frontmatter.default_handlers.TOMLHandler()))

    return found


async def run(folder, update=False):
    async with httpx.AsyncClient(timeout=30.0) as client:
        tasks = []
        for filename in folder.glob("**/*.md"):
            if filename.name == "_index.md":
                continue
            tasks.append(asyncio.ensure_future(check_file(client, filename, update)))
        found = any(await asyncio.gather(*tasks))
        return found


async def main():
    if len(sys.argv) < 3:
        print(f"Syntax: {sys.argv[0]} check|fix FOLDER")
        sys.exit(1)
    update = sys.argv[1] == "fix"
    apps_folder = pathlib.Path(sys.argv[2])
    found = await run(apps_folder, update)
    if found and not update:
        print(f'Errors found! Run "{sys.argv[0]} fix {apps_folder}" to apply suggested changes.', file=sys.stderr)
        sys.exit(1)


if __name__ == "__main__":
    asyncio.run(main())
